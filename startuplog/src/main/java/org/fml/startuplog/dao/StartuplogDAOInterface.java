package org.fml.startuplog.dao;

import java.text.ParseException;
import java.util.List;

import org.apache.log4j.Logger;
import org.fml.startuplog.model.ABCAnalysis;
import org.fml.startuplog.model.Customer;
import org.fml.startuplog.model.Functions;
import org.fml.startuplog.model.MakeBuyDecision;
import org.fml.startuplog.model.MarketPowerPortfolio;
import org.fml.startuplog.model.Material;
import org.fml.startuplog.model.MaterialClassification;
import org.fml.startuplog.model.PhaseModel;
import org.fml.startuplog.model.Procurement;
import org.fml.startuplog.model.StorageFacility;
import org.fml.startuplog.model.Supplier;
import org.fml.startuplog.model.User;

public interface StartuplogDAOInterface {

	final Logger logger = Logger.getLogger(StartuplogDAO.class);

	/**
	 * Methods to access material table / master table
	 */
	public List<Material> getAllMaterial_all_column();

	public List<Material> getAllMaterial_number_name();

	public List<Material> getAllMaterial_for_procurement();

	public Material getMaterial(String materialID);

	/**
	 * Methods to access storage facility table
	 */
	public StorageFacility get_material_from_storagefacility(String material_id);

	public void storeMaterial_into_storagefacility(String materialID, String storage_facility, String allInfoGiven,
			String store_in_delivery_packaging, String store_in_dispatch_packaging, String storage_unit_type,
			String are_storage_unit_stackable, String storage_packaging_stacking_height,
			String amount_of_items_per_storage_packaging, String material_weight_incl_storage_packaging,
			String storage_packaging_length, String storage_packaging_height, String storage_packaging_width,
			String storage_bin_assignment, String storage_allocation, String storage_removal_strategy,
			String material_quantity, String order_picking, String mech_auto_possibility);

	/**
	 * Methods to access supplier table
	 */
	public Supplier getSupplier(String materialID);

	public void storeSupplier(String materialID, String supplier_name, String delivery_packaging_type,
			double amount_of_items_per_delivery_packaging, int delivery_packaging_stacking_height,
			double material_weight_incl_delivery_packaging, int delivery_packaging_length, int delivery_packaging_width,
			int delivery_packaging_height);

	/**
	 * Methods to access customer table
	 */
	public Customer getCustomer(String materialID);

	public void storeCustomer(String materialID, String customer_name, String dispatch_packaging_type,
			double amount_of_items_per_dispatch_packaging, int dispatch_packaging_stacking_height,
			double material_weight_incl_dispatch_packaging, int dispatch_packaging_length, int dispatch_packaging_width,
			int dispatch_packaging_height);

	/**
	 * Methods to access procurement table
	 */
	public Procurement getProcerement(String materialID);

	public void storeProcurement(String materialID, String Supplier_number_strategy, String Market_field_strategy,
			String Supply_strategy, String market_research_result, String product_feature,
			String predictability_procurement, String flexibility, String complexity, String subcription_price,
			String order_and_transaction_costs, String transport_costs, String storage_and_capital_costs,
			String procurement_time, String risk_by_supplier_failure, String transport_risk, String currency_risk,
			String exchange_rate_risk, String customs_problem_risk, String dependence_on_supplier,
			String number_of_suppliers_and_contacts, String expenditure_on_bureaucracy, String language,
			String currency, String exchange_rate_utilization);

	/**
	 * Methods to access Phasemodel Table
	 */
	public void savePhase(String stage, String username) throws ParseException;

	public void saveVariant(String variant, String username) throws ParseException;

	public PhaseModel loadLatestPhaseModel(String username);

	/**
	 * Methods to access material_classification table
	 */
	public void saveToMaterialClassification(String user, String materialID, String supply_risk,
			String further_criteria, String further_criteria_value, String classification);

	public void saveMarketPowerPortfolio(String user, String materialID, String supply_risk,
			String further_criteria_value, String classification);

	public MaterialClassification retrieveInformationFromMaterialClassification(String materialID, String user);

	public MarketPowerPortfolio getExistingPortfolio(String supplierID, String user);

	/**
	 * Methods to access abc analysis table
	 */
	public List<ABCAnalysis> fetchExistingABCAnalysis(String user);

	public void updateMaterialAbcAnalysis(ABCAnalysis updatedAnalysisObject, String user);

	public void updateMaterialAbcAnalysisWithCategory(ABCAnalysis updatedAnalysisObject, String user);

	/**
	 * Methods accessing users table.
	 */
	public List<User> fetchUserList();
	
	public boolean validateUser(String username, String password);

	public boolean addNewUser(String username, String password, boolean isActive, boolean isAdmin);

	public boolean editUser(String username, boolean isActive, boolean isAdmin);

	public boolean deleteUser(String username);

	/**
	 * Methods to access startuplog_functions table
	 */
	public List<Functions> getAllFunctions();

	/**
	 * Methods to access makebuydecision table
	 */
	public List<MakeBuyDecision> fetchExistingMakeBuyDecisions(String user, String stage);

	// public List<Material> getAllMaterialForMake();
	List<Material> getAllMaterialForMake();

	public List<Supplier> getSuppliers();

}