package org.fml.startuplog.exception;

public class NoChangesToUpdate extends Exception{

	public NoChangesToUpdate() {
		super("No changes to update");
	}

}
