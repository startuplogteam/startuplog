package org.fml.startuplog.exception;

public class NoDecisonsFetchedException extends Exception {
	public NoDecisonsFetchedException() {
		super("No Materials available to display");

	}

}
