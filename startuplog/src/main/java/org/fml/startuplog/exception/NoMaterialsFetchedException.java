package org.fml.startuplog.exception;

public class NoMaterialsFetchedException extends Exception {
	public NoMaterialsFetchedException() {
		super("No Materials available to display");
	}

}
