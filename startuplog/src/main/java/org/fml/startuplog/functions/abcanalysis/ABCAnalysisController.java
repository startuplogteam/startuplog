package org.fml.startuplog.functions.abcanalysis;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.fml.startuplog.exception.NoChangesToUpdate;
import org.fml.startuplog.exception.NoMaterialsFetchedException;
import org.fml.startuplog.model.ABCAnalysis;
import org.fml.startuplog.model.ABCAnalysisCategory;
import org.fml.startuplog.model.ABCAnalysisWrapper;
import org.fml.startuplog.model.Material;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class ABCAnalysisController {

	@Autowired
	private ABCAnalysisService service;
	private List<ABCAnalysis> abcAnalysis = new ArrayList<ABCAnalysis>();
	
	@RequestMapping(value = "/abc_analysis", method = RequestMethod.GET)
	public String preLoadData(ModelMap model, @ModelAttribute("user") String user) {
		ABCAnalysisCategory category = null;
		List<Material> materials;
		try {
			
			abcAnalysis=service.fetchExistingABCAnalysis(user);
			materials = service.fetchMaterialList();
			if(materials==null){
				throw new NoMaterialsFetchedException();
			}
			if(materials.isEmpty()){
					throw new NoMaterialsFetchedException();
			}
			
			if(abcAnalysis.isEmpty()){
				for(Material material : materials){
					ABCAnalysis analysis = new ABCAnalysis();
					analysis.setMaterialId(material.getMaterial_number());
					analysis.setMaterialName(material.getMaterial_name());
					abcAnalysis.add(analysis);
				}
			}
			else{
				loop : 
				for(Material material : materials){
					for(ABCAnalysis analysis : abcAnalysis){
						if(analysis.getMaterialId().equals(material.getMaterial_number())){
							continue loop;
						}
					}
					ABCAnalysis analysis = new ABCAnalysis();
					analysis.setMaterialId(material.getMaterial_number());
					analysis.setMaterialName(material.getMaterial_name());
					abcAnalysis.add(analysis);
				}
				category = service.fetchCategoryWiseMaterials(user);
				
			}
			
			model.put("abcanalysis", abcAnalysis);
			if(category!=null){
				model.put("abcanalysiscategoryA", category.getA());
				model.put("abcanalysiscategoryB", category.getB());
				model.put("abcanalysiscategoryC", category.getC());
			}
			
			
		}catch (NoMaterialsFetchedException e) {
			model.put("errormsg", e.getMessage());
		}
		return "abcAnalysis";
	}
	
	@RequestMapping(value = "/abc_analysis", method = RequestMethod.POST, consumes="application/json")
	public 	@ResponseBody ABCAnalysisCategory saveMaterialClassification(@ModelAttribute("user")String user,@RequestBody ABCAnalysisWrapper abcAnalysisWrapper,ModelMap model) {
		
		List<ABCAnalysis> newABCAnalysis = abcAnalysisWrapper.getAnalyses();
		List<ABCAnalysis> toUpdate = new ArrayList<ABCAnalysis>();
	
		
		for(ABCAnalysis analysisOld : abcAnalysis)
			for(ABCAnalysis analysisNew : newABCAnalysis){
				if(analysisOld.getMaterialId().equals(analysisNew.getMaterialId())){
					if((!(analysisOld.getSalesPerYear()==analysisNew.getSalesPerYear()))||
					   (!(analysisOld.getPricePerPiece()==analysisNew.getPricePerPiece()))){
							toUpdate.add(analysisNew);
					}	
				}
			}
		
		try{
			if(toUpdate.size()==0){
				throw new NoChangesToUpdate();
			}
		}
		catch(NoChangesToUpdate e){
			model.put("errormsg", e.getMessage());
			return null;
		}
		
		for(int i=0;i<toUpdate.size()-1;i++)
			for(int j=0;j<toUpdate.size()-i-1;j++) {
				if((toUpdate.get(j).getSalesPerYear()*toUpdate.get(j).getPricePerPiece())<(toUpdate.get(j+1).getSalesPerYear()*toUpdate.get(j+1).getPricePerPiece())) {
					Collections.swap(toUpdate, j, j+1);
				}
			}
		
		for(int i=0;i<toUpdate.size();i++) {
			System.out.println("from UI:"+toUpdate.get(i).getSalesPerYear()*toUpdate.get(i).getPricePerPiece());
		}
		
		service.updateABCAnalysis(toUpdate,user);
		this.abcAnalysis = newABCAnalysis;
		
		ABCAnalysisCategory category = service.fetchCategoryWiseMaterials(user);
		return category;
	}
	
	@ExceptionHandler (HttpSessionRequiredException.class)
	public String notLoggedIn(){
		return "redirect:login";
	}
}