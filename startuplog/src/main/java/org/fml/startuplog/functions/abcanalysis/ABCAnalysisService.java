package org.fml.startuplog.functions.abcanalysis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import org.fml.startuplog.model.ABCAnalysis;
import org.fml.startuplog.model.ABCAnalysisCategory;
import org.fml.startuplog.model.Material;
import org.fml.startuplog.service.MasterService;
import org.springframework.stereotype.Service;

@Service
public class ABCAnalysisService extends MasterService {
	
	public List<Material> fetchMaterialList(){
		return startuplogDAOObject.getAllMaterial_for_procurement();
	}

	public List<ABCAnalysis> fetchExistingABCAnalysis(String user){
		return startuplogDAOObject.fetchExistingABCAnalysis(user);
	}

	public void updateABCAnalysis(List<ABCAnalysis> toUpdate, String user) {
		try {
		
		for(ABCAnalysis analysis : toUpdate){
			startuplogDAOObject.updateMaterialAbcAnalysis(analysis,user);
		}
		
		
		List<ABCAnalysis> existingAnalysis = startuplogDAOObject.fetchExistingABCAnalysis(user);
		
		
		
		//for sorting the list in descending order
		for(int i=0;i<existingAnalysis.size()-1;i++)
			for(int j=0;j<existingAnalysis.size()-i-1;j++) {
				if(existingAnalysis.get(j).getTotalSales()<existingAnalysis.get(j+1).getTotalSales()) {
					Collections.swap(existingAnalysis, j, j+1);
				}
			}
		
		for(int i=0;i<existingAnalysis.size();i++) {
			System.out.println("from database:"+existingAnalysis.get(i).getTotalSales());
		}
		
		if(!existingAnalysis.isEmpty()) {
			existingAnalysis.get(0).setTotalSalesCumulated(existingAnalysis.get(0).getTotalSales());
		
			for(int i=0;i<existingAnalysis.size()-1;i++){
				existingAnalysis.get(i+1).setTotalSalesCumulated(existingAnalysis.get(i).getTotalSalesCumulated()+existingAnalysis.get(i+1).getTotalSales());
			}
			double totalValue = existingAnalysis.get(existingAnalysis.size()-1).getTotalSalesCumulated();
			for(int i=0;i<existingAnalysis.size();i++){
				existingAnalysis.get(i).setTotalSalesPercent((existingAnalysis.get(i).getTotalSales()/totalValue)*100);
				existingAnalysis.get(i).setTotalSalesCumulatedPercent((existingAnalysis.get(i).getTotalSalesCumulated()/totalValue)*100);
			}
		
			for(ABCAnalysis analysis : existingAnalysis){
				System.out.println("\n"+analysis.getTotalSales()+"---"+analysis.getTotalSalesCumulated()+"---"+analysis.getTotalSalesPercent	()+"---"+analysis.getTotalSalesCumulatedPercent());
				if(analysis.getTotalSalesCumulatedPercent()>=95)
					analysis.setCategory('C');
				else if(analysis.getTotalSalesCumulatedPercent()>=80)
					analysis.setCategory('B');
				else
					analysis.setCategory('A');
			
			startuplogDAOObject.updateMaterialAbcAnalysisWithCategory(analysis,user);	
			}
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public ABCAnalysisCategory fetchCategoryWiseMaterials(String user) {
		List<String> A = new ArrayList<String>();
		List<String> B = new ArrayList<String>();
		List<String> C = new ArrayList<String>();
		
		ABCAnalysisCategory category = new ABCAnalysisCategory();
		
		List<ABCAnalysis> existingAnalysis = startuplogDAOObject.fetchExistingABCAnalysis(user);
		
		
		for(ABCAnalysis analysis : existingAnalysis){
			if(analysis.getCategory()=='A'){
				A.add(analysis.getMaterialName());
			}
			else if(analysis.getCategory()=='B'){
				B.add(analysis.getMaterialName());
			}
			else if(analysis.getCategory()=='C'){
				C.add(analysis.getMaterialName());
			}
		}
		category.setA(A);
		category.setB(B);
		category.setC(C);
		return category;
	}
}
