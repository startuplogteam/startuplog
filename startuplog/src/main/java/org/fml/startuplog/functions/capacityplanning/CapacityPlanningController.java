package org.fml.startuplog.functions.capacityplanning;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.fml.startuplog.model.CapacityPlanning;
import org.fml.startuplog.model.DemandPlanning;
import org.fml.startuplog.model.ProductionSteps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class CapacityPlanningController {
	
	@Autowired
	private CapacityPlanningService service;
	
	@RequestMapping(value = "/plan_capacity", method = RequestMethod.GET)
	public String displayPage(ModelMap model,@ModelAttribute CapacityPlanning capacityPlanning) {
		
		capacityPlanning = service.fetchExistingCapacityPlanning(capacityPlanning,model.get("user").toString());
		
		if(capacityPlanning.getProductionSteps()==null){
			List<ProductionSteps> steps = new ArrayList<ProductionSteps>();
			ProductionSteps productionStep = new ProductionSteps();
			productionStep.setStepNumber(1);
			steps.add(productionStep);
			capacityPlanning.setProductionSteps(steps);
		}
		if(capacityPlanning.getDemandPlanning()==null){
			List<DemandPlanning> demands = new ArrayList<DemandPlanning>();
			DemandPlanning demand = new DemandPlanning();
			demand.setYear(Calendar.getInstance().get(Calendar.YEAR));
			demands.add(demand);
			capacityPlanning.setDemandPlanning(demands);
		}
		return "capacityPlanning";
	}
	
	@RequestMapping(value = "/plan_capacity", method = RequestMethod.POST)
	public String savePlansAndComputeEstimate(ModelMap model, @ModelAttribute CapacityPlanning capacityPlanning) {
	
		capacityPlanning = service.computeEstimate(capacityPlanning,model.get("user").toString());
		
		return "capacityPlanning";
	}
}
