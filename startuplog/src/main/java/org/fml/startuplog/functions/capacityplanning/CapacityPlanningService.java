package org.fml.startuplog.functions.capacityplanning;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.fml.startuplog.model.CapacityPlanning;
import org.fml.startuplog.model.DemandPlanning;
import org.fml.startuplog.model.MachineCapacity;
import org.fml.startuplog.model.MachinesNeeded;
import org.fml.startuplog.model.ProductionSteps;
import org.fml.startuplog.model.StaffCapacity;
import org.fml.startuplog.model.WorkingTime;
import org.fml.startuplog.service.MasterService;
import org.springframework.stereotype.Service;

@Service
public class CapacityPlanningService extends MasterService{

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("startuplog");
	private CapacityPlanning capacityPlanning;
	private double totalTNStaff, availableTime;
	private HashMap<String, Double> machines;
	
	public CapacityPlanning computeEstimate(CapacityPlanning capacityPlanning,String user) {
		
		this.capacityPlanning = capacityPlanning;
		
		//computing time needed for staff
		this.totalTNStaff=0.0;
		for(ProductionSteps step : capacityPlanning.getProductionSteps()){
			totalTNStaff += step.getTNForStaff();  
		}
		//compute available time for staff/machine
		availableTime = capacityPlanning.getWHPerDay()*(double)capacityPlanning.getWDPerWeek()*(45.00)*(60.00);
		
		//populating List<StaffCapacity> 
		List<StaffCapacity> staffCapacity = new ArrayList<StaffCapacity>();
		
		for(DemandPlanning demand :capacityPlanning.getDemandPlanning()){
			double staffsNeeded = computeStaffCapacityForYear(demand.getYear(),demand.getEstimate());
			StaffCapacity capacity = new StaffCapacity(demand.getYear(),staffsNeeded);
			staffCapacity.add(capacity);
		}
		
		//Finding the unique machines and the total time required for all the steps with that machine
		machines = new HashMap<String, Double>();
		for(ProductionSteps step : capacityPlanning.getProductionSteps()){
			if(machines.containsKey(step.getMachineDesignation())){
				Double sum = machines.get(step.getMachineDesignation());
				machines.put(step.getMachineDesignation(), sum+step.getTNForMachine());
			}
			else{
				machines.put(step.getMachineDesignation(), step.getTNForMachine());
			}
		}
		
		//Populating List<MachinesNeeded>
		List<MachinesNeeded> machinesNeeded = new ArrayList<MachinesNeeded>();
		Iterator<Entry<String, Double>> iterator = machines.entrySet().iterator();
		while(iterator.hasNext()){
			Entry<String, Double> pair = iterator.next();
			MachinesNeeded machine = new MachinesNeeded();
			machine.setMachineDesignation(pair.getKey());
			List<MachineCapacity> capacities = new ArrayList<MachineCapacity>();
			for(DemandPlanning demand :capacityPlanning.getDemandPlanning()){
				double machineCount = computeMachineCapacityForYear(demand.getYear(),demand.getEstimate(),pair.getKey());
				MachineCapacity capacity = new MachineCapacity(demand.getYear(),machineCount);
				capacities.add(capacity);
			}
			machine.setMachineCapacity(capacities);
			machinesNeeded.add(machine);
		}
		capacityPlanning.setStaffCapacity(staffCapacity);
		capacityPlanning.setMachinesNeeded(machinesNeeded);
		
		saveChanges(user);
		
		return capacityPlanning;
	}


	private void saveChanges(String user) {
		EntityManager em = emf.createEntityManager();
		WorkingTime workingTime = new WorkingTime();
		workingTime.setUncertainityFactor(capacityPlanning.getUncertainityFactor());
		workingTime.setWDPerWeek(capacityPlanning.getWDPerWeek());
		workingTime.setWHPerDay(capacityPlanning.getWHPerDay());
		workingTime.setWorkTimeId(1);
		
		TypedQuery<WorkingTime> fetchWorkingTimes = em.createQuery("SELECT w FROM WorkingTime w where w.username=?1", WorkingTime.class);
		fetchWorkingTimes.setParameter(1, user);
		List<WorkingTime> workingDetails = fetchWorkingTimes.getResultList();
		if(!workingDetails.isEmpty()){
			workingTime.setId(workingDetails.get(0).getId());
			workingTime.setUsername(user);
			em.getTransaction().begin();
				em.merge(workingTime);
			em.getTransaction().commit();
		}
		else {
			int count=0;
			Query fetchMaxWorkId = em.createQuery("select count(*) from WorkingTime wt");
			count = Integer.parseInt(fetchMaxWorkId.getResultList().get(0).toString());
		
			workingTime.setId(++count);
			workingTime.setUsername(user);
			em.getTransaction().begin();
				em.merge(workingTime);
			em.getTransaction().commit();
		}
		
		List<DemandPlanning> demands = capacityPlanning.getDemandPlanning();
		for(DemandPlanning demand : demands){
			TypedQuery<DemandPlanning> fetchDemandDetails = em.createQuery("SELECT d FROM DemandPlanning d where d.username=?1 and d.year=?2", DemandPlanning.class);
			fetchDemandDetails.setParameter(1, user);
			fetchDemandDetails.setParameter(2, demand.getYear());
			List<DemandPlanning> demandDetails = fetchDemandDetails.getResultList();
			if(!demandDetails.isEmpty()){
				demand.setId(demandDetails.get(0).getId());
				demand.setUsername(user);
				em.getTransaction().begin();
					em.merge(demand);
				em.getTransaction().commit();
			}
			else {
			em.getTransaction().begin();				
				Query fetchMaxDemandId = em.createQuery("select count(*) from DemandPlanning dp");
				int count=0;
				count = Integer.parseInt(fetchMaxDemandId.getResultList().get(0).toString());
				demand.setId(++count);
				demand.setUsername(user);
				em.merge(demand);
			em.getTransaction().commit();
			}
		}
		
		List<ProductionSteps> steps = capacityPlanning.getProductionSteps();
		for(ProductionSteps step : steps){
			
			TypedQuery<ProductionSteps> fetchProductionSteps = em.createQuery("SELECT p FROM ProductionSteps p where p.username=?1 and p.stepNumber=?2", ProductionSteps.class);
			fetchProductionSteps.setParameter(1, user);
			fetchProductionSteps.setParameter(2, step.getStepNumber());
			List<ProductionSteps> productionDetails = fetchProductionSteps.getResultList();
			if(!productionDetails.isEmpty()){
				step.setId(productionDetails.get(0).getId());
				step.setUsername(user);
				em.getTransaction().begin();
					em.merge(step);
				em.getTransaction().commit();
			}
			else {
			em.getTransaction().begin();
				Query fetchMaxProdctionId = em.createQuery("select count(*) from ProductionSteps ps");
				int count=0;
				count = Integer.parseInt(fetchMaxProdctionId.getResultList().get(0).toString());
				step.setId(++count);
				step.setUsername(user);
				em.merge(step);
			em.getTransaction().commit();
			}
		}		
		
		//Case to delete rows from ProductionSteps and DemandPlanning
		
		TypedQuery<ProductionSteps> fetchProductionSteps = em.createQuery("SELECT p FROM ProductionSteps p where p.username=?1", ProductionSteps.class);
		fetchProductionSteps.setParameter(1, user);
		List<ProductionSteps> productionDetails = fetchProductionSteps.getResultList();
		for(ProductionSteps inTable: productionDetails){
			boolean flag = false;
			for(ProductionSteps step : steps){
				if(inTable.getStepNumber()==step.getStepNumber()){
					flag = true;
				}
			}
			if(!flag){
				em.getTransaction().begin();
				em.createQuery("delete FROM ProductionSteps p where p.username='"+user+"' and p.stepNumber="+inTable.getStepNumber()+"").executeUpdate();
				em.getTransaction().commit();
			}
		}
		
		TypedQuery<DemandPlanning> fetchDemandDetails = em.createQuery("SELECT d FROM DemandPlanning d where d.username=?1", DemandPlanning.class);
		fetchDemandDetails.setParameter(1, user);
		List<DemandPlanning> demandDetails = fetchDemandDetails.getResultList();
		for(DemandPlanning inTable : demandDetails){	
			boolean flag = true;
			for(DemandPlanning demand : demands){
				if(inTable.getYear()==demand.getYear()){
					flag = false;
				}
			}
			if(flag){
				
				em.getTransaction().begin();
					em.createQuery("delete FROM DemandPlanning d where d.username='"+user+"' and d.year="+inTable.getYear()+"").executeUpdate();
				em.getTransaction().commit();
			}
		
		}
		em.close();
	}

	private double computeStaffCapacityForYear(int year,int estimate) {
		double capacityForStaff = (((totalTNStaff*(double)estimate)/availableTime)*(1+capacityPlanning.getUncertainityFactor()));
		DecimalFormat format = new DecimalFormat("#######.00");
		return Double.parseDouble(format.format(capacityForStaff));
	}	
	
	private double computeMachineCapacityForYear(int year,int estimate, String machineDesignation) {
		Double totalTNMachine = machines.get(machineDesignation);
		double capacityForMachine = (((totalTNMachine*(double)estimate)/availableTime)*(1+capacityPlanning.getUncertainityFactor()));
		DecimalFormat format = new DecimalFormat("#######.00");
		return Double.parseDouble(format.format(capacityForMachine));
	}

	public CapacityPlanning fetchExistingCapacityPlanning(CapacityPlanning capacityPlanning,String user) {
		
		EntityManager em = emf.createEntityManager();
		
		TypedQuery<WorkingTime> fetchWorkingTimes = em.createQuery("SELECT w FROM WorkingTime w where w.username=?1", WorkingTime.class);
		fetchWorkingTimes.setParameter(1, user);
		List<WorkingTime> workingDetails = fetchWorkingTimes.getResultList();
		if(!workingDetails.isEmpty()){
			capacityPlanning.setWDPerWeek(workingDetails.get(0).getWDPerWeek());
			capacityPlanning.setWHPerDay(workingDetails.get(0).getWHPerDay());
			capacityPlanning.setUncertainityFactor(workingDetails.get(0).getUncertainityFactor());
		}
		
		TypedQuery<ProductionSteps> fetchProductionSteps = em.createQuery("SELECT p FROM ProductionSteps p where p.username=?1 order by p.stepNumber", ProductionSteps.class);
		fetchProductionSteps.setParameter(1, user);
		List<ProductionSteps> productionDetails = fetchProductionSteps.getResultList();
		if(!productionDetails.isEmpty()){
			capacityPlanning.setProductionSteps(productionDetails);
		}
		
		TypedQuery<DemandPlanning> fetchDemandDetails = em.createQuery("SELECT d FROM DemandPlanning d where d.username=?1 order by d.year", DemandPlanning.class);
		fetchDemandDetails.setParameter(1, user);
		List<DemandPlanning> demandDetails = fetchDemandDetails.getResultList();
		if(!demandDetails.isEmpty()){
			capacityPlanning.setDemandPlanning(demandDetails);
		}
		em.close();
		return capacityPlanning;
	}
	
}