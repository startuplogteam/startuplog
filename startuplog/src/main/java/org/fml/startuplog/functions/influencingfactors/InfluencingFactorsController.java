package org.fml.startuplog.functions.influencingfactors;

import org.fml.startuplog.model.InfluencingFactors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class InfluencingFactorsController {

	@Autowired
	private InfluencingFactorsService service;
	
	@RequestMapping(value = "/saveinfluencingfactors", method = RequestMethod.GET)
	public void saveInfluencingFactors(ModelMap model, @RequestParam String competency,@RequestParam String productType, @RequestParam String distribution, @RequestParam String productNature,@RequestParam double salesTarget, @RequestParam double salesCurrent, @RequestParam double customers, @RequestParam double logisticsBudget){
		
		service.saveInfluencingFactors(model.get("user").toString(),competency,productType,distribution, productNature, salesTarget, salesCurrent, customers, logisticsBudget);
	}

	public InfluencingFactors loadExistingInfluencingFactors(ModelMap model) {
		// TODO Auto-generated method stub
		return service.getInfluencingFactors(model.get("user").toString());
	}
}
