package org.fml.startuplog.functions.influencingfactors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.fml.startuplog.model.InfluencingFactors;
import org.springframework.stereotype.Service;

@Service
public class InfluencingFactorsService {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("startuplog");
	
	public void saveInfluencingFactors(String user, String competency, String productType, String distribution, String productNature,
			double salesTarget, double salesCurrent, double customers, double logisticsBudget) {
		
		EntityManager em = emf.createEntityManager();
		InfluencingFactors factors = new InfluencingFactors();
		factors.setUserName(user);
		factors.setCompetency(competency);
		factors.setProductType(productType);
		factors.setDistribution(distribution);
		factors.setProductNature(productNature);
		factors.setSalesTarget(salesTarget);
		factors.setSalesCurrent(salesCurrent);
		factors.setCustomers(customers);
		factors.setLogisticsBudget(logisticsBudget);
		
		em.getTransaction().begin();
			em.merge(factors);
		em.getTransaction().commit();
		
		em.close();
		
	}

	public InfluencingFactors getInfluencingFactors(String user) {
		EntityManager em = emf.createEntityManager();
		InfluencingFactors factors = new InfluencingFactors();
		factors = em.find(InfluencingFactors.class, user);
		em.close();
		return factors;
		
	}
	
}
