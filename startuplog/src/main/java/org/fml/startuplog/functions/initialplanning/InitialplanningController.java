package org.fml.startuplog.functions.initialplanning;

import java.io.IOException;
import org.apache.log4j.Logger;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.fml.startuplog.dao.StartuplogDAO;
import org.fml.startuplog.model.Material;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class InitialplanningController {

	@Autowired
	private InitialplanningService impService;

	private final Logger logger = Logger.getLogger(InitialplanningController.class);

	/**
	 * Returns initialplanning.jsp Saves list of available materials and list of
	 * previously selected materials for update into modelmap
	 */
	@RequestMapping(value = "/initialplanning", method = RequestMethod.GET)
	public String initialPlanning(ModelMap model, @ModelAttribute("user") String user) throws IOException, ParseException {
		List<Material> materialList = impService.getAllMaterial_number_name();

		if (materialList == null) {
			logger.error("NULL returned as material List");
		} 
		model.put("materialList", materialList);
		logger.info("Storage Facility Invoked");
		return "initialplanning";
	}

	/**
	 * Returns all material suggestions into json format
	 */
	@RequestMapping(value = "/getMaterialSuggestionAsJson", method = RequestMethod.GET)
	@ResponseBody
	public String getJson(@RequestParam String material_id) throws IOException, ParseException {
		String material_as_json_string = impService.getMaterialDataAsJson(material_id);
		if (material_as_json_string == null) {
			logger.error("NULL returned as json string of the material with material ID " + material_id);
			return "{}";
		} else {
			return material_as_json_string;
		}
	}

	/**
	 * Return previously save data of a material
	 * 
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@RequestMapping(value = "/getPreviousInitialplanningData", method = RequestMethod.GET)
	@ResponseBody
	public String getPreviousData(@RequestParam String materialID) throws IOException, ParseException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String json_string_previous_initial_planning_data = impService.getMaterialFromStorageFacilityAsJson(materialID);
		if (json_string_previous_initial_planning_data.endsWith("{}")){
			logger.info("Retrieved {} for material with ID" + materialID);
		}
		return impService.getMaterialFromStorageFacilityAsJson(materialID);
	}

	/**
	 * Returns materialType
	 */
	@RequestMapping(value = "/getMaterialType", method = RequestMethod.POST)
	@ResponseBody
	public String getMaterialType(@RequestParam String material_id) throws IOException, ParseException {
		
		Material material = impService.getMaterial(material_id);
		if (material == null){
			logger.error("NULL returned form Material Table for ID "+ material_id);
			return null;
		}
		else
		{
			return material.getMaterial_type();
		}
		
	}

	@RequestMapping(value = "/getMaterialName", method = RequestMethod.GET)
	@ResponseBody
	public String getMaterialName(@RequestParam String materialID) throws IOException, ParseException {

		Material material = impService.getMaterial(materialID);
		if (material == null){
			logger.error("NULL returned form Material Table for ID "+ materialID);
			return null;
		}
		else
		{
			return material.getMaterial_name();
		}
		
	}

	/**
	 * Returns whether material is in customer/supplier table or not
	 */
	@RequestMapping(value = "/check_supplier_customer_table", method = RequestMethod.GET)
	@ResponseBody
	public String check_packaging_possibility(@RequestParam String materialID) {
		
		boolean exist = impService.checkMaterial_for_packaging(materialID);
		logger.info("Checked material with ID "+ materialID + " in Supplier or Customer Table. Found "+ exist);
		return String.valueOf(exist);
	}

	/**
	 * Triggered when update information invoked
	 * 
	 * @return initialplanning-update_informations.jsp file Save materialID,
	 *         materialType and material existence in customer/supplier table
	 *         for future use
	 */
	@RequestMapping(value = "/initialplanning-update_informations", method = RequestMethod.POST)
	public String getJSPForTabs() throws IOException, ParseException {
		logger.info("Read JSP file for Tab");
		return "initialplanning-update_informations";
	}

	/**
	 * Triggered when save button of initialplanning-update_information clicked
	 * Before saving or updating the material first checks whether it is in
	 * supplier or customer table to ensure that user selected delivery or
	 * dispatch packing but material is not in supplier or customer table
	 * 
	 * Make unit decision Strategy decision Final Storage decision Save into DB
	 */
	@RequestMapping(value = "/saveIntoStorageFacility", method = RequestMethod.GET)
	public String saveInitialPlanningMaterialInfo(ServletRequest request) throws IOException, ParseException {

		Map<String, String[]> paramMap = request.getParameterMap();
	
		if (paramMap.size() == 7){
			logger.error("Nothing passed when material information updated");
		}
		
		String storageUnitDecision = impService.storageUnitDecision(paramMap);
		if (storageUnitDecision.equals("failsupplier")) {
			logger.error("Failed to retrieve information about material from Supplier Table");
		} else if (storageUnitDecision.equals("failcustomer")) {
			logger.error("Failed to retrieve information about material from Customer Table");
		} else {
			impService.storageStrategyDecision(paramMap);
			impService.finalDecisionMaking(paramMap, storageUnitDecision);
			logger.info("Successfully updated material information");
		}

		return "redirect:initialplanning";
	}

	/**
	 * Triggered when cancel button invoked
	 * 
	 * @return initialplanning.jsp
	 */
	@RequestMapping(value = "/saveInitialPlanningMaterialInfo", params = "cancel", method = RequestMethod.GET)
	public String cancelInitialPlanningMaterialInfo() throws IOException, ParseException {
		logger.info("Cancel button pressed in the Tab");
		return "initialplanning";
	}
	
	@ExceptionHandler (HttpSessionRequiredException.class)
	public String notLoggedIn(){
		return "redirect:login";
	}

}
