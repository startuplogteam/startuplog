package org.fml.startuplog.functions.initialplanning;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.fml.startuplog.model.Customer;
import org.fml.startuplog.model.Material;
import org.fml.startuplog.model.StorageFacility;
import org.fml.startuplog.model.Supplier;
import org.fml.startuplog.service.MasterService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

@Service
public class InitialplanningService extends MasterService {

	@Autowired
	private ServletContext context;

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	@Qualifier("messageSource")
	private MessageSource messageSource;

	// Checker
	private boolean allStrategyQuestionAnswered = false;
	private boolean allUnitQuestionAnswered = false;

	// storage unit
	private String storage_in_delivery_packaging = null;
	private String storage_in_dispatch_packaging = null;
	private String storage_packaging_type = null;
	private String are_storage_unit_stackable = null;
	private String storage_packaging_stacking_height = null;
	private String amount_of_items_per_storage_packaging = null;
	private String material_weight_incl_storage_packaging = null;
	private String storage_packaging_length = null;
	private String storage_packaging_width = null;
	private String storage_packaging_height = null;

	// Storage Strategy
	private String storage_bin_assignment = null;
	private String storage_allocation = null;
	private String storage_removal_strategy = null;

	// further questions
	private String material_quantity = null;
	private String order_picking = null;
	private String mech_auto_possibility = null;

	private List<String> storageBinStrategy;
	private List<String> storageLocationStrategy;
	private List<String> storageRemovalStrategy;

	public InitialplanningService() {
		initialize();
	}

	public String getRealPath(String filePath) {
		return context.getRealPath("/WEB-INF/") + filePath;
	}

	public List<Material> getAllMaterial_number_name() {
		return startuplogDAOObject.getAllMaterial_number_name();
	}

	public Material getMaterial(String materialID) {
		return startuplogDAOObject.getMaterial(materialID);
	}

	private String createJson(StorageFacility material_in_storagefacility, String materialName) throws IOException {

		JSONObject material_json = new JSONObject();
		JSONArray facilities;
		String[] tokens;
		if (material_in_storagefacility != null) {
			if (material_in_storagefacility.getIsAllInfoGiven().equals("true")) {
				tokens = material_in_storagefacility.getStorage_facility().split(",");
				facilities = new JSONArray();
				facilities.add(translet_to_locale(materialName));
				if (tokens.length == 1 && tokens[0].equals("")) {
					facilities.add("");
				} else {
					for (String suggestion : tokens) {
						if (!suggestion.equals(" ")) {
							facilities.add(
									messageSource.getMessage(suggestion.trim(), null, LocaleContextHolder.getLocale()));
						}
					}
				}
				material_json.put(material_in_storagefacility.getMaterial_number(), facilities);
				//facilities.remove(0);
				if (facilities.size() == 1)
				{
					logger.info("Material with ID " + material_in_storagefacility.getMaterial_number() + " has no suggestion");
				}
				else{
					logger.info("Material with ID " + material_in_storagefacility.getMaterial_number() + " has suggestion "+ facilities.toJSONString());
				}
				
			}
			else{
				logger.info("All info given false for material ID" + material_in_storagefacility.getMaterial_number() );
			
			}
		}
		else{
			logger.info(materialName + " not availble at Storage Facility Table");
		}

		return material_json.toJSONString();

	}

	public String getMaterialDataAsJson(String material_id) throws IOException {
		StorageFacility material_in_storagefacility = startuplogDAOObject
				.get_material_from_storagefacility(material_id);
		Material material = startuplogDAOObject.getMaterial(material_id);
		if (material_in_storagefacility == null) {
			logger.info("NULL retured from StorageFacility Table with material ID " + material_id);
			return null;
		} else if (material == null) {
			logger.error("NULL retured from Material Table with material ID " + material_id);
			return null;
		} else {

			return createJson(material_in_storagefacility, material.getMaterial_name());
		}
	}

	private void initialize() {
		// two indicators
		allStrategyQuestionAnswered = false;
		allUnitQuestionAnswered = false;

		// unknown group

		// storage unit
		storage_in_delivery_packaging = null;
		storage_in_dispatch_packaging = null;
		storage_packaging_type = null;
		are_storage_unit_stackable = null;
		storage_packaging_stacking_height = null;
		amount_of_items_per_storage_packaging = null;
		material_weight_incl_storage_packaging = null;
		storage_packaging_length = null;
		storage_packaging_width = null;
		storage_packaging_height = null;

		// Storage Strategy
		storage_bin_assignment = null;
		storage_allocation = null;
		storage_removal_strategy = null;

		// further questions
		material_quantity = null;
		order_picking = null;
		mech_auto_possibility = null;

		// some lists
		storageBinStrategy = new ArrayList<>();
		storageLocationStrategy = new ArrayList<>();
		storageRemovalStrategy = new ArrayList<>();

	}

	private boolean retrieveDataFromCustomerTable(String materialID) {

		Customer customer = startuplogDAOObject.getCustomer(materialID);
		if (customer != null) {
			storage_packaging_type = customer.getDispatch_packaging_type();
			amount_of_items_per_storage_packaging = customer.getAmount_of_items_per_dispatch_packaging();
			storage_packaging_stacking_height = customer.getDispatch_packaging_height();
			material_weight_incl_storage_packaging = customer.getMaterial_weight_incl_dispatch_packaging();
			storage_packaging_height = customer.getDispatch_packaging_height();
			storage_packaging_length = customer.getDispatch_packaging_length();
			storage_packaging_width = customer.getDispatch_packaging_width();
			return true;
		} else {
			return false;

		}
	}

	private boolean retrieveDataFromSupplierTable(String materialID) {

		Supplier supplier = startuplogDAOObject.getSupplier(materialID);

		if (supplier != null) {
			storage_packaging_type = supplier.getDelivery_packaging_type();
			amount_of_items_per_storage_packaging = supplier.getAmount_of_items_per_delivery_packaging();
			storage_packaging_stacking_height = supplier.getDelivery_packaging_height();
			material_weight_incl_storage_packaging = supplier.getMaterial_weight_incl_delivery_packaging();
			storage_packaging_height = supplier.getDelivery_packaging_height();
			storage_packaging_length = supplier.getDelivery_packaging_length();
			storage_packaging_width = supplier.getDelivery_packaging_width();
			return true;
		} else {
			return false;
		}
	}

	private String unitDecisionMakingFromDispatchDelivery() {
		String storage_unit_decision = " ";
		int[][] decision = new int[2][16];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 16; j++) {
				decision[i][j] = 0;
			}
		}

		switch (storage_packaging_type) {

		case "unpacked_small":
			decision[0][0] = 1;
			break;
		case "unpacked_big":
			decision[0][1] = 1;
			break;
		case "unpacked_long":
			decision[0][2] = 1;
			break;
		case "box_open":
			decision[0][3] = 1;
			break;
		case "carton":
			decision[0][4] = 1;
			break;
		case "carton_pallet":
			decision[0][5] = 1;
			decision[0][6] = 1;
			break;
		case "pallet":
			decision[0][7] = 1;
			decision[0][8] = 1;
			break;
		case "mesh box":
			decision[0][9] = 1;
			decision[0][10] = 1;
			break;
		case "container_big":
			decision[0][11] = 1;
			decision[0][12] = 1;
			break;
		case "container_small":
			decision[0][13] = 1;
			break;
		case "container_small_p":
			decision[0][14] = 1;
			decision[0][15] = 1;
			break;

		}
		if (Integer.parseInt(storage_packaging_stacking_height) > 1) {
			decision[1][0] = 1;
			decision[1][1] = 1;
			decision[1][3] = 1;
			decision[1][4] = 1;
			decision[1][5] = 1;
			decision[1][7] = 1;
			decision[1][9] = 1;
			decision[1][11] = 1;
			decision[1][13] = 1;
			decision[1][14] = 1;
		} else {
			decision[1][0] = 1;
			decision[1][1] = 1;
			decision[1][3] = 1;
			decision[1][4] = 1;
			decision[1][6] = 1;
			decision[1][8] = 1;
			decision[1][10] = 1;
			decision[1][12] = 1;
			decision[1][14] = 1;
		}
		List<String> allPossibleOutcomes = new ArrayList<>();
		allPossibleOutcomes.add("small article in bulk");
		allPossibleOutcomes.add("big article in bulk");
		allPossibleOutcomes.add("long objects (e.g. metal pole)");
		allPossibleOutcomes.add("small parts in box");
		allPossibleOutcomes.add("parts in carton");
		allPossibleOutcomes.add("parts in carton on pallets (stackable)");
		allPossibleOutcomes.add("parts in carton on pallets (non-stackable)");
		allPossibleOutcomes.add("parts on pallets (stackable)");
		allPossibleOutcomes.add("parts on pallets (non-stackable)");
		allPossibleOutcomes.add("parts in mesh box (stackable)");
		allPossibleOutcomes.add("arts in mesh box (non-stackable)");
		allPossibleOutcomes.add("parts in big container (stackable)");
		allPossibleOutcomes.add("parts in big container (non-stackable)");
		allPossibleOutcomes.add("parts in small container");
		allPossibleOutcomes.add("parts in small container on pallets (stackable)");
		allPossibleOutcomes.add("parts in small container on pallets (non-stackable)");

		for (int j = 0; j < 16; j++) {
			boolean allColumnCrossed = true;
			for (int i = 0; i < 2; i++) {
				if (decision[i][j] == 0) {
					allColumnCrossed = false;
				}
			}
			if (allColumnCrossed == true) {
				storage_unit_decision = allPossibleOutcomes.get(j);
			}
		}
		return storage_unit_decision;
	}

	private String unitDecision(Map<String, String[]> paramMap) {
		String storage_unit_decision = " ";
		int[][] decision = new int[2][16];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 16; j++) {
				decision[i][j] = 0;
			}
		}
		if (paramMap.containsKey("storage_packaging_type")) {
			storage_packaging_type = paramMap.get("storage_packaging_type")[0];

			switch (storage_packaging_type) {

			case "unpacked_small":
				decision[0][0] = 1;
				break;
			case "unpacked_big":
				decision[0][1] = 1;
				break;
			case "unpacked_long":
				decision[0][2] = 1;
				break;
			case "box_open":
				decision[0][3] = 1;
				break;
			case "carton":
				decision[0][4] = 1;
				break;
			case "carton_pallet":
				decision[0][5] = 1;
				decision[0][6] = 1;
				break;
			case "pallet":
				decision[0][7] = 1;
				decision[0][8] = 1;
				break;
			case "box_mesh":
				decision[0][9] = 1;
				decision[0][10] = 1;
				break;
			case "container_big":
				decision[0][11] = 1;
				decision[0][12] = 1;
				break;
			case "container_small":
				decision[0][13] = 1;
				break;
			case "container_small_p":
				decision[0][14] = 1;
				decision[0][15] = 1;
				break;
			}
		}
		if (paramMap.containsKey("are_storage_unit_stackable")) {
			switch (paramMap.get("are_storage_unit_stackable")[0]) {
			case "yes":
				decision[1][0] = 1;
				decision[1][1] = 1;
				decision[1][3] = 1;
				decision[1][4] = 1;
				decision[1][5] = 1;
				decision[1][7] = 1;
				decision[1][9] = 1;
				decision[1][11] = 1;
				decision[1][13] = 1;
				decision[1][14] = 1;
				are_storage_unit_stackable = "yes";
				break;
			case "no":
				decision[1][0] = 1;
				decision[1][1] = 1;
				decision[1][3] = 1;
				decision[1][4] = 1;
				decision[1][6] = 1;
				decision[1][8] = 1;
				decision[1][10] = 1;
				decision[1][13] = 1;
				decision[1][12] = 1;
				decision[1][15] = 1;
				are_storage_unit_stackable = "no";
				break;

			}
		}

		if (paramMap.containsKey("storage_packaging_stacking_height")) {
			if (paramMap.containsKey("are_storage_unit_stackable")) {
				if (paramMap.get("are_storage_unit_stackable")[0].equals("no")) {
					storage_packaging_stacking_height = "1";
				} else {
					storage_packaging_stacking_height = paramMap.get("storage_packaging_stacking_height")[0];
				}
			}
		}

		if (paramMap.containsKey("amount_of_items_per_storage_packaging")) {
			if (storage_packaging_type.equals("unpacked_small") || storage_packaging_type.equals("unpacked_big")
					|| storage_packaging_type.equals("unpacked_long")) {
				amount_of_items_per_storage_packaging = "1";
			} else {
				amount_of_items_per_storage_packaging = paramMap.get("amount_of_items_per_storage_packaging")[0];
			}
		}

		if (paramMap.containsKey("storage_packaging_length")) {
			if (paramMap.containsKey("storage_packaging_type")) {

				if (storage_packaging_type.equals("unpacked_small") || storage_packaging_type.equals("unpacked_big")
						|| storage_packaging_type.equals("unpacked_long")) {
					storage_packaging_length = "0";
				} else {
					storage_packaging_length = paramMap.get("storage_packaging_length")[0];
				}
			}
		}
		if (paramMap.containsKey("storage_packaging_width")) {
			if (paramMap.containsKey("storage_packaging_type")) {
				if (storage_packaging_type.equals("unpacked_small") || storage_packaging_type.equals("unpacked_big")
						|| storage_packaging_type.equals("unpacked_long")) {
					storage_packaging_width = "0";
				} else {
					storage_packaging_width = paramMap.get("storage_packaging_width")[0];
				}
			}
		}
		if (paramMap.containsKey("storage_packaging_height")) {
			if (paramMap.containsKey("storage_packaging_type")) {

				if (storage_packaging_type.equals("unpacked_small") || storage_packaging_type.equals("unpacked_big")
						|| storage_packaging_type.equals("unpacked_long")) {
					storage_packaging_height = "0";
				} else {
					storage_packaging_height = paramMap.get("storage_packaging_height")[0];
				}
			}
		}

		List<String> allPossibleOutcomes = new ArrayList<>();
		allPossibleOutcomes.add("small article in bulk");
		allPossibleOutcomes.add("big article in bulk");
		allPossibleOutcomes.add("long objects (e.g. metal pole)");
		allPossibleOutcomes.add("small parts in box");
		allPossibleOutcomes.add("parts in carton");
		allPossibleOutcomes.add("parts in carton on pallets (stackable)");
		allPossibleOutcomes.add("parts in carton on pallets (non-stackable)");
		allPossibleOutcomes.add("parts on pallets (stackable)");
		allPossibleOutcomes.add("parts on pallets (non-stackable)");
		allPossibleOutcomes.add("parts in mesh box (stackable)");
		allPossibleOutcomes.add("arts in mesh box (non-stackable)");
		allPossibleOutcomes.add("parts in big container (stackable)");
		allPossibleOutcomes.add("parts in big container (non-stackable)");
		allPossibleOutcomes.add("parts in small container");
		allPossibleOutcomes.add("parts in small container on pallets (stackable)");
		allPossibleOutcomes.add("parts in small container on pallets (non-stackable)");

		for (int j = 0; j < 16; j++) {
			boolean allColumnCrossed = true;
			for (int i = 0; i < 2; i++) {
				if (decision[i][j] == 0) {
					allColumnCrossed = false;
				}
			}
			if (allColumnCrossed == true) {
				storage_unit_decision = allPossibleOutcomes.get(j);
			}
		}
		logger.info("Storage Unit Decision computed");
		return storage_unit_decision;
	}

	public String storageUnitDecision(Map<String, String[]> paramMap) {

		String materialID = paramMap.get("material_id")[0];
		String storage_unit_decision = " ";
		int[][] decision = new int[2][16];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 16; j++) {
				decision[i][j] = 0;
			}
		}

		if (paramMap.containsKey("storage_in_delivery_packaging")
				|| paramMap.containsKey("storage_in_dispatch_packaging")) {

			if (paramMap.containsKey("storage_in_delivery_packaging")) {
				if (paramMap.get("storage_in_delivery_packaging")[0].equals("yes")) {

					allUnitQuestionAnswered = true;
					storage_in_delivery_packaging = "yes";

					if (retrieveDataFromSupplierTable(materialID)) {
						logger.info("Delivery Packaging Yes. Retrieved information from Supplier Table");
						return unitDecisionMakingFromDispatchDelivery();
					} else
						return "failsupplier";
				} else {
					storage_in_delivery_packaging = "no";
					allUnitQuestionAnswered = allUnitQuestionAnswered(paramMap);
					logger.info("Delivery packaging no. Use form data to compute Storage Unit Decision");
					storage_unit_decision = unitDecision(paramMap);
				}
			} else {
				if (paramMap.get("storage_in_dispatch_packaging")[0].equals("yes")) {

					allUnitQuestionAnswered = true;
					storage_in_dispatch_packaging = "yes";
					if (retrieveDataFromCustomerTable(materialID)){
						logger.info("Dispatch Packaging Yes. Retrieved information from Customer Table");
						return unitDecisionMakingFromDispatchDelivery();
						}
					else
						return "failcustomer";
				} else {
					storage_in_dispatch_packaging = "no";
					allUnitQuestionAnswered = allUnitQuestionAnswered(paramMap);
					logger.info("Dispatch packaging no. Use form data to compute Storage Unit Decision");
					storage_unit_decision = unitDecision(paramMap);
				}
			}

		} else {

			allUnitQuestionAnswered = allUnitQuestionAnswered(paramMap);
			logger.info("Finished product. No Dispatch/Delivery question. Storage Decision made from form data");
			storage_unit_decision = unitDecision(paramMap);

		}
		return storage_unit_decision;

	}

	private boolean allUnitQuestionAnswered(Map<String, String[]> paramMap) {
		boolean first_decision = false;
		boolean second_decision = false;

		if (paramMap.containsKey("storage_packaging_type") && paramMap.containsKey("are_storage_unit_stackable")) {
			System.out.println("Both okay");
			if (paramMap.get("are_storage_unit_stackable")[0].equals("yes")) {
				if (paramMap.containsKey("storage_packaging_stacking_height")) {
					if (!paramMap.get("storage_packaging_stacking_height")[0].equals("")) {
						first_decision = true;
					}
				}
			} else {
				first_decision = true;
			}

			if (!paramMap.get("storage_packaging_type")[0].equals("unpacked_small")
					&& !paramMap.get("storage_packaging_type")[0].equals("unpacked_big")
					&& !paramMap.get("storage_packaging_type")[0].equals("unpacked_long")) {
				if (paramMap.containsKey("amount_of_items_per_storage_packaging")
						&& paramMap.containsKey("storage_packaging_length")
						&& paramMap.containsKey("storage_packaging_width")
						&& paramMap.containsKey("storage_packaging_height")) {
					if (!paramMap.get("amount_of_items_per_storage_packaging")[0].equals("")
							&& !paramMap.get("storage_packaging_length")[0].equals("")
							&& !paramMap.get("storage_packaging_width")[0].equals("")
							&& !paramMap.get("storage_packaging_height")[0].equals("")) {
						second_decision = true;
					}

				}
			} else {
				second_decision = true;
			}

		}
		if (first_decision == true && second_decision == true) {
			return true;
		} else {
			return false;
		}
	}

	public List<String> storageStrategyDecision(Map<String, String[]> paramMap) {

		List<String> posibilitiesOfStorageStrategy = new ArrayList<>();
		if (paramMap.containsKey("storage_bin_assignment") && paramMap.containsKey("storage_allocation")
				&& paramMap.containsKey("storage_removal_strategy")) {
			allStrategyQuestionAnswered = true;

			switch (paramMap.get("storage_bin_assignment")[0].toLowerCase()) {
			case "yes":
				storage_bin_assignment = "fixed";
				storageBinStrategy.add("fixed allocation of storage bin");
				break;
			case "no":
				storage_bin_assignment = "free";
				storageBinStrategy.add("free allocation of storage bin");
				break;
			default:
				storage_bin_assignment = "irrelevant";
				storageBinStrategy.add("irrelevant");
				break;
			}

			switch (paramMap.get("storage_allocation")[0].toLowerCase()) {
			case "yes":
				storage_allocation = "single material art";
				storageLocationStrategy.add("allocation with single material art");
				break;
			case "no":
				storage_allocation = "mixed material arts";
				storageLocationStrategy.add("allocation with mixed material arts");
				break;
			default:
				storage_allocation = "irrelevant";
				storageLocationStrategy.add("irrelevant");
				break;

			}

			switch (paramMap.get("storage_removal_strategy")[0].toLowerCase()) {
			case "fifo":
				storage_removal_strategy = "FIFO";
				storageRemovalStrategy.add("FIFO");
				break;
			case "lifo":
				storage_removal_strategy = "LIFO";
				storageRemovalStrategy.add("LIFO");
				break;
			default:
				storage_removal_strategy = "irrelevant";
				storageRemovalStrategy.add("irrelevant");
				break;
			}
		} else {
			if (paramMap.containsKey("storage_bin_assignment")) {
				storage_bin_assignment = paramMap.get("storage_bin_assignment")[0];
			}
			if (paramMap.containsKey("storage_allocation")) {
				storage_allocation = paramMap.get("storage_allocation")[0];
			}
			if (paramMap.containsKey("storage_removal_strategy")) {
				storage_removal_strategy = paramMap.get("storage_removal_strategy")[0];
			}
		}

		logger.info("Returned all selection as list of storage Strategy and miscellaneous");
		return posibilitiesOfStorageStrategy;

	}

	private void updateMaterial(String materialID, String storage_facility, String allInfoGiven) {
		startuplogDAOObject.storeMaterial_into_storagefacility(materialID, storage_facility, allInfoGiven,
				storage_in_delivery_packaging, storage_in_dispatch_packaging, storage_packaging_type,
				are_storage_unit_stackable, storage_packaging_stacking_height, amount_of_items_per_storage_packaging,
				material_weight_incl_storage_packaging, storage_packaging_length, storage_packaging_height,
				storage_packaging_width, storage_bin_assignment, storage_allocation, storage_removal_strategy,
				material_quantity, order_picking, mech_auto_possibility);

	}

	public void finalDecisionMaking(Map<String, String[]> paramMap, String decisionStorageUnit) {
		String materialID = paramMap.get("material_id")[0];
		String storage_facility = " ";
		if (paramMap.containsKey("material_quantity") && paramMap.containsKey("order_picking")
				&& paramMap.containsKey("mech_auto_possibility")) {
			material_quantity = paramMap.get("material_quantity")[0];
			order_picking = paramMap.get("order_picking")[0];
			mech_auto_possibility = paramMap.get("mech_auto_possibility")[0];
			if (allUnitQuestionAnswered == true && allStrategyQuestionAnswered == true) {

				int[][] decision = new int[7][13];
				for (int i = 0; i < 7; i++) {
					for (int j = 0; j < 13; j++) {
						decision[i][j] = 0;
					}
				}

				switch (decisionStorageUnit) {
				case "small article in bulk":
					decision[0][3] = 1;
					break;
				case "big article in bulk":
					decision[0][3] = 1;
					break;
				case "long objects (e.g. metal pole)":
					decision[0][6] = 1;
					decision[0][7] = 1;
					break;
				case "small parts in box":
					decision[0][3] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts in carton":
					for (int i = 0; i < 4; i++) {
						decision[0][i] = 1;
					}
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts in carton on pallets (stackable)":
					for (int i = 0; i < 3; i++) {
						decision[0][i] = 1;
					}
					decision[0][4] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts in carton on pallets (non-stackable)":
					for (int i = 1; i < 3; i++) {
						decision[0][i] = 1;
					}
					decision[0][4] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts on pallets (stackable)":
					for (int i = 0; i < 3; i++) {
						decision[0][i] = 1;
					}
					decision[0][4] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts on pallets (non-stackable)":
					for (int i = 1; i < 3; i++) {
						decision[0][i] = 1;
					}
					decision[0][4] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts in mesh box (stackable)":
					for (int i = 0; i < 3; i++) {
						decision[0][i] = 1;
					}
					decision[0][5] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts in mesh box (non-stackable)":
					for (int i = 1; i < 3; i++) {
						decision[0][i] = 1;
					}
					decision[0][5] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts in big container (stackable)":
					for (int i = 0; i < 3; i++) {
						decision[0][i] = 1;
					}
					decision[0][5] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts in big container (non-stackable)":
					for (int i = 1; i < 3; i++) {
						decision[0][i] = 1;
					}
					decision[0][5] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts in small container":
					decision[0][3] = 1;
					decision[0][5] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts in small container on pallets (stackable)":
					for (int i = 0; i < 3; i++) {
						decision[0][i] = 1;
					}
					decision[0][4] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;
				case "parts in small container on pallets (non-stackable)":
					decision[0][1] = 1;
					decision[0][2] = 1;
					decision[0][4] = 1;
					for (int i = 8; i < 13; i++) {
						decision[0][i] = 1;
					}
					break;

				}
				for (String strategy : storageBinStrategy) {
					switch (strategy) {
					case "fixed allocation of storage bin":
						for (int i = 0; i < 13; i++) {
							decision[1][i] = 1;
						}
						break;
					case "free allocation of storage bin":
						System.out.println(strategy);
						for (int i = 3; i < 6; i++) {
							decision[1][i] = 1;
						}
						break;
					default:
						for (int i = 0; i < 13; i++) {
							decision[1][i] = 1;
						}
						break;
					}
				}
				for (String strategy : storageLocationStrategy) {
					switch (strategy) {
					case "allocation with single material art":

						for (int i = 0; i < 13; i++) {
							decision[2][i] = 1;
						}
						break;
					case "allocation with mixed material arts":

						for (int i = 3; i < 4; i++) {
							decision[2][i] = 1;
						}
						break;
					default:

						for (int i = 0; i < 13; i++) {
							decision[2][i] = 1;
						}
						break;
					}
				}
				for (String strategy : storageRemovalStrategy) {
					switch (strategy) {
					case "FIFO":

						decision[3][0] = 1;
						for (int i = 2; i < 11; i++) {
							decision[3][i] = 1;
						}
						break;
					case "LIFO":

						decision[3][0] = 1;
						decision[3][1] = 1;
						decision[3][11] = 1;
						decision[3][12] = 1;
						for (int i = 3; i < 8; i++) {
							decision[3][i] = 1;
						}
						break;

					default:

						for (int i = 0; i < 13; i++) {
							decision[3][i] = 1;
						}
						break;
					}
				}

				switch (paramMap.get("material_quantity")[0]) {

				case "high quantity":

					decision[2][0] = 1;
					for (int i = 6; i < 13; i++) {
						decision[4][i] = 1;
					}
					break;
				case "medium-sized quantity":

					for (int i = 0; i < 4; i++) {
						decision[4][i] = 1;
					}
					for (int i = 6; i < 13; i++) {
						decision[4][i] = 1;
					}
					break;
				case "low quantity":

					decision[4][0] = 1;
					for (int i = 3; i < 8; i++) {
						decision[4][i] = 1;
					}
					break;
				}

				switch (paramMap.get("order_picking")[0]) {
				case "yes":

					decision[5][3] = 1;
					decision[5][4] = 1;
					decision[5][5] = 1;
					decision[5][8] = 1;
					decision[5][9] = 1;
					break;
				case "no":

					for (int i = 0; i < 13; i++) {
						decision[5][i] = 1;
					}
					break;
				}
				switch (paramMap.get("mech_auto_possibility")[0]) {
				case "yes":

					decision[6][3] = 1;
					decision[6][4] = 1;
					decision[6][5] = 1;
					decision[6][9] = 1;
					decision[6][10] = 1;
					break;
				case "no":

					for (int i = 0; i < 13; i++) {
						decision[6][i] = 1;
					}
					break;
				case "it depends on.":

					for (int i = 3; i < 8; i++) {
						decision[6][i] = 1;
					}
					decision[6][9] = 1;
					decision[6][10] = 1;
					decision[6][12] = 1;
					break;

				}
				List<String> allFinalPosibilities = new ArrayList<>();
				allFinalPosibilities.add("decision.Block_storage");
				allFinalPosibilities.add("decision.Drive-in");
				allFinalPosibilities.add("decision.Drive-through");
				allFinalPosibilities.add("decision.modular_racking");
				allFinalPosibilities.add("decision.Pallets_racking");
				allFinalPosibilities.add("decision.Container_racking");
				allFinalPosibilities.add("decision.Cantilever_racking_without");
				allFinalPosibilities.add("decision.Cantilever_racking");
				allFinalPosibilities.add("decision.flow_racking_without");
				allFinalPosibilities.add("decision.flow_racking");
				allFinalPosibilities.add("decision.flow_racking_powered");
				allFinalPosibilities.add("decision.push_back_without ");
				allFinalPosibilities.add("decision.push_back");

				for (int j = 0; j < 13; j++) {
					boolean allColumnCrossed = true;
					for (int i = 0; i < 7; i++) {
						if (decision[i][j] == 0) {
							allColumnCrossed = false;
						}

					}
					if (allColumnCrossed == true) {
						if (storage_facility.equals(" ")) {
							storage_facility = allFinalPosibilities.get(j);
						} else {
							storage_facility = storage_facility + ", " + allFinalPosibilities.get(j);
						}
					}
				}
				
				updateMaterial(materialID, storage_facility, "true");
				logger.info("Material Information updated with all info given true");
				return;
			}
		}
		if (paramMap.containsKey("material_quantity")) {
			material_quantity = paramMap.get("material_quantity")[0];

		}
		if (paramMap.containsKey("order_picking")) {
			order_picking = paramMap.get("order_picking")[0];
		}
		if (paramMap.containsKey("mech_auto_possibility")) {
			mech_auto_possibility = paramMap.get("mech_auto_possibility")[0];
		}
		updateMaterial(materialID, storage_facility, "false");
		logger.info("Material Information updated with all info given false");
	}

	public String getMaterialFromStorageFacilityAsJson(String materialID)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		StorageFacility material_in_storagefacility = startuplogDAOObject.get_material_from_storagefacility(materialID);
		JSONObject object = new JSONObject();
		if (material_in_storagefacility != null) {
			Class storageFacility = StorageFacility.class;
			for (Method m : storageFacility.getMethods()) {
				if (m.getName().startsWith("get") && !m.getName().equals("getClass")) {
					Object return_value = m.invoke(material_in_storagefacility);
					if (m.invoke(material_in_storagefacility) == null) {
						object.put(m.getName().substring(3).toLowerCase(), "null");
					} else {
						String return_value_string = return_value.toString();

						switch (return_value_string) {
						case "fixed":
							return_value_string = "yes";
							break;
						case "free":
							return_value_string = "no";
							break;
						case "single material art":
							return_value_string = "yes";
						case "mixed material arts":
							return_value_string = "no";
							break;
						case "FIFO":
							return_value_string = "fifo";
							break;
						case "LIFO":
							return_value_string = "lifo";
							break;
						}

						object.put(m.getName().substring(3).toLowerCase(), return_value_string);
					}
				}

			}
		} else {
			logger.error("Material with ID" + materialID + " not avaiable in the StorageFacility Table");
		}
		return object.toJSONString();

	}

	private boolean check_material_in_suppliertable(String materialID) {
		Supplier material_in_supplier = startuplogDAOObject.getSupplier(materialID);

		if (material_in_supplier != null)
			return true;
		return false;
	}

	private boolean check_material_in_customertable(String materialID) {
		Customer material_in_customer = startuplogDAOObject.getCustomer(materialID);
		if (material_in_customer != null)
			return true;
		return false;
	} 

	public boolean checkMaterial_for_packaging(String materialID) {
		Material material = startuplogDAOObject.getMaterial(materialID);
		if (material == null){
			logger.error("Could not retrieve information of material ID " + materialID);
			return false;
		}
		String materialType = material.getMaterial_type();
		if (materialType.equals("finished product")) {
			return check_material_in_customertable(materialID);
		} else if (materialType.equals("raw material")) {

			return check_material_in_suppliertable(materialID);
		}
		return false;
	}

	private String translet_to_locale(String material_name) {
		return messageSource.getMessage(material_name, null, LocaleContextHolder.getLocale());
	}

}
