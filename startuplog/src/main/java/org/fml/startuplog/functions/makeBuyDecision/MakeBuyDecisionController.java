package org.fml.startuplog.functions.makeBuyDecision;

import java.util.List;

import org.apache.log4j.Logger;
import org.fml.startuplog.exception.NoDecisonsFetchedException;
import org.fml.startuplog.exception.NoMaterialsFetchedException;
import org.fml.startuplog.model.ABCAnalysis;
import org.fml.startuplog.model.MakeBuyDecision;
import org.fml.startuplog.model.Material;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import javax.servlet.http.HttpSession;


/**
 * @author jjohn
 *
 */
@Controller
@SessionAttributes("user")
public class MakeBuyDecisionController {

	@Autowired
	private MakeBuyDecisionService makeBuyDecisionService;
	private List<MakeBuyDecision> makeBuyDecisions;
	private final Logger logger = Logger.getLogger(this.getClass());

//	/*
//	 * Handle decision page display, irrespective of the page type param that is
//	 * set. Returns makeBuyDecision.jsp
//	 */
//	@RequestMapping(value = "/makeBuyDecision", method = RequestMethod.POST)
//	public String displayDecisionPage(HttpSession session, ModelMap model, @ModelAttribute("user") String user) {
//
//		return "makeBuyDecision";
//	
//		
//	}

	/*
	 * Handle the pageType display based on the params passed from stages.jsp.
	 * Returns /makeBuyDecision
	 */
	@RequestMapping(value = "/makeBuyDecisionRouter", params = "seed", method = RequestMethod.POST)
	public String decideSeedStage(@ModelAttribute("user") String user, ModelMap model, SessionStatus sessionStatus, HttpSession session) {
		session.setAttribute("selectedStage", "seed");
		System.out.println("Hallo from redirect");

		makeBuyDecisionService.saveStage("Seed Stage", user);
		model.put("makeBuyType", "typeA");
		return "/makeBuyDecision";
	}

	/**
	 * Triggered when Startup:Conception stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeBuyDecisionRouter", params = "startup1", method = RequestMethod.POST)
	public String startup1(ModelMap model, @ModelAttribute("user") String user, HttpSession session) {
		session.setAttribute("selectedStage", "startup1");
		System.out.println("Hallo from redirect");

		makeBuyDecisionService.saveStage("Startup:Conception", user);
		model.put("makeBuyType", "typeA");
		logger.info("Startup:Conception Saved");
		return "makeBuyDecision";

	}

	/**
	 * Triggered when Startup:Construction stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeBuyDecisionRouter", params = "startup2", method = RequestMethod.POST)
	public String startup2(ModelMap model, @ModelAttribute("user") String user, SessionStatus sessionStatus, HttpSession session) {
		System.out.println("Hallo from redirect");

		// currentStage = "Startup:Construction";
		session.setAttribute("selectedStage", "startup2");

		makeBuyDecisionService.saveStage("Startup:Construction", user);
		model.put("makeBuyType", "typeA");
		logger.info("Startup:Conception Saved");
		return "makeBuyDecision";

	}

	/**
	 * Triggered when Startup:Elaboration stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeBuyDecisionRouter", params = "startup3", method = RequestMethod.POST)
	public String startup3(ModelMap model, @ModelAttribute("user") String user, SessionStatus sessionStatus, HttpSession session) {

		// currentStage = "Startup:Elaboration";
		session.setAttribute("selectedStage", "startup3");

		makeBuyDecisionService.saveStage("Startup:Elaboration", user);
		model.put("makeBuyType", "typeA");
		logger.info("Startup:Elaboration Saved");
		return "makeBuyDecision";
	}

	/**
	 * Triggered when Startup:Prototyping stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeBuyDecisionRouter", params = "startup4", method = RequestMethod.POST)
	public String startup4(ModelMap model, @ModelAttribute("user") String user, SessionStatus sessionStatus, HttpSession session) {
		System.out.println("Hallo from redirect");

		// currentStage = "Startup:Prototyping";
		System.out.println("Hallo from redirect");
		session.setAttribute("selectedStage", "startup4");

		makeBuyDecisionService.saveStage("Startup:Prototyping", user);
		model.put("makeBuyType", "typeA");
		logger.info("Startup:Prototyping Saved");
		return "makeBuyDecision";
	}

	/**
	 * Triggered when First stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeBuyDecisionRouter", params = "expansion")
	public String first(ModelMap model, @ModelAttribute("user") String user, SessionStatus sessionStatus, HttpSession session) {
		// currentStage = "First Stage";
		
		session.setAttribute("selectedStage", "expansion");
		System.out.println("Hallo from redirect");

		makeBuyDecisionService.saveStage("First Stage", user);
		model.put("makeBuyType", "typeB");
		logger.info("First Stage Saved");

		return"redirect:makeBuyDecision";

	}

	/**
	 * Triggered when Second stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeBuyDecisionRouter", params = "later", method = RequestMethod.POST)
	public String second(ModelMap model, @ModelAttribute("user") String user, HttpSession session) {
		// currentStage = "Second Stage";
		System.out.println("Hallo from redirect");

		session.setAttribute("selectedStage", "later");

		makeBuyDecisionService.saveStage("First Stage", user);
		model.put("makeBuyType", "typeB");
		logger.info("First Stage Saved");

		return"redirect:makeBuyDecision";
	}
	
	@RequestMapping(value = "/makeBuyDecision")
	public String routeMakeBuyDecision(ModelMap model, @ModelAttribute("user") String user, HttpSession session) {
		// currentStage = "Second Stage";
		String selectedStage = (String)session.getAttribute("selectedStage");

		System.out.println("Selected Stage Outer: " + selectedStage);
		if("expansion".equalsIgnoreCase(selectedStage)||"later".equalsIgnoreCase(selectedStage)){
			System.out.println("Selected Stage Inner: " + selectedStage);
			//makeBuyDecisionService.saveStage("First Stage", user);
			model.put("makeBuyType", "typeB");
			try {			
				makeBuyDecisions = makeBuyDecisionService.fetchExistingMakeBuyDecisions(user, selectedStage);
				
				if(!makeBuyDecisions.isEmpty()){
					model.put("makeBuyDecisionList", makeBuyDecisions);
					}
				else{
					throw new NoDecisonsFetchedException();
				}
				
			}catch (NoDecisonsFetchedException e) {
				model.put("errormsg", e.getMessage());
			}
			return "makeBuyDecision";
		}
		
		else{
			//makeBuyDecisionService.saveStage("First Stage", user);
			model.put("makeBuyType", "typeA");
			return "makeBuyDecision";
		}

	}

	@RequestMapping(value = "/makeBuyDecisionRouter", params = "typeBSave", method = RequestMethod.POST)
	public String saveProductDecisionTypeB(HttpSession session, ModelMap model, RedirectAttributes attributes, @ModelAttribute("user") String user, @RequestParam String productName, @RequestParam String prototype, String seriesProd, String storageDispatch) {
		// makebuyService.saveStage("Second Stage", user);
		String selectedStage = (String)session.getAttribute("selectedStage");

		makeBuyDecisionService.saveProductDecision(user, productName, prototype, seriesProd, storageDispatch, selectedStage);
		logger.info("Save product decision for Stage 6 or 7, Type B Decision");
		//model.put("makeBuyType", "typeB");
		//session.setAttribute("makeBuyStage", "typeBSave");
		//attributes.addAttribute("expansion","");
		return "redirect:makeBuyDecision";
	}
	
	@RequestMapping(value = "/makeBuyDecisionRouter", params = "TypeAcontinue", method = RequestMethod.POST)
	public String saveProductDecisionTypeA(HttpSession session, ModelMap model, RedirectAttributes attributes, @ModelAttribute("user") String user, @RequestParam String prototypeA, String seriesProdA, String storageDispatchA) {
		// makebuyService.saveStage("Second Stage", user);
		String selectedStage = (String)session.getAttribute("selectedStage");

		makeBuyDecisionService.saveProductDecision(user, "TypeA", prototypeA, seriesProdA, storageDispatchA, selectedStage);
		logger.info("Save product decision for Stage 6 or 7, Type B Decision");
		session.setAttribute("variantType", "typeACaseA");
		return "redirect:function";
	}

	@RequestMapping(value = "/makeBuyDecisionRouter", params = "support2", method = RequestMethod.POST)
	public String requestDecisionSupport(ModelMap model, @ModelAttribute("user") String user) {
		// makebuyService.saveStage("Second Stage", user);
		logger.info("Directed to the logistics page.");
		return "redirect:makeorbuy";
	}
	
	@RequestMapping(value = "/makeBuyDecisionRouter", params = "continue", method = RequestMethod.POST)
	public String continueToLogisticsTasks(ModelMap model, @ModelAttribute("user") String user, HttpSession session) {
		// makebuyService.saveStage("Second Stage", user);
		session.setAttribute("makeBuyStage", "typeBSave");
		logger.info("Directed to the logistics page.");
		return "redirect:function";
	}
	

	@ExceptionHandler(HttpSessionRequiredException.class)
	public String notLoggedIn() {
		return "redirect:login";
	}
}
