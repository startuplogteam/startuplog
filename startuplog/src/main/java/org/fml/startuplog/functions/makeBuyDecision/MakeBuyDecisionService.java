package org.fml.startuplog.functions.makeBuyDecision;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.fml.startuplog.model.MakeBuyDecision;
import org.fml.startuplog.service.MasterService;
import org.springframework.stereotype.Service;

@Service
public class MakeBuyDecisionService extends MasterService {

	//private EntityManagerFactory emf = Persistence.createEntityManagerFactory("startuplog");
	private final Logger logger = Logger.getLogger(this.getClass());

	public List<MakeBuyDecision> fetchExistingMakeBuyDecisions(String user, String stage) {
		return startuplogDAOObject.fetchExistingMakeBuyDecisions(user, stage);
	}

	public void saveStage(String stage, String username) {
		try {
			startuplogDAOObject.savePhase(stage, username);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void saveProductDecision(String userName, String productName, String prototype, String seriesProd, String storageDispatch, String stage) {
		try {
			startuplogDAOObject.saveMakeBuyProductDecision(userName, productName, prototype, seriesProd, storageDispatch, stage);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void saveDecisionVariant(String variant, String username){
		try {
			variant = String.valueOf(Integer.parseInt(variant) -1);
			startuplogDAOObject.saveVariant(variant, username);
			logger.info("Variant "+ variant + " Saved");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
	}
	

}
