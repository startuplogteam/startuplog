package org.fml.startuplog.functions.makeorBuyDecisionComponent;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class MakeOrBuyDecisionComponentController {

	@RequestMapping(value = "/makebuycomponent", method = RequestMethod.GET)
	public String preLoadMaterials(ModelMap model, @ModelAttribute("user") String user) {
		
		return "makeOrBuyDecisionComponent";
	}

	@ExceptionHandler (HttpSessionRequiredException.class)
	public String notLoggedIn(){
		return "redirect:login";
	}
}
