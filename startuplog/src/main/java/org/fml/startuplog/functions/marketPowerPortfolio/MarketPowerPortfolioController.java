package org.fml.startuplog.functions.marketPowerPortfolio;

import java.util.List;

import org.apache.log4j.Logger;
import org.fml.startuplog.dao.StartuplogDAO;
import org.fml.startuplog.model.MarketPowerPortfolio;
import org.fml.startuplog.model.MaterialClassification;
import org.fml.startuplog.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class MarketPowerPortfolioController {

	@Autowired
	private StartuplogDAO startuplogDAOService;
	@Autowired
	private MarketPowerPortfolioService marketPowerPortfolioService;

	private final Logger logger = Logger.getLogger(this.getClass());

	@RequestMapping(value = "/marketpowerportfolio", method = RequestMethod.GET)
	public String invokeMarketPowerPortfolio(ModelMap model, @ModelAttribute("user") String user) {
		List<Supplier> suppliers = startuplogDAOService.getSuppliers();
		if (suppliers == null) {
			logger.error("No suppliers found");
			model.put("errormsg", "No suppliers found");
		} else {
			model.put("supplierNames", suppliers);
			logger.info("invokeMarketPowerPortfolio");
		}

		return "marketPowerPortfolio";
	}

	@RequestMapping(value = "/marketpowerportfolio", method = RequestMethod.POST)
	@ResponseBody
	public String calculateAndSaveClassification(@ModelAttribute("user") String user, @RequestParam String material_id,
			@RequestParam String supply_risk, @RequestParam String further_criteria) {

		System.out.println("material_id"+material_id + "supplier_dependency"+supply_risk+"further_value"+further_criteria);
		String classification = marketPowerPortfolioService.getClassification(supply_risk, further_criteria);
		logger.info("Market power portfolio classification is : " + classification);

		marketPowerPortfolioService.saveClassification(user, material_id, supply_risk,
				further_criteria, classification);
		logger.info("Classification successfully saved for material with ID " + material_id);
		return classification;
	}

	@RequestMapping(value = "/previousMarketPortfolio", method = RequestMethod.GET)
	@ResponseBody
	public MarketPowerPortfolio previousclassificationdecision(@ModelAttribute("user") String user,
			@RequestParam String material_id) {

		System.out.println("Username is :"+user+ " and id is : "+material_id);
		MarketPowerPortfolio marketPowerPortfolio = marketPowerPortfolioService
				.getExistingPortfolio(material_id, user);
		if (marketPowerPortfolio == null) {
			logger.info("No information of Supplier ID " + material_id + " in the Market Portfolio table");
		} else {
			logger.info("Information retrieve of Supplier ID " + material_id + " from Market Portfolio table");
		}

		return marketPowerPortfolio;
	}

	@ExceptionHandler(HttpSessionRequiredException.class)
	public String notLoggedIn() {
		return "redirect:login";
	}
}
