package org.fml.startuplog.functions.marketPowerPortfolio;

import org.fml.startuplog.model.MarketPowerPortfolio;
import org.fml.startuplog.service.MasterService;
import org.springframework.stereotype.Service;

@Service
public class MarketPowerPortfolioService extends MasterService{
	
	public String getClassification(String supply_risk, String further_value) {
		if (supply_risk.equals("low") && further_value.equals("low"))
			return "upperleft";
		else if (supply_risk.equals("low") && further_value.equals("high"))
			return "upperright";
		else if (supply_risk.equals("high") && further_value.equals("low"))
			return "lowerleft";
		else
			return "lowerright";
	}
	
	public void saveClassification (String user, String supplierID, String supply_risk, String further_criteria_value, String classification)
	{
		startuplogDAOObject.saveMarketPowerPortfolio(user, supplierID, supply_risk, further_criteria_value, classification);
	}
	
	public MarketPowerPortfolio getExistingPortfolio(String supplierID,String user)
	{
		return startuplogDAOObject.getExistingPortfolio(supplierID,user);
	}
}
