package org.fml.startuplog.functions.materialclassification;

import java.util.List;

import org.apache.log4j.Logger;
import org.fml.startuplog.dao.StartuplogDAO;
import org.fml.startuplog.model.Material;
import org.fml.startuplog.model.MaterialClassification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class MaterialClassificationController {

	@Autowired
	private StartuplogDAO startuplogDAOService;
	@Autowired
	private MaterialClassificationService materialClassificationService;

	private final Logger logger = Logger.getLogger(this.getClass());

	@RequestMapping(value = "/materialclassification", method = RequestMethod.GET)
	public String preMaterialClassification(ModelMap model, @ModelAttribute("user") String user) {
		List<Material> materials = startuplogDAOService.getAllMaterial_for_procurement();
		if (materials == null) {
			logger.error("NULL returned as material list");
			model.put("errormsg", "NULL returned as material list");
		}
		model.put("materialdetails", materials);
		logger.info("Supply risk is  Invoked");
		return "materialClassification";
	}

	@RequestMapping(value = "/materialclassification", method = RequestMethod.POST)
	@ResponseBody
	public String calculateAndSaveClassification(@ModelAttribute("user") String user, @RequestParam String material_id, @RequestParam String supply_risk,
			@RequestParam String further_criteria, @RequestParam String further_value) {

		String classification = materialClassificationService.getClassification(supply_risk, further_value);
		logger.info("Computed classification "+ classification);
		
		materialClassificationService.saveClassification(user, material_id, supply_risk, further_criteria, further_value,
				classification);
		logger.info("Classification successfully saved for material with ID " + material_id);
		return classification;
	}

	@RequestMapping(value = "/previousclassificationdecision", method = RequestMethod.GET)
	@ResponseBody
	public MaterialClassification previousclassificationdecision(@ModelAttribute("user") String user, @RequestParam String material_id) {

		MaterialClassification materialClassification = materialClassificationService.retrievePreviousClassification(material_id,user); 
		if (materialClassification == null)
		{
			logger.info("No information of material ID " + material_id +" in the material classification table");
		}
		else
		{
			logger.info("Information retrieve of material ID " + material_id +" from material classification table");
		}
		
		return  materialClassification;
	}
	
	@ExceptionHandler (HttpSessionRequiredException.class)
	public String notLoggedIn(){
		return "redirect:login";
	}
}
