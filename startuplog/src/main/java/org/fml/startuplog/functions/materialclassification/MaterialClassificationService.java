package org.fml.startuplog.functions.materialclassification;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.fml.startuplog.dao.StartuplogDAO;
import org.fml.startuplog.model.Material;
import org.fml.startuplog.model.MaterialClassification;
import org.fml.startuplog.service.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MaterialClassificationService extends MasterService{
	
	public String getClassification(String supply_risk, String further_value) {
		if (supply_risk.equals("high") && further_value.equals("high"))
			return "upperright";
		else if (supply_risk.equals("high") && further_value.equals("low"))
			return "upperleft";
		else if (supply_risk.equals("low") && further_value.equals("low"))
			return "lowerleft";
		else
			return "lowerright";
	}
	
	public void saveClassification (String user, String materialID, String supply_risk, String further_criteria, String further_criteria_value, String classification)
	{
		startuplogDAOObject.saveToMaterialClassification(user, materialID, supply_risk, further_criteria, further_criteria_value, classification);
	}
	
	public MaterialClassification retrievePreviousClassification(String material_id,String user)
	{
		return startuplogDAOObject.retrieveInformationFromMaterialClassification(material_id,user);
	}
}
