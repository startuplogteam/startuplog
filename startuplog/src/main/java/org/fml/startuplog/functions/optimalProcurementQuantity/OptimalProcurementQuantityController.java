package org.fml.startuplog.functions.optimalProcurementQuantity;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class OptimalProcurementQuantityController {

	@RequestMapping(value = "/optimalprocurementquantity", method = RequestMethod.GET)
	public String preLoadMaterials(ModelMap model, @ModelAttribute("user") String user) {
		
		return "optimalProcurementQuantity";
	}

	@ExceptionHandler (HttpSessionRequiredException.class)
	public String notLoggedIn(){
		return "redirect:login";
	}
}
