package org.fml.startuplog.functions.procurement;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.fml.startuplog.dao.StartuplogDAO;
import org.fml.startuplog.functions.initialplanning.InitialplanningService;
import org.fml.startuplog.model.Material;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class ProcurementController {

	@Autowired
	private ProcurementService prService;

	@Autowired
	private StartuplogDAO startuplogDAOService;

	private final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Show procurement.jsp Saves list of available materials into modelmap to
	 * be accessible in the view
	 */
	@RequestMapping(value = "/procurement", method = RequestMethod.GET)
	public String procurementDisplay(ModelMap model, @ModelAttribute("user") String user) {
		List<Material> materialList = startuplogDAOService.getAllMaterial_for_procurement();
		if (materialList == null) {
			logger.error("NULL returned as material List");
		}
		model.put("materialList", materialList);
		logger.info("Procurement Invoked");
		return "procurement";
	}

	/**
	 * Returns previously saved procurement suggestions and question answer
	 */
	@RequestMapping(value = "/getPreviousProcurement", method = RequestMethod.POST)
	@ResponseBody
	public String getPreviousProcurement(@RequestParam String material_id) {
		String previous_material_info_in_procurement = prService.getProcurementJsonFormat(material_id);
		if (previous_material_info_in_procurement.equals("{}")) {
			logger.info("Returned {} as previous procurement data for material ID " + material_id);
		} else {
			logger.info("Retrieved previous data from Procurement table of material ID " + material_id);
		}
		return previous_material_info_in_procurement;
	}

	/**
	 * Save procurement suggestions and question answer into DB
	 */
	@RequestMapping(value = "/saveProcurement", method = RequestMethod.POST)
	public String saveProcurement(HttpServletRequest request) {

		Map<String, String[]> paramMap = request.getParameterMap();
		int[][] supplier_number_strategy = new int[21][2];
		int[][] market_field_strategy = new int[21][2];
		int[][] supply_strategy = new int[21][3];

		String market_research_result = null, product_feature = null, predictability_procurement = null,
				flexibility = null, complexity = null, subcription_price = null, order_and_transaction_costs = null,
				transport_costs = null, storage_and_capital_costs = null, procurement_time = null,
				risk_by_supplier_failure = null, transport_risk = null, currency_risk = null, exchange_rate_risk = null,
				customs_problem_risk = null, dependence_on_supplier = null, number_of_suppliers_and_contacts = null,
				expenditure_on_bureaucracy = null, language = null, currency = null, exchange_rate_utilization = null;
		if (paramMap.containsKey("market_research_result")) {
			market_research_result = paramMap.get("market_research_result")[0];
			switch (market_research_result) {
			case "one_supplier":
				supplier_number_strategy[0][0] = 1;

				market_field_strategy[0][0] = 1;
				market_field_strategy[0][1] = 1;

				supply_strategy[0][0] = 1;
				supply_strategy[0][1] = 1;
				supply_strategy[0][2] = 1;
				break;
			case "several_suppliers":
				supplier_number_strategy[0][0] = 1;
				supplier_number_strategy[0][1] = 1;

				market_field_strategy[0][0] = 1;
				market_field_strategy[0][1] = 1;

				supply_strategy[0][0] = 1;
				supply_strategy[0][1] = 1;
				supply_strategy[0][2] = 1;
				break;
			case "suppliers_own_country":
				supplier_number_strategy[0][0] = 1;
				supplier_number_strategy[0][1] = 1;

				market_field_strategy[0][0] = 1;

				supply_strategy[0][0] = 1;
				supply_strategy[0][1] = 1;
				supply_strategy[0][2] = 1;
				break;
			case "suppliers_other_countries":
				supplier_number_strategy[0][0] = 1;
				supplier_number_strategy[0][1] = 1;

				market_field_strategy[0][1] = 1;
				supply_strategy[0][0] = 1;
				supply_strategy[0][1] = 1;

				break;
			case "suppliers_own_and_other_countries":
				supplier_number_strategy[0][0] = 1;
				supplier_number_strategy[0][1] = 1;

				market_field_strategy[0][0] = 1;
				market_field_strategy[0][1] = 1;

				supply_strategy[0][0] = 1;
				supply_strategy[0][1] = 1;
				supply_strategy[0][2] = 1;
				break;

			}
		}
		if (paramMap.containsKey("product_feature")) {
			product_feature = paramMap.get("product_feature")[0];
			switch (product_feature) {
			case "product_complex":
				supplier_number_strategy[1][0] = 1;

				market_field_strategy[1][0] = 1;
				market_field_strategy[1][1] = 1;

				supply_strategy[1][0] = 1;
				supply_strategy[1][1] = 1;
				supply_strategy[1][2] = 1;
				break;
			case "product_development":
				supplier_number_strategy[1][0] = 1;

				market_field_strategy[1][0] = 1;
				market_field_strategy[1][1] = 1;

				supply_strategy[1][0] = 1;
				supply_strategy[1][1] = 1;
				supply_strategy[1][2] = 1;
				break;
			case "product_customized":
				supplier_number_strategy[1][0] = 1;
				supplier_number_strategy[1][1] = 1;

				market_field_strategy[1][0] = 1;

				supply_strategy[1][2] = 1;
				break;
			case "product_few_specs":

				supplier_number_strategy[1][0] = 1;
				supplier_number_strategy[1][1] = 1;

				market_field_strategy[1][0] = 1;
				market_field_strategy[1][1] = 1;

				supply_strategy[1][0] = 1;
				supply_strategy[1][1] = 1;
				supply_strategy[1][2] = 1;

				break;
			case "product_standardized":

				supplier_number_strategy[1][0] = 1;
				supplier_number_strategy[1][1] = 1;

				market_field_strategy[1][0] = 1;
				market_field_strategy[1][1] = 1;

				supply_strategy[1][0] = 1;
				supply_strategy[1][1] = 1;
				supply_strategy[1][2] = 1;
				break;

			}
		}

		if (paramMap.containsKey("predictability_procurement")) {
			predictability_procurement = paramMap.get("predictability_procurement")[0];
			switch (predictability_procurement) {
			case "less":
				supplier_number_strategy[2][0] = 1;
				supplier_number_strategy[2][1] = 1;

				market_field_strategy[2][0] = 1;
				market_field_strategy[2][1] = 1;

				supply_strategy[2][0] = 1;

				supply_strategy[2][2] = 1;
				break;
			case "well":
				supplier_number_strategy[2][0] = 1;
				supplier_number_strategy[2][1] = 1;

				market_field_strategy[2][0] = 1;
				market_field_strategy[2][1] = 1;

				supply_strategy[2][1] = 1;

				break;

			}
		}
		if (paramMap.containsKey("flexibility")) {

			flexibility = paramMap.get("flexibility")[0];
			switch (flexibility) {
			case "high":

				supplier_number_strategy[3][1] = 1;

				market_field_strategy[3][0] = 1;

				supply_strategy[3][0] = 1;

				break;
			case "low":
				supplier_number_strategy[3][0] = 1;

				market_field_strategy[3][1] = 1;

				supply_strategy[3][1] = 1;
				supply_strategy[3][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[3][0] = 1;
				supplier_number_strategy[2][1] = 1;

				market_field_strategy[3][0] = 1;
				market_field_strategy[3][1] = 1;

				supply_strategy[3][0] = 1;
				supply_strategy[3][1] = 1;
				supply_strategy[3][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("complexity")) {
			complexity = paramMap.get("complexity")[0];
			switch (complexity) {
			case "high":

				supplier_number_strategy[4][1] = 1;

				market_field_strategy[4][1] = 1;

				supply_strategy[4][1] = 1;
				supply_strategy[4][2] = 1;

				break;
			case "low":
				supplier_number_strategy[4][0] = 1;

				market_field_strategy[4][0] = 1;

				supply_strategy[4][0] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[4][0] = 1;
				supplier_number_strategy[4][1] = 1;

				market_field_strategy[4][0] = 1;
				market_field_strategy[4][1] = 1;

				supply_strategy[4][0] = 1;
				supply_strategy[4][1] = 1;
				supply_strategy[4][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("subcription_price")) {
			subcription_price = paramMap.get("subcription_price")[0];
			switch (subcription_price) {
			case "high":

				supplier_number_strategy[5][1] = 1;

				market_field_strategy[5][0] = 1;

				supply_strategy[5][0] = 1;
				supply_strategy[5][1] = 1;
				supply_strategy[5][2] = 1;

				break;
			case "low":
				supplier_number_strategy[5][0] = 1;

				market_field_strategy[5][1] = 1;

				supply_strategy[5][0] = 1;
				supply_strategy[5][1] = 1;
				supply_strategy[5][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[5][0] = 1;
				supplier_number_strategy[5][1] = 1;

				market_field_strategy[5][0] = 1;
				market_field_strategy[5][1] = 1;

				supply_strategy[5][0] = 1;
				supply_strategy[5][1] = 1;
				supply_strategy[5][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("order_and_transaction_costs")) {
			order_and_transaction_costs = paramMap.get("order_and_transaction_costs")[0];
			switch (order_and_transaction_costs) {
			case "high":

				supplier_number_strategy[6][1] = 1;

				market_field_strategy[6][0] = 1;
				market_field_strategy[6][1] = 1;

				supply_strategy[6][0] = 1;
				supply_strategy[6][1] = 1;
				supply_strategy[6][2] = 1;

				break;
			case "low":
				supplier_number_strategy[6][0] = 1;

				market_field_strategy[6][1] = 1;
				market_field_strategy[6][0] = 1;

				supply_strategy[6][0] = 1;
				supply_strategy[6][1] = 1;
				supply_strategy[6][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[6][0] = 1;
				supplier_number_strategy[6][1] = 1;

				market_field_strategy[6][0] = 1;
				market_field_strategy[6][1] = 1;

				supply_strategy[6][0] = 1;
				supply_strategy[6][1] = 1;
				supply_strategy[6][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("transport_costs")) {
			transport_costs = paramMap.get("transport_costs")[0];
			switch (transport_costs) {
			case "high":

				supplier_number_strategy[7][1] = 1;

				market_field_strategy[7][0] = 1;

				supply_strategy[7][1] = 1;
				supply_strategy[7][2] = 1;

				break;
			case "low":
				supplier_number_strategy[7][0] = 1;

				market_field_strategy[7][1] = 1;

				supply_strategy[7][0] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[7][0] = 1;
				supplier_number_strategy[7][1] = 1;

				market_field_strategy[7][0] = 1;
				market_field_strategy[7][1] = 1;

				supply_strategy[7][0] = 1;
				supply_strategy[7][1] = 1;
				supply_strategy[7][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("storage_and_capital_costs")) {
			storage_and_capital_costs = paramMap.get("storage_and_capital_costs")[0];
			switch (storage_and_capital_costs) {
			case "high":

				supplier_number_strategy[8][0] = 1;
				supplier_number_strategy[8][1] = 1;

				market_field_strategy[8][0] = 1;
				market_field_strategy[8][1] = 1;

				supply_strategy[8][0] = 1;

				break;
			case "low":
				supplier_number_strategy[8][0] = 1;
				supplier_number_strategy[8][1] = 1;

				market_field_strategy[8][0] = 1;
				market_field_strategy[8][1] = 1;

				supply_strategy[8][1] = 1;
				supply_strategy[8][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[8][0] = 1;
				supplier_number_strategy[8][1] = 1;

				market_field_strategy[8][0] = 1;
				market_field_strategy[8][1] = 1;

				supply_strategy[8][0] = 1;
				supply_strategy[8][1] = 1;
				supply_strategy[8][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("procurement_time")) {
			procurement_time = paramMap.get("procurement_time")[0];
			switch (procurement_time) {
			case "high":

				supplier_number_strategy[9][0] = 1;
				supplier_number_strategy[9][1] = 1;

				market_field_strategy[9][1] = 1;

				supply_strategy[9][0] = 1;

				break;
			case "low":
				supplier_number_strategy[9][0] = 1;
				supplier_number_strategy[9][1] = 1;

				market_field_strategy[9][0] = 1;

				supply_strategy[9][1] = 1;
				supply_strategy[9][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[9][0] = 1;
				supplier_number_strategy[9][1] = 1;

				market_field_strategy[9][0] = 1;
				market_field_strategy[9][1] = 1;

				supply_strategy[9][0] = 1;
				supply_strategy[9][1] = 1;
				supply_strategy[9][2] = 1;

				break;

			}
		}

		if (paramMap.containsKey("risk_by_supplier_failure")) {
			risk_by_supplier_failure = paramMap.get("risk_by_supplier_failure")[0];
			switch (risk_by_supplier_failure) {
			case "high":

				supplier_number_strategy[10][0] = 1;

				market_field_strategy[10][1] = 1;
				market_field_strategy[10][0] = 1;

				supply_strategy[10][0] = 1;
				supply_strategy[10][1] = 1;
				supply_strategy[10][2] = 1;

				break;
			case "low":

				supplier_number_strategy[10][1] = 1;

				market_field_strategy[10][1] = 1;
				market_field_strategy[10][0] = 1;

				supply_strategy[10][0] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[10][0] = 1;
				supplier_number_strategy[10][1] = 1;

				market_field_strategy[10][1] = 1;
				market_field_strategy[10][0] = 1;

				supply_strategy[10][0] = 1;
				supply_strategy[10][1] = 1;
				supply_strategy[10][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("transport_risk")) {
			transport_risk = paramMap.get("transport_risk")[0];
			switch (transport_risk) {
			case "high":

				supplier_number_strategy[11][0] = 1;
				supplier_number_strategy[11][1] = 1;

				market_field_strategy[11][0] = 1;

				supply_strategy[11][0] = 1;
				supply_strategy[11][1] = 1;
				supply_strategy[11][2] = 1;

				break;
			case "low":

				supplier_number_strategy[11][0] = 1;
				supplier_number_strategy[11][1] = 1;

				market_field_strategy[11][1] = 1;

				supply_strategy[11][0] = 1;
				supply_strategy[11][1] = 1;
				supply_strategy[11][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[11][0] = 1;
				supplier_number_strategy[11][1] = 1;

				market_field_strategy[11][1] = 1;
				market_field_strategy[11][0] = 1;

				supply_strategy[11][0] = 1;
				supply_strategy[11][1] = 1;
				supply_strategy[11][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("currency_risk")) {
			currency_risk = paramMap.get("currency_risk")[0];
			switch (currency_risk) {
			case "high":

				supplier_number_strategy[12][0] = 1;
				supplier_number_strategy[12][1] = 1;

				market_field_strategy[12][0] = 1;

				supply_strategy[12][0] = 1;
				supply_strategy[12][1] = 1;
				supply_strategy[12][2] = 1;

				break;
			case "low":

				supplier_number_strategy[12][0] = 1;
				supplier_number_strategy[12][1] = 1;

				market_field_strategy[12][1] = 1;

				supply_strategy[12][0] = 1;
				supply_strategy[12][1] = 1;
				supply_strategy[12][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[12][0] = 1;
				supplier_number_strategy[12][1] = 1;

				market_field_strategy[12][1] = 1;
				market_field_strategy[12][0] = 1;

				supply_strategy[12][0] = 1;
				supply_strategy[12][1] = 1;
				supply_strategy[12][2] = 1;

				break;

			}
		}

		if (paramMap.containsKey("exchange_rate_risk")) {
			exchange_rate_risk = paramMap.get("exchange_rate_risk")[0];
			switch (exchange_rate_risk) {
			case "high":

				supplier_number_strategy[13][0] = 1;
				supplier_number_strategy[13][1] = 1;

				market_field_strategy[13][1] = 1;

				supply_strategy[13][0] = 1;
				supply_strategy[13][1] = 1;
				supply_strategy[13][2] = 1;

				break;
			case "low":

				supplier_number_strategy[13][0] = 1;
				supplier_number_strategy[13][1] = 1;

				market_field_strategy[13][0] = 1;

				supply_strategy[13][0] = 1;
				supply_strategy[13][1] = 1;
				supply_strategy[13][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[13][0] = 1;
				supplier_number_strategy[13][1] = 1;

				market_field_strategy[13][0] = 1;
				market_field_strategy[13][1] = 1;

				supply_strategy[13][0] = 1;
				supply_strategy[13][1] = 1;
				supply_strategy[13][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("customs_problem_risk")) {
			customs_problem_risk = paramMap.get("customs_problem_risk")[0];
			switch (customs_problem_risk) {
			case "high":

				supplier_number_strategy[14][0] = 1;
				supplier_number_strategy[14][1] = 1;

				market_field_strategy[14][1] = 1;

				supply_strategy[14][0] = 1;
				supply_strategy[14][1] = 1;
				supply_strategy[14][2] = 1;

				break;
			case "low":

				supplier_number_strategy[14][0] = 1;
				supplier_number_strategy[14][1] = 1;

				market_field_strategy[14][0] = 1;

				supply_strategy[14][0] = 1;
				supply_strategy[14][1] = 1;
				supply_strategy[14][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[14][0] = 1;
				supplier_number_strategy[14][1] = 1;

				market_field_strategy[14][0] = 1;
				market_field_strategy[14][1] = 1;

				supply_strategy[14][0] = 1;
				supply_strategy[14][1] = 1;
				supply_strategy[14][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("dependence_on_supplier")) {
			dependence_on_supplier = paramMap.get("dependence_on_supplier")[0];
			switch (dependence_on_supplier) {
			case "high":

				supplier_number_strategy[15][0] = 1;

				market_field_strategy[15][0] = 1;

				supply_strategy[15][0] = 1;

				break;
			case "low":

				supplier_number_strategy[15][1] = 1;

				market_field_strategy[15][1] = 1;

				supply_strategy[15][1] = 1;
				supply_strategy[15][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[15][0] = 1;
				supplier_number_strategy[15][1] = 1;

				market_field_strategy[15][0] = 1;
				market_field_strategy[15][1] = 1;

				supply_strategy[15][0] = 1;
				supply_strategy[15][1] = 1;
				supply_strategy[15][2] = 1;

				break;

			}
		}

		if (paramMap.containsKey("number_of_suppliers_and_contacts")) {
			number_of_suppliers_and_contacts = paramMap.get("number_of_suppliers_and_contacts")[0];
			switch (number_of_suppliers_and_contacts) {
			case "high":

				supplier_number_strategy[16][1] = 1;

				market_field_strategy[16][0] = 1;
				market_field_strategy[16][1] = 1;

				supply_strategy[16][0] = 1;
				supply_strategy[16][1] = 1;
				supply_strategy[16][2] = 1;

				break;
			case "low":

				supplier_number_strategy[16][0] = 1;

				market_field_strategy[16][0] = 1;
				market_field_strategy[16][1] = 1;

				supply_strategy[16][0] = 1;
				supply_strategy[16][1] = 1;
				supply_strategy[16][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[16][0] = 1;
				supplier_number_strategy[16][1] = 1;

				market_field_strategy[16][0] = 1;
				market_field_strategy[16][1] = 1;

				supply_strategy[16][0] = 1;
				supply_strategy[16][1] = 1;
				supply_strategy[16][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("expenditure_on_bureaucracy")) {
			expenditure_on_bureaucracy = paramMap.get("expenditure_on_bureaucracy")[0];
			switch (expenditure_on_bureaucracy) {
			case "high":

				supplier_number_strategy[17][0] = 1;

				market_field_strategy[17][1] = 1;

				supply_strategy[17][0] = 1;
				supply_strategy[17][1] = 1;
				supply_strategy[17][2] = 1;

				break;
			case "low":

				supplier_number_strategy[17][1] = 1;

				market_field_strategy[17][0] = 1;

				supply_strategy[17][0] = 1;
				supply_strategy[17][1] = 1;
				supply_strategy[17][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[17][0] = 1;
				supplier_number_strategy[17][1] = 1;

				market_field_strategy[17][0] = 1;
				market_field_strategy[17][1] = 1;

				supply_strategy[17][0] = 1;
				supply_strategy[17][1] = 1;
				supply_strategy[17][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("language")) {
			language = paramMap.get("language")[0];
			switch (language) {
			case "same":

				supplier_number_strategy[18][0] = 1;
				supplier_number_strategy[18][1] = 1;

				market_field_strategy[18][1] = 1;

				supply_strategy[18][0] = 1;
				supply_strategy[18][1] = 1;
				supply_strategy[18][2] = 1;

				break;
			case "different":

				supplier_number_strategy[18][0] = 1;
				supplier_number_strategy[18][1] = 1;

				market_field_strategy[18][0] = 1;

				supply_strategy[18][0] = 1;
				supply_strategy[18][1] = 1;
				supply_strategy[18][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[18][0] = 1;
				supplier_number_strategy[18][1] = 1;

				market_field_strategy[18][0] = 1;
				market_field_strategy[18][1] = 1;

				supply_strategy[18][0] = 1;
				supply_strategy[18][1] = 1;
				supply_strategy[18][2] = 1;

				break;

			}
		}
		if (paramMap.containsKey("currency")) {
			currency = paramMap.get("currency")[0];
			switch (currency) {
			case "same":

				supplier_number_strategy[19][0] = 1;
				supplier_number_strategy[19][1] = 1;

				market_field_strategy[19][1] = 1;

				supply_strategy[19][0] = 1;
				supply_strategy[19][1] = 1;
				supply_strategy[19][2] = 1;

				break;
			case "different":

				supplier_number_strategy[19][0] = 1;
				supplier_number_strategy[19][1] = 1;

				market_field_strategy[19][0] = 1;

				supply_strategy[19][0] = 1;
				supply_strategy[19][1] = 1;
				supply_strategy[19][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[19][0] = 1;
				supplier_number_strategy[19][1] = 1;

				market_field_strategy[19][0] = 1;
				market_field_strategy[19][1] = 1;

				supply_strategy[19][0] = 1;
				supply_strategy[19][1] = 1;
				supply_strategy[19][2] = 1;

				break;

			}
		}

		if (paramMap.containsKey("exchange_rate_utilization")) {
			exchange_rate_utilization = paramMap.get("exchange_rate_utilization")[0];
			switch (exchange_rate_utilization) {
			case "possible":

				supplier_number_strategy[20][0] = 1;
				supplier_number_strategy[20][1] = 1;

				market_field_strategy[20][0] = 1;

				supply_strategy[20][0] = 1;
				supply_strategy[20][1] = 1;
				supply_strategy[20][2] = 1;

				break;
			case "not_possible":

				supplier_number_strategy[20][0] = 1;
				supplier_number_strategy[20][1] = 1;

				market_field_strategy[20][1] = 1;

				supply_strategy[20][0] = 1;
				supply_strategy[20][1] = 1;
				supply_strategy[20][2] = 1;

				break;
			case "irrelevant":
				supplier_number_strategy[20][0] = 1;
				supplier_number_strategy[20][1] = 1;

				market_field_strategy[20][0] = 1;
				market_field_strategy[20][1] = 1;

				supply_strategy[20][0] = 1;
				supply_strategy[20][1] = 1;
				supply_strategy[20][2] = 1;

				break;

			}
		}

		if (paramMap.containsKey("market_research_result")) {
			market_research_result = paramMap.get("market_research_result")[0];
		}
		if (market_research_result.equals("one_supplier")) {
			for (int i = 0; i < 21; i++) {
				supplier_number_strategy[i][1] = -1;
			}
		} else if (market_research_result.equals("suppliers_own_country")) {
			for (int i = 0; i < 21; i++) {
				market_field_strategy[i][1] = -1;
			}
		}

		else if (market_research_result.equals("suppliers_other_countries")) {
			for (int i = 0; i < 21; i++) {
				market_field_strategy[i][0] = -1;
			}
		}

		if (paramMap.containsKey("product_feature")) {
			product_feature = paramMap.get("product_feature")[0];
		}
		if (product_feature.equals("product_complex")) {
			for (int i = 0; i < 21; i++) {
				supplier_number_strategy[i][1] = -1;
			}
		} else if (product_feature.equals("product_development")) {
			for (int i = 0; i < 21; i++) {
				supplier_number_strategy[i][1] = -1;
			}
		} else if (product_feature.equals("product_customized")) {
			for (int i = 0; i < 21; i++) {
				supply_strategy[i][1] = -1;
				supply_strategy[i][0] = -1;
			}
		}
		/*
		 * else if (product_feature.equals("product_few_specs")) { for (int i =
		 * 0; i < 21; i++) { supplier_number_strategy[i][0] = -1; } } else if
		 * (product_feature.equals("product_standardized")) { for (int i = 0; i
		 * < 21; i++) { supplier_number_strategy[i][0] = -1; } }
		 */
		if (paramMap.containsKey("predictability_procurement")) {
			predictability_procurement = paramMap.get("predictability_procurement")[0];
		}
		if (predictability_procurement.equals("less")) {
			for (int i = 0; i < 21; i++) {
				supply_strategy[i][1] = -1;
			}
		} else if (predictability_procurement.equals("well")) {
			for (int i = 0; i < 21; i++) {
				supply_strategy[i][0] = -1;
				supply_strategy[i][2] = -1;
			}
		}

		int single_sourcing = 0, multiple_sourcing = 0, local_sourcing = 0, global_sourcing = 0, stock_piling = 0,
				just_in_time = 0, just_in_sequence = 0;

		for (int i = 0; i < 21; i++) {
			single_sourcing += supplier_number_strategy[i][0];
			multiple_sourcing += supplier_number_strategy[i][1];

			local_sourcing += market_field_strategy[i][0];
			global_sourcing += market_field_strategy[i][1];

			stock_piling += supply_strategy[i][0];
			just_in_time += supply_strategy[i][1];
			just_in_sequence += supply_strategy[i][2];
		}
		String Supplier_number_strategy = null, Market_field_strategy = null, Supply_strategy = null;
		System.out.println("Single Sourcing " + single_sourcing + " Multiple Sourceing " + multiple_sourcing);
		System.out.println("Local Sourcing " + local_sourcing + " Global Sourcing " + global_sourcing);
		System.out.println(
				"Stock piling " + stock_piling + " Just in time " + just_in_time + " Just in se" + just_in_sequence);
		if (single_sourcing > multiple_sourcing) {
			if (single_sourcing > -1)
				Supplier_number_strategy = "<spring:message code=\"procurement.strategy.supplierNumber\" />";
		} else {
			if (multiple_sourcing > -1)
				Supplier_number_strategy = "Multiple Sourcing";
		}

		if (local_sourcing > global_sourcing) {
			if (local_sourcing > -1)
				Market_field_strategy = "Local Sourcing";
		} else {
			if (global_sourcing > -1)
				Market_field_strategy = "Global Sourcing";
		}

		int max = Math.max(Math.max(stock_piling, just_in_time), just_in_sequence);

		if (max == stock_piling) {
			if (stock_piling > -1)
				Supply_strategy = "Stockpiling";
		} else if (max == just_in_time) {
			if (just_in_time > -1)
				Supply_strategy = "Just in Time";
		} else {
			if (just_in_sequence > -1)
				Supply_strategy = "Just in Sequence";
		}
		prService.storeProcurement(paramMap.get("material_id")[0], Supplier_number_strategy, Market_field_strategy,
				Supply_strategy, market_research_result, product_feature, predictability_procurement, flexibility,
				complexity, subcription_price, order_and_transaction_costs, transport_costs, storage_and_capital_costs,
				procurement_time, risk_by_supplier_failure, transport_risk, currency_risk, exchange_rate_risk,
				customs_problem_risk, dependence_on_supplier, number_of_suppliers_and_contacts,
				expenditure_on_bureaucracy, language, currency, exchange_rate_utilization);
		logger.info("Procurement Table Updated. Supplier Num: " + Supplier_number_strategy + " Market Str: "
				+ Market_field_strategy + " Supply Str: " + supply_strategy);
		return "procurement";
	} 
	
	@ExceptionHandler (HttpSessionRequiredException.class)
	public String notLoggedIn(){
		return "redirect:login";
	}

}
