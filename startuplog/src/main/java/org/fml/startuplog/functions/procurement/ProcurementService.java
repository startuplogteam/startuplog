package org.fml.startuplog.functions.procurement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;
import org.fml.startuplog.dao.StartuplogDAO;
import org.fml.startuplog.model.Procurement;
import org.fml.startuplog.service.MasterService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcurementService extends MasterService{

	private final Logger logger = Logger.getLogger(this.getClass());
	
	public String getProcurementJsonFormat(String materialID) {
		
		Procurement procurement = startuplogDAOObject.getProcerement(materialID);
		JSONObject finalObj = new JSONObject();
		if (procurement != null) {
			if (procurement.getSupplier_number_strategy() != null || procurement.getMarket_field_strategy() != null
					|| procurement.getSupply_strategy() != null) {

				finalObj.put("Supplier Number Strategy", procurement.getSupplier_number_strategy());
				finalObj.put("Market Field Strategy", procurement.getMarket_field_strategy());
				finalObj.put("Supply Strategy", procurement.getSupply_strategy());

			}
			finalObj.put("market_research_result", procurement.getMarket_research_result());
			finalObj.put("product_feature", procurement.getProduct_feature());
			finalObj.put("predictability_procurement", procurement.getPredictability_procurement());
			finalObj.put("flexibility", procurement.getFlexibility());
			finalObj.put("complexity", procurement.getComplexity());
			finalObj.put("subcription_price", procurement.getSubcription_price());
			finalObj.put("order_and_transaction_costs", procurement.getOrder_and_transaction_costs());
			finalObj.put("transport_costs", procurement.getTransport_costs());
			finalObj.put("storage_and_capital_costs", procurement.getStorage_and_capital_costs());
			finalObj.put("procurement_time", procurement.getProcurement_time());
			finalObj.put("risk_by_supplier_failure", procurement.getRisk_by_supplier_failure());
			finalObj.put("transport_risk", procurement.getTransport_risk());
			finalObj.put("currency_risk", procurement.getCurrency_risk());
			finalObj.put("exchange_rate_risk", procurement.getExchange_rate_risk());
			finalObj.put("customs_problem_risk", procurement.getCustoms_problem_risk());
			finalObj.put("dependence_on_supplier", procurement.getDependence_on_supplier());
			finalObj.put("number_of_suppliers_and_contacts", procurement.getNumber_of_suppliers_and_contacts());
			finalObj.put("expenditure_on_bureaucracy", procurement.getExpenditure_on_bureaucracy());
			finalObj.put("language", procurement.getLanguage());
			finalObj.put("currency", procurement.getCurrency());
			finalObj.put("exchange_rate_utilization", procurement.getExchange_rate_utilization());
		}
		else{
			logger.error("NULL retured from Procurement Table for material ID "+ materialID);
		}
		return finalObj.toJSONString();
	}

	public void storeProcurement(String material_id, String Supplier_number_strategy, String Market_field_strategy,
			String Supply_strategy, String market_research_result, String  product_feature, String  predictability_procurement,
			String  flexibility, String complexity, String  subcription_price, String  order_and_transaction_costs,
			String  transport_costs, String  storage_and_capital_costs, String  procurement_time,
			String  risk_by_supplier_failure, String  transport_risk, String  currency_risk, String  exchange_rate_risk,
			String  customs_problem_risk, String  dependence_on_supplier, String  number_of_suppliers_and_contacts,
			String  expenditure_on_bureaucracy, String  language, String  currency, String  exchange_rate_utilization) {

		startuplogDAOObject.storeProcurement(material_id, Supplier_number_strategy, Market_field_strategy, Supply_strategy, market_research_result, product_feature, predictability_procurement, flexibility, complexity, subcription_price, order_and_transaction_costs, transport_costs, storage_and_capital_costs, procurement_time, risk_by_supplier_failure, transport_risk, currency_risk, exchange_rate_risk, customs_problem_risk, dependence_on_supplier, number_of_suppliers_and_contacts, expenditure_on_bureaucracy, language, currency, exchange_rate_utilization);

	}

}
