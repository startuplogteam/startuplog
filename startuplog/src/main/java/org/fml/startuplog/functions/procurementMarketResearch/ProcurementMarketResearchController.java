package org.fml.startuplog.functions.procurementMarketResearch;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class ProcurementMarketResearchController {

	@RequestMapping(value = "/procurementmarketresearch", method = RequestMethod.GET)
	public String preLoadMaterials(ModelMap model, @ModelAttribute("user") String user) {
		
		return "procurementMarketResearch";
	}

	@ExceptionHandler (HttpSessionRequiredException.class)
	public String notLoggedIn(){
		return "redirect:login";
	}
}
