package org.fml.startuplog.functions.supplierselection;

import java.util.List;


import org.fml.startuplog.exception.NoMaterialsFetchedException;
import org.fml.startuplog.model.Material;
import org.fml.startuplog.model.SupplierSelection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class SupplierSelectionController {

	@Autowired
	private SupplierSelectionService service;
	
	@RequestMapping(value = "/supplierselection", method = RequestMethod.GET)
	public String preLoadMaterials(ModelMap model, @ModelAttribute("user") String user) {
		List<Material> materials;
		try{
			materials = service.fetchMaterialList();
			if(materials==null){
				throw new NoMaterialsFetchedException();
			}
			if(materials.isEmpty()){
					throw new NoMaterialsFetchedException();
			}
			model.put("materialList", materials);
		}
		catch(NoMaterialsFetchedException e){
			model.put("errormsg", e.getMessage());
		}
		
		return "supplierSelection";
	}
	
	@RequestMapping(value = "/fetchsuppliers", method = RequestMethod.GET)
	@ResponseBody 
	public List<SupplierSelection> fetchSupplierDetails(@RequestParam String material_id,ModelMap model,@ModelAttribute("user") String user){
		
		List<SupplierSelection> supplierDetails = service.fetchSuppliersForSelection(material_id,user);
		return supplierDetails;
	}
	
	@RequestMapping(value = "/addcriteria", method = RequestMethod.GET)    
	@ResponseStatus(value = HttpStatus.OK)
	public void addCriteria(@RequestParam String criteria,@RequestParam int material, @ModelAttribute("user") String user){
	
		service.addCriteria(criteria,material, user);
		
	}
	
	@RequestMapping(value = "/deletecriteria", method = RequestMethod.GET)    
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteCriteria(@RequestParam String criteria,@RequestParam int material, @ModelAttribute("user") String user ){
		
		service.deleteCriteria(criteria,material,user);
		
	}
	
	@RequestMapping(value = "/updateSupplierValue", method = RequestMethod.GET)    
	@ResponseStatus(value = HttpStatus.OK)
	public void updateSupplierValue(@RequestParam String criteria,@RequestParam int id,@RequestParam String supplier,@ModelAttribute("user") String user, @RequestParam int value ){
		service.updateSupplierValue(criteria,id,supplier,user,value);
		
	}
	
	@RequestMapping(value = "/savesupplier", method = RequestMethod.GET)    
	@ResponseStatus(value = HttpStatus.OK)
	public void saveSuppliers(@RequestParam String supplier,@RequestParam String material,@ModelAttribute("user") String user){
		service.saveSuppliers(supplier,material,user);
		
	}
	@ExceptionHandler (HttpSessionRequiredException.class)
	public String notLoggedIn(){
		return "redirect:login";
	}
}
