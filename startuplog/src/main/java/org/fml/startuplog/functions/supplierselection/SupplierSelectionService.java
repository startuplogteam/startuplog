package org.fml.startuplog.functions.supplierselection;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.fml.startuplog.model.Material;
import org.fml.startuplog.model.SupplierSelection;
import org.fml.startuplog.service.MasterService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;

@Service
public class SupplierSelectionService extends MasterService{

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("startuplog");

	public List<Material> fetchMaterialList() {
		return startuplogDAOObject.getAllMaterial_for_procurement();
	}

	public List<SupplierSelection> fetchSuppliersForSelection(String materialId,String user) {
		EntityManager em = emf.createEntityManager();
		TypedQuery<SupplierSelection> fetchSuppliers = em
				.createQuery("select s from SupplierSelection s where s.materialNumber=?1 and s.username=?2", SupplierSelection.class);
		fetchSuppliers.setParameter(1, materialId);
		fetchSuppliers.setParameter(2, user);
		List<SupplierSelection> suppliers = fetchSuppliers.getResultList();
		em.close();
		return suppliers;
	}

	public void addCriteria(String criteria, int materialId, String user) {
		EntityManager em = emf.createEntityManager();
		TypedQuery<String> fetchSupplierNames = em.createQuery(
				"select distinct(s.supplierName) from SupplierSelection s where s.materialNumber=?", String.class);
		fetchSupplierNames.setParameter(1, materialId + "");
		List<String> supplierNames = fetchSupplierNames.getResultList();

		Query fetchMaxId = em.createQuery("select max(s.id) from SupplierSelection s");
		int count = (int) fetchMaxId.getResultList().get(0);

		for (String name : supplierNames) {
			SupplierSelection supplierSelection = new SupplierSelection();
			supplierSelection.setId(++count);
			supplierSelection.setCriteria(criteria);
			supplierSelection.setMaterialNumber(materialId + "");
			supplierSelection.setSupplierName(name);
			supplierSelection.setValue(5);
			supplierSelection.setUsername(user);
			em.getTransaction().begin();
			em.persist(supplierSelection);
			em.getTransaction().commit();
		}
		em.close();
	}

	public void deleteCriteria(String criteria, int material,String user) {
		EntityManager em = emf.createEntityManager();
		Query deleteCriteria = em
				.createQuery("delete from SupplierSelection s where s.criteria=? and s.materialNumber=? and s.username=?");
		
		deleteCriteria.setParameter(1, criteria);
		deleteCriteria.setParameter(2, material + "");
		deleteCriteria.setParameter(3, user);
		
		em.getTransaction().begin();
		deleteCriteria.executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	public void updateSupplierValue(String criteria, int id, String supplier, String user, int value) {
		EntityManager em = emf.createEntityManager();
		Query updateValue = em.createQuery(
				"update SupplierSelection s set s.value=? where s.criteria=? and s.materialNumber=? and s.supplierName=? and s.username=?");
		updateValue.setParameter(1, value);
		updateValue.setParameter(2, criteria);
		updateValue.setParameter(3, id + "");
		updateValue.setParameter(4, supplier);
		updateValue.setParameter(5, user);
		em.getTransaction().begin();
		updateValue.executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	public void saveSuppliers(String supplier, String material, String user) {
		// TODO Auto-generated method stub
		EntityManager em = emf.createEntityManager();
		Query updateValue = em.createQuery(
				"update SupplierSelection s set s.selected=? where s.supplierName=? and s.materialNumber=? and s.username=?");
		updateValue.setParameter(1, 'y');
		updateValue.setParameter(2, supplier);
		updateValue.setParameter(3, material);
		updateValue.setParameter(4, user);
		em.getTransaction().begin();
			updateValue.executeUpdate();
		em.getTransaction().commit();
		em.close();
	}
}
