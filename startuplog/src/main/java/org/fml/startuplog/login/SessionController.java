
package org.fml.startuplog.login;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.fml.startuplog.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.HttpSessionRequiredException;

@Controller
@SessionAttributes("user")
public class SessionController {

	@Autowired
	SessionService loginService;
	private final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Show indicators GET method
	 * 
	 * @return indicators.jsp
	 */
	@RequestMapping(value = "/indicators", method = RequestMethod.GET)
	public String showIndicators(@ModelAttribute("user") String user) {
		return "indicators";
	}

	@ExceptionHandler(HttpSessionRequiredException.class)
	public String notLoggedIn() {
		return "redirect:login";
	}

	/**
	 * Show login from GET method
	 * 
	 * @return login.jsp
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String loginRedirect(SessionStatus status) {

		status.setComplete();
		return "login";
	}

	/**
	 * Show login from GET method
	 * 
	 * @return login.jsp
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {

		return "login";
	}

	/**
	 * Validate user credentials and load indicators POST method
	 * 
	 * @return indicators.jsp
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loadIndicators(ModelMap model, @RequestParam String username, @RequestParam String password,
			HttpSession session) {

		if (loginService.validateUser(username, password)) {
			logger.info("User " + username + " successfully logged in.");
			model.addAttribute("user", username);
			if (loginService.checkUserStage(username)) {
				int stageNumber = loginService.fetchCurrentDevelopmentalStage(username);
				model.put("suggestion" + stageNumber, "suggestion");
				logger.info("Current UserStage is :" + stageNumber);
				return "redirect:workspace";
			}
			return "redirect:indicators";
		} else {
			model.put("loginerror", "Error: invalid credintials. Try again.");
			return "login";
		}
	}
	
	@RequestMapping(value = "/addUser", method = RequestMethod.GET)
	public String addUser(ModelMap model) {
		List<User> userList = loginService.fetchUserList();
		model.put("userList", userList);
		return "addUser";
	}
	

	/**
	 * POST method for retrieving user addition page.
	 * 
	 * @return addUser.jsp
	 */
	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public String addUser(ModelMap model, @RequestParam String username, @RequestParam String password, boolean isAdmin,
			boolean isActive, HttpSession session) {

		List<User> userList = loginService.fetchUserList();
		model.put("userList", userList);
		
		if (null != username && null != password) {
			boolean isSuccess = loginService.addNewUser(username, password, isActive, isAdmin);
			if (isSuccess) {
				model.put("op_status", "User successfully added.");
				return "redirect:addUser";
			} else {
				model.put("op_status", "User addition unsuccessful.");
				return "redirect:addUser";
			}

		} else {
			model.put("op_status", "Error: Username or password should not be empty. Try again.");
			return "redirect:addUser";
		}
	}

	/**
	 * POST method for editing an existing user in the system.
	 * 
	 * @return addUser.jsp
	 */
	@RequestMapping(value = "/editUser", method = RequestMethod.POST)
	public String editUser(ModelMap model, @RequestParam String username, boolean isAdmin, boolean isActive,
			HttpSession session) {

		if (null != username) {
			boolean isSuccess = loginService.editUser(username, isActive, isAdmin);
			if (isSuccess) {
				model.put("op_status", "User successfully edited.");
				return "redirect:addUser";
			} else {
				model.put("op_status", "User editing unsuccessful.");
				return "redirect:addUser";
			}
		}

		else {
			model.put("op_status", "Error: Username should not be empty. Try again.");
			return "redirect:addUser";
		}
	}

	/**
	 * POST method for deleting an existing user in the system.
	 * 
	 * @return addUser.jsp
	 */
	@RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
	public String deleteUser(ModelMap model, @RequestParam String username, HttpSession session) {

		if (null != username) {
			boolean isSuccess = loginService.deleteUser(username);
			if (isSuccess) {
				model.put("op_status", "User successfully deleted.");
				return "redirect:addUser";
			} else {
				model.put("op_status", "User deletion unsuccessful.");
				return "redirect:addUser";
			}

		} else {
			model.put("op_status", "Error: User doesn't exist in the system.");
			return "login";
		}
	}
}
