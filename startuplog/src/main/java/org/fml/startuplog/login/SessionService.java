package org.fml.startuplog.login;

import java.util.List;

import org.fml.startuplog.dao.StartuplogDAO;
import org.fml.startuplog.model.PhaseModel;
import org.fml.startuplog.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SessionService {

	@Autowired
	private StartuplogDAO daoService;

	public boolean validateUser(String username, String password) {
		return daoService.validateUser(username, password);
	}

	public List<User> fetchUserList() {
		return daoService.fetchUserList();
	}
	public boolean addNewUser(String username, String password, boolean isActive, boolean isAdmin) {
		return daoService.addNewUser(username, password, isActive, isAdmin);
	}

	public boolean editUser(String username, boolean isActive, boolean isAdmin) {
		return daoService.editUser(username, isActive, isAdmin);
	}

	public boolean deleteUser(String username) {
		return daoService.deleteUser(username);
	}

	public boolean checkUserStage(String username) {
		PhaseModel userPhase = daoService.loadLatestPhaseModel(username);
		if (userPhase != null) {
			if (userPhase.getStage() != null && userPhase.getStage() != null)
				return true;
		}
		return false;
	}

	public int fetchCurrentDevelopmentalStage(String username) {
		PhaseModel userPhase = daoService.loadLatestPhaseModel(username);
		if (null != userPhase) {
			if (null != userPhase.getStage())
				switch (userPhase.getStage()) {
				case "Pre-seed Stage":
					return 0;
				case "Seed Stage":
					return 1;
				case "Startup:Conception":
					return 2;
				case "Startup:Construction":
					return 3;
				case "Startup:Elaboration":
					return 4;
				case "Startup:Prototyping":
					return 5;
				case "First Stage":
					return 6;
				case "Second Stage":
					return 7;
				case "Third Stage":
					return 8;
				case "Forth Stage":
					return 9;

				}
			return 10;
		}
		return 10;
	}

}
