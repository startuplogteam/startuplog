package org.fml.startuplog.logisticstask;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.fml.startuplog.model.PhaseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@SessionAttributes("user")
public class LogisticsTaskController {

	@Autowired
	private LogisticsTaskService logicsTaskService;
	String stage;

	private final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Any intention to load function page directly without invoking indicators
	 * will lead to redirecting indicator page again GET method
	 * 
	 * @return indicators.jsp
	 */
	@RequestMapping(value = "/function", method = RequestMethod.GET)
	public String loadIndicators(ModelMap model) throws Exception {
		logger.info("/function forwared to /logistics");
		System.out.println("Checkpoint 0 passed");

		return "redirect:logistictasks";
	}

	@RequestMapping(value = "/logistictasks", method = RequestMethod.GET)
	public String showLogisticsTasks(@ModelAttribute("user") String user, SessionStatus sessionStatus, ModelMap model,
			HttpSession session) throws Exception {

		System.out.println("Checkpoint 1 passed");

		String logisticStage = (String) session.getAttribute("selectedStage");

		PhaseModel phaseModel = logicsTaskService.loadLastestPhaseModel(user);
		if (phaseModel != null) {
			System.out.println("Checkpoint 2 passed");

			int logisticTasksList[][][] = logicsTaskService.getLogisticTasksList(user, logisticStage);
			System.out.println("logisticTaskList Size " + logisticTasksList.length);
			model.addAttribute("function", logisticTasksList);
			stage = phaseModel.getStage();
			System.out.println("Stage is " + stage);
			model.put("stage", logicsTaskService.getStageCorrespondingIndex(stage));
			logger.info("Retrieved Stage: " + stage);
			try {

				System.out.println("Checkpoint passed");
				phaseModel = logicsTaskService.loadLastestPhaseModel(user);
				int variant = Integer.parseInt(phaseModel.getVariant());
				model.put("variant", variant);
				logger.info("Retrived variant: " + variant);
			} catch (Exception e) {
				// no function under this variant
				// legal variants are from 0 to 5
				logger.error(e);
				logger.info("Added variant 6 which is out of range (0-5)");
				model.put("variant", 6);
			}

		}

		return "function";
	}

	@RequestMapping(value = "/function", params = "back", method = RequestMethod.GET)
	public String backFrommakebuy(ModelMap model, @ModelAttribute("user") String user) {
		logger.info("Back to Stages");
		return "redirect:stages";
	}

	@ExceptionHandler(HttpSessionRequiredException.class)
	public String notLoggedIn() {
		return "redirect:login";
	}
}
