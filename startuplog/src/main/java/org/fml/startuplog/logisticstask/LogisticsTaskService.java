package org.fml.startuplog.logisticstask;

import java.util.List;

import org.apache.log4j.Logger;
import org.fml.startuplog.exception.NoDecisonsFetchedException;
import org.fml.startuplog.functions.makeBuyDecision.MakeBuyDecisionService;
import org.fml.startuplog.model.MakeBuyDecision;
import org.fml.startuplog.service.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jjohn
 *
 */
@Service
public class LogisticsTaskService extends MasterService {

	public LogisticsTaskService() {
		// initLogisticTasksList();
	}

	@Autowired
	private MakeBuyDecisionService makeBuyDecisionService;
	private final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * First index indicates stage Second index is for variant Third index
	 * stands for function, where the index map is as below
	 * 
	 * Note the array indexing and the stage numbering difference.
	 * 
	 * 1 : Initial Planning 5 : ABC Analysis 8 : Supply Risk Portfolio 7 :
	 * Supplier Selection 8 : Capacity Planning
	 * 
	 * Stage index and stage names
	 * 
	 * 0-2: Ignore for old model. 3: Product Development 4: Product Conception
	 * 5: Prototyping 6: Expansion Stage 7: Later Stage 8-9 : Ignore, for old
	 * model.
	 * 
	 * Variants : Corresponds to one of the 6 combinations of make and buy.
	 * Variant 6 is ignored as it doesn't result in any functions.
	 * 
	 */
	private int function[][][] = new int[11][7][46];

	private void initLogisticTasksList() {
		for (int i = 0; i < 5; i++) {

			// Activate functions that are shown in the side-bar for each of the
			// logged-in users.

			// General
			// activate product logistic requirements for stage five, any
			// variant from 1 to
			// 5.
			function[1][i][4] = 1;
			function[3][i][4] = 1;
			function[4][i][4] = 1;
			function[5][i][4] = 1;
			function[6][i][4] = 1;
			function[7][i][4] = 1;

			// activate makeOrBuy Decision product for stage five, any
			// variant from 1 to
			// 5.
			function[1][i][5] = 1;
			function[3][i][5] = 1;
			function[4][i][5] = 1;
			function[5][i][5] = 1;
			function[6][i][5] = 1;
			function[7][i][5] = 1;

			// activate makeOrBuy Decision component for stage five, only
			// variants 1 and 2
			if (i != 4) {
				function[4][i][6] = 1;
				function[5][i][6] = 1;
			}
			if (i == 0 || i == 1) {
				function[6][i][6] = 1;
				function[7][i][6] = 1;
			}

			// activate changeManagement for stage five, any
			// variant from 1 to
			// 5.
			function[3][i][7] = 1;
			function[4][i][7] = 1;
			function[5][i][7] = 1;
			function[6][i][7] = 1;
			function[7][i][7] = 1;

			// activate ABC analysis for stage five , variants 1 and 2

			if (i == 0 || i == 1) {
				function[6][i][8] = 1;
				function[7][i][8] = 1;
			}
			if (i != 4) {
				function[4][i][8] = 1;
				function[5][i][8] = 1;
			}

			// activate processPlanning for stage five, any
			// variant from 1 to
			// 5.
			function[5][i][7] = 1;
			function[6][i][7] = 1;
			function[7][i][7] = 1;

			// activate layoutPlanning for stage five, any
			// variant from 1 to
			// 5.
			function[5][i][10] = 1;
			function[6][i][10] = 1;
			function[7][i][10] = 1;

			// Procurement
			// activate materialRequirementsPlanning for stage five, any
			// variant from 1 to
			// 5.
			if (i == 0 || i == 1) {
				function[6][i][11] = 1;
				function[7][i][11] = 1;
			}
			if (i != 4) {
				function[4][i][11] = 1;
				function[5][i][11] = 1;
			}

			// activate optimalProcurementQuantity for stage five, any
			// variant from 1 to
			// 5.
			if (i == 0 || i == 1) {
				function[6][i][12] = 1;
				function[7][i][12] = 1;
			}
			if (i != 4) {
				function[4][i][12] = 1;
				function[5][i][12] = 1;
			}

			// activate procurementMarketResearch for stage five, any
			// variant from 1 to
			// 6.
			function[1][i][13] = 1;
			function[3][i][13] = 1;
			function[4][i][13] = 1;
			function[5][i][13] = 1;
			function[6][i][13] = 1;
			function[7][i][13] = 1;

			// activate supplyRiskPortfolio for stage five, variants 1 and 2
			if (i != 4) {
				function[4][i][14] = 1;
				function[5][i][14] = 1;
			}
			if (i == 0 || i == 1) {
				function[6][i][14] = 1;
				function[7][i][14] = 1;
			}

			// activate marketPowerPortfolio for stage five, variants 1 and 2
			if (i != 4) {
				function[4][i][15] = 1;
				function[5][i][15] = 1;
			}
			if (i == 0 || i == 1) {
				function[6][i][15] = 1;
				function[7][i][15] = 1;
			}

			// activate procurementStrategy for stage four to nine, any variant
			// from 1
			// to 5. 16 represents procurementStrategy
			if (i != 4) {
				function[4][i][16] = 1;
			}
			function[5][i][16] = 1;
			function[6][i][16] = 1;
			function[7][i][16] = 1;

			// activate supplierSelection from stage five to eight, any variant
			// from 1 to 5. 17 represents supplierSelection
			function[4][i][17] = 1;
			function[5][i][17] = 1;
			function[6][i][17] = 1;
			function[7][i][17] = 1;

			// activate operativePurchasing from stage five to eight, any
			// variant
			// from 1 to 5. 17 represents operativePurchasing
			function[5][i][18] = 1;
			function[6][i][18] = 1;
			function[7][i][18] = 1;

			// activate supplierManagement from stage five to eight, any variant
			// from 1 to 5. 17 represents supplierManagement
			function[5][i][19] = 1;
			function[6][i][19] = 1;
			function[7][i][19] = 1;

			// WarehouseLogisticsTasks
			// activate stockPlanning from stage five to six, any variant
			// from 1 to 5. 17 represents supplierManagement
			function[1][i][20] = 1;
			function[3][i][20] = 1;
			function[4][i][20] = 1;
			function[5][i][20] = 1;
			function[6][i][20] = 1;
			function[7][i][20] = 1;

			// activate storageFacilityPlanning from stage five to six, any
			// variant
			// from 1 to 5. 17 represents supplierManagement
			function[4][i][21] = 1;
			function[5][i][21] = 1;
			function[6][i][21] = 1;
			function[7][i][21] = 1;

			// activate wareHouseDimensioning from stage five to six, any
			// variant
			// from 1 to 5. 22 represents wareHouseDimensioning
			function[4][i][22] = 1;
			function[5][i][22] = 1;
			function[6][i][22] = 1;
			function[7][i][22] = 1;

			// activate storageFacilityInstallation from stage five to six, any
			// variant
			// from 1 to 5. 23 represents storageFacilityInstallation
			function[4][i][23] = 1;
			function[5][i][23] = 1;
			function[6][i][23] = 1;
			function[7][i][23] = 1;

			// activate wareHouseOperation from stage five to six, any variant
			// from 1 to 5. 38 represents wareHouseOperation
			function[5][i][24] = 1;
			function[6][i][24] = 1;
			function[7][i][24] = 1;

			// Production Logistics

			// activate materialProvisionPlanning from stage six to eight, only
			// for
			// variants 1 and 2.
			if (i == 0 || i == 1) {
				function[5][i][25] = 1;
				function[6][i][25] = 1;
				function[7][i][25] = 1;
			}

			// activate capacityPlanning from stage six to eight, only for
			// variants 1 and 2.
			if (i == 0 || i == 1) {
				function[5][i][26] = 1;
				function[6][i][26] = 1;
				function[7][i][26] = 1;
			}

			// activate controllingProductionSupplies from stage six to eight,
			// only for
			// variants 1 and 2.
			if (i == 0 || i == 1) {
				function[6][i][27] = 1;
				function[7][i][27] = 1;
			}

			// activate productionProgramPlanning from stage six to eight, only
			// for
			// variants 1 and 2.
			if (i == 0 || i == 1) {
				function[6][i][28] = 1;
				function[7][i][28] = 1;
			}

			// activate quantityPlanning from stage six to eight, only for
			// variants 1 and 2.
			if (i == 0 || i == 1) {
				function[6][i][29] = 1;
				function[7][i][29] = 1;
			}

			// activate schedulingAndOperativeCapacityPlanning from stage six to
			// eight, only for
			// variants 1 and 2.
			if (i == 0 || i == 1) {
				function[6][i][30] = 1;
				function[7][i][30] = 1;
			}

			// activate orderRequest from stage six to eight, only for
			// variants 1 and 2.
			if (i == 0 || i == 1) {
				function[6][i][31] = 1;
				function[7][i][31] = 1;
			}

			// activate controllingCapacityAndOrders from stage six to eight,
			// only for
			// variants 1 and 2.
			if (i == 0 || i == 1) {
				function[6][i][32] = 1;
				function[7][i][32] = 1;
			}

			// Distribution Logistics

			// activate distributionStrategy from stage six to eight, only for
			// variants 1 and 2.
			function[1][i][33] = 1;
			function[3][i][33] = 1;
			function[4][i][33] = 1;
			function[5][i][33] = 1;
			function[6][i][33] = 1;
			function[7][i][33] = 1;

			// activate distributionConcept from stage six to eight, only for
			// variants 1 and 2.
			function[1][i][34] = 1;
			function[3][i][34] = 1;
			function[4][i][34] = 1;
			function[5][i][34] = 1;
			function[6][i][34] = 1;
			function[7][i][34] = 1;

			// activate packagingPlanning from stage one to seven, all variants
			function[1][i][35] = 1;
			function[3][i][35] = 1;
			function[4][i][35] = 1;
			function[5][i][35] = 1;
			function[6][i][35] = 1;
			function[7][i][35] = 1;

			// activate functions 36-38
			if (i != 3) {
				function[6][i][36] = 1;
				function[7][i][36] = 1;
				function[6][i][37] = 1;
				function[7][i][37] = 1;
				function[6][i][38] = 1;
				function[7][i][38] = 1;
			}

			// Logistics Controlling

			// activate controllingLogisticsCosts from stage six to eight, only
			// for
			// variants 1 and 2.
			function[6][i][39] = 1;
			function[7][i][39] = 1;

		}
		// specialCases

		function[1][5][4] = 1;
		function[3][5][4] = 1;
		function[4][5][4] = 1;
		function[5][5][4] = 1;
		function[6][5][4] = 1;
		function[7][5][4] = 1;

		function[1][5][5] = 1;
		function[3][5][5] = 1;
		function[4][5][5] = 1;
		function[5][5][5] = 1;
		function[6][5][5] = 1;
		function[7][5][5] = 1;

		function[3][5][7] = 1;
		function[4][5][7] = 1;
		function[5][5][7] = 1;
		function[6][5][7] = 1;
		function[7][5][7] = 1;

		function[1][5][13] = 1;
		function[3][5][13] = 1;
		function[4][5][13] = 1;
		function[5][5][13] = 1;
		function[6][5][13] = 1;
		function[7][5][13] = 1;

		function[1][5][33] = 1;
		function[3][5][33] = 1;
		function[4][5][33] = 1;
		function[5][5][33] = 1;
		function[6][5][33] = 1;
		function[7][5][33] = 1;

		function[1][5][34] = 1;
		function[3][5][34] = 1;
		function[4][5][34] = 1;
		function[5][5][34] = 1;
		function[6][5][34] = 1;
		function[7][5][34] = 1;

		function[1][5][35] = 1;
		function[3][5][35] = 1;
		function[4][5][35] = 1;
		function[5][5][35] = 1;
		function[6][5][35] = 1;
		function[7][5][35] = 1;

		function[6][5][39] = 1;
		function[7][5][39] = 1;

		function[5][5][16] = 1;
		function[6][5][16] = 1;
		function[7][5][16] = 1;

		function[4][5][17] = 1;
		function[5][5][17] = 1;
		function[6][5][17] = 1;
		function[7][5][17] = 1;

		function[5][5][18] = 1;
		function[6][5][18] = 1;
		function[7][5][18] = 1;

		function[5][5][19] = 1;
		function[6][5][19] = 1;
		function[7][5][19] = 1;
	}

	private void saveVariantForProducts(String user, String logisticStage) {
		List<MakeBuyDecision> makeBuyDecisions;
		String variant = "7";
		try {
			makeBuyDecisions = makeBuyDecisionService.fetchExistingMakeBuyDecisions(user, logisticStage);

			boolean case1 = false;
			boolean case2 = false;
			boolean case3 = false;
			boolean case4 = false;
			boolean case5 = false;

			if (!makeBuyDecisions.isEmpty()) {
				for (MakeBuyDecision makeBuyDecision : makeBuyDecisions) {
					System.out.println("Checkpoint passed");

					String prototype;
					String seriesProd;
					String storeDispatch;

					if ("In-house production".equalsIgnoreCase(makeBuyDecision.getPrototype())) {
						prototype = "make";
					} else {
						prototype = "buy";
					}
					if ("In-house production".equalsIgnoreCase(makeBuyDecision.getSeriesProd())) {
						seriesProd = "make";
					} else {
						seriesProd = "buy";
					}
					if ("In-house production".equalsIgnoreCase(makeBuyDecision.getStorageDispatch())) {
						storeDispatch = "make";
					} else {
						storeDispatch = "buy";
					}

					if ("make".equalsIgnoreCase(prototype) && "make".equalsIgnoreCase(seriesProd)
							&& "make".equalsIgnoreCase(storeDispatch)) {
						case1 = case1 || true;

					}
					if ("buy".equalsIgnoreCase(prototype) && "make".equalsIgnoreCase(seriesProd)
							&& "make".equalsIgnoreCase(storeDispatch)) {
						case2 = case2 || true;
					}
					if ("make".equalsIgnoreCase(prototype) && "make".equalsIgnoreCase(seriesProd)
							&& "buy".equalsIgnoreCase(storeDispatch)) {
						case3 = case3 || true;

					}
					if ("buy".equalsIgnoreCase(prototype) && "make".equalsIgnoreCase(seriesProd)
							&& "buy".equalsIgnoreCase(storeDispatch)) {
						case4 = case4 || true;

					}
					if ("make".equalsIgnoreCase(prototype) && "buy".equalsIgnoreCase(seriesProd)
							&& "buy".equalsIgnoreCase(storeDispatch)) {
						case5 = case5 || true;

					}

				}

			} else {
				throw new NoDecisonsFetchedException();
			}

			if (case1 || case2) {
				variant = "1";
			} else if (case3 || case4 || case5) {
				variant = "3";
			}
			System.out.println("Variant is " + variant);
			makeBuyDecisionService.saveDecisionVariant(variant, user);

		} catch (NoDecisonsFetchedException e) {
			logger.info(e.getMessage());
		}

	}

	public int[][][] getLogisticTasksList(String user, String logisticStage) {

		saveVariantForProducts(user, logisticStage);
		initLogisticTasksList();

		return function;
	}

	public int getStageCorrespondingIndex(String stage) {
		switch (stage) {
		case "Pre-seed Stage":
			return 0;
		case "Seed Stage":
			return 1;
		case "Startup:Conception":
			return 2;
		case "Startup:Construction":
			return 3;
		case "Startup:Elaboration":
			return 4;
		case "Startup:Prototyping":
			return 5;
		case "First Stage":
			return 6;
		case "Second Stage":
			return 7;
		case "Third Stage":
			return 8;
		case "Forth Stage":
			return 9;

		}
		return 10;
	}
}
