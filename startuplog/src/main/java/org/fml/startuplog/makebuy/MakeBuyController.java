package org.fml.startuplog.makebuy;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@SessionAttributes("user")
public class MakeBuyController {

	@Autowired
	private MakeBuyService makebuyService;

	private final Logger logger = Logger.getLogger(this.getClass());

	@RequestMapping(value = "/makeorbuy", method = RequestMethod.GET)
	public String showmakebuy(@ModelAttribute("user") String user) {

		return "makebuy";
	}

//	@RequestMapping(value = "/makeorbuy", method = RequestMethod.POST)
//	public String displayMakeBuyQuestions(@ModelAttribute("user") String user) {
//		System.out.println("Inside make or buy");
//		return "makebuy";
//	}

	/**
	 * Triggered when pre-seed stage selected name of input type comes as param
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeorbuy", params = "pre-seed", method = RequestMethod.POST)
	public String pre_seed(SessionStatus sessionStatus) {
		logger.info("Pre-seed stage selected. No logistic actions message is shown");

		return "redirect:/preseed";
	}

	/*
	 * Commented due to the removal of Seed Stage.
	 * 
	 * @RequestMapping(value = "/preseed", method=RequestMethod.GET) public
	 * String showPreseedMessage(SessionStatus sessionStatus) {
	 * 
	 * return "Pre-seed"; }
	 */

	/**
	 * Triggered when seed stage is selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeorbuy", params = "seed", method = RequestMethod.POST)
	public String seed(@ModelAttribute("user") String user, SessionStatus sessionStatus) {

		makebuyService.saveStage("Seed Stage", user);
		logger.info("Seed Stage Saved.");
		return "makebuy";
	}

	/**
	 * Triggered when Startup:Conception stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeorbuy", params = "startup1", method = RequestMethod.POST)
	public String startup1(ModelMap model, @ModelAttribute("user") String user) {

		makebuyService.saveStage("Startup:Conception", user);
		logger.info("Startup:Conception Saved");
		return "makebuy";

	}

	/**
	 * Triggered when Startup:Construction stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeorbuy", params = "startup2", method = RequestMethod.POST)
	public String startup2(ModelMap model, @ModelAttribute("user") String user, SessionStatus sessionStatus) {

		// currentStage = "Startup:Construction";

		makebuyService.saveStage("Startup:Construction", user);
		logger.info("Startup:Conception Saved");
		return "makebuy";

	}

	/**
	 * Triggered when Startup:Elaboration stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeorbuy", params = "startup3", method = RequestMethod.POST)
	public String startup3(ModelMap model, @ModelAttribute("user") String user, SessionStatus sessionStatus) {

		// currentStage = "Startup:Elaboration";

		makebuyService.saveStage("Startup:Elaboration", user);
		logger.info("Startup:Elaboration Saved");
		return "makebuy";
	}

	/**
	 * Triggered when Startup:Prototyping stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeorbuy", params = "startup4", method = RequestMethod.POST)
	public String startup4(ModelMap model, @ModelAttribute("user") String user, SessionStatus sessionStatus) {

		// currentStage = "Startup:Prototyping";

		makebuyService.saveStage("Startup:Prototyping", user);
		logger.info("Startup:Prototyping Saved");
		return "makebuy";
	}

	/**
	 * Triggered when First stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeorbuy", params = "expansion", method = RequestMethod.POST)
	public String first(ModelMap model, @ModelAttribute("user") String user, SessionStatus sessionStatus) {
		// currentStage = "First Stage";

		makebuyService.saveStage("First Stage", user);
		logger.info("First Stage Saved");
		return "makebuy";
	}

	/**
	 * Triggered when Second stage selected
	 * 
	 * @return makebuy.jsp
	 */
	@RequestMapping(value = "/makeorbuy", params = "later", method = RequestMethod.POST)
	public String second(ModelMap model, @ModelAttribute("user") String user) {
		// currentStage = "Second Stage";

		makebuyService.saveStage("Second Stage", user);
		logger.info("Second Stage Saved");
		return "makebuy";
	}

	/*
	 * Commenting due to removal of the stages.
	 * 
	 */

	/**
	 * Triggered when Third stage selected
	 * 
	 * @return makebuy.jsp
	 */

	/*
	 * @RequestMapping(value = "/makeorbuy", params = "third", method =
	 * RequestMethod.POST) public String third(ModelMap
	 * model, @ModelAttribute("user") String user, SessionStatus sessionStatus)
	 * { //currentStage = "Third Stage";
	 * 
	 * makebuyService.saveStage("Third Stage", user);
	 * logger.info("Third Stage Saved"); return "makebuy"; }
	 * 
	 */

	/**
	 * Triggered when Forth stage selected
	 * 
	 * @return makebuy.jsp
	 */

	/*
	 * @RequestMapping(value = "/makeorbuy", params = "forth", method =
	 * RequestMethod.POST) public String forth(ModelMap
	 * model, @ModelAttribute("user") String user, SessionStatus sessionStatus)
	 * { //currentStage = "Forth Stage"; makebuyService.saveStage("Forth Stage",
	 * user); logger.info("Forth Stage Saved"); return "makebuy"; }
	 */
	@RequestMapping(value = "/putVariant", method = RequestMethod.POST)
	public void setVariant(@RequestParam String variant_number, @ModelAttribute("user") String user,
			SessionStatus sessionStatus) {
		makebuyService.saveVariant(variant_number, user);

	}

	@ExceptionHandler(HttpSessionRequiredException.class)
	public String notLoggedIn() {
		return "redirect:login";
	}
}
