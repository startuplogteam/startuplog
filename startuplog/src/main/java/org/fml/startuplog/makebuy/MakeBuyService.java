package org.fml.startuplog.makebuy;

import org.apache.log4j.Logger;
import org.fml.startuplog.service.MasterService;
import org.springframework.stereotype.Service;

@Service
public class MakeBuyService extends MasterService{
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	public void saveStage(String stage, String username)
	{
		try {
			startuplogDAOObject.savePhase(stage, username);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void saveVariant(String variant, String username){
		System.out.println("Variant inside"+Integer.parseInt(variant));
		
		try {
			
			variant = String.valueOf(Integer.parseInt(variant) -1);
			System.out.println("Variant inside 2"+variant);
			startuplogDAOObject.saveVariant(variant, username);
			logger.info("Variant "+ variant + " Saved");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
	}
	
}
