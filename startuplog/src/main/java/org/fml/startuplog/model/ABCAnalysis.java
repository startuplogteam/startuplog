package org.fml.startuplog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="abc_analysis")
public class ABCAnalysis {

	@Id
	private int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(name="material_id")
	private String materialId;
	
	@Column(name="material_name")
	private String materialName;
	
	@Column(name="sales_per_year")
	private int salesPerYear;
	
	@Column(name="price_per_piece")
	private double pricePerPiece;
	
	@Column(name="total_sales")
	private double totalSales;
	
	@Column(name="total_sales_cumulated")
	private double totalSalesCumulated;
	
	@Column(name="total_sales_percent")
	private double totalSalesPercent;
	
	@Column(name="cumulated_percent")
	private double totalSalesCumulatedPercent;
	
	private char category;
	
	private String username;
	
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public int getSalesPerYear() {
		return salesPerYear;
	}
	public void setSalesPerYear(int salesPerYear) {
		this.salesPerYear = salesPerYear;
	}
	public double getPricePerPiece() {
		return pricePerPiece;
	}
	public void setPricePerPiece(double pricePerPiece) {
		this.pricePerPiece = pricePerPiece;
	}
	public double getTotalSales() {
		return totalSales;
	}
	public void setTotalSales(double totalSales) {
		this.totalSales = totalSales;
	}
	public double getTotalSalesCumulated() {
		return totalSalesCumulated;
	}
	public void setTotalSalesCumulated(double totalSalesCumulated) {
		this.totalSalesCumulated = totalSalesCumulated;
	}
	public double getTotalSalesPercent() {
		return totalSalesPercent;
	}
	public void setTotalSalesPercent(double totalSalesPercent) {
		this.totalSalesPercent = totalSalesPercent;
	}
	public double getTotalSalesCumulatedPercent() {
		return totalSalesCumulatedPercent;
	}
	public void setTotalSalesCumulatedPercent(double totalSalesCumulatedPercent) {
		this.totalSalesCumulatedPercent = totalSalesCumulatedPercent;
	}
	public char getCategory() {
		return category;
	}
	public void setCategory(char category) {
		this.category = category;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
