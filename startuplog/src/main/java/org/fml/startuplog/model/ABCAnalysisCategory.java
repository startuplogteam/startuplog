package org.fml.startuplog.model;

import java.util.List;

public class ABCAnalysisCategory {
		
		private List<String> A;
		private List<String> B;
		private List<String> C;
		
		public List<String> getA() {
			return A;
		}
		public void setA(List<String> a) {
			A = a;
		}
		public List<String> getB() {
			return B;
		}
		public void setB(List<String> b) {
			B = b;
		}
		public List<String> getC() {
			return C;
		}
		public void setC(List<String> c) {
			C = c;
		}
		
}