package org.fml.startuplog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="capacity_data")
public class CapacityEstimated {

	@Id
	private int id;
	private int year;
	@Column(name="staff_needed")
	private double staffNeeded;
	@Column(name="machine_needed")
	private double machineNeeded;
	@Column(name="machine_designation")
	private String machineDesignation;
	private String username;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public CapacityEstimated(){
		
	}
	
	public CapacityEstimated(int year,double staffNeeded,double machineNeeded,String machineDesignation){
		this.year = year;
		this.staffNeeded = staffNeeded;
		this.machineNeeded = machineNeeded;
		this.machineDesignation = machineDesignation;
	}
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public double getStaffNeeded() {
		return staffNeeded;
	}
	public void setStaffNeeded(double staffNeeded) {
		this.staffNeeded = staffNeeded;
	}
	public double getMachineNeeded() {
		return machineNeeded;
	}
	public void setMachineNeeded(double machineNeeded) {
		this.machineNeeded = machineNeeded;
	}
	public String getMachineDesignation() {
		return machineDesignation;
	}
	public void setMachineDesignation(String machineDesignation) {
		this.machineDesignation = machineDesignation;
	}
	
	
}
