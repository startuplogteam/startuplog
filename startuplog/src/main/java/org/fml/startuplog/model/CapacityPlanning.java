package org.fml.startuplog.model;

import java.util.List;

public class CapacityPlanning {

	private int WDPerWeek;
	private double WHPerDay;
	private double uncertainityFactor;
	private List<ProductionSteps> productionSteps;
	private List<DemandPlanning> demandPlanning;
	private List<StaffCapacity> staffCapacity;
	private List<MachinesNeeded> machinesNeeded;
	
	public CapacityPlanning(){
		
	}
	
	public CapacityPlanning(int WDPerWeek,int WHPerDay,double uncertainityFactor,List<ProductionSteps> productionSteps,List<DemandPlanning> demandPlanning,List<StaffCapacity> staffCapacity,List<MachinesNeeded> machinesNeeded){
		this.WDPerWeek = WDPerWeek;
		this.WHPerDay = WHPerDay;
		this.uncertainityFactor = uncertainityFactor;
		this.productionSteps = productionSteps;
		this.demandPlanning = demandPlanning;
		this.staffCapacity = staffCapacity;
		this.machinesNeeded = machinesNeeded;
	}

	public int getWDPerWeek() {
		return WDPerWeek;
	}

	public void setWDPerWeek(int wDPerWeek) {
		WDPerWeek = wDPerWeek;
	}

	public double getWHPerDay() {
		return WHPerDay;
	}

	public void setWHPerDay(double wHPerDay) {
		WHPerDay = wHPerDay;
	}

	public double getUncertainityFactor() {
		return uncertainityFactor;
	}

	public void setUncertainityFactor(double uncertainityFactor) {
		this.uncertainityFactor = uncertainityFactor;
	}

	public List<ProductionSteps> getProductionSteps() {
		return productionSteps;
	}

	public void setProductionSteps(List<ProductionSteps> productionSteps) {
		this.productionSteps = productionSteps;
	}

	public List<DemandPlanning> getDemandPlanning() {
		return demandPlanning;
	}

	public void setDemandPlanning(List<DemandPlanning> demandPlanning) {
		this.demandPlanning = demandPlanning;
	}

	public List<StaffCapacity> getStaffCapacity() {
		return staffCapacity;
	}

	public void setStaffCapacity(List<StaffCapacity> staffCapacity) {
		this.staffCapacity = staffCapacity;
	}

	public List<MachinesNeeded> getMachinesNeeded() {
		return machinesNeeded;
	}

	public void setMachinesNeeded(List<MachinesNeeded> machinesNeeded) {
		this.machinesNeeded = machinesNeeded;
	}
	
} 
