package org.fml.startuplog.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "customerdata")
public class Customer {
	
	@Id
	private int id;
	private String material_number;
	private String customer_name;
	private String material_name;
	private String dispatch_packaging_type;
	private String amount_of_items_per_dispatch_packaging;
	private String dispatch_packaging_stacking_height;
	private String material_weight_incl_dispatch_packaging;
	private String dispatch_packaging_length;
	private String dispatch_packaging_width;
	private String dispatch_packaging_height;
	public String getMaterial_number() {
		return material_number;
	}
	public void setMaterial_number(String material_number) {
		this.material_number = material_number;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getDispatch_packaging_type() {
		return dispatch_packaging_type;
	}
	public void setDispatch_packaging_type(String dispatch_packaging_type) {
		this.dispatch_packaging_type = dispatch_packaging_type;
	}
	public String getAmount_of_items_per_dispatch_packaging() {
		return amount_of_items_per_dispatch_packaging;
	}
	public void setAmount_of_items_per_dispatch_packaging(String amount_of_items_per_dispatch_packaging) {
		this.amount_of_items_per_dispatch_packaging = amount_of_items_per_dispatch_packaging;
	}
	public String getDispatch_packaging_stacking_height() {
		return dispatch_packaging_stacking_height;
	}
	public void setDispatch_packaging_stacking_height(String dispatch_packaging_stacking_height) {
		this.dispatch_packaging_stacking_height = dispatch_packaging_stacking_height;
	}
	public String getMaterial_weight_incl_dispatch_packaging() {
		return material_weight_incl_dispatch_packaging;
	}
	public void setMaterial_weight_incl_dispatch_packaging(String material_weight_incl_dispatch_packaging) {
		this.material_weight_incl_dispatch_packaging = material_weight_incl_dispatch_packaging;
	}
	public String getDispatch_packaging_length() {
		return dispatch_packaging_length;
	}
	public void setDispatch_packaging_length(String dispatch_packaging_length) {
		this.dispatch_packaging_length = dispatch_packaging_length;
	}
	public String getDispatch_packaging_width() {
		return dispatch_packaging_width;
	}
	public void setDispatch_packaging_width(String dispatch_packaging_width) {
		this.dispatch_packaging_width = dispatch_packaging_width;
	}
	public String getDispatch_packaging_height() {
		return dispatch_packaging_height;
	}
	public void setDispatch_packaging_height(String dispatch_packaging_height) {
		this.dispatch_packaging_height = dispatch_packaging_height;
	}
	public String getMaterial_name() {
		return material_name;
	}
	public void setMaterial_name(String material_name) {
		this.material_name = material_name;
	}
	
	


}
