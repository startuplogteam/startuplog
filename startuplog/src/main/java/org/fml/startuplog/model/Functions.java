package org.fml.startuplog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="startuplog_functions")
public class Functions {

	public Functions() {

	}

	@Id
	@Column(name="function_id")
	private int functionID;
	
	@Column(name="function_handle")
	private String functionHandle;
	
	@Column(name="function_name")
	private String functionName;
	
	@Column(name="function_class")
	private String functionClass;

	public int getFunctionID() {
		return functionID;
	}

	public void setFunctionID(int functionID) {
		this.functionID = functionID;
	}

	public String getFunctionHandle() {
		return functionHandle;
	}

	public void setFunctionHandle(String functionHandle) {
		this.functionHandle = functionHandle;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getFunctionClass() {
		return functionClass;
	}

	public void setFunctionClass(String functionClass) {
		this.functionClass = functionClass;
	}

}
