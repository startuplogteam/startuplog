package org.fml.startuplog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="influencing_factors")
public class InfluencingFactors {

	@Id
	@Column(name="user_name")
	private String userName;
	@Column(name="core_competency")
	private String competency;
	@Column(name="product_type")
	private String productType;
	@Column(name="distribution_channel")
	private String distribution;
	@Column(name="product_nature")
	private String productNature;
	@Column(name="sales_target")
	private double salesTarget;
	@Column(name="sales_current")
	private double salesCurrent;
	@Column(name="customers")
	private double customers;
	@Column(name="logistics_budget")
	private double logisticsBudget;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCompetency() {
		return competency;
	}
	public void setCompetency(String competency) {
		this.competency = competency;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getDistribution() {
		return distribution;
	}
	public void setDistribution(String distribution) {
		this.distribution = distribution;
	}
	public String getProductNature() {
		return productNature;
	}
	public void setProductNature(String productNature) {
		this.productNature = productNature;
	}
	public double getSalesTarget() {
		return salesTarget;
	}
	public void setSalesTarget(double salesTarget) {
		this.salesTarget = salesTarget;
	}
	public double getSalesCurrent() {
		return salesCurrent;
	}
	public void setSalesCurrent(double salesCurrent) {
		this.salesCurrent = salesCurrent;
	}
	public double getCustomers() {
		return customers;
	}
	public void setCustomers(double customers) {
		this.customers = customers;
	}
	public double getLogisticsBudget() {
		return logisticsBudget;
	}
	public void setLogisticsBudget(double logisticsBudget) {
		this.logisticsBudget = logisticsBudget;
	}
	
	
}