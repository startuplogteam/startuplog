package org.fml.startuplog.model;

public class MachineCapacity {

	private int year;
	private double neededMachines;
	
	public MachineCapacity(){
		
	}
	
	public MachineCapacity(int year, double neededMachines){
		this.year = year;
		this.neededMachines = neededMachines;
	}
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public double getNeededMachines() {
		return neededMachines;
	}
	public void setNeededMachines(double neededMachines) {
		this.neededMachines = neededMachines;
	}	
}
