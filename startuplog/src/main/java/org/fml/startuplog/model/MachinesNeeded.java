package org.fml.startuplog.model;

import java.util.List;

public class MachinesNeeded {

	private String machineDesignation;
	private List<MachineCapacity> machineCapacity;
	
	public MachinesNeeded(){
		
	}
	
	public MachinesNeeded(String machineDesignation,List<MachineCapacity> machineCapacity){
		this.machineDesignation = machineDesignation;
		this.machineCapacity = machineCapacity;
	}
	
	public String getMachineDesignation() {
		return machineDesignation;
	}
	public void setMachineDesignation(String machineDesignation) {
		this.machineDesignation = machineDesignation;
	}
	public List<MachineCapacity> getMachineCapacity() {
		return machineCapacity;
	}
	public void setMachineCapacity(List<MachineCapacity> machineCapacity) {
		this.machineCapacity = machineCapacity;
	}
	
}
