package org.fml.startuplog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(ProductId.class)
@Table(name = "makebuydecision")
public class MakeBuyDecision {
	@Id
	@Column(name = "product")
	private String productName;

	@Id
	@Column(name = "logistics_stage")
	private String logisticStage;

	@Id
	@Column(name = "user_name")
	private String userName;

	@Column(name = "prototype")
	private String prototype;

	@Column(name = "series_product")
	private String seriesProd;

	@Column(name = "storage_dispatch")
	private String storageDispatch;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getLogisticStage() {
		return logisticStage;
	}

	public void setLogisticStage(String logisticStage) {
		this.logisticStage = logisticStage;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPrototype() {
		return prototype;
	}

	public void setPrototype(String prototype) {
		this.prototype = prototype;
	}

	public String getSeriesProd() {
		return seriesProd;
	}

	public void setSeriesProd(String seriesProd) {
		this.seriesProd = seriesProd;
	}

	public String getStorageDispatch() {
		return storageDispatch;
	}

	public void setStorageDispatch(String storageDispatch) {
		this.storageDispatch = storageDispatch;
	}

}
