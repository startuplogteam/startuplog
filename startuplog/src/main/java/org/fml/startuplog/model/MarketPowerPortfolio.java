package org.fml.startuplog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "market_portfolio")
public class MarketPowerPortfolio {
	@Id
	private int id;

	@Column(name = "supplier_id")
	private String supplier_id;

	@Column(name = "criteria1")
	private String criteria_1;

	@Column(name = "criteria2")
	private String criteria_2;

	@Column(name = "classification")
	private String classification;

	@Column(name = "username")
	private String username;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSupplier_id() {
		return supplier_id;
	}

	public void setSupplier_id(String supplier_id) {
		this.supplier_id = supplier_id;
	}

	public String getCriteria_1() {
		return criteria_1;
	}

	public void setCriteria_1(String criteria_1) {
		this.criteria_1 = criteria_1;
	}

	public String getCriteria_2() {
		return criteria_2;
	}

	public void setCriteria_2(String criteria_2) {
		this.criteria_2 = criteria_2;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
