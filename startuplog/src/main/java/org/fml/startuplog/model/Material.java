package org.fml.startuplog.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mastertable")
public class Material {

	// primary key
	@Id
	private String material_number;

	// Given data about material
	private String material_name;
	private String material_type;
	private String material_unit;
	private double material_weight;
	private int material_length;
	private int material_width;
	private int material_height;
	private String make_buy; 


	public Material() {

	}

	public String getMake_buy() {
		return make_buy;
	}

	public void setMake_buy(String make_buy) {
		this.make_buy = make_buy;
	}

	public Material(String materialNumber, String materialName) {
		this.material_number = materialNumber;
		this.material_name = materialName;
	}

	public String getMaterial_number() {
		return material_number;
	}

	public void setMaterial_number(String material_number) {
		this.material_number = material_number;
	}

	public String getMaterial_name() {
		return material_name;
	}

	public void setMaterial_name(String material_name) {
		this.material_name = material_name;
	}

	public String getMaterial_type() {
		return material_type;
	}

	public void setMaterial_type(String material_type) {
		this.material_type = material_type;
	}

	public String getMaterial_unit() {
		return material_unit;
	}

	public void setMaterial_unit(String material_unit) {
		this.material_unit = material_unit;
	}

	public double getMaterial_weight() {
		return material_weight;
	}

	public void setMaterial_weight(double material_weight) {
		this.material_weight = material_weight;
	}

	public int getMaterial_length() {
		return material_length;
	}

	public void setMaterial_length(int material_length) {
		this.material_length = material_length;
	}

	public int getMaterial_width() {
		return material_width;
	}

	public void setMaterial_width(int material_width) {
		this.material_width = material_width;
	}

	public int getMaterial_height() {
		return material_height;
	}

	public void setMaterial_height(int material_height) {
		this.material_height = material_height;
	}

}