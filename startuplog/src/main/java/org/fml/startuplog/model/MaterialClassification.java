package org.fml.startuplog.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="material_classification")
public class MaterialClassification {
	@Id
	private int id;
	private String material_number;
	private String supply_risk;
	private String further_criteria;
	private String further_criteria_value;
	private String classification;
	private String username;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMaterial_number() {
		return material_number;
	}
	public void setMaterial_number(String material_number) {
		this.material_number = material_number;
	}
	public String getSupply_risk() {
		return supply_risk;
	}
	public void setSupply_risk(String supply_risk) {
		this.supply_risk = supply_risk;
	}
	public String getFurther_criteria() {
		return further_criteria;
	}
	public void setFurther_criteria(String further_criteria) {
		this.further_criteria = further_criteria;
	}
	public String getFurther_criteria_value() {
		return further_criteria_value;
	}
	public void setFurther_criteria_value(String further_criteria_value) {
		this.further_criteria_value = further_criteria_value;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

}
