package org.fml.startuplog.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="procurement")
public class Procurement {
	@Id
	private String material_number;
	// procurement
	private String supplier_number_strategy;
	private String market_field_strategy;
	private String supply_strategy;
	
	private String market_research_result;
	private String  product_feature;
	private String  predictability_procurement;
	private String  flexibility;
	private String complexity;
	private String  subcription_price;
	private String  order_and_transaction_costs;
	
	private String  transport_costs;
	private String  storage_and_capital_costs;
	private String  procurement_time;
	
	private String  risk_by_supplier_failure;
	private String  transport_risk;
	private String  currency_risk;
	private String  exchange_rate_risk;
	private String  customs_problem_risk;
	private String  dependence_on_supplier;
	private String  number_of_suppliers_and_contacts;
	private String  expenditure_on_bureaucracy;
	private String  language;
	private String  currency;
	private String  exchange_rate_utilization;
	
	public String getMarket_research_result() {
		return market_research_result;
	}
	public void setMarket_research_result(String market_research_result) {
		this.market_research_result = market_research_result;
	}
	public String getProduct_feature() {
		return product_feature;
	}
	public void setProduct_feature(String product_feature) {
		this.product_feature = product_feature;
	}
	public String getPredictability_procurement() {
		return predictability_procurement;
	}
	public void setPredictability_procurement(String predictability_procurement) {
		this.predictability_procurement = predictability_procurement;
	}
	public String getFlexibility() {
		return flexibility;
	}
	public void setFlexibility(String flexibility) {
		this.flexibility = flexibility;
	}
	public String getComplexity() {
		return complexity;
	}
	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}
	public String getSubcription_price() {
		return subcription_price;
	}
	public void setSubcription_price(String subcription_price) {
		this.subcription_price = subcription_price;
	}
	public String getOrder_and_transaction_costs() {
		return order_and_transaction_costs;
	}
	public void setOrder_and_transaction_costs(String order_and_transaction_costs) {
		this.order_and_transaction_costs = order_and_transaction_costs;
	}
	public String getTransport_costs() {
		return transport_costs;
	}
	public void setTransport_costs(String transport_costs) {
		this.transport_costs = transport_costs;
	}
	public String getStorage_and_capital_costs() {
		return storage_and_capital_costs;
	}
	public void setStorage_and_capital_costs(String storage_and_capital_costs) {
		this.storage_and_capital_costs = storage_and_capital_costs;
	}
	public String getProcurement_time() {
		return procurement_time;
	}
	public void setProcurement_time(String procurement_time) {
		this.procurement_time = procurement_time;
	}
	public String getRisk_by_supplier_failure() {
		return risk_by_supplier_failure;
	}
	public void setRisk_by_supplier_failure(String risk_by_supplier_failure) {
		this.risk_by_supplier_failure = risk_by_supplier_failure;
	}
	public String getTransport_risk() {
		return transport_risk;
	}
	public void setTransport_risk(String transport_risk) {
		this.transport_risk = transport_risk;
	}
	public String getCurrency_risk() {
		return currency_risk;
	}
	public void setCurrency_risk(String currency_risk) {
		this.currency_risk = currency_risk;
	}
	public String getExchange_rate_risk() {
		return exchange_rate_risk;
	}
	public void setExchange_rate_risk(String exchange_rate_risk) {
		this.exchange_rate_risk = exchange_rate_risk;
	}
	public String getCustoms_problem_risk() {
		return customs_problem_risk;
	}
	public void setCustoms_problem_risk(String customs_problem_risk) {
		this.customs_problem_risk = customs_problem_risk;
	}
	public String getDependence_on_supplier() {
		return dependence_on_supplier;
	}
	public void setDependence_on_supplier(String dependence_on_supplier) {
		this.dependence_on_supplier = dependence_on_supplier;
	}
	public String getNumber_of_suppliers_and_contacts() {
		return number_of_suppliers_and_contacts;
	}
	public void setNumber_of_suppliers_and_contacts(String number_of_suppliers_and_contacts) {
		this.number_of_suppliers_and_contacts = number_of_suppliers_and_contacts;
	}
	public String getExpenditure_on_bureaucracy() {
		return expenditure_on_bureaucracy;
	}
	public void setExpenditure_on_bureaucracy(String expenditure_on_bureaucracy) {
		this.expenditure_on_bureaucracy = expenditure_on_bureaucracy;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getExchange_rate_utilization() {
		return exchange_rate_utilization;
	}
	public void setExchange_rate_utilization(String exchange_rate_utilization) {
		this.exchange_rate_utilization = exchange_rate_utilization;
	}
	public String getMaterial_number() {
		return material_number;
	}
	public void setMaterial_number(String material_number) {
		this.material_number = material_number;
	}
	public String getSupplier_number_strategy() {
		return supplier_number_strategy;
	}
	public void setSupplier_number_strategy(String supplier_number_strategy) {
		this.supplier_number_strategy = supplier_number_strategy;
	}
	public String getMarket_field_strategy() {
		return market_field_strategy;
	}
	public void setMarket_field_strategy(String market_field_strategy) {
		this.market_field_strategy = market_field_strategy;
	}
	public String getSupply_strategy() {
		return supply_strategy;
	}
	public void setSupply_strategy(String supply_strategy) {
		this.supply_strategy = supply_strategy;
	}

}
