package org.fml.startuplog.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

public class ProductId implements Serializable {
	@Id
	@Column(name = "product")
	private String productName;

	@Id
	@Column(name = "logistics_stage")
	private String logisticStage;

	@Id
	@Column(name = "user_name")
	private String userName;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getLogisticStage() {
		return logisticStage;
	}

	public void setLogisticStage(String logisticStage) {
		this.logisticStage = logisticStage;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int hashCode() {
		return 0;

	}

	public boolean equals(Object O) {
		return false;

	}
}
