package org.fml.startuplog.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "production_steps")
public class ProductionSteps {

	@Id
	private int id;
	private int stepNumber;
	private String description;
	private double TNForMachine;
	private double TNForStaff;
	private String machineDesignation;
	private String username;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public ProductionSteps(){
		
	}
	
	public ProductionSteps(int stepNumber,String description,double TNForMachine,double TNForStaff,String machineDesignation){
		this.stepNumber = stepNumber;
		this.description = description;
		this.TNForMachine = TNForMachine;
		this.TNForStaff = TNForStaff;
		this.machineDesignation = machineDesignation;
	}
	
	public int getStepNumber() {
		return stepNumber;
	}
	public void setStepNumber(int stepNumber) {
		this.stepNumber = stepNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getTNForMachine() {
		return TNForMachine;
	}
	public void setTNForMachine(double tNForMachine) {
		TNForMachine = tNForMachine;
	}
	public double getTNForStaff() {
		return TNForStaff;
	}
	public void setTNForStaff(double tNForStaff) {
		TNForStaff = tNForStaff;
	}
	public String getMachineDesignation() {
		return machineDesignation;
	}
	public void setMachineDesignation(String machineDesignation) {
		this.machineDesignation = machineDesignation;
	}
	
	
}
