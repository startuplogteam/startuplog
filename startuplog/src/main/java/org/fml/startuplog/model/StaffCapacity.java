package org.fml.startuplog.model;

public class StaffCapacity {

	private int year;
	private double staffsNeeded;

	public StaffCapacity(){
		
	}
	
	public StaffCapacity(int year,double staffsNeeded){
		this.year = year;
		this.staffsNeeded = staffsNeeded;
	}
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public double getStaffsNeeded() {
		return staffsNeeded;
	}
	public void setStaffsNeeded(double staffsNeeded) {
		this.staffsNeeded = staffsNeeded;
	}
	
	
}
