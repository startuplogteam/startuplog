package org.fml.startuplog.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "storagefacility")
public class StorageFacility {
	// primary key
	@Id
	private String material_number;
	private String storage_posibility; // changed
	
	// storage unit
	private String storage_in_delivery_packaging; // changed
	private String storage_in_dispatch_packaging; // changed
	private String storage_packaging_type;
	private String are_storage_unit_stackable;
	private String storage_packaging_stacking_height;
	private String amount_of_items_per_storage_packaging;
	private String material_weight_incl_storage_packaging;
	private String storage_packaging_length;
	private String storage_packaging_width;
	private String storage_packaging_height;

	// storage strategy
	private String storage_bin_assignment;
	private String storage_allocation;
	private String storage_removal_strategy;

	// further questions
	private String material_quantity;
	private String order_picking;
	private String mech_auto_possibility;
	// final decision
	private String storage_facility;

	// flag to detect all info given
	private String isAllInfoGiven;
	
	public String getMaterial_number() {
		return material_number;
	}

	public void setMaterial_number(String material_number) {
		this.material_number = material_number;
	}

	public String getStorage_posibility() {
		return storage_posibility;
	}

	public void setStorage_posibility(String storage_posibility) {
		this.storage_posibility = storage_posibility;
	}

	public String getStorage_in_delivery_packaging() {
		return storage_in_delivery_packaging;
	}

	public void setStorage_in_delivery_packaging(String storage_in_delivery_packaging) {
		this.storage_in_delivery_packaging = storage_in_delivery_packaging;
	}

	public String getStorage_in_dispatch_packaging() {
		return storage_in_dispatch_packaging;
	}

	public void setStorage_in_dispatch_packaging(String storage_in_dispatch_packaging) {
		this.storage_in_dispatch_packaging = storage_in_dispatch_packaging;
	}

	public String getStorage_packaging_type() {
		return storage_packaging_type;
	}

	public void setStorage_packaging_type(String storage_packaging_type) {
		this.storage_packaging_type = storage_packaging_type;
	}

	public String getAmount_of_items_per_storage_packaging() {
		return amount_of_items_per_storage_packaging;
	}

	public void setAmount_of_items_per_storage_packaging(String amount_of_items_per_storage_packaging) {
		this.amount_of_items_per_storage_packaging = amount_of_items_per_storage_packaging;
	}

	public String getStorage_packaging_stacking_height() {
		return storage_packaging_stacking_height;
	}

	public void setStorage_packaging_stacking_height(String storage_packaging_stacking_height) {
		this.storage_packaging_stacking_height = storage_packaging_stacking_height;
	}

	public String getMaterial_weight_incl_storage_packaging() {
		return material_weight_incl_storage_packaging;
	}

	public void setMaterial_weight_incl_storage_packaging(String material_weight_incl_storage_packaging) {
		this.material_weight_incl_storage_packaging = material_weight_incl_storage_packaging;
	}

	public String getStorage_packaging_length() {
		return storage_packaging_length;
	}

	public void setStorage_packaging_length(String storage_packaging_length) {
		this.storage_packaging_length = storage_packaging_length;
	}

	public String getStorage_packaging_width() {
		return storage_packaging_width;
	}

	public void setStorage_packaging_width(String storage_packaging_width) {
		this.storage_packaging_width = storage_packaging_width;
	}

	public String getStorage_packaging_height() {
		return storage_packaging_height;
	}

	public void setStorage_packaging_height(String storage_packaging_height) {
		this.storage_packaging_height = storage_packaging_height;
	}

	public String getStorage_bin_assignment() {
		return storage_bin_assignment;
	}

	public void setStorage_bin_assignment(String storage_bin_assignment) {
		this.storage_bin_assignment = storage_bin_assignment;
	}

	public String getStorage_allocation() {
		return storage_allocation;
	}

	public void setStorage_allocation(String storage_allocation) {
		this.storage_allocation = storage_allocation;
	}

	public String getStorage_removal_strategy() {
		return storage_removal_strategy;
	}

	public void setStorage_removal_strategy(String storage_removal_strategy) {
		this.storage_removal_strategy = storage_removal_strategy;
	}

	public String getMaterial_quantity() {
		return material_quantity;
	}

	public void setMaterial_quantity(String material_quantity) {
		this.material_quantity = material_quantity;
	}

	public String getOrder_picking() {
		return order_picking;
	}

	public void setOrder_picking(String order_picking) {
		this.order_picking = order_picking;
	}

	public String getMech_auto_possibility() {
		return mech_auto_possibility;
	}

	public void setMech_auto_possibility(String mech_auto_possibility) {
		this.mech_auto_possibility = mech_auto_possibility;
	}

	public String getStorage_facility() {
		return storage_facility;
	}

	public void setStorage_facility(String storage_facility) {
		this.storage_facility = storage_facility;
	}

	public String getIsAllInfoGiven() {
		return isAllInfoGiven;
	}

	public void setIsAllInfoGiven(String isAllInfoGiven) {
		this.isAllInfoGiven = isAllInfoGiven;
	}

	public String getAre_storage_unit_stackable() {
		return are_storage_unit_stackable;
	}
 
	public void setAre_storage_unit_stackable(String are_storage_unit_stackable) {
		this.are_storage_unit_stackable = are_storage_unit_stackable;
	}



}
