package org.fml.startuplog.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "supplierdata")
public class Supplier {
	@Id
	private int id;
	private String material_number;

	private String supplier_name;

	private String delivery_packaging_type;
	private String amount_of_items_per_delivery_packaging;
	private String delivery_packaging_stacking_height;
	private String material_weight_incl_delivery_packaging;
	private String delivery_packaging_length;
	private String delivery_packaging_width;
	private String delivery_packaging_height;
	private String material_name;

	public String getMaterial_number() {
		return material_number;
	}

	public void setMaterial_number(String material_number) {
		this.material_number = material_number;
	}

	public String getSupplier_name() {
		return supplier_name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSupplier_name(String supplier_name) {
		this.supplier_name = supplier_name;
	}

	public String getDelivery_packaging_type() {
		return delivery_packaging_type;
	}

	public void setDelivery_packaging_type(String delivery_packaging_type) {
		this.delivery_packaging_type = delivery_packaging_type;
	}

	public String getAmount_of_items_per_delivery_packaging() {
		return amount_of_items_per_delivery_packaging;
	}

	public void setAmount_of_items_per_delivery_packaging(String amount_of_items_per_delivery_packaging) {
		this.amount_of_items_per_delivery_packaging = amount_of_items_per_delivery_packaging;
	}

	public String getDelivery_packaging_stacking_height() {
		return delivery_packaging_stacking_height;
	}

	public void setDelivery_packaging_stacking_height(String delivery_packaging_stacking_height) {
		this.delivery_packaging_stacking_height = delivery_packaging_stacking_height;
	}

	public String getMaterial_weight_incl_delivery_packaging() {
		return material_weight_incl_delivery_packaging;
	}

	public void setMaterial_weight_incl_delivery_packaging(String material_weight_incl_delivery_packaging) {
		this.material_weight_incl_delivery_packaging = material_weight_incl_delivery_packaging;
	}

	public String getDelivery_packaging_length() {
		return delivery_packaging_length;
	}

	public void setDelivery_packaging_length(String delivery_packaging_length) {
		this.delivery_packaging_length = delivery_packaging_length;
	}

	public String getDelivery_packaging_width() {
		return delivery_packaging_width;
	}

	public void setDelivery_packaging_width(String delivery_packaging_width) {
		this.delivery_packaging_width = delivery_packaging_width;
	}

	public String getDelivery_packaging_height() {
		return delivery_packaging_height;
	}

	public void setDelivery_packaging_height(String delivery_packaging_height) {
		this.delivery_packaging_height = delivery_packaging_height;
	}

	public String getMaterial_name() {
		return material_name;
	}

	public void setMaterial_name(String material_name) {
		this.material_name = material_name;
	}

}
