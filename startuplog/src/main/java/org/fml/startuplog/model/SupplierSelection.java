package org.fml.startuplog.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="supplier_selection")
public class SupplierSelection {

	@Id
	private int id;
	

	private String criteria;
	
	@Column(name="supplier_name")
	private String supplierName;
	
	@Column(name="material_number")
	private String materialNumber;

	private int value;
	
	private char selected;
	
	private String username;
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getMaterialNumber() {
		return materialNumber;
	}

	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	public char getSelected() {
		return selected;
	}
	public void setSelected(char selected) {
		this.selected = selected;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
