package org.fml.startuplog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "worktime_data")
public class WorkingTime {
	
	@Id
	private int id;
	@Column(name="workid")
	private int workTimeId;
	@Column(name="workdays_per_week")
	private int WDPerWeek;
	@Column(name="workhours_per_day")
	private double WHPerDay;
	private double uncertainityFactor;
	private String username;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public WorkingTime(){
		
	}
	public int getWorkTimeId() {
		return workTimeId;
	}
	public void setWorkTimeId(int workTimeId) {
		this.workTimeId = workTimeId;
	}
	public int getWDPerWeek() {
		return WDPerWeek;
	}
	public void setWDPerWeek(int wDPerWeek) {
		WDPerWeek = wDPerWeek;
	}
	public double getWHPerDay() {
		return WHPerDay;
	}
	public void setWHPerDay(double wHPerDay) {
		WHPerDay = wHPerDay;
	}
	public double getUncertainityFactor() {
		return uncertainityFactor;
	}
	public void setUncertainityFactor(double uncertainityFactor) {
		this.uncertainityFactor = uncertainityFactor;
	}	

}
