package org.fml.startuplog.service;

import java.util.List;

import org.fml.startuplog.dao.StartuplogDAO;
import org.fml.startuplog.model.Functions;
import org.fml.startuplog.model.PhaseModel;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class MasterService {
	@Autowired
	protected StartuplogDAO startuplogDAOObject;
	public PhaseModel loadLastestPhaseModel(String username)
	{
		return startuplogDAOObject.loadLatestPhaseModel(username);
	}
	
	public List<Functions> getAllFunctions()
	{
		return startuplogDAOObject.getAllFunctions();
	}
	
}
