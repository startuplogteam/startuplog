package org.fml.startuplog.stage;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.fml.startuplog.model.PhaseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@SessionAttributes("user")
public class StageController{
	
	@Autowired
	private StageService stageService;
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	/**
	 * Calculate stage from answers of participant on indicators POST method
	 * 
	 * @return stages.jsp
	 */
	@RequestMapping(value = "/stages", method = RequestMethod.POST)
	public String getStageFromIndicators(HttpServletRequest request, ModelMap model, SessionStatus sessionStatus) {
		
		
		
		int[] suggestedStages = stageService.getStage(request);
		System.out.println("Phase Model"+ suggestedStages[0]+"Hi"+ suggestedStages[1]);
		for (int i = 0; i < suggestedStages.length; i++) {
			// suggestion is passed to view to indicate suggested stages
			// this is used as css class to show a bulb
			if (suggestedStages[i] == 1) {
				int j=i;
				j++;
				model.put("suggestion" + j, "suggestion");
			}

		}
		return "stages";
	}
	
	/**
	 * Calculate stage from answers of participant on indicators POST method
	 * 
	 * @return stages.jsp
	 */
	@RequestMapping(value = "/stages", method = RequestMethod.GET)
	public String getPreviousStage(@ModelAttribute("user") String user, HttpServletRequest request, ModelMap model) {
		
		
		PhaseModel phaseModel = stageService.loadLastestPhaseModel(user);
		if (phaseModel != null){
			model.put("suggestion" + stageService.stageIndexFromString(phaseModel.getStage()), "suggestion");
			return "stages";
		}
		else{
			return "redirect:indicators";
		}
	}

	/**
	 * Back to previous page
	 * 
	 * @return redirect to login
	 */
	@RequestMapping(value = "/stages", params="back", method = RequestMethod.POST)
	public String backFromIndicators(@ModelAttribute("user") String user) {
		logger.info("Backed to login from indicators");
		return "redirect:login";
	}
	
	@ExceptionHandler (HttpSessionRequiredException.class)
	public String notLoggedIn(){
		return "redirect:login";
	}

}
