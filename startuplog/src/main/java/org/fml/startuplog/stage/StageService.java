package org.fml.startuplog.stage;

import java.util.Arrays;
import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.fml.startuplog.service.MasterService;
import org.springframework.stereotype.Service;

@Service
public class StageService extends MasterService {

	public StageService() {
		mapIndicatorToStage();
	}

	private int[] allStages = new int[4];
	private Map<String, String[]> paramMap;
	private Map<String, Integer> indicatorMap = new HashMap<>();
	private final Logger logger = Logger.getLogger(this.getClass());

	public void mapIndicatorToStage() {

		// Handle indicator : Role of founder.
		indicatorMap.put("rf_1", 0);
		indicatorMap.put("rf_2", 1);
		indicatorMap.put("rf_3", 2);
		indicatorMap.put("rf_4", 3);

		// Handle indicator : Standardization.
		indicatorMap.put("st_1", 0);
		indicatorMap.put("st_2", 1);
		indicatorMap.put("st_3", 2);
		indicatorMap.put("st_4", 3);
		indicatorMap.put("st_5", 3);

		// Handle indicator : Organizational Structures.
		indicatorMap.put("os_1", 0);
		indicatorMap.put("os_2", 1);
		indicatorMap.put("os_3", 2);
		indicatorMap.put("os_4", 3);

		// Handle indicator : Market Uncertainity.
		indicatorMap.put("mu_1", 0);
		indicatorMap.put("mu_2", 1);
		indicatorMap.put("mu_3", 2);
		indicatorMap.put("mu_4", 3);

		// Handle indicator : Technology Readiness Level.
		indicatorMap.put("tr_1", 0);
		indicatorMap.put("tr_2", 0);
		indicatorMap.put("tr_3", 0);
		indicatorMap.put("tr_4", 1);
		indicatorMap.put("tr_5", 1);
		indicatorMap.put("tr_6", 2);
		indicatorMap.put("tr_7", 3);

		// Handle indicator : Sales / Customer Benefits.
		indicatorMap.put("sb_1", 0);
		indicatorMap.put("sb_2", 1);
		indicatorMap.put("sb_3", 2);
		indicatorMap.put("sb_4", 3);

		// Handle indicator : Marketing and sales activities.
		indicatorMap.put("ms_1", 0);
		indicatorMap.put("ms_2", 0);
		indicatorMap.put("ms_3", 1);
		indicatorMap.put("ms_4", 1);
		indicatorMap.put("ms_5", 4);
		indicatorMap.put("ms_6", 4);
	}

	public int[] getStage(HttpServletRequest request) {
		paramMap = request.getParameterMap();

		if (paramMap.isEmpty()) {
			logger.error("Empty request placed in : " + getClass());
		} else {

			for (Map.Entry<String, Integer> entry : indicatorMap.entrySet()) {
				String indicatorKey = entry.getKey();
				Integer index = entry.getValue();

				if (paramMap.containsKey(indicatorKey)) {
					if ((indicatorKey.equalsIgnoreCase("ms_5")) || (indicatorKey.equalsIgnoreCase("ms_6"))) {
						allStages[2] += 1;
						allStages[3] += 1;
					}

					else {
						allStages[index] += 1;
					}
				}
			}
		}

		logger.info("Successfully populated stages. Suggestions available" + java.util.Arrays.toString(allStages));

		return getQualifiedStages(allStages);

	}

	/**
	 * @param stageArray
	 * @return stageArray
	 * 
	 *         Returns the suggested development stage based on the ranking of
	 *         the parameters from the stage array containing the indicator
	 *         selections.
	 */
	public int[] getQualifiedStages(int[] stageArray) {
		IntSummaryStatistics stat = Arrays.stream(stageArray).summaryStatistics();

		int max = stat.getMax();
		if (max != 0) {
			for (int i = 0; i < stageArray.length; i++) {
				if (stageArray[i] == max) {
					stageArray[i] = 1;
				} else {
					stageArray[i] = 0;
				}
			}
		}

		return stageArray;
	}

	public int stageIndexFromString(String stage) {
		switch (stage) {
		case "Seed Stage":
			return 1;
		case "Startup:Conception":
			return 2;
		case "Startup:Construction":
			return 3;
		case "Startup:Elaboration":
			return 4;
		case "Startup:Prototyping":
			return 5;
		case "Expansion Stage":
			return 6;
		case "Later Stage":
			return 7;
		default:
			return 100;
		}
	}

}
