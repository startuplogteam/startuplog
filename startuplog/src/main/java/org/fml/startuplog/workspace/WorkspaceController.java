package org.fml.startuplog.workspace;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.fml.startuplog.logisticstask.LogisticsTaskService;
import org.fml.startuplog.model.Functions;
import org.fml.startuplog.model.PhaseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
public class WorkspaceController {

	@Autowired
	@Qualifier("messageSource")
	private MessageSource messageSource;

	@Autowired
	private LogisticsTaskService logicsTaskService;
	
	@Autowired
	private org.fml.startuplog.functions.influencingfactors.InfluencingFactorsController influencingFactorsController;

	private final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Show workspace.jsp Saves list of selected functions to be shown in the
	 * list
	 */
//	@RequestMapping(value = "/workspace", method = RequestMethod.POST)
//	public String workSpace(HttpServletRequest request, ModelMap model) {
//
//		List<String> wareHouseLogistics = new ArrayList<>();
//		List<String> procuremenLogistics = new ArrayList<>();
//		List<String> general = new ArrayList<>();
//
//		Map<String, String[]> paramMap = request.getParameterMap();
//
//		if (paramMap.size() == 0) {
//			logger.info("No logistic tasks available");
//		}
//		if (paramMap.containsKey("initial_planning")) {
//			logger.info("Storage strategy is avialalbe at Workspace");
//			wareHouseLogistics.add(paramMap.get("initial_planning")[0]);
//		}
//		if (paramMap.containsKey("procurement")) {
//			logger.info("Procurement is avialalbe at Workspace");
//			procuremenLogistics.add(paramMap.get("procurement")[0]);
//		}
//		if (paramMap.containsKey("materialclassification")) {
//			logger.info(" Supply risk  is avialalbe at Workspace");
//			procuremenLogistics.add(paramMap.get("materialclassification")[0]);
//		}
//
//		if (paramMap.containsKey("supplierselection")) {
//			logger.info("Supplier selection is avialalbe at Workspace");
//			procuremenLogistics.add(paramMap.get("supplierselection")[0]);
//		}
//
//		if (paramMap.containsKey("abc_analysis")) {
//			logger.info("Abc is avialalbe at Workspace");
//			general.add(paramMap.get("abc_analysis")[0]);
//		}
//
//		if (wareHouseLogistics.size() == 0) {
//			logger.info("No warehouse logistic tasks avaiable at Workspace");
//		}
//
//		if (procuremenLogistics.size() == 0) {
//			logger.info("No procurement logistic tasks avaiable at Workspace");
//		}
//
//		if (general.size() == 0) {
//			logger.info("No general tasks avaiable at Workspace");
//		}
//		model.put("wareHousePlanningList", wareHouseLogistics);
//
//		model.put("procurementStrategyList", procuremenLogistics);
//
//		model.put("generalList", general);
//
//		return "workspace";
//	}

	/*
	 * public List <String> getProcurementLogisticsTasksList(){
	 * 
	 * }
	 */
	/**
	 * Show workspace.jsp Saves list of selected functions to be shown in the
	 * list
	 */
	@RequestMapping(value = "/workspace", method = RequestMethod.GET)
	public String functionImplementation(ModelMap model, @ModelAttribute("user") String user) {

		PhaseModel phaseModel = logicsTaskService.loadLastestPhaseModel(user);
		List<Functions> allFunctionsList = logicsTaskService.getAllFunctions();

		if (phaseModel != null) {
			int stage = logicsTaskService.getStageCorrespondingIndex(phaseModel.getStage());
			String stageName = "fourth";
			
			switch (stage) {
			case 1: stageName = "seed";
				
				break;
			case 2: stageName = "startup1";
			
			break;
			case 3: stageName = "startup2";
			
			break;
			case 4: stageName = "startup3";
			
			break;
			case 5: stageName = "startup4";
			
			break;
			case 6: stageName = "expansion";
			
			break;
			case 7: stageName = "later";
			
			break;

			default:
				break;
			}
			int logisticTasksList[][][] = logicsTaskService.getLogisticTasksList(user, stageName);
			
			int variant = 0;

			List<String> procurementList = new ArrayList<>();
			List<String> productionLogisticsList = new ArrayList<>();
			List<String> generalList = new ArrayList<>();
			List<String> wareHouseLogisticsList = new ArrayList<>();
			List<String> distributionLogisticsList = new ArrayList<>();
			List<String> logisticsControllingList = new ArrayList<>();


			try {
				variant = Integer.parseInt(phaseModel.getVariant());
				logger.info("Retrived variant: " + variant);

			} catch (Exception e) {
				// no function under this variant
				// legal variants are from 0 to 5
				variant = 6;

				logger.error(e);
				logger.info("Added variant 6 which is out of range (0-5)");
			}
			if (null != allFunctionsList)
				for (Functions function : allFunctionsList) {
					if (null != function) {

						if (1 == function.getFunctionID() || 2 == function.getFunctionID()) {
							generalList.add(function.getFunctionHandle() + ":" + messageSource
									.getMessage(function.getFunctionName(), null, LocaleContextHolder.getLocale()));
							logger.info(function.getFunctionName() + " is available at Workspace for stage " + stage
									+ " variant " + variant);
						} else if ("productionLogistics".equalsIgnoreCase(function.getFunctionClass()))
							if (logisticTasksList[stage][variant][function.getFunctionID()] == 1) {
								productionLogisticsList.add(function.getFunctionHandle() + ":" + messageSource
										.getMessage(function.getFunctionName(), null, LocaleContextHolder.getLocale()));
								logger.info(function.getFunctionName() + " is available at Workspace for stage " + stage
										+ " variant " + variant);
							}
						if ("procurement".equalsIgnoreCase(function.getFunctionClass()))
							if (logisticTasksList[stage][variant][function.getFunctionID()] == 1) {
								procurementList.add(function.getFunctionHandle() + ":" + messageSource
										.getMessage(function.getFunctionName(), null, LocaleContextHolder.getLocale()));
								logger.info(function.getFunctionName() + " is available at Workspace for stage " + stage
										+ " variant " + variant);
							}
						if ("general".equalsIgnoreCase(function.getFunctionClass()))
							if (logisticTasksList[stage][variant][function.getFunctionID()] == 1) {
								generalList.add(function.getFunctionHandle() + ":" + messageSource
										.getMessage(function.getFunctionName(), null, LocaleContextHolder.getLocale()));
								logger.info(function.getFunctionName() + " is available at Workspace for stage " + stage
										+ " variant " + variant);
							}
						if ("warehouseLogistics".equalsIgnoreCase(function.getFunctionClass()))
							if (logisticTasksList[stage][variant][function.getFunctionID()] == 1) {
								wareHouseLogisticsList.add(function.getFunctionHandle() + ":" + messageSource
										.getMessage(function.getFunctionName(), null, LocaleContextHolder.getLocale()));
								logger.info(function.getFunctionName() + " is available at Workspace for stage " + stage
										+ " variant " + variant);
							}
						if ("distributionLogistics".equalsIgnoreCase(function.getFunctionClass()))
							if (logisticTasksList[stage][variant][function.getFunctionID()] == 1) {
								distributionLogisticsList.add(function.getFunctionHandle() + ":" + messageSource
										.getMessage(function.getFunctionName(), null, LocaleContextHolder.getLocale()));
								logger.info(function.getFunctionName() + " is available at Workspace for stage " + stage
										+ " variant " + variant);
							}
					}
				}

			if (wareHouseLogisticsList.size() == 0) {
				logger.info("No warehouse logistic tasks avaiable for stage " + stage + " variant " + variant);
			}

			if (procurementList.size() == 0) {
				logger.info("No procurement logistic tasks avaiable for stage " + stage + " variant " + variant);
			}

			if (generalList.size() == 0) {
				logger.info("No general tasks avaiable for stage " + stage + " variant " + variant);
			}

			model.put("wareHousePlanningList", wareHouseLogisticsList);
			model.put("procurementStrategyList", procurementList);
			model.put("generalList", generalList);
			model.put("productionLogisticsList", productionLogisticsList);
			model.put("distributionLogisticsList", distributionLogisticsList);
			model.put("influencingfactors",influencingFactorsController.loadExistingInfluencingFactors(model));
			model.put("logisticsControllingList", logisticsControllingList);

			
		} else {
			logger.error("Can't retrieve latest phasemodel");
		}

		return "workspace";
	}

	/**
	 * Back button of workspace
	 */
	@RequestMapping(value = "/workspace", params = "back", method = RequestMethod.POST)
	public String returnFromLogisticsTasksToMakeBuy(@ModelAttribute("user") String user) {
		// Return to the previous page based on conditions.
		// 
		logger.info("Back to Makeorbuy");
		return "redirect:/makeorbuy";
	}

	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (ModelMap model, HttpServletRequest request) {
		model.clear();
	    return "redirect:/login";
	}

	@ExceptionHandler(HttpSessionRequiredException.class)
	public String notLoggedIn() {
		return "redirect:login";
	}

}
