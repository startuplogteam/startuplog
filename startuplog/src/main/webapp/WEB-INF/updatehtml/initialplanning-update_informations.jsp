<div class="container">
    <div id="popupClose" class="glyphicon glyphicon-remove"></div>
    <form id="initialplanning-update_informations-form" class="form" action="/saveInitialPlanningMaterialInfo" method="post">
        <div id="informations">
            <div id="tabs" class="c-tabs no-js">
                <div class="c-tabs-nav">
                    <a href="#" class="c-tabs-nav__link is-active"><spring:message code="initial_planning_update_storage_unit"> </spring:message></a>
                    <a href="#" class="c-tabs-nav__link">Storage strategy</a>
                    <a href="#" class="c-tabs-nav__link">Further information</a>
                </div>
                <div class="c-tab is-active" id="questions_storage_unit"><!--STORAGE UNIT---------------------------------->
                    <div class="c-tab__content">
                        <div class="boundary">
                            <ul class="hiddenbulletpoints hidden" id="unit_q1a">
                                <li>
                                    <div class="label head">
                                        <spring:message code="initial_planning_update_storage_unit_q1"></spring:message>
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="store_in_delivery_packaging" id="unit_q1aYes" class="hidden" value="yes" />
                                    <label class="label checkmark highlight" for="unit_q1aYes">
                                        Yes
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="store_in_delivery_packaging" id="unit_q1aNo" class="hidden" value="no" />
                                    <label class="label checkmark highlight" for="unit_q1aNo">
                                        No
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="boundary">
                            <ul class="hiddenbulletpoints hidden" id="unit_q1b">
                                <li>
                                    <div class="label head">
                                        Should the material be stored in the dispatch packaging?
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="store_in_dispatch_packaging" id="unit_q1bYes" class="hidden" value="yes" />
                                    <label class="label checkmark highlight" for="unit_q1bYes">
                                        Yes
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="store_in_dispatch_packaging" id="unit_q1bNo" class="hidden" value="no" />
                                    <label class="label checkmark highlight" for="unit_q1bNo">
                                        No
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="boundary">
                            <ul class="hiddenbulletpoints hidden" id="unit_q2">
                                <li>
                                    <div class="label head">
                                        In which storage unit is the material to be stored?
                                    </div>
                                </li>
                                <li class="choice">
                                    <select name="storage_unit_type" value="yes">
                                        <option value="not_chosen"></option>
                                        <option value="unpacked_small">material unpacked (small)</option>
                                        <option value="unpacked_big">material unpacked (big)</option>
                                        <option value="unpacked_long">long material unpacked (e.g. metal rods)</option>
                                        <option value="box_open">open box for small parts</option>
                                        <option value="carton">carton</option>
                                        <option value="carton_pallet">cartons on pallet</option>
                                        <option value="pallet">pallet</option>
                                        <option value="box_mesh">mesh box</option>
                                        <option value="container_big">big container</option>
                                        <option value="container_small">small container</option>
                                        <option value="container_small_p">small container on pallet</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                        <div class="boundary">
                            <ul class="hiddenbulletpoints hidden" id="unit_q3">
                                <li>
                                    <div class="label head">
                                        Are the storage units stackable?
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_unit_stackable" id="unit_q3Yes" class="hidden" value="yes" />
                                    <label class="label checkmark highlight" for="unit_q3Yes">
                                        Yes
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_unit_stackable" id="unit_q3No" class="hidden" value="no" />
                                    <label class="label checkmark highlight" for="unit_q3No">
                                        No
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="boundary">
                            <ul class="hiddenbulletpoints hidden" id="unit_q4">
                                <li>
                                    <div class="label head">
                                        How many storage units can be stacked on top of each other?
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="number" name="storage_unit_stackable_amount" />
                                </li>
                            </ul>
                        </div>
                        <div class="boundary">
                            <ul class="hiddenbulletpoints hidden" id="unit_q5">
                                <li>
                                    <div class="label head">
                                        How many parts can be stored in the storage unit?
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="number" name="storage_unit_parts_max" class="long"/>
                                </li>
                            </ul>
                        </div>
                        <div class="boundary">
                            <ul class="hiddenbulletpoints hidden" id="unit_q6">
                                <li>
                                    <div class="label head">
                                        How big is the storage unit? (in mm)
                                    </div>
                                </li>
                                <li class="choice">
                                    length: <input type="number" name="storage_unit_length" placeholder="in mm"/>
                                </li>
                                <li class="choice">
                                    width: <input type="number" name="storage_unit_width" placeholder="in mm"/>
                                </li>
                                <li class="choice">
                                    height: <input type="number" name="storage_unit_height" placeholder="in mm"/>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="c-tab" id="questions_storage_strategy"><!--STORAGE STRATEGY------------------------------>
                    <div class="c-tab__content">
                        <div class="boundary">
                            <ul class="hiddenbulletpoints" id="strategy_q1">
                                <li>
                                    <div class="label head">
                                        Should the material have a fixed storage bin?
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_bin_assignment" id="strategy_q1Yes" class="hidden" value="yes" />
                                    <label class="label checkmark highlight" for="strategy_q1Yes">
                                        Yes
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_bin_assignment" id="strategy_q1No" class="hidden" value="no" />
                                    <label class="label checkmark highlight" for="strategy_q1No">
                                        No
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_bin_assignment" id="strategy_q1Irrelevant" class="hidden" value="irrelevant" />
                                    <label class="label checkmark highlight" for="strategy_q1Irrelevant">
                                        irrelevant
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="boundary">
                            <ul class="hiddenbulletpoints" id="strategy_q2">
                                <li>
                                    <div class="label head">
                                        Should only this material be in the storage bin? 
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_allocation" id="strategy_q2Yes" class="hidden" value="yes" />
                                    <label class="label checkmark highlight" for="strategy_q2Yes">
                                        Yes
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_allocation" id="strategy_q2No" class="hidden" value="no" />
                                    <label class="label checkmark highlight" for="strategy_q2No">
                                        No
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_allocation" id="strategy_q2Irrelevant" class="hidden" value="irrelevant" />
                                    <label class="label checkmark highlight" for="strategy_q2Irrelevant">
                                        irrelevant
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="boundary">
                            <ul class="hiddenbulletpoints" id="strategy_q3">
                                <li>
                                    <div class="label head">
                                        In what way is the storage and removal of the material carried out?
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_removal_strategy" id="strategy_q3Fifo" class="hidden" value="fifo" />
                                    <label class="label checkmark highlight" for="strategy_q3Fifo">
                                        FIFO
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_removal_strategy" id="strategy_q3Lifo" class="hidden" value="lifo" />
                                    <label class="label checkmark highlight" for="strategy_q3Lifo">
                                        LIFO
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_removal_strategy" id="strategy_q3Irrelevant" class="hidden" value="irrelevant" />
                                    <label class="label checkmark highlight" for="strategy_q3Irrelevant">
                                        irrelevant
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="c-tab" id="questions_misc"><!--FURTHER INFORMATION--------------------------->
                    <div class="c-tab__content">
                        <div class="boundary">
                            <ul class="hiddenbulletpoints" id="misc_q1">
                                <li>
                                    <div class="label head">
                                        How about the quantity of the material?
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_quantity" id="misc_q1High" class="hidden" value="high quantity" />
                                    <label class="label checkmark highlight" for="misc_q1High">
                                        High quantity
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_quantity" id="misc_q1Medium" class="hidden" value="medium-sized quantity" />
                                    <label class="label checkmark highlight" for="misc_q1Medium">
                                        medium-sized quantity
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_quantity" id="misc_q1Low" class="hidden" value="low quantity" />
                                    <label class="label checkmark highlight" for="misc_q1Low">
                                        low quantity
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="boundary">
                            <ul class="hiddenbulletpoints" id="misc_q2">
                                <li>
                                    <div class="label head">
                                        Should the storage facility be suitable for <div class="tooltip">picking purposes
                                        <span class="tooltiptext">Aus einer im Lager bereitgestellten Gesamtmenge z.B. von Paletten- und oder Beh&auml;ltereinheiten werden Teilmengen nach vorgegebner Bedarfsinformation entnommen und zu einem Auftrag zusammengestellt.</span>
                                        </div>?
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_picking" id="misc_q2Yes" class="hidden" value="yes" />
                                    <label class="label checkmark highlight" for="misc_q2Yes">
                                        Yes
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_picking" id="misc_q2No" class="hidden" value="no" />
                                    <label class="label checkmark highlight" for="misc_q2No">
                                        No
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="boundary">
                            <ul class="hiddenbulletpoints" id="misc_q3">
                                <li>
                                    <div class="label head">
                                        Is the (later) automation of the warehouse important to you, or must the storage system be automatable in the future?
                                    </div>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_automation" id="misc_q3Yes" class="hidden" value="yes" />
                                    <label class="label checkmark highlight" for="misc_q3Yes">
                                        Yes
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_automation" id="misc_q3No" class="hidden" value="no" />
                                    <label class="label checkmark highlight" for="misc_q3No">
                                        No
                                    </label>
                                </li>
                                <li class="choice">
                                    <input type="radio" name="storage_automation" id="misc_q3Irrelevant" class="hidden" value="it depends on." />
                                    <label class="label checkmark highlight" for="misc_q3Irrelevant">
                                        It depends on.
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div style="clear:both"></div>
        <div id="nav" class="table-row navigation">
            <button type="button" id="popupPrev" class="form_button glyphicon glyphicon-chevron-left"></button>
            <button type="button" id="popupNext" class="form_button glyphicon glyphicon-chevron-right"></button>
            <button id="popupSave" class="form_button">save</button>
        </div>
        <div id="response"></div>
    </form>
</div>
