<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/multiselect.css">
<link rel="stylesheet" href="resources/css/styles.css">
<link rel="stylesheet" href="resources/css/warehouse_logistics.css">
<link rel="stylesheet" href="resources/css/tabs.css">
<link rel="stylesheet" href="resources/css/tooltip.css">
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script src="resources/js/jquery-3.1.1.min.js"></script>
<script src="resources/js/multiselect.min.js"></script>
<script src="resources/js/tabs.js"></script>
<script src="resources/js/survey.js"></script>
<title>StartupLog</title>
<script type="text/javascript">

        $(document).ready(function ()
        {
            $("#nav .form_button").click(function ()
            {
                 window.location = '/indicators';
            });
            
            $("#suggestions_button").click(function () {
            	 window.location = '/indicators';
            });

        });
    </script>
</head>
<body>
    <div class="form">
        <p><b><spring:message code="stages.preseed.selection.message1"/></b></p>
        <p><spring:message code="stages.preseed.selection.message2"/></p>
    </div>
    <div class="container">
        <div class= "topcorner">
                  	<a id = "en"  href="?language=en">English</a>  |   <a id ="de" href="?language=de">Deutsch</a>
    	</div>
        
         <button id="suggestions_button" type="button" class="form_button navigation">Back</button>
    </div>
    
</body>
</html>