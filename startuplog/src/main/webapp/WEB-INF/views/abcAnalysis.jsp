<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>StartupLog</title>
<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/multiselect.css">
<link rel="stylesheet" href="resources/css/styles.css">
<link rel="stylesheet" href="resources/css/warehouse_logistics.css">
<link rel="stylesheet" href="resources/css/tabs.css">
<link rel="stylesheet" href="resources/css/tabs2.css">
<link rel="stylesheet" href="resources/css/editContent.css">
<link rel="stylesheet" href="resources/css/tooltip.css">
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script src="resources/js/jquery-3.1.1.min.js"></script>
<script src="resources/js/multiselect.min.js"></script>
<script src="resources/js/tabs.js"></script>
<script src="resources/js/survey.js"></script>


<script type="text/javascript">
	var appendMaterialToCategory = function(data, category, categoryElement) {
		if (data.hasOwnProperty(category)) {
			data[category].forEach(function(material) {
				categoryElement.append("<li>" + material + "</li>");
			});
		}
	}

	var getABC = function(form) {
		var dataArray = $("#abc_analysis").serializeArray(), dataObj = {}, dataToSend = '{"analyses":[';

		$(dataArray).each(
				function(i, field) {
					if (i % 4 == 0) {
						dataToSend = dataToSend + '{';
					}
					dataToSend = dataToSend + '"' + field.name + '":"'
							+ field.value + '",';
					if (i % 4 == 3) {
						dataToSend = dataToSend.substring(0,
								dataToSend.length - 1);
						dataToSend = dataToSend + '},';
					}
				});
		dataToSend = dataToSend.substring(0, dataToSend.length - 1);
		dataToSend = dataToSend + ']}';

		//var dataToSend = '{"tests":[{"materialId":"2","materialName":"Saddle"},{"materialId":"1","materialName":"Hello"}]}';

		$.ajax({
			type : form.attr('method'),
			url : form.attr('action'),
			contentType : 'application/json',
			data : dataToSend,
			dataType : "json",
			success : function(data) {

				category_a = $("#category_a");
				category_b = $("#category_b");
				category_c = $("#category_c");
				category_a.empty();
				category_b.empty();
				category_c.empty();
				appendMaterialToCategory(data, "a", category_a);
				appendMaterialToCategory(data, "b", category_b);
				appendMaterialToCategory(data, "c", category_c);
			},
			error : function(exception) {

			}
		});
	}

	$(document).ready(function() {
		
		$('.edit').click(function(){
			  $(this).hide();
			  $('.box').addClass('editable');
			  $('.text').attr('contenteditable', 'true');  
			  $('.save').show();
			});

			$('.save').click(function(){
			  $(this).hide();
			  $('.box').removeClass('editable');
			  $('.text').removeAttr('contenteditable');
			  $('.edit').show();
			});

		$(".tabbable").find(".tab").hide();
	    $(".tabbable").find(".tab").first().show();
	    $(".tabbable").find(".tabs li").first().find("a").addClass("active");
	    $(".tabbable").find(".tabs").find("a").click(function(){
	        tab = $(this).attr("href");
	        $(".tabbable").find(".tab").hide();
	        $(".tabbable").find(".tabs").find("a").removeClass("active");
	        $(tab).show();
	        $(this).addClass("active");
	        return false;
	    });
		
		$("input").change("change", function() {
			var row = $(this).closest(".table-row");
			var result = row.find("input:last");
			var inputs = row.find('input[type="number"]');
			var result1 = inputs.eq(0).val();
			var result2 = inputs.eq(1).val();
			if ($.isNumeric(result1) && $.isNumeric(result2)) {
				result.val(result1 * result2);
			} else {
				result.val("0");
			}
		});
		$("#getSuggestions").click(function() {

			getABC($("#abc_analysis"));
		});
	});
</script>
</head>
<body>

	<div class="tabbable">
    <ul class="tabs">
        <li><a href="#tab1">Beschreibung</a></li>
        <li><a href="#tab2">Anwendungs-Tool</a></li>
    </ul>
    <div class="tabcontent">
        <div id="tab1" class="tab box">
                       
  <span class="edit">Edit</span>
  <span class="save">Save</span>
  <div class="text">
    Hover this box and click on edit! - You can edit me then.
    <br>When you finished - click save and you saved it.
  </div>

            
        </div>
        <div id="tab2" class="tab">
        
        <div class="info">
		<p>
			<spring:message code="function.general.abcAnalysis.info.p1" />
		</p>
		<p>
			<spring:message code="function.general.abcAnalysis.info.p2" />
		</p>
		<p>
			<spring:message code="function.general.abcAnalysis.info.p3" />
		</p>
		<p>
			<spring:message code="function.general.abcAnalysis.info.p4" />
		</p>
		<p>
			<spring:message code="function.general.abcAnalysis.info.p5" />
		</p>
	</div>
	<div class="container">
		<form id="abc_analysis" class="form" action="/abc_analysis"
			method="post">
			<div class="scroll_wrapper">
				<div class="table">
					<div class="table-row">
						<div class="table-cell header" style="font-weight: bold; padding: 12px 0px 12px 0px;">
							<spring:message
								code="function.general.abcAnalysis.materialNumber" />
						</div>
						<div class="table-cell header" style="font-weight: bold; padding: 12px 36px 12px 0px;">
							<spring:message code="function.general.abcAnalysis.materialName" />
						</div>
						<div class="table-cell header" style="font-weight: bold; padding: 12px 0px 12px 0px;">
							<spring:message code="function.general.abcAnalysis.numberPerYear" />
						</div>
						<div class="table-cell header" style="width:50px"></div>
						<div class="table-cell header" style="font-weight: bold; padding: 12px 0px 12px 0px;">
							<spring:message code="function.general.abcAnalysis.pricePerPiece" /> (Euro)
						</div>
						<div class="table-cell header" style="width:50px"></div>
						<div class="table-cell header" style="font-weight: bold; padding: 12px 0px 12px 0px;">
							<spring:message code="function.general.abcAnalysis.valuePerYear" />
						</div>
					</div>
				</div>
				<div class="table">
					<c:forEach var="material" items="${abcanalysis}">
						<div class="table-row">
							<div class="table-cell">
								${material.materialId} <input type="hidden" name="materialId"
									value="${material.materialId}" />
							</div>
							<div class="table-cell" style="text-align:left">
								<label>${material.materialName}</label> <input type="hidden"
									name="materialName" value="${material.materialName}" />
							</div>
							<div class="table-cell">
								<input type="number" name="salesPerYear"
									value="${material.salesPerYear}" min="0" />
							</div>
							<div class="table-cell" style="width:50px">x</div>
							<div class="table-cell">
								<input type="number" name="pricePerPiece"
									value="${material.pricePerPiece}" min="0" />
							</div>
							<div class="table-cell" style="width:50px">=</div>
							<div class="table-cell sales">
								<input type="number" name="totalPrice"
									value="${material.totalSales}" min="0" disabled />
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
			<div id="nav">
				<button type="button" id="getSuggestions" style="width: 45%; margin-left: 441px"
					class="btn btn-block form_button">
					<spring:message code="save" />
				</button>
			</div>
			<div id="suggestions" class="boundary">
				<h1>
					<spring:message
						code="function.general.abcAnalysis.materialClassification" />
				</h1>
				<div id="suggestions_table">
					<div class="table">
						<div class="table-row">
							<div class="table-cell header" style="font-weight:bold; color: #FFF">
								<spring:message
									code="function.general.abcAnalysis.materialClass" />
							</div>
							<div class="table-cell header" style="font-weight:bold; color: #FFF"">A</div>
							<div class="table-cell header" style="font-weight:bold; color: #FFF"">B</div>
							<div class="table-cell header" style="font-weight:bold; color: #FFF"">C</div>
						</div>
					</div>
					<div class="scroll_wrapper">
						<div class="table">
							<div class="table-row">
								<div class="table-cell header" style="font-weight:bold; color: #FFF"">
									<spring:message
										code="function.general.abcAnalysis.materialName" />
								</div>
								<div class="table-cell">
									<ul class="hiddenbulletpoints" id="category_a">
										<c:forEach var="categorya" items="${abcanalysiscategoryA}">
											<li style="color:gray">${categorya}</li>
										</c:forEach>
									</ul>
								</div>
								<div class="table-cell">
									<ul class="hiddenbulletpoints" id="category_b">
										<c:forEach var="categoryb" items="${abcanalysiscategoryB}">
											<li style="color:gray">${categoryb}</li>
										</c:forEach>
									</ul>
								</div>
								<div class="table-cell">
									<ul class="hiddenbulletpoints" id="category_c">
										<c:forEach var="categoryc" items="${abcanalysiscategoryC}">
											<li style="color:gray">${categoryc}</li>
										</c:forEach>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="clear: both"></div>
		</form>
	</div>
	<div class="info">
		<p>
			<font color="red">${errormsg}</font>
		</p>
	</div>
        
        </div>
        </div>
        </div>
        

	
</body>
</html>
