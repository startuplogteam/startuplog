<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>StartupLog</title>
<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/jquery-ui.css">
<link rel="stylesheet" href="resources/css/multiselect.css">
<link rel="stylesheet" href="resources/css/styles.css">
<link rel="stylesheet" href="resources/css/warehouse_logistics.css">
<link rel="stylesheet" href="resources/css/tabs.css">
<link rel="stylesheet" href="resources/css/tabs2.css">
<link rel="stylesheet" href="resources/css/editContent.css">
<link rel="stylesheet" href="resources/css/tooltip.css">
<script src="resources/js/jquery-3.1.1.min.js"></script>
<script src="resources/js/jquery-ui.js"></script>
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script type="text/javascript">

	$(document).ready(function() {
		
		$('[name="delete-button"]').click(function(){
        	
			var name = $(this).parent().parent().children('#table-username').text().trim();
        			if(confirm("Are you sure to delete user : "+name)==false){
        			 return true;
        			}
        			$.ajax({
           	 			type: "POST",
           	 			url: "/deleteUser",
           				data: { username : name },
           	 			success: function () {
                            window.location.reload();
        	 			},
           	 			error: function (exception) {

           				}
            		});
        			
        	
    	});
		
    	
		
		$('[name="edit-button"]').click(function() {
			var name = $(this).parent().parent().children('#table-username').text().trim();
			var admin = $(this).parent().parent().children('#table-isAdmin').text().trim();
			var active = $(this).parent().parent().children('#table-isActive').text().trim();
			$("#edit-username").val(name);
			if (admin=='Yes') {
				$("#edit-isAdmin").prop( "checked", true );
				}
			else {
				$('#edit-isAdmin').prop('checked', false);
			}
			
			if (active=='Yes') {
				$("#edit-isActive").prop( "checked", true );
				}
			else {
				$('#edit-isActive').prop('checked', false);
			}
			
		    $("#edit-popup").dialog({
					height: 400,
					width: 600,
					modal: true,
					buttons: {
						"Close": function () {
							$(this).dialog("close");
					}
				}
			});
	});
	
		
		var lang = "${pageContext.response.locale}";
		if (lang == "de") {
			$('#de').addClass('visited');
			$('#en').removeClass('visited');

		} else {
			$("#en").addClass("visited");
			$('#de').removeClass('visited');
		}

	});
	
</script>
</head>
<body>
<div>
	<form action="/addUser" method="POST" class="addUser-form ">
			<h3> Add User</h3>
			<h6> ${op_status}</h6>
			<div class="addUser">
				<div class="table">
					<div class="table-row">
						<div class="table-cell header" style="font-weight: bold; padding: 12px 0px 12px 0px;">
							User-Name <input type="text" name="username"/>
						</div>
						<div class="table-cell header" style="font-weight: bold; padding: 12px 36px 12px 0px;">
							Password <input type="password" name="password"/>
						</div>
						<div class="table-cell header" style="font-weight: bold; padding: 12px 0px 12px 0px;">
							Administrator <input type="checkbox" name="isAdmin" />
						</div>
						<div class="table-cell header" style="font-weight: bold; padding: 12px 0px 12px 0px;">
							Is-Active <input type="checkbox" name="isActive" />
						</div>
					</div>
				</div>
			</div>
			<button class="form_button" style="width: 10%; margin-left: 23px; margin-top: 25px">Add User</button>
			
			
							
			<div id="suggestions" class="boundary">
				<h1>
					User
				</h1>
				<div id="suggestions_table">
					<div id="header" class="table">
						<div class="table-row">
							<div class="table-cell header">
								User-Name
							</div>
							<div class="table-cell header">
								Administrator
							</div>
							<div class="table-cell header">
								Active
							</div>
							<div class="table-cell header">
							<i class="glyphicon glyphicon-pencil"></i>/<i class="	glyphicon glyphicon-trash"></i>
							</div>
						</div>
					</div>
					
					<div class="table" id="data_table">
					<c:forEach var="u" items="${userList}">
						<div class="table-row">
							<div class="table-cell" id="table-username">
								${u.username} 
							</div>
							<div class="table-cell" id="table-isAdmin">
								${u.isAdmin ? 'Yes' : 'No'} 
							</div>
							<div class="table-cell" id="table-isActive" >
								${u.isActive ? 'Yes' : 'No'} 
							</div>
							<div class="table-cell ">
								<button type="button" name="edit-button"
						class="btn btn-block" style="background-color: #FFF; border: none">
									<i class="glyphicon glyphicon-pencil" ></i>
								</button>/
								<button type="button" name="delete-button"
						class="btn btn-block" style="background-color: #FFF; border: none">
							<i class="	glyphicon glyphicon-trash"></i>
							</button>
							</div>
							
							
						</div>
						</c:forEach>
					</div>
		
				</div>
			</div>
		</form>
		
	<form action="/editUser" method="POST" id="edit-popup" style="display: none">
		<h3> Edit User</h3>
  		<div style="font-weight: bold; padding: 12px 0px 12px 0px;">
				User-Name <input type="text" id="edit-username" name="username"/>
		</div>
		<div style="font-weight: bold; padding: 12px 0px 12px 0px;">
				Administrator <input type="checkbox" id="edit-isAdmin" name="isAdmin"  />
		</div>
		<div  style="font-weight: bold; padding: 12px 0px 12px 0px;">
				Is-Active <input type="checkbox" id="edit-isActive" name="isActive" />
		</div>
		<button class="form_button" style="width: 18%; margin-left: 23px; margin-top: 25px">Edit User</button>
	</form>
	

</div>
		
</body>
</html>


