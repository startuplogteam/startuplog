<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>StartupLog</title>
<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/multiselect.css">
<link rel="stylesheet" href="resources/css/styles.css">
<link rel="stylesheet" href="resources/css/warehouse_logistics.css">
<link rel="stylesheet" href="resources/css/tabs.css">
<link rel="stylesheet" href="resources/css/tabs2.css">
<link rel="stylesheet" href="resources/css/editContent.css">
<link rel="stylesheet" href="resources/css/tooltip.css">
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script src="resources/js/jquery-3.1.1.min.js"></script>
<script src="resources/js/multiselect.min.js"></script>
<script src="resources/js/tabs.js"></script>
<script src="resources/js/survey.js"></script>

<script type="text/javascript">
	function getPage(url) {
		$.ajax({
			url : url,
			dataType : "html",
			success : function(data) {
				$("#scroll_wrapper").html(data);
			},
			type : "POST"
		});
	}

	$(document)
			.ready(
					function() {
						
						$('.edit').click(function(){
							  $(this).hide();
							  $('.box').addClass('editable');
							  $('.text').attr('contenteditable', 'true');  
							  $('.save').show();
							});

							$('.save').click(function(){
							  $(this).hide();
							  $('.box').removeClass('editable');
							  $('.text').removeAttr('contenteditable');
							  $('.edit').show();
							});
						
						$(".tabbable").find(".tab").hide();
					    $(".tabbable").find(".tab").first().show();
					    $(".tabbable").find(".tabs li").first().find("a").addClass("active");
					    $(".tabbable").find(".tabs").find("a").click(function(){
					        tab = $(this).attr("href");
					        $(".tabbable").find(".tab").hide();
					        $(".tabbable").find(".tabs").find("a").removeClass("active");
					        $(tab).show();
					        $(this).addClass("active");
					        return false;
					    });

						$("#addproductionstage")
								.click(
										function() {

											var rowcount = parseInt($('#productionsteps tbody tr').length) - 1;
											var stepnumber = parseInt($(
													'#productionsteps tbody tr:last')
													.prev().find(
															'td:eq(1) label')
													.html()) + 1;

											var markup = "<tr><td style='text-align:center;'><input type='button' class='deleteproductionstage'  value='-'></td>"
													+ "<td style='text-align:center;'><label style='border-top: 2px solid #A9A9A9; border-left: 2px solid #A9A9A9; padding-left: 25px;padding-right: 25px; padding-top: 4px; padding-bottom: 4px; border-radius: 6px; background: #dcecf8;'>"
													+ stepnumber
													+ "</label><input type='hidden' name='productionSteps["+rowcount+"].stepNumber' value='"+stepnumber+"'></td>"
													+ "<td style='text-align:center;'><input style='width:90%; border-radius: 6px;background: #b3d9ff;' type='text' name='productionSteps["
													+ rowcount
													+ "].description'></td>"
													+ "<td style='text-align:center;'><input style='width:50%;' type='number' name='productionSteps["
													+ rowcount
													+ "].TNForMachine'></td>"
													+ "<td style='text-align:center;'><input style='width:50%;' type='number' name='productionSteps["
													+ rowcount
													+ "].TNForStaff'></td>"
													+ "<td style='text-align:center;'><input style='border-radius: 6px;background: #b3d9ff;' type='text' name='productionSteps["+rowcount+"].machineDesignation'></td></tr>";

											$('#productionsteps tbody tr:last')
													.prev().after(markup);
										});

						$(document).on('click', ".deleteproductionstage",
								function() {
									$(this).closest('tr').remove();
								});

						$("#addplanningyear")
								.click(
										function() {
											var rowcount = parseInt($('#demandplanning tbody tr').length) - 1;
											var newyear = parseInt($(
													'#demandplanning tbody tr:last')
													.prev().find(
															'td:eq(1) input')
													.val()) + 1;
											$('#demandplanning tbody tr:last')
													.prev()
													.after(
															"<tr><td style='text-align:center;'><input type='button' class='deleteplanningyear'  value='-'></td><td style='text-align:center;'><input type='number' name='demandPlanning["+rowcount+"].year' value='"+newyear+"'></td><td style='text-align:center;'><input type='number' name='demandPlanning["+rowcount+"].estimate'></td></tr>");

										});

						$(document).on('click', ".deleteplanningyear",
								function() {
									$(this).closest('tr').remove();

								});

					});
</script>
</head>
<body>

	<div class="tabbable">
    <ul class="tabs">
        <li><a href="#tab1">Beschreibung</a></li>
        <li><a href="#tab2">Anwendungs-Tool</a></li>
    </ul>
    <div class="tabcontent">
        <div id="tab1" class="tab box">
                       
  <span class="edit">Edit</span>
  <span class="save">Save</span>
  <div class="text">
    Hover this box and click on edit! - You can edit me then.
    <br>When you finished - click save and you saved it.
    </div>

            
        </div>
        <div id="tab2" class="tab">
        
        <div class="info">
		<p>
			<b><spring:message
					code="productionLogistics.capacityPlanning.info.heading" /></b>
		</p>
		<p>
			<b><spring:message
					code="productionLogistics.capacityPlanning.info.p1" /></b>
		</p>
		<p>
			<spring:message code="productionLogistics.capacityPlanning.info.p2" />
		</p>
		<p>
			<spring:message code="productionLogistics.capacityPlanning.info.p3" />
		</p>
	</div>

	<div class="container">
		<form:form style="text-align: left !important;" id="capacity_planning"
			class="form" action="/plan_capacity" method="post"
			modelAttribute="capacityPlanning">
			<div class="scroll_wrapper">
				<div class="container" style="margin-bottom: 25px;">
					<p
						style="margin-bottom: 30px; display: inline-block; margin-right: 600px;">
						<b><spring:message
								code="productionLogistics.capacityPlanning.workingTime" /></b>
					</p>
					<p style="display: inline-block; margin-right: 15px;">
						<b><spring:message
								code="productionLogistics.capacityPlanning.uncertainityFactor" /></b>
					</p>
					<form:input path="uncertainityFactor" style="margin-right:3px;"
						type='number'></form:input>
					%
					<div>
						<p
							style="margin-left: 45px; display: inline-block; margin-right: 15px">
							<spring:message
								code="productionLogistics.capacityPlanning.workingDaysPerWeek" />
						</p>
						<form:input path="WDPerWeek" style="margin-right:3px;"
							type='number'></form:input>
						<spring:message
							code="productionLogistics.capacityPlanning.daysPerWeek" />
					</div>
					<p
						style="margin-left: 45px; display: inline-block; margin-right: 18px">
						<spring:message
							code="productionLogistics.capacityPlanning.workingDaysPerDay" />
					</p>
					<form:input path="WHPerDay" style="margin-right:5px;" type='number'></form:input>
					<spring:message
						code="productionLogistics.capacityPlanning.hoursPerDay" />
				</div>

				<div class="container" style="margin-bottom: 25px;">
					<div style="margin-bottom: 25px;">
						<p>
							<b><spring:message
									code="productionLogistics.capacityPlanning.productionSteps" /></b>
						</p>
					</div>
					<table id="productionsteps"
						style="width: 70%; background-color: #e6e6e6;">
						<thead>
							<tr>
								<th style="border-bottom: none;"></th>
								<th style="border-bottom: none; text-align: center;"><spring:message
										code="productionLogistics.capacityPlanning.number" /></th>
								<th style="border-bottom: none; text-align: center;"><spring:message
										code="productionLogistics.capacityPlanning.description" /></th>
								<th style="text-align: center;" colspan="2"><spring:message
										code="productionLogistics.capacityPlanning.timeNeeded" /></th>
								<th style="border-bottom: none; text-align: center;">
										<spring:message
										code="productionLogistics.capacityPlanning.machineDesignation" /></th>
							</tr>
							<tr>
								<th style="border-top: none;"></th>
								<th style="border-top: none;"></th>
								<th style="border: none;"></th>
								<th style="text-align: center;"><spring:message
										code="productionLogistics.capacityPlanning.Staff" /></th>
								<th style="text-align: center;"><spring:message
										code="productionLogistics.capacityPlanning.machine" /></th>
								<th style="border-top: none;"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${capacityPlanning.productionSteps}"
								var="production" varStatus="status">
								<tr>
									<td style="text-align: center;"><input type="button"
										class="deleteproductionstage" value="-"></td>
									<td style="text-align: center;"><label
										style="border-top: 2px solid #A9A9A9; border-left: 2px solid #A9A9A9; padding-left: 25px; padding-right: 25px; padding-top: 4px; padding-bottom: 4px; border-radius: 6px; background: #dcecf8;">${production.stepNumber}</label><input
										type="hidden"
										name="productionSteps[${status.index}].stepNumber"
										value="${production.stepNumber}"></td>
									<td style="text-align: center;"><input type="text"
										style="width: 90%; border-radius: 6px; background: #b3d9ff;"
										name="productionSteps[${status.index}].description"
										value="${production.description}"></td>
									<td style="text-align: center;"><input style="width: 50%;"
										type="number"
										name="productionSteps[${status.index}].TNForMachine"
										value="${production.TNForMachine}"></td>
									<td style="text-align: center;"><input style="width: 50%;"
										type="number"
										name="productionSteps[${status.index}].TNForStaff"
										value="${production.TNForStaff}"></td>
									<td style="text-align: center;"><input
										style="border-radius: 6px; background: #b3d9ff;" type="text"
										name="productionSteps[${status.index}].machineDesignation"
										value="${production.machineDesignation}"></td>
								</tr>
							</c:forEach>

							<tr>
								<td><input style="text-align: center;"
									id="addproductionstage" type="button" value="+" /></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="container" style="margin-bottom: 25px;">
					<div style="margin-bottom: 25px;">
						<p style="display: inline-block; margin-right: 15px;">
							<b><spring:message
									code="productionLogistics.capacityPlanning.demandPlanning" /></b>
						</p>
					</div>
					<table id="demandplanning" style="background-color: #e6e6e6;">
						<thead>
							<tr>
								<th></th>
								<th style="text-align: center;"><spring:message
										code="productionLogistics.capacityPlanning.year" /></th>
								<th style="text-align: center;"><spring:message
										code="productionLogistics.capacityPlanning.estimate" /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${capacityPlanning.demandPlanning}"
								var="demand" varStatus="status">
								<tr>
									<td style="text-align: center;"><input type="button"
										class="deleteplanningyear" value="-"></td>
									<td style="text-align: center;"><input type="number"
										name="demandPlanning[${status.index}].year"
										value="${demand.year}"></td>
									<td style="text-align: center;"><input type="number"
										name="demandPlanning[${status.index}].estimate"
										value="${demand.estimate}"></td>
								</tr>
							</c:forEach>
							<tr>
								<td><input style="text-align: center;" id="addplanningyear"
									type="button" value="+" /></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>

				<input style="margin-bottom: 25px;" type="submit"
					value='<spring:message code="productionLogistics.capacityPlanning.saveAndCompute" />'>

				<c:if test="${not empty capacityPlanning.staffCapacity}">


					<div class="container">
						<p style="margin-bottom: 25px;">
							<b><spring:message
									code="productionLogistics.capacityPlanning.capacityNeeded" /></b>
						</p>
						<div style="width: 80%;">
							<p
								style="display: inline-block; margin-right: 37%; margin-left: 15px;">Staff</p>
							<p style="display: inline-block;">
								<spring:message
									code="productionLogistics.capacityPlanning.machine" />
							</p>
						</div>
						<div>
							<table
								style="vertical-align: top; display: inline-block; background-color: #e6e6e6; margin-right: 10%">
								<thead>
									<tr>
										<c:forEach items="${capacityPlanning.staffCapacity}"
											var="staff">
											<th style="text-align: center;">${staff.year}</th>
										</c:forEach>

									</tr>
								</thead>
								<tbody>
									<tr>
										<c:forEach items="${capacityPlanning.staffCapacity}"
											var="staff">
											<td style="text-align: center;"><input type="number"
												value="${staff.staffsNeeded}" disabled></td>
										</c:forEach>
									</tr>
								</tbody>
							</table>


							<table style="display: inline-block; background-color: #e6e6e6;">
								<thead>
									<tr>
										<th style="border-bottom: none; text-align: center;"><spring:message
												code="productionLogistics.capacityPlanning.machineDesignation" /></th>
										<th style="border-right: none; text-align: center;"><spring:message
												code="productionLogistics.capacityPlanning.requiredNumber" /></th>
									</tr>
									<tr>
										<th style="border-top: none;"></th>
										<c:forEach items="${capacityPlanning.staffCapacity}"
											var="staff">
											<th style="text-align: center;">${staff.year}</th>
										</c:forEach>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${capacityPlanning.machinesNeeded}"
										var="machines">
										<tr>
											<td style="text-align: center;"><input
												style="border-radius: 6px; background: #b3d9ff;" type="text"
												value="${machines.machineDesignation}" disabled></td>
											<c:forEach items="${machines.machineCapacity}"
												var="machinecapacity">
												<td style="text-align: center;"><input type="number"
													value="${machinecapacity.neededMachines}" disabled></td>
											</c:forEach>
										</tr>
									</c:forEach>
								</tbody>
							</table>

						</div>
					</div>
				</c:if>

			</div>
		</form:form>
	</div>
        
        </div>
     </div>
   	</div>
</body>
</html>