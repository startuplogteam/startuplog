<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>StartupLog</title>

<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/multiselect.css">
<link rel="stylesheet" href="resources/css/styles.css">
<link rel="stylesheet" href="resources/css/warehouse_logistics.css">
<link rel="stylesheet" href="resources/css/tabs.css">
<link rel="stylesheet" href="resources/css/tabs2.css">
<link rel="stylesheet" href="resources/css/editContent.css">
<link rel="stylesheet" href="resources/css/tooltip.css">
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script src="resources/js/jquery-3.1.1.min.js"></script>
<script src="resources/js/multiselect.min.js"></script>
<script src="resources/js/tabs.js"></script>
<script src="resources/js/survey.js"></script>


<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$('.edit').click(function(){
							  $(this).hide();
							  $('.box').addClass('editable');
							  $('.text').attr('contenteditable', 'true');  
							  $('.save').show();
							});

							$('.save').click(function(){
							  $(this).hide();
							  $('.box').removeClass('editable');
							  $('.text').removeAttr('contenteditable');
							  $('.edit').show();
							});
						
						$(".tabbable").find(".tab").hide();
					    $(".tabbable").find(".tab").first().show();
					    $(".tabbable").find(".tabs li").first().find("a").addClass("active");
					    $(".tabbable").find(".tabs").find("a").click(function(){
					        tab = $(this).attr("href");
					        $(".tabbable").find(".tab").hide();
					        $(".tabbable").find(".tabs").find("a").removeClass("active");
					        $(tab).show();
					        $(this).addClass("active");
					        return false;
					    });		
					});
</script>

</head>
<body>
	<div class="tabbable">
    	<ul class="tabs">
        	<li><a href="#tab1">Beschreibung</a></li>
    	</ul>
    	<div class="tabcontent">
       	 <div id="tab1" class="tab box">
                       
  				<span class="edit">Edit</span>
  				<span class="save">Save</span>
  				<div class="text">
    				Hover this box and click on edit! - You can edit me then.
   			 	<br>When you finished - click save and you saved it.
    			</div>
      	 </div>
   		 </div>
	</div>

	

</body>
</html>