<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>StartupLog</title>
<link rel="stylesheet" href="resources/css/styles.css">
<script src="resources/js/jquery-3.1.1.min.js"></script>
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script type="text/javascript">
	$(document).ready(function() {
		var lang = "${pageContext.response.locale}";
		if (lang == "de") {
			$('#de').addClass('visited');
			$('#en').removeClass('visited');

		} else {
			$("#en").addClass("visited");
			$('#de').removeClass('visited');
		}

	});
</script>
</head>
<body>
		<div class="info">
			<p>
				<b><spring:message code="logisticstasks.info.p1" /></b>
			</p>
			<p>
				<spring:message code="logisticstasks.info.p2" />
			</p>
			<p>
				<spring:message code="logisticstasks.info.p3" />
			</p>
		</div>


		<form id="functions-form" action="/workspace" class="form"
			method="GET">
			<table>
				<tr>
					<td class="functd">
						<div class="stage-row">
						<ul class="hiddenbulletpoints">
							<li class="tablerow_head"><span> <spring:message
										code="function.general" />
							</span></li>
							<c:if test="${function[stage][variant][3] == 1}">
								<li class="choice">
								<label for="stage1_func1" class="tablerow_label">
								<input type="checkbox" id="stage1_func1"
									 name="abc_analysis"
									value="/default:<spring:message code="function.general.influencingFactors.shortname"/>"
									 /><span class="check"></span> 
										<spring:message
											code="function.general.influencingFactors.shortname" />
								</label>
								</li>
							</c:if>
							<c:if test="${function[stage][variant][4] == 1}">
								<li class="choice">
								<label for="stage1_func2" class="tablerow_label">
								<input type="checkbox" id="stage1_func2"
									 name="abc_analysis"
									value="/abc_analysis:<spring:message code="function.general.proLogRequirements.shortname"/>"
									 /><span class="check"></span> 
										<spring:message
											code="function.general.proLogRequirements.shortname" />
								</label>
								</li>
							</c:if>
							<c:if test="${function[stage][variant][5] == 1}">
								<li class="choice">
								<label for="stage1_func3" class="tablerow_label">
								<input type="checkbox" id="stage1_func3"
									 name="abc_analysis"
									value="/abc_analysis:<spring:message code="function.general.makeOrBuyDecisionProduct.shortname"/>"
									 /><span class="check"></span> 
										<spring:message
											code="function.general.makeOrBuyDecisionProduct.shortname" />
								</label></li>
							</c:if>
							<c:if test="${function[stage][variant][6] == 1}">
								<li class="choice">
								<label for="stage1_func4" class="tablerow_label">
								<input type="checkbox" id="stage1_func4"
									 name="abc_analysis"
									value="/abc_analysis:<spring:message code="function.general.makeOrBuyDecisionComponent.shortname"/>"
									 /><span class="check"></span> 
										<spring:message
											code="function.general.makeOrBuyDecisionComponent.shortname" />
								</label></li>
							</c:if>
							<c:if test="${function[stage][variant][7] == 1}">
								<li class="choice">
								<label for="stage1_func5" class="tablerow_label">
								<input type="checkbox" id="stage1_func5"
									 name="abc_analysis"
									value="/abc_analysis:<spring:message code="function.general.changeManagement.shortname"/>"
									 /><span class="check"></span> 
										<spring:message
											code="function.general.changeManagement.shortname" />
								</label></li>
							</c:if>
							<c:if test="${function[stage][variant][8] == 1}">
								<li class="choice">
								<label for="stage1_func6" class="tablerow_label">
								<input type="checkbox" id="stage1_func6"
									 name="abc_analysis"
									value="/abc_analysis:<spring:message code="function.general.abc_analysis.shortname"/>"
									 /><span class="check"></span> 
										<spring:message code="function.general.abc_analysis" />
								</label></li>
							</c:if>
							<c:if test="${function[stage][variant][9] == 1}">
								<li class="choice">
								<label for="stage1_func7" class="tablerow_label">
								<input type="checkbox" id="stage1_func7"
									 name="abc_analysis"
									value="/abc_analysis:<spring:message code="function.general.processPlanning.shortname"/>"
									 /><span class="check"></span> 
										<spring:message
											code="function.general.processPlanning.shortname" />
								</label></li>
							</c:if>
							<c:if test="${function[stage][variant][9] == 1}">
								<li class="choice">
								<label for="stage1_func8" class="tablerow_label">
								<input type="checkbox" id="stage1_func8"
									 name="abc_analysis"
									value="/abc_analysis:<spring:message code="function.general.layoutPlanning.shortname"/>"
									 /><span class="check"></span> 
										<spring:message
											code="function.general.layoutPlanning.shortname" />
								</label></li>
							</c:if>
						</ul>
					</div>
					</td>
					<td class="functd"> 
					<div class="stage-row">
					<ul class="hiddenbulletpoints">
						<li class="tablerow_head"><span> <spring:message
									code="function.procurement" />
						</span></li>
						<c:if test="${function[stage][variant][11] == 1}">
							<li class="choice">
							 <label for="stage2_func1" class="tablerow_label">
							<input type="checkbox" id="stage2_func1"
								 name="procurement"
								value="/default:<spring:message code="function.procurement.materialRequirementsPlanning.shortname"/>"
								  /><span class="check"></span>
									<spring:message
										code="function.procurement.materialRequirementsPlanning.shortname" />
							</label></li>
						</c:if>
						<c:if test="${function[stage][variant][12] == 1}">
							<li class="choice">
							 <label for="stage2_func2" class="tablerow_label">
							<input type="checkbox" id="stage2_func2"
								 name="procurement"
								value="/default:<spring:message code="function.procurement.optimalProcurementQuantity.shortname"/>"
								 /><span class="check"></span>
									<spring:message
										code="function.procurement.optimalProcurementQuantity.shortname" />
							</label></li>
						</c:if>
						<c:if test="${function[stage][variant][13] == 1}">
							<li class="choice">
							 <label for="stage2_func3" class="tablerow_label">
							<input type="checkbox" id="stage2_func3"
								 name="procurement"
								value="/default:<spring:message code="function.procurement.procurementMarketResearch.shortname"/>"
								 /><span class="check"></span>
									<spring:message
										code="function.procurement.procurementMarketResearch.shortname" />
							</label></li>
						</c:if>
						
						<c:if test="${function[stage][variant][6] == 1}">
							<li class="choice">
							<label for="stage2_func4" class="tablerow_label">
							<input type="checkbox" id="stage2_func4"
								 name="materialclassification"
								value="/materialclassification:<spring:message code="function.procurement.materialclassification.shortname"/>"
								 /><span class="check"></span> 
									<spring:message
										code="function.procurement.materialclassification" />
							</label></li>
						</c:if>
						
						<c:if test="${function[stage][variant][15] == 1}">
							<li class="choice">
							<label for="stage2_func5" class="tablerow_label">
							<input type="checkbox" id="stage2_func5"
								 name="procurement"
								value="/default:<spring:message code="function.procurement.marketPowerPortfolio.shortname"/>"
								 /><span class="check"></span> 
									<spring:message
										code="function.procurement.marketPowerPortfolio.shortname" />
							</label></li>
						</c:if>
						<c:if test="${function[stage][variant][16] == 1}">
							<li class="choice">
							 <label for="stage2_func6" class="tablerow_label">
							<input type="checkbox" id="stage2_func6"
								 name="procurement"
								value="/procurement:<spring:message code="function.procurement.strategy.shortname"/>"
								 /><span class="check"></span>
									<spring:message code="function.procurement.strategy" />
							</label></li>
						</c:if>



						<c:if test="${function[stage][variant][17] == 1}">
							<li class="choice">
							 <label for="stage2_func7" class="tablerow_label">
							<input type="checkbox" id="stage2_func7"
								 name="supplierselection"
								value="/supplierselection:<spring:message code="function.procurement.supplierselection.shortname"/>"
								 /><span class="check"></span>
									<spring:message
										code="function.procurement.supplierselection.shortname" />
							</label></li>
						</c:if>
					</ul>
				</div>
				</td>
				
				</tr>
				<tr>
				
				<td class="functd">
					<div class="stage-row">
					<ul class="hiddenbulletpoints">
						<li class="tablerow_head"><span> <spring:message
									code="function.warehouse_Logistics" />
						</span></li>
						<c:if test="${function[stage][variant][20] == 1}">
							<li class="choice">
							<label for="stage3_func1" class="tablerow_label">
							<input type="checkbox" id="stage3_func1"
								 name="initial_planning"
								value="/initialplanning:<spring:message code="function.warehouseLogistics.stockPlanning"/>"
								 /><span class="check"></span> 
									<spring:message
										code="function.warehouseLogistics.stockPlanning" />
							</label></li>
						</c:if>
						
												<c:if test="${function[stage][variant][23] == 1}">
							<li class="choice">
							<label for="stage3_func2" class="tablerow_label">
							<input type="checkbox" id="stage3_func2"
								 name="default"
								value="/default:<spring:message code="function.warehouse_Logistics.initial_product.shortname"/>"
								 /><span class="check"></span> 
									<spring:message
										code="function.warehouse_Logistics.initial_product.shortname" />
							</label></li>
						</c:if>
						
						<c:if test="${function[stage][variant][22] == 1}">
							<li class="choice">
							 <label for="stage3_func3" class="tablerow_label">
							<input type="checkbox" id="stage3_func3"
								 name="default"
								value="/default:<spring:message code="function.warehouseLogistics.warehouseDimensioning"/>"
								 /><span class="check"></span>
									<spring:message
										code="function.warehouseLogistics.warehouseDimensioning" />
							</label></li>
						</c:if>
						<c:if test="${function[stage][variant][23] == 1}">
							<li class="choice">
							<label for="stage3_func4" class="tablerow_label">
							<input type="checkbox" id="stage3_func4"
								 name="default"
								value="/default:<spring:message code="function.warehouseLogistics.storageFacilityInstallation"/>"
								 /><span class="check"></span> 
									<spring:message
										code="function.warehouseLogistics.storageFacilityInstallation" />
							</label></li>
						</c:if>
						<c:if test="${function[stage][variant][24] == 1}">
							<li class="choice">
							<label for="stage3_func5"
								class="tablerow_label"> 
							<input type="checkbox" id="stage3_func5"
								  /><span class="check"></span> <spring:message
										code="function.warehouse_Logistics.warehouse_operation" />
							</label></li>
						</c:if>
					</ul>
				</div>
				</td>
					
					<td class="functd">
						<div class="stage-row">
						<ul class="hiddenbulletpoints">
							<li class="tablerow_head"><span> <spring:message
										code="function.production_logistics" />
							</span></li>
							<c:if test="${function[stage][variant][26] == 1}">
								<li class="choice">
								<label for="stage4_func1"
									class="tablerow_label">
								<input type="checkbox" id="stage4_func1"
									  /><span class="check"></span> <spring:message
											code="function.productionLogistics.capacityPlanning.shortname" />
								</label></li>
							</c:if>
	
						</ul>
					</div>
					</td>
				</tr>	
				<tr>
				<td class="functd">
					<div class="stage-row">
					<ul class="hiddenbulletpoints">
						<li class="tablerow_head"><span> <spring:message
									code="function.distribution_logistics" />
						</span></li>
												<c:if test="${function[stage][variant][33] == 1}">
							<li class="choice">
							<label for="stage5_func1"
								class="tablerow_label">
							<input type="checkbox" id="stage5_func1"
								  /><span class="check"></span>  <spring:message
										code="function.distributionLogistics.distributionStrategy.shortname" />
							</label></li>
						</c:if>
						<c:if test="${function[stage][variant][34] == 1}">
							<li class="choice">
							<label for="stage5_func2"
								class="tablerow_label"> 
							<input type="checkbox" id="stage5_func2"
								  /><span class="check"></span> <spring:message
										code="function.distributionLogistics.distributionConcept.shortname" />
							</label></li>
						</c:if>
						<c:if test="${function[stage][variant][35] == 1}">
							<li class="choice">
							 <label for="stage5_func3"
								class="tablerow_label">
							<input type="checkbox" id="stage5_func3"
								  /><span class="check"></span> <spring:message
										code="function.distributionLogistics.packagingPlanning.shortname" />
							</label></li>
						</c:if>
					</ul>
				</div>
				
					</td>
					<td class="functd">
					<div class="stage-row">
					<ul class="hiddenbulletpoints">
						<li class="tablerow_head"><span> <spring:message
									code="function.controlling_logistics" />
						</span></li>
												<c:if test="${function[stage][variant][39] == 1}">
							<li class="choice">
							<label for="stage6_func1"
								class="tablerow_label">
							<input type="checkbox" id="stage6_func1"
								  /><span class="check"></span>  <spring:message
										code="function.logisticsControlling.controllingLogisticsCosts.shortname" />
							</label></li>
						</c:if>
					</ul>
				</div>
					</td>
				</tr>			
			</table>	

			<div style="clear: both;"></div>
			<div id="nav" class="table-row navigation">
				<input type="submit" value="<spring:message code="back"/>"
					name="backB" class="form_button">
				<button class="form_button">
					<spring:message code="next" />
				</button>
			</div>
		</form>
	
</body>
</html>


