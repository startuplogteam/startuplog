<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- let's add tag srping:url -->
<spring:url value="/resources/css/styles.css" var="startupLogCSS" />
<spring:url value="/resources/js/jquery-3.1.1.min.js" var="startupLogJS" />
<spring:url value="/resources/images/startuplog_logo_1.ico" var="favcon" />
<link href="${startupLogCSS}" rel="stylesheet" />
<link href="${favcon}" rel="icon" />
<script src="${startupLogJS}"></script>

<title>StartupLog</title>
</head>
<body>
	<div class="info">
		<p>
			<b><spring:message code="indicators.welcome" /></b>
		</p>
		<p>
			<spring:message code="indicators.message" />
		</p>
		<p>
			<spring:message code="indicators.message2" />
		</p>
		<p>
			<spring:message code="indicators.note" />
		</p>
	</div>

	<form id="survey-form" class="form" action="/stages" method="POST">
		<div id="indicators">
			<div class="boundary">
				<ul class="hiddenbulletpoints">

					<li>
						<div
							style="height: 25px; border: 2px inset #AAD2F0; border-radius: 6px; width: 100px; text-align: center; line-height: 18px; float: left; padding: 4px;">
							<b><spring:message code="indicators.column1.heading" /></b>
						</div>
					</li>
					<li><label
						style="height: 25px; border: 2px inset #AAD2F0; border-radius: 6px; width: 800px; text-align: center; line-height: 18px; float: left; background: #DCECF8; padding: 6px;">
							<b><spring:message code="indicators.column2.heading" /></b>
					</label></li>

				</ul>
			</div>
			<div class="boundary">
				<ul class="hiddenbulletpoints">

					<li>
						<div class="label head">
							<spring:message code="indicators.sixth_indicator" />
						</div>
					</li>
					<li class="choice"><input type="checkbox" id="survey_q6a1"
						class="hidden" name="sb_1" /> <label
						class="label checkmark highlight" for="survey_q6a1"> <spring:message
								code="indicators.sixth_indicator.first_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q6a2"
						class="hidden" name="sb_2" /> <label
						class="label checkmark highlight" for="survey_q6a2"> <spring:message
								code="indicators.sixth_indicator.second_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q6a3"
						class="hidden" name="sb_3" /> <label
						class="label checkmark highlight" for="survey_q6a3"> <spring:message
								code="indicators.sixth_indicator.third_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q6a4"
						class="hidden" name="sb_4" /> <label
						class="label checkmark highlight" for="survey_q6a4"> <spring:message
								code="indicators.sixth_indicator.forth_choice" />
					</label></li>
				</ul>
			</div>
			<div class="boundary">
				<ul class="hiddenbulletpoints">
					<li>
						<div class="label head">
							<spring:message code="indicators.seventh_indicator" />
						</div>
					</li>
					<li class="choice"><input type="checkbox" id="survey_q7a1"
						class="hidden" name="ms_1" /> <label
						class="label checkmark highlight" for="survey_q7a1"> <spring:message
								code="indicators.seventh_indicator.first_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q7a2"
						class="hidden" name="ms_2" /> <label
						class="label checkmark highlight" for="survey_q7a2"> <spring:message
								code="indicators.seventh_indicator.second_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q7a3"
						class="hidden" name="ms_3" /> <label
						class="label checkmark highlight" for="survey_q7a3"> <spring:message
								code="indicators.seventh_indicator.third_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q7a4"
						class="hidden" name="ms_4" /> <label
						class="label checkmark highlight" for="survey_q7a4"> <spring:message
								code="indicators.seventh_indicator.forth_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q7a5"
						class="hidden" name="ms_5" /> <label
						class="label checkmark highlight" for="survey_q7a5"> <spring:message
								code="indicators.seventh_indicator.fifth_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q7a6"
						class="hidden" name="ms_6" /> <label
						class="label checkmark highlight" for="survey_q7a6"> <spring:message
								code="indicators.seventh_indicator.sixth_choice" />
					</label></li>
				</ul>
			</div>
			<div class="boundary">
				<ul class="hiddenbulletpoints flat">
					<li>
						<div class="label head flat">
							<spring:message code="indicators.forth_indicator" />
						</div>
					</li>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q4a1"
							class="hidden" name="mu_1" /> <label
							class="label checkmark highlight" for="survey_q4a1"> <spring:message
									code="indicators.forth_indicator.first_choice" />
						</label></li>
						<span class="tooltiptext">Da es sich meistens um ein
							vollkommen neues Produkt handelt, ist die Reaktion von
							(potentiellen) Kunden nicht vorhersehbar. Das Startup kennt den
							potenziellen Absatzmarkt kaum.</span>
					</div>
					<li class="choice"><input type="checkbox" id="survey_q4a2"
						class="hidden" name="mu_2" /> <label
						class="label checkmark highlight" for="survey_q4a2"> <spring:message
								code="indicators.forth_indicator.second_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q4a3"
						class="hidden" name="mu_3" /> <label
						class="label checkmark highlight" for="survey_q4a3"> <spring:message
								code="indicators.forth_indicator.third_choice" />
					</label></li>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q4a4"
							class="hidden" name="mu_4" /> <label
							class="label checkmark highlight" for="survey_q4a4"> <spring:message
									code="indicators.forth_indicator.forth_choice" />
						</label></li>
						<span class="tooltiptext">Das Startup kennt den Absatzmarkt
							f�r sein Produkt sehr gut und kann die potenzielle Absatzmenge
							sehr gut einsch�tzen und vorhersehen.</span>
					</div>
				</ul>
			</div>
			<div class="boundary">
				<ul class="hiddenbulletpoints">
					<li>
						<div class="label head">
							<spring:message code="indicators.fifth_indicator" />
						</div>
					</li>
					<li class="choice"><input type="checkbox" id="survey_q5a1"
						class="hidden" name="tr_1" /> <label
						class="label checkmark highlight" for="survey_q5a1"> <spring:message
								code="indicators.fifth_indicator.first_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q5a2"
						class="hidden" name="tr_2" /> <label
						class="label checkmark highlight" for="survey_q5a2"> <spring:message
								code="indicators.fifth_indicator.second_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q5a3"
						class="hidden" name="tr_3" /> <label
						class="label checkmark highlight" for="survey_q5a3"> <spring:message
								code="indicators.fifth_indicator.third_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q5a4"
						class="hidden" name="tr_4" /> <label
						class="label checkmark highlight" for="survey_q5a4"> <spring:message
								code="indicators.fifth_indicator.forth_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q5a5"
						class="hidden" name="tr_5" /> <label
						class="label checkmark highlight" for="survey_q5a5"> <spring:message
								code="indicators.fifth_indicator.fifth_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q5a6"
						class="hidden" name="tr_6" /> <label
						class="label checkmark highlight" for="survey_q5a6"> <spring:message
								code="indicators.fifth_indicator.sixth_choice" />
					</label></li>
					<li class="choice"><input type="checkbox" id="survey_q5a7"
						class="hidden" name="tr_7" /> <label
						class="label checkmark highlight" for="survey_q5a7"> <spring:message
								code="indicators.fifth_indicator.seventh_choice" />
					</label></li>
				</ul>
			</div>
			<div class="boundary">
				<ul class="hiddenbulletpoints">
					<li>
						<div class="label head">
							<spring:message code="indicators.first_indicator" />
						</div>
					</li>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q1a1"
							class="hidden" name="rf_1" /> <label
							class="label checkmark highlight" for="survey_q1a1"> <spring:message
									code="indicators.first_indicator.first_choice" />
						</label></li> <span class="tooltiptext">Hier ist die Phase gemeint, in
							der der oder die Gr�nder das ganze Aufgabenspektrum �bernehmen.</span>
					</div>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q1a2"
							class="hidden" name="rf_2" /> <label
							class="label checkmark highlight" for="survey_q1a2"> <spring:message
									code="indicators.first_indicator.second_choice" />
						</label></li> <span class="tooltiptext">der bzw. die Gr�nder �bernehmen
							ebenfalls Aufgaben aus dem Gesamtspektrum, allerdings gibt es
							neben den Gr�ndern weiter Mitarbeiter und eine klarere
							Arbeitsteilung im Unternehmen.</span>
					</div>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q1a3"
							class="hidden" name="rf_3" /> <label
							class="label checkmark highlight" for="survey_q1a3"> <spring:message
									code="indicators.first_indicator.third_choice" />
						</label></li> <span class="tooltiptext">In dieser Phase fokussiert sich
							der Gr�nder auf die strategischen Aufgaben des Startups - das
							operative Gesch�ft hat einigerma�en etablierte Strukturen und
							Gesch�ftsabl�ufe, die vor allem von anderen Mitarbeitern
							�bernommen werden.</span>
					</div>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q1a4"
							class="hidden" name="rf_4" /> <label
							class="label checkmark highlight" for="survey_q1a4"> <spring:message
									code="indicators.first_indicator.forth_choice" />
						</label></li> <span class="tooltiptext">der Gr�nder gibt ein Teil der
							strategischen Aufgaben des Startups an weitere Mitarbeit ab.</span>
					</div>
				</ul>
			</div>
			<div class="boundary">
				<ul class="hiddenbulletpoints">
					<li>
						<div class="label head">
							<spring:message code="indicators.second_indicator" />
						</div>
					</li>
					<li class="choice"><input type="checkbox" id="survey_q2a1"
						class="hidden" name="st_1" /> <label
						class="label checkmark highlight" for="survey_q2a1"> <spring:message
								code="indicators.second_indicator.first_choice" />
					</label></li>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q2a2"
							class="hidden" name="st_2" /> <label
							class="label checkmark highlight" for="survey_q2a2"> <spring:message
									code="indicators.second_indicator.second_choice" />
						</label></li> <span class="tooltiptext">Gesch�ftsabl�ufe werden
							vereinfacht sowie Prozesse verk�rzt und beschleunigt wodurch
							Aufwand und Kosten gesenkt werden</span>
					</div>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q2a3"
							class="hidden" name="st_3" /> <label
							class="label checkmark highlight" for="survey_q2a3"> <spring:message
									code="indicators.second_indicator.third_choice" />
						</label></li> <span class="tooltiptext">Prozesse werden �ber alle
							Abteilungen, an allen Standorten mit den gleichen Werkzeugen und
							Management-Regeln durchgef�hrt.</span>
					</div>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q2a4"
							class="hidden" name="st_4" /> <label
							class="label checkmark highlight" for="survey_q2a4"> <spring:message
									code="indicators.second_indicator.forth_choice" />
						</label></li> <span class="tooltiptext">Etablierte Prozesse werden
							optimiert</span>
					</div>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q2a5"
							class="hidden" name="st_5" /> <label
							class="label checkmark highlight" for="survey_q2a5"> <spring:message
									code="indicators.second_indicator.fifth_choice" />
						</label></li> <span class="tooltiptext">optimale Zeit-/ Arbeits- und
							Kapitalaufw�nde bei Kernprozessen.</span>
					</div>
				</ul>
			</div>
			<div class="boundary">
				<ul class="hiddenbulletpoints">
					<li>
						<div class="label head">
							<spring:message code="indicators.third_indicator" />
						</div>
					</li>
					<li class="choice"><input type="checkbox" id="survey_q3a1"
						class="hidden" name="os_1" /> <label
						class="label checkmark highlight" for="survey_q3a1"> <spring:message
								code="indicators.third_indicator.first_choice" />
					</label></li>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q3a2"
							class="hidden" name="os_2" /> <label
							class="label checkmark highlight" for="survey_q3a2"> <spring:message
									code="indicators.third_indicator.second_choice" />
						</label></li> <span class="tooltiptext">f�r diese Phase sind eine
							st�rkere Aufgabendifferenzierung und Arbeitsteilung typisch.</span>
					</div>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q3a3"
							class="hidden" name="os_3" /> <label
							class="label checkmark highlight" for="survey_q3a3"> <spring:message
									code="indicators.third_indicator.third_choice" />
						</label></li> <span class="tooltiptext">Es werden erste Abteilungen
							aufgebaut</span>
					</div>
					<div class="tooltip">
						<li class="choice"><input type="checkbox" id="survey_q3a4"
							class="hidden" name="os_4" /> <label
							class="label checkmark highlight" for="survey_q3a4"> <spring:message
									code="indicators.third_indicator.forth_choice" />
						</label></li> <span class="tooltiptext">Abteilungen werden zu gr��eren
							Organisationseinheiten zusammengefasst mit fest definierten und
							dauerhaften Zust�ndigkeiten</span>
					</div>
				</ul>
			</div>
		</div>
		<div style="clear: both"></div>
		<div id="nav" class="table-row navigation">
			<button class="form_button" name="back">
				<spring:message code="back" />
			</button>
			<button class="form_button">
				<spring:message code="next" />
			</button>
		</div>
	</form>
	</div>
</body>
</html>