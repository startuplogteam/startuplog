<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet" href="resources/css/tooltip.css">

<div class="container">
	<div id="popupClose" class="glyphicon glyphicon-remove"></div>
	<form id="initialplanning-update_informations-form" class="form"
		action="/saveIntoStorageFacility" method="get">
		<div id="informations">
			<div id="tabs" class="c-tabs no-js">
				<div class="c-tabs-nav">
					<div class="tooltip">
						<a href="#" class="c-tabs-nav__link is-active"><spring:message
								code="initial_planning_update_storage_unit" /></a> <span
							class="tooltiptext" style="left: 20%;">Lagereinheiten[LE] entstehen durch
							B�ndeln von F�lleinheiten in einem Beh�lter, auf einer Palette
							oder in einem anderen Ladehilfsmittel zum Zweck der Lagerung.
							(vgl. [Gud-2010, S. 413.])</span>
					</div>

					<div class="tooltip">
						<a href="#" class="c-tabs-nav__link"><spring:message
								code="initial_planning_update.storageStrategy" /></a> <span
							class="tooltiptext" style="left: -10%;">Aus einer im Lager bereitgestellten
							Gesamtmenge z.B. von Paletten- und oder Beh&auml;ltereinheiten
							werden Teilmengen nach vorgegebner Bedarfsinformation entnommen
							und zu einem Auftrag zusammengestellt.</span>
					</div>

					<div class="tooltip">
						<a href="#" class="c-tabs-nav__link"><spring:message
								code="initial_planning_update.furtherInfo" /></a><span
							class="tooltiptext" style="left: -30%;">Aus einer im Lager bereitgestellten
							Gesamtmenge z.B. von Paletten- und oder Beh&auml;ltereinheiten
							werden Teilmengen nach vorgegebner Bedarfsinformation entnommen
							und zu einem Auftrag zusammengestellt.</span>

					</div>
				</div>
				<div class="c-tab is-active" id="questions_storage_unit">
					<!--STORAGE UNIT---------------------------------->
					<div class="c-tab__content">
						<div class="boundary">
							<ul class="hiddenbulletpoints hidden" id="unit_q1a">
								<li>
									<div class="label head">
										<spring:message code="initial_planning_update_storage_unit_q1" />
									</div>
								</li>
								<li class="choice"><input type="radio"
									name="storage_in_delivery_packaging" id="unit_q1aYes"
									class="hidden" value="yes" /> <label
									class="label checkmark highlight" for="unit_q1aYes"> <spring:message
											code="yes" />
								</label></li>
								<li class="choice"><input type="radio"
									name="storage_in_delivery_packaging" id="unit_q1aNo"
									class="hidden" value="no" /> <label
									class="label checkmark highlight" for="unit_q1aNo"> <spring:message
											code="no" />
								</label></li>
							</ul>
						</div>
						<div class="boundary">
							<ul class="hiddenbulletpoints hidden" id="unit_q1b">
								<li>
									<div class="label head">
										<spring:message code="initial_planning_update_storage_unit_q1" />
									</div>
								</li>
								<li class="choice"><input type="radio"
									name="storage_in_dispatch_packaging" id="unit_q1bYes"
									class="hidden" value="yes" /> <label
									class="label checkmark highlight" for="unit_q1bYes"> <spring:message
											code="yes" />
								</label></li>
								<li class="choice"><input type="radio"
									name="storage_in_dispatch_packaging" id="unit_q1bNo"
									class="hidden" value="no" /> <label
									class="label checkmark highlight" for="unit_q1bNo"> <spring:message
											code="no" />
								</label></li>
							</ul>
						</div>
						<div class="boundary">
							<ul class="hiddenbulletpoints hidden" id="unit_q2">
								<li>
									<div class="label head">
										<spring:message code="initial_planning_update_storage_unit_q2" />
									</div>
								</li>
								<li class="choice"><select name="storage_packaging_type"
									id="storage_packaging_type" value="yes">
										<option value="not_chosen"></option>
										<option value="unpacked_small"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.unpacked_small" /></option>
										<option value="unpacked_big"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.unpacked_big" /></option>
										<option value="unpacked_long"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.unpacked_long" /></option>
										<option value="box_open"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.box_open" /></option>
										<option value="carton"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.carton" /></option>
										<option value="carton_pallet"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.carton_pallet" /></option>
										<option value="pallet"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.pallet" /></option>
										<option value="box_mesh"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.box_mesh" /></option>
										<option value="container_big"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.container_big" /></option>
										<option value="container_small"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.container_small" /></option>
										<option value="container_small_p"><spring:message
												code="initial_planning_update_storage_unit.storagePackingType.container_small_p" /></option>
								</select></li>
							</ul>
						</div>
						<div class="boundary">
							<ul class="hiddenbulletpoints hidden" id="unit_q3">
								<li>
									<div class="label head">
										<spring:message code="initial_planning_update_storage_unit_q3" />
									</div>
								</li>
								<li class="choice"><input type="radio"
									name="are_storage_unit_stackable" id="unit_q3Yes"
									class="hidden" value="yes" /> <label
									class="label checkmark highlight" for="unit_q3Yes"> <spring:message
											code="yes" />
								</label></li>
								<li class="choice"><input type="radio"
									name="are_storage_unit_stackable" id="unit_q3No" class="hidden"
									value="no" /> <label class="label checkmark highlight"
									for="unit_q3No"> <spring:message code="no" />
								</label></li>
							</ul>
						</div>
						<div class="boundary">
							<ul class="hiddenbulletpoints hidden" id="unit_q4">
								<li>
									<div class="label head">
										<spring:message code="initial_planning_update_storage_unit_q4" />
									</div>
								</li>
								<li class="choice"><input type="number"
									name="storage_packaging_stacking_height" min="1" /></li>
							</ul>
						</div>
						<div class="boundary">
							<ul class="hiddenbulletpoints hidden" id="unit_q5">
								<li>
									<div class="label head">
										<spring:message code="initial_planning_update_storage_unit_q5" />
									</div>
								</li>
								<li class="choice"><input type="number"
									name="amount_of_items_per_storage_packaging" class="long"
									min="0" /></li>
							</ul>
						</div>
						<div class="boundary">
							<ul class="hiddenbulletpoints hidden" id="unit_q6">
								<li>
									<div class="label head">
										<spring:message code="initial_planning_update_storage_unit_q6" />
									</div>
								</li>
								<li class="choice"><spring:message
										code="initial_planning_update_storage_unit_length" /><input
									type="number" name="storage_packaging_length"
									placeholder="in mm" min="0" /></li>
								<li class="choice"><spring:message
										code="initial_planning_update_storage_unit_width" /><input
									type="number" name="storage_packaging_width"
									placeholder="in mm" min="0" /></li>
								<li class="choice"><spring:message
										code="initial_planning_update_storage_unit_height" /><input
									type="number" name="storage_packaging_height"
									placeholder="in mm" min="0" /></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="c-tab" id="questions_storage_strategy">
					<!--STORAGE STRATEGY------------------------------>
					<div class="c-tab__content">
						<div class="boundary">
							<ul class="hiddenbulletpoints" id="strategy_q1">
								<li>
									<div class="label head">
										<spring:message
											code="initial_planning_update_storage_strategy_q1" />
									</div>
								</li>
								<li class="choice"><input type="radio"
									name="storage_bin_assignment" id="strategy_q1Yes"
									class="hidden" value="yes" /> <label
									class="label checkmark highlight" for="strategy_q1Yes">
										<spring:message code="yes" />
								</label></li>
								<li class="choice"><input type="radio"
									name="storage_bin_assignment" id="strategy_q1No" class="hidden"
									value="no" /> <label class="label checkmark highlight"
									for="strategy_q1No"> <spring:message code="no" />
								</label></li>
								<li class="choice"><input type="radio"
									name="storage_bin_assignment" id="strategy_q1Irrelevant"
									class="hidden" value="irrelevant" /> <label
									class="label checkmark highlight" for="strategy_q1Irrelevant">
										<spring:message
											code="initial_planning_update_storage_strategy_irrelevant" />
								</label></li>
							</ul>
						</div>
						<div class="boundary">
							<ul class="hiddenbulletpoints" id="strategy_q2">
								<li>
									<div class="label head">
										<spring:message
											code="initial_planning_update_storage_strategy_q2" />
									</div>
								</li>
								<li class="choice"><input type="radio"
									name="storage_allocation" id="strategy_q2Yes" class="hidden"
									value="yes" /> <label class="label checkmark highlight"
									for="strategy_q2Yes"> <spring:message code="yes" />
								</label></li>
								<li class="choice"><input type="radio"
									name="storage_allocation" id="strategy_q2No" class="hidden"
									value="no" /> <label class="label checkmark highlight"
									for="strategy_q2No"> <spring:message code="no" />
								</label></li>
								<li class="choice"><input type="radio"
									name="storage_allocation" id="strategy_q2Irrelevant"
									class="hidden" value="irrelevant" /> <label
									class="label checkmark highlight" for="strategy_q2Irrelevant">
										<spring:message
											code="initial_planning_update_storage_strategy_irrelevant" />
								</label></li>
							</ul>
						</div>
						<div class="boundary">
							<ul class="hiddenbulletpoints" id="strategy_q3">
								<li>
									<div class="label head">
										<spring:message
											code="initial_planning_update_storage_strategy_q3" />
									</div>
								</li>
								<li class="choice"><div class="tooltip">
								<input type="radio"
									name="storage_removal_strategy" id="strategy_q3Fifo"
									class="hidden" value="fifo" />
										<label class="label checkmark highlight" for="strategy_q3Fifo">
											FIFO </label><span class="tooltiptext" style = "width: 120px;">Beim FIFO-Prinzip
											werden die einzelnen Ladeeinheiten in der Reihenfolge ihrer
											Einlagerung ausgelagert werden. (vgl. [Gud-2010, S. 600.])</span>
									</div></li>
								<li class="choice"><div class="tooltip"><input type="radio"
									name="storage_removal_strategy" id="strategy_q3Lifo"
									class="hidden" value="lifo" />
										<label class="label checkmark highlight" for="strategy_q3Lifo">
											LIFO <span class="tooltiptext" style = "width: 120px;">Beim LIFO-Prinzip
												werden die zuletzt eingelagerten Ladeeinheiten als erstes
												ausgelagert. (vgl. [Hei-2011, S. 68.])</span>
										</label>
									</div></li>
								<li class="choice"><input type="radio"
									name="storage_removal_strategy" id="strategy_q3Irrelevant"
									class="hidden" value="irrelevant" /> <label
									class="label checkmark highlight" for="strategy_q3Irrelevant">
										<spring:message
											code="initial_planning_update_storage_strategy_irrelevant" />
								</label></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="c-tab" id="questions_misc">
					<!--FURTHER INFORMATION--------------------------->
					<div class="c-tab__content">
						<div class="boundary">
							<ul class="hiddenbulletpoints" id="misc_q1">
								<li>
									<div class="label head">
										<spring:message code="initial_planning_update_misc_q1" />
									</div>
								</li>
								<li class="choice"><input type="radio"
									name="material_quantity" id="misc_q1High" class="hidden"
									value="high quantity" /> <label
									class="label checkmark highlight" for="misc_q1High"> <spring:message
											code="initial_planning_update_misc_highQuantity" /></label></li>
								<li class="choice"><input type="radio"
									name="material_quantity" id="misc_q1Medium" class="hidden"
									value="medium-sized quantity" /> <label
									class="label checkmark highlight" for="misc_q1Medium">
										<spring:message
											code="initial_planning_update_misc_mediumQuantity" />
								</label></li>
								<li class="choice"><input type="radio"
									name="material_quantity" id="misc_q1Low" class="hidden"
									value="low quantity" /> <label
									class="label checkmark highlight" for="misc_q1Low"><spring:message
											code="initial_planning_update_misc_lowQuantity" /></label></li>
							</ul>
						</div>
						<div class="boundary">
							<ul class="hiddenbulletpoints" id="misc_q2">
								<li>
									<div class="label head">

										<div class="tooltip">
											<spring:message code="initial_planning_update_misc_q2" />
											<span class="tooltiptext">Kommissionieren bezeichnet
												das Zusammenstellen von Einzelpositionen zu einem Auftrag.
												(z.B. Zusammenstellen von Material, das f�r die Produktion
												eines bestimmten Produktes notwendig ist) [Hom-2011, S. 152]</span>
										</div>

									</div>
								</li>
								<li class="choice"><input type="radio" name="order_picking"
									id="misc_q2Yes" class="hidden" value="yes" /> <label
									class="label checkmark highlight" for="misc_q2Yes"> <spring:message
											code="yes" />
								</label></li>
								<li class="choice"><input type="radio" name="order_picking"
									id="misc_q2No" class="hidden" value="no" /> <label
									class="label checkmark highlight" for="misc_q2No"> <spring:message
											code="no" />
								</label></li>
							</ul>
						</div>
						<div class="boundary">
							<ul class="hiddenbulletpoints" id="misc_q3">
								<li>
									<div class="label head">
										<spring:message code="initial_planning_update_misc_q3" />
									</div>
								</li>
								<li class="choice"><input type="radio"
									name="mech_auto_possibility" id="misc_q3Yes"
									class="storage_automation hidden" value="yes" /> <label
									class="label checkmark highlight" for="misc_q3Yes"> <spring:message
											code="yes" />
								</label></li>
								<li class="choice"><input type="radio"
									name="mech_auto_possibility" id="misc_q3No" class="hidden"
									value="no" /> <label class="label checkmark highlight"
									for="misc_q3No"> <spring:message code="no" />
								</label></li>
								<li class="choice"><input type="radio"
									name="mech_auto_possibility" id="misc_q3Irrelevant"
									class="hidden" value="it depends on." /> <label
									class="label checkmark highlight" for="misc_q3Irrelevant">
										<spring:message code="initial_planning_update_misc_depends" />
								</label></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div style="clear: both"></div>
		<div id="nav" class="table-row navigation">
			<button type="button" id="popupPrev"
				class="form_button glyphicon glyphicon-chevron-left"></button>
			<button type="button" id="popupNext"
				class="form_button glyphicon glyphicon-chevron-right"></button>
			<input type="hidden" name="material_id" id="material_id_field"
				value="">
			<button id="popupSave" class="form_button">
				<spring:message code="save" />
			</button>
		</div>
		<div id="response"></div>
	</form>
</div>