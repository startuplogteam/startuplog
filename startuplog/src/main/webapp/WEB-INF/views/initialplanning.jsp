<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>StartupLog</title>
<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/multiselect.css">
<link rel="stylesheet" href="resources/css/styles.css">
<link rel="stylesheet" href="resources/css/warehouse_logistics.css">
<link rel="stylesheet" href="resources/css/tabs.css">
<link rel="stylesheet" href="resources/css/tabs2.css">
<link rel="stylesheet" href="resources/css/editContent.css">
<link rel="stylesheet" href="resources/css/tooltip.css">
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script src="resources/js/jquery-3.1.1.min.js"></script>
<script src="resources/js/multiselect.min.js"></script>
<script src="resources/js/tabs.js"></script>
<script src="resources/js/survey.js"></script>
<script src="resources/js/loadData.js"></script>

<script type="text/javascript">
	var appendMaterialRow = function(material, suggestion, element) {
		var material_name = suggestion[0];
		var sliced_suggestion = suggestion.slice(1, suggestion.length);

		if (sliced_suggestion.length == 0) {
			sliced_suggestion = "<spring:message code="all_info_given_no_match"/>"
		}
		$(element)
				.append(
						'<div class="table-row material_' + material + '"> \
                            <div class="table-cell"> \
                                '
								+ material
								+ ' \
                            </div> \
                            <div class="table-cell"> \
                            '
								+ material_name
								+ ' \
                        	</div> \
                            <div class="table-cell"> \
                                '
								+ sliced_suggestion
								+ ' \
                            </div> \
                        </div>');

	}

	$.fn.appendIncompleteMaterialRow = function(material, container) {

		$
				.ajax({
					type : "GET",
					url : "/getMaterialName",
					data : {
						materialID : material
					},
					success : function(material_name) {
						$(container)
								.append(
										'<div class="table-row material_' + material + '"> \
                                   <div class="table-cell"> \
                                       '
												+ material
												+ ' \
                                   </div> \
                                   <div class="table-cell"> \
                                   '
												+ material_name
												+ ' \
                               </div> \
                                   <div class="table-cell incomplete"> \
                                       <spring:message code="all_info_not_given"/> \
                                   </div> \
                               </div>');

					},
					error : function(exception) {

					}
				});

	}

	var checkAreAllQuestionsValid = function(root, tabIndex) {
		if (root.areAllSelectedDescendantsValid())
			$("#informations .c-tabs-nav__link:eq(" + tabIndex + ")")
					.removeClass("incomplete");
		else
			$("#informations .c-tabs-nav__link:eq(" + tabIndex + ")").addClass(
					"incomplete");
	}

	var check_all_information_are_provided = function() {
		// check miscellineous 
		var misc = 0;
		if ($('input[name=material_quantity]').is(':checked')) {
			misc += 1;
		}
		if ($('input[name=order_picking]').is(':checked')) {
			misc += 1;
		}

		if ($('input[name=mech_auto_possibility]').is(':checked')) {
			misc += 1;
		}

		if (misc == 3) {
			$("#informations .c-tabs-nav__link:eq(2)")
					.removeClass("incomplete");
		}

		// check storage 
		var storage = 0;
		if ($('input[name=storage_bin_assignment]').is(':checked')) {
			storage += 1;
		}
		if ($('input[name=storage_allocation]').is(':checked')) {
			storage += 1;
		}

		if ($('input[name=storage_removal_strategy]').is(':checked')) {
			storage += 1;
		}

		if (storage == 3) {
			$("#informations .c-tabs-nav__link:eq(1)")
					.removeClass("incomplete");
		}
 
		// Check storage Unit
		storage_unit_complete = true;
		if (!$('input[name=are_storage_unit_stackable]').is(':checked')) {
			storage_unit_complete = false;
		}

		if ($('input[name=are_storage_unit_stackable]:checked').val() == "yes") {
			if (!$('input[name=storage_packaging_stacking_height]').val()) {
				storage_unit_complete = false;
			}
		}
		
		if ($("#storage_packaging_type option:selected").text() == ""){
			storage_unit_complete = false;
		}
		
		if ($("#storage_packaging_type option:selected").text() != "material unpacked (small)" && $("#storage_packaging_type option:selected").text() != "material unpacked (big)" && $("#storage_packaging_type option:selected").text() != "long material unpacked (e.g. metal rods)" ){
			if (!$('input[name=amount_of_items_per_storage_packaging]').val() || !$('input[name=storage_packaging_length]').val() || !$('input[name=storage_packaging_width]').val() || !$('input[name=storage_packaging_height]').val()) {
				
				storage_unit_complete = false;
			}
		}
		
		if ($('input[name=storage_in_delivery_packaging]:checked').val() == "yes" || $('input[name=storage_in_dispatch_packaging]:checked').val() == "yes" ) {
			storage_unit_complete = true;
		}
		
		if (storage_unit_complete == true) {
			$("#informations .c-tabs-nav__link:eq(0)")
					.removeClass("incomplete");

		}

	}
	var fetch_previous_data = function(material_id) {

		$
				.ajax({
					url : "/getPreviousInitialplanningData",
					data : {
						materialID : material_id
					},
					dataType : "json",
					success : function(data) {
						for ( var key in data) {
							$(
									"input[name=" + key + "][value='"
											+ data[key] + "']").prop("checked",
									true);

						}

						if (!jQuery.isEmptyObject(data)) {
							$("#unit_q2").removeClass("hidden");
							$("#unit_q3").removeClass("hidden");

							$("select[name=storage_packaging_type]").val(
									data['storage_packaging_type']);

							if (data['are_storage_unit_stackable'] == 'yes') {
								$("#unit_q4").removeClass("hidden");
								$(
										'input[name=storage_packaging_stacking_height]')
										.val(
												data['storage_packaging_stacking_height']);

							}
							if (data['storage_packaging_type'] != "unpacked_small"
									&& data['storage_packaging_type'] != "unpacked_big"
									&& data['storage_packaging_type'] != "unpacked_long") {

								$("#unit_q5").removeClass("hidden");
								$("#unit_q6").removeClass("hidden");

								$(
										'input[name=amount_of_items_per_storage_packaging]')
										.val(
												data['amount_of_items_per_storage_packaging']);

								$('input[name=storage_packaging_length]').val(
										data['storage_packaging_length']);
								$('input[name=storage_packaging_width]').val(
										data['storage_packaging_width']);
								$('input[name=storage_packaging_height]').val(
										data['storage_packaging_height']);
							}
						}

						check_all_information_are_provided();

					},
					error : function(exception) {
						//alert(JSON.stringify(exception));
					},
					type : 'GET'
				});

	}
	var check_packaging_possibility = function(material_id) {
		$.ajax({
			url : "/check_supplier_customer_table",
			data : {
				materialID : material_id
			},
			dataType : "text",
			success : function(exist) {
				if (exist == "false") {

					$("#unit_q1aYes").attr('disabled', true);

					$("#unit_q1bYes").attr('disabled', true);

				}
			},
			error : function(exception) {
				alert(JSON.stringify(exception))
			},
			type : "GET"
		});

	}

	var loadPopupScripts = function(material_id) {
		MaterialTypeEnum = {
			PURCHASED : "raw material",
			SEMIFINISHED : "semi-finished product",
			FINISHED : "finished product"
		}
		$
				.ajax({
					url : "/getMaterialType",
					data : {
						material_id : material_id
					},
					dataType : "text",
					success : function(typeOfMaterial) {

						var unit_root = null;
						var unit_q6 = getNumericInputQuestion("#unit_q6");
						var unit_q5 = getNumericInputQuestion("#unit_q5");
						var unit_q4 = getNumericInputQuestion("#unit_q4");
						var unit_q4_to_q6 = new QuestionGroup([ unit_q4,
								unit_q5, unit_q6 ]);
						var unit_q5_to_q6 = new QuestionGroup([ unit_q5,
								unit_q6 ]);

						var unit_q3_unpacked = getRadiobuttonsQuestion(
								"#unit_q3", {
									"yes" : unit_q4,
									"no" : new SurveyTail()
								});
						var unit_q3 = getRadiobuttonsQuestion("#unit_q3", {
							"yes" : unit_q4_to_q6,
							"no" : unit_q5_to_q6
						});
						var unit_q2 = getSelectionQuestion("#unit_q2", {
							"unpacked_small" : unit_q3_unpacked,
							"unpacked_big" : unit_q3_unpacked,
							"unpacked_long" : unit_q3_unpacked,
							"box_open" : unit_q3,
							"carton" : unit_q3,
							"carton_pallet" : unit_q3,
							"pallet" : unit_q3,
							"box_mesh" : unit_q3,
							"container_big" : unit_q3,
							"container_small" : unit_q3,
							"container_small_p" : unit_q3
						});

						switch (typeOfMaterial) {
						case MaterialTypeEnum.PURCHASED: {
							var unit_q1 = getRadiobuttonsQuestion_Enabled(
									"#unit_q1a", {
										"yes" : new SurveyTail(),
										"no" : unit_q2
									});
							$("#unit_q1a").removeClass("hidden");
							unit_root = unit_q1;
							break;
						}
						case MaterialTypeEnum.SEMIFINISHED: {
							unit_q2.enabled = true;
							$("#unit_q2").removeClass("hidden");

							unit_root = unit_q2;
							break;
						}
						case MaterialTypeEnum.FINISHED: {
							var unit_q1 = getRadiobuttonsQuestion_Enabled(
									"#unit_q1b", {
										"yes" : new SurveyTail(),
										"no" : unit_q2
									});
							$("#unit_q1b").removeClass("hidden");
							unit_root = unit_q1;
							break;
						}
						}

						var strategy_q3 = getRadiobuttonsQuestion_Enabled("#strategy_q3");
						var strategy_q2 = getRadiobuttonsQuestion_Enabled("#strategy_q2");
						var strategy_q1 = getRadiobuttonsQuestion_Enabled("#strategy_q1");
						var strategy_group = new QuestionGroup([ strategy_q1,
								strategy_q2, strategy_q3 ]);
						var misc_q3 = getRadiobuttonsQuestion_Enabled("#misc_q3");
						var misc_q2 = getRadiobuttonsQuestion_Enabled("#misc_q2");
						var misc_q1 = getRadiobuttonsQuestion_Enabled("#misc_q1");
						var misc_group = new QuestionGroup([ misc_q1, misc_q2,
								misc_q3 ]);

						var myTabs = tabs({
							el : '#tabs',
							tabNavigationLinks : '.c-tabs-nav__link',
							tabContentContainers : '.c-tab'
						});
						myTabs.init();

						var initialplanningForm = $('#initialplanning-update_informations-form');

						initialplanningForm.submit(function(e) { //ajax handling of the form to prevent redirect

							$("#material_id_field").val(
									$('#search_to option:selected').val());
							$.ajax({
								type : initialplanningForm.attr('method'),
								url : initialplanningForm.attr('action'),
								data : initialplanningForm.serialize(), // serializes the form's elements.
								success : function(data) {
									$("#popupSave").prop("disabled", true);
									$("#response").fadeOut(
											100,
											function() {
												$("#response").html(
														"successfully saved");
											})
									$("#response").fadeIn(500).fadeOut(100)
											.fadeIn(500).fadeOut(100).fadeIn(
													500);
								},
								error : function(exception) {
									$("#popupSave").prop("disabled", true);
									$("#response").fadeOut(
											100,
											function() {
												$("#response").html(
														"error while saving");
											})
									$("#response").fadeIn(500).fadeOut(100)
											.fadeIn(500).fadeOut(100).fadeIn(
													500);
									//alert(JSON.stringify(exception));
								}
							});

							e.preventDefault(); // avoid to execute the actual submit of the form.
						});

						$('#informations-dialog').on('click', '#popupNext',
								function() {
									var index = myTabs.getActiveTabIndex() + 1;
									if (index > 2)
										index -= 3;
									myTabs.goToTab(index);
								});
						$('#informations-dialog').on('click', '#popupPrev',
								function() {

									var index = myTabs.getActiveTabIndex() - 1;
									if (index < 0)
										index += 3;
									myTabs.goToTab(index);
								});

						$(document).on("questionChangeEvent", function(e) {
							checkAreAllQuestionsValid(unit_root, 0);
							checkAreAllQuestionsValid(strategy_group, 1);
							checkAreAllQuestionsValid(misc_group, 2);
							$("#popupSave").prop("disabled", false);
						});

						checkAreAllQuestionsValid(unit_root, 0);
						checkAreAllQuestionsValid(strategy_group, 1);
						checkAreAllQuestionsValid(misc_group, 2);
						$("#popupSave").prop("disabled", true);
					},
					error : function(exception) {
					},
					type : "POST"
				});
	}

	var areInformationsComplete = function(material_ID_to_plan) {
		$.ajax({
			url : "/getMaterialSuggestionAsJson",
			data : {
				material_id : material_ID_to_plan
			},
			dataType : "json",
			success : function(data) {

				if (data.hasOwnProperty(material_ID_to_plan)) {

					$('#search_to option[value="' + material_ID_to_plan + '"]')
							.removeClass("informationsIncomplete").addClass(
									"informationsComplete");
					appendMaterialRow(material_ID_to_plan,
							data[material_ID_to_plan],
							$("#scroll_wrapper #content"));

				} else {

					$('#search_to option[value="' + material_ID_to_plan + '"]')
							.removeClass("informationsComplete").addClass(
									"informationsIncomplete");
					$('#scroll_wrapper #content').appendIncompleteMaterialRow(
							material_ID_to_plan, "#scroll_wrapper #content");
				}

			},
			error : function(exception) {
				alert("Problem in get_material_suggestion");
				alert('Exeption:' + JSON.stringify(exception));

			},
			type : "GET"
		});
	}

	var closePopup = function() {
		$('#informations-dialog, #mask').fadeOut("fast");
	}
	var openPopup = function() {
		if ($('#search_to option:selected').val() != undefined) {
			$.ajax({
				url : "/initialplanning-update_informations",
				dataType : "html",
				success : function(data) {
					$("#mask").fadeIn("fast");
					$("#informations-dialog").html(data).fadeIn("fast");
					loadPopupScripts($('#search_to option:selected').val());
					check_packaging_possibility($('#search_to option:selected')
							.val());
					fetch_previous_data($('#search_to option:selected').val());

				},
				error : function(exception) {

				},
				type : "POST"
			});
			return false;
		} else {
			alert("<spring:message code="select_material_first"/>");
		}
	}

	$(document).ready(
			function() {
				
				$('.edit').click(function(){
					  $(this).hide();
					  $('.box').addClass('editable');
					  $('.text').attr('contenteditable', 'true');  
					  $('.save').show();
					});

					$('.save').click(function(){
					  $(this).hide();
					  $('.box').removeClass('editable');
					  $('.text').removeAttr('contenteditable');
					  $('.edit').show();
					});
				
				
				$(".tabbable").find(".tab").hide();
			    $(".tabbable").find(".tab").first().show();
			    $(".tabbable").find(".tabs li").first().find("a").addClass("active");
			    $(".tabbable").find(".tabs").find("a").click(function(){
			        tab = $(this).attr("href");
			        $(".tabbable").find(".tab").hide();
			        $(".tabbable").find(".tabs").find("a").removeClass("active");
			        $(tab).show();
			        $(this).addClass("active");
			        return false;
			    });
				
				
				$('#informations-dialog').on(
						'click',
						'#popupClose',
						function() {
							closePopup();
							$(
									'#scroll_wrapper #content .material_'
											+ $('#search_to option:selected')
													.val()).remove();
							areInformationsComplete($(
									'#search_to option:selected').val());

						});
				$(document).on("click", function(e) {
					$("#popupSave").prop("disabled", false);
					check_all_information_are_provided();
				});
				$('#search').multiselect({
					right : 'selected',
					left : 'all',
					rightSelected : 'search_rightSelected',
					leftSelected : 'search_leftSelected',
					leftAll : 'search_leftAll',
					afterMoveToRight : function(left, right, options) {
						var material_to_plan;
						//get the ID from Jquery-Object Array
						//option contains only one ID, ID of the selected material

						options.each(function() {

							material_to_plan = $(this).val();

						});

						areInformationsComplete(material_to_plan);
					},
					afterMoveToLeft : function(left, right, options) {
						var material_to_remove;
						options.each(function() { //get the ID from Jquery-Object Array
							material_to_remove = $(this).val();
						});
						$('#scroll_wrapper #content .material_'+ material_to_remove).remove();
					
					}
				});

				$("#search_updateInformations").on('click', openPopup);

				$("#search_to").off("dblclick", "option");
				$("#search_to").on("dblclick", "option", function(e) {
					openPopup();
				});
			});
</script>
</head>
<body>

	<div class="tabbable">
    <ul class="tabs">
        <li><a href="#tab1">Beschreibung</a></li>
        <li><a href="#tab2">Anwendungs-Tool</a></li>
    </ul>
    <div class="tabcontent">
         <div id="tab1" class="tab box">
                       
  <span class="edit">Edit</span>
  <span class="save">Save</span>
  <div class="text">
    Hover this box and click on edit! - You can edit me then.
    <br>When you finished - click save and you saved it.
    </div>

            
        </div>
        <div id="tab2" class="tab">
        
        <div class="info">
		<p>
			<spring:message code="initial_planning.message_1" />
		</p>
		<p>
			<spring:message code="initial_planning.message_2" />
		</p>
		<p>
			<spring:message code="initial_planning.message_3" />
		</p>
	</div>
	<div class="container">
		<form id="initial_planning" class="form">
			<div>
				<div id="left">
					<div class="search_header">
						<spring:message code="initial_planning.available_materials" />
					</div>
					<select id="search" class="form-control" size="8" multiple>
						<c:forEach var="material" items="${materialList}">
							<option value="${material.material_number }"><spring:message
									code="${material.material_name}" /></option>
						</c:forEach>
					</select>
					<button type="button" id="search_rightSelected"
						class="btn btn-block form_button right">
						<i class="glyphicon glyphicon-chevron-right"></i>
					</button>
				</div>
				<div id="right">
					<div class="search_header">
						<spring:message code="initial_planning.selected_materials" />
					</div>
					<select id="search_to" class="form-control" size="8"></select>
					<button type="button" id="search_leftSelected"
						class="btn btn-block form_button">
						<i class="glyphicon glyphicon-chevron-left"></i>
					</button>
					<button type="button" id="search_leftAll"
						class="btn btn-block form_button">
						<i class="glyphicon glyphicon-chevron-left"></i><i
							class="glyphicon glyphicon-chevron-left"></i>
					</button>
					<button type="button" id="search_updateInformations"
						class="btn btn-block form_button right">
						<spring:message code="initial_planning.update_information" />
					</button>
				</div>
			</div>
			<div id="suggestions" class="boundary">
				<h1>
					<spring:message code="initial_planning.warehouse_suggestions" />
				</h1>
				<div id="suggestions_table">
					<div id="header" class="table">
						<div class="table-row">
							<div class="table-cell header">
								<spring:message code="initial_planning.material_id" />
							</div>
							<div class="table-cell header">
								<spring:message code="initial_planning.material_name" />
							</div>
							<div class="table-cell header">
								<spring:message
									code="initial_planning.possible_storage_facility" />
							</div>
						</div>
					</div>
					<div id="scroll_wrapper">
						<div id="content" class="table"></div>
					</div>
				</div>
			</div>
			<div style="clear: both"></div>
		</form>
	</div>
	<div id="mask"></div>
	<div id="informations-dialog" class="popup"></div>
        
        </div>
        </div>
        </div>
        

	
</body>
</html>
