<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- let's add tag srping:url -->
<spring:url value="/resources/css/styles.css" var="startupLogCSS" />
<spring:url value="/resources/js/jquery-3.1.1.min.js" var="startupLogJS" />
<spring:url value="/resources/images/startuplog_logo_1.ico" var="favcon" />
<link href="${startupLogCSS}" rel="stylesheet" />
<link href="${favcon}" rel="icon" />
<script src="${startupLogJS}"></script>
<title>StartupLog</title>
  <script type="text/javascript">
   $(document).ready(function ()
        {
			var lang = "${pageContext.response.locale}";
		
			if (lang == "de")
				{
					$('#de').addClass('visited');
				
				}
			else
				{
					$( "#en" ).addClass( "visited" );
				}


		});
  
 
  </script>
</head>
<body>
	
      <div class="container">
      <div class= "topcorner">
                  	<a id = "en"  href="?language=en">en</a>  |   <a id ="de" href="?language=de">de</a>
                  </div>
          <div id="login-form" class="form"> 
              <div class="thumbnail">
                  <img src="resources/images/StartupLog_logo.png">
              </div>
              <form action="/login" method="POST">
              <h6 class="loginerror"> ${loginerror}</h6>
                  <input class="form_input" placeholder=<spring:message code="login.username"/> name="username" type="text">
                  <input class="form_input" placeholder=<spring:message code="login.password"/> name="password" type="password">
                  <button class="form_button"><spring:message code="login.submit"/></button>
                  
                  
              </form>
          </div>
      </div>
</body>
</html>