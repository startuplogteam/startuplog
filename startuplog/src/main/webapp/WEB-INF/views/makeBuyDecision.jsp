<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>StartupLog</title>

<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/jquery-ui.css">
<link rel="stylesheet" href="resources/css/multiselect.css">
<link rel="stylesheet" href="resources/css/styles.css">
<link rel="stylesheet" href="resources/css/warehouse_logistics.css">
<link rel="stylesheet" href="resources/css/tabs.css">
<link rel="stylesheet" href="resources/css/tabs2.css">
<link rel="stylesheet" href="resources/css/editContent.css">
<link rel="stylesheet" href="resources/css/tooltip.css">
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script src="resources/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="resources/js/multiselect.min.js" type="text/javascript"></script>
<script src="resources/js/jquery-ui.js"></script>
<script src="resources/js/tabs.js" type="text/javascript"></script>
<script src="resources/js/survey.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$('[name="delete-button"]').click(function(){
				        	
							var name = $(this).parent().parent().children('#productName').text();
				        			if(confirm("Are you sure to delete product : "+name)==false){
				        			 return true;
				        			}
				        		/*	$.ajax({
				           	 			type: "POST",
				           	 			url: "/deleteUser",
				           				data: { username : name },
				           	 			success: function () {
				                            window.location.reload();
				        	 			},
				           	 			error: function (exception) {

				           				}
				            		});*/
				        			
				        	
				    	});
						

						function recalculate() {

							var totals = [];
							var temp = [];
							var outputs = $('#resultrow').find(
									'input[type="number"]');
							for (var i = 0; i < supplierCount; i++) {
								var total = 0;
								$(
										'table tbody tr:not(:last):not(:last):not(:last)')
										.each(
												function(j, row) {
													var $row = $(row);
													var inputs = $row
															.find('select[name="selectValue"]');
													total = parseInt(total)
															+ parseInt(inputs
																	.eq(i)
																	.val());
												});
								outputs.eq(i).val(total);
								totals[i] = total;
								temp[i] = total;
							}

							var ranks = $('#rankrow').find(
									'input[type="number"]');
							temp.sort(function(a, b) {
								return b - a
							});
							for (var i = 0; i < supplierCount; i++) {
								ranks.eq(i).val($.inArray(totals[i], temp) + 1);
							}

						}

						$(document)
								.on(
										'change',
										".selectchange",
										function() {

											var newValue = $(this).val();
											var $tds = $(this).parents('tr')
													.find('td');
											var critera = $tds.eq(1).text();
											var columnIndex = $(this).parents(
													'td').index();
											var supplierName = $(
													"#suppliertable")
													.find('th').eq(columnIndex)
													.html();
											var material_number = $(
													"#materialdrop option:selected")
													.val();

											$
													.ajax({
														type : "GET",
														url : "/updateSupplierValue",
														contentType : 'application/json',
														data : {
															id : material_number,
															criteria : critera,
															supplier : supplierName,
															value : newValue
														},
														dataType : "json",
														success : function(data) {
														},
														error : function(
																exception) {

														}

													});

											recalculate();
										});

						$("#materialdrop")
								.change(
										function() {

											var materialID = $(this).val();

											$
													.ajax({
														type : "GET",
														url : "/fetchsuppliers",
														contentType : 'application/json',
														data : {
															material_id : materialID
														},
														dataType : "json",
														success : function(data) {
															$('#toShow').show();
															$("#successmsg")
																	.hide();
															if (data.length == 0) {
																$('#toShow')
																		.hide();
															}
															var supplierNames = [];
															$(
																	'#suppliertable thead')
																	.empty();
															$(
																	'#suppliertable tbody')
																	.empty();
															$(
																	'#suppliertable thead')
																	.append(
																			'<th>Select</th><th>Criteria</th>');

															for (var i = 0; i < data.length; i++) {
																if (!supplierNames
																		.includes(data[i].supplierName)) {
																	supplierNames
																			.push(data[i].supplierName);
																}
															}
															supplierCount = supplierNames.length;

															for (var i = 0; i < supplierNames.length; i++) {
																$(
																		'table thead th:last-child')
																		.after(
																				'<th>'
																						+ supplierNames[i]
																						+ '</th>');
															}

															var criterias = [];
															for (var i = 0; i < data.length; i++) {
																if (!criterias
																		.includes(data[i].criteria)) {
																	criterias
																			.push(data[i].criteria);
																}
															}
															for (var i = 0; i < criterias.length; i++) {
																var appendHtml = "";
																for (var j = 0; j < supplierNames.length; j++) {
																	for (var n = 0; n < data.length; n++) {
																		if ((data[n].supplierName == supplierNames[j])
																				&& (data[n].criteria == criterias[i])) {
																			appendHtml = appendHtml
																					+ "<td><select name='selectValue' class='selectchange'>";
																			for (var k = 1; k <= 10; k++) {
																				if (k == data[n].value) {
																					appendHtml = appendHtml
																							+ "<option selected value="+k+">"
																							+ k
																							+ "</option>";
																				} else
																					appendHtml = appendHtml
																							+ "<option value="+k+">"
																							+ k
																							+ "</option>";
																			}
																			appendHtml = appendHtml
																					+ "</select'></td>";
																		}
																	}
																}
																$('table tbody')
																		.append(
																				'<tr><td><input type="checkbox" name="record"></td><td>'
																						+ criterias[i]
																						+ '</td>'
																						+ appendHtml
																						+ '</tr>');
															}
															var lastrow = '<tr id="resultrow"><td><b>Total</b></td><td></td>';
															for (var i = 0; i < supplierCount; i++) {
																lastrow = lastrow
																		+ '<td><input type="number" disabled></td>';
															}
															lastrow = lastrow
																	+ '</tr>';

															lastrow = lastrow
																	+ '<tr id="rankrow"><td><b>Ranking</b></td><td></td>';
															for (var i = 0; i < supplierCount; i++) {
																lastrow = lastrow
																		+ '<td><input type="number" disabled></td>';
															}
															lastrow = lastrow
																	+ '</tr>';

															lastrow = lastrow
																	+ '<tr id="selectrow"><td><b>Choose Supplier</b></td><td></td>';
															for (var i = 0; i < supplierCount; i++) {
																lastrow = lastrow
																		+ '<td><input type="checkbox" name="selectsupplier"></td>';
															}
															lastrow = lastrow
																	+ '</tr>';

															$('table tbody')
																	.append(
																			lastrow);

															recalculate();

														},
														error : function(
																exception) {

														}
													});
										});

						$(".add-row")
								.click(
										function() {
											var newcriteria = $("#newcriteria")
													.val();
											if (newcriteria == '')
												return;
											var material_number = $(
													"#materialdrop option:selected")
													.val();
											$
													.ajax({
														type : "GET",
														url : "/addcriteria",
														contentType : 'application/json',
														data : {
															criteria : newcriteria,
															material : material_number
														},
														dataType : "json",
														success : function() {

														},
														error : function(
																exception) {

														}
													});

											var appendHtml = "";
											for (var j = 0; j < supplierCount; j++) {
												appendHtml = appendHtml
														+ "<td><select name='selectValue' class='selectchange'>";
												for (var k = 1; k <= 10; k++) {
													if (k == 5) {
														appendHtml = appendHtml
																+ "<option selected value="+k+">"
																+ k
																+ "</option>";
													} else
														appendHtml = appendHtml
																+ "<option value="+k+">"
																+ k
																+ "</option>";
												}
												appendHtml = appendHtml
														+ "</select></td>";
											}
											var markup = "<tr><td><input type='checkbox' name='record'></td><td>"
													+ newcriteria
													+ "</td>"
													+ appendHtml + "</tr>";
											$("table tbody tr:last").prev()
													.prev().prev()
													.after(markup);

											recalculate();

										});

						$("#delete-row")
								.click(
										function() {
											$("table tbody")
													.find(
															'input[name="record"]')
													.each(
															function() {
																if ($(this)
																		.is(
																				":checked")) {
																	var criteria = $(
																			this)
																			.closest(
																					'td')
																			.next(
																					'td')
																			.text();
																	var material_number = $(
																			"#materialdrop option:selected")
																			.val();
																	if (confirm("Are you sure to delete criteria : "
																			+ criteria) == false) {
																		$(this)
																				.prop(
																						'checked',
																						false);
																		return true;
																	}

																	$
																			.ajax({
																				type : "GET",
																				url : "/deletecriteria",
																				contentType : 'application/json',
																				data : {
																					criteria : criteria,
																					material : material_number
																				},
																				dataType : "json",
																				success : function() {

																				},
																				error : function(
																						exception) {

																				}
																			});

																	$(this)
																			.parents(
																					"tr")
																			.remove();

																	recalculate();
																}
															});
										});

						$("#saveChanges")
								.click(
										function() {
											$("#selectrow")
													.find(
															'input[name="selectsupplier"]')
													.each(
															function() {
																if ($(this)
																		.is(
																				":checked")) {
																	var columnIndex = $(
																			this)
																			.parents(
																					'td')
																			.index();
																	var supplierName = $(
																			"#suppliertable")
																			.find(
																					'th')
																			.eq(
																					columnIndex)
																			.text();
																	var material_number = $(
																			"#materialdrop option:selected")
																			.val();

																	$
																			.ajax({
																				type : "GET",
																				url : "/savesupplier",
																				contentType : 'application/json',
																				data : {
																					supplier : supplierName,
																					material : material_number
																				},
																				dataType : "json",
																				success : function() {

																				},
																				error : function(
																						exception) {

																				}
																			});
																}
															});
											$("#successmsg").show();
										});

					});
</script>
</head>
<body>
	<c:if test="${makeBuyType eq 'typeA'}">

		<div class="info">
			<p>
				<b><spring:message code="makeorbuy.info.p1" /></b>
			</p>
			<p>
				<spring:message code="makeorbuy.typeA.info.p2" />
			</p>
			<p>
				<spring:message code="makeorbuy.typeA.info.p3" />
			</p>
		</div>
		<div class="container">
			<form id="makeBuyDecisionTypeA" class="form"
				action="makeBuyDecisionRouter" method="POST">
				<table class="makeorbuyDecisionTable">
					<tr>
						<td width="280"><spring:message
								code="makeorbuy.decision.prototype" /> : </td>
						<td width="274"><input type="radio" id="prototype"
							name="prototypeA" value="In-house production"> <spring:message
								code="makeorbuy.decision.inHouseProduction" /><br></td>
						<td width="300"><input type="radio" id="outsourcing1"
							name="prototypeA" value="Outsourcing"> <spring:message code="makeorbuy.decision.outsourcing" /><br></td>
					</tr>
					<tr>
						<td><spring:message
								code="makeorbuy.decision.seriesProduction" /> : </td>
						<td><input type="radio" id="seriesProd" name="seriesProdA"
							value="In-house production"> <spring:message
								code="makeorbuy.decision.inHouseProduction" /><br></td>
						<td><input type="radio" id="seriesProd" name="seriesProdA"
							value="Outsourcing"> <spring:message code="makeorbuy.decision.outsourcing" /><br></td>
					</tr>
					<tr>
						<td><spring:message
								code="makeorbuy.decision.storageDispatchPreperation" /> : </td>
						<td><input type="radio" id="storageDispatch"
							name="storageDispatchA" value="In-house production"> <spring:message
								code="makeorbuy.decision.inHouseProduction" /><br></td>
						<td><input type="radio" id="storageDispatch"
							name="storageDispatchA" value="External Service
							Provider">
							<spring:message code="makeorbuy.decision.externalServiceProvider" /><br></td>
					</tr>

				</table>
				<br /> <br /> <br />
				<div class="navigation" align="center">
					<input type="submit" class="makebuy_button" name="TypeAcontinue"
						value='<spring:message code="makeorbuy.continuedecision"/>' />
				</div>

				<p>
					<spring:message code="makeorbuy.typeA.info.p4" />
				</p>
				<div class="navigation" align="center">
					<input type="submit" class="makebuy_button" name="support2"
						value='<spring:message code="makeorbuy.decisionsupport"/>' />
				</div>
			</form>
		</div>
	</c:if>

	<c:if test="${makeBuyType eq 'typeB'}">

		<div class="info">
			<p>
				<b><spring:message code="makeorbuy.info.p1" /></b>
			</p>
			<p>
				<spring:message code="makeorbuy.typeB.info.p2" />
			</p>
			<p>
				<spring:message code="makeorbuy.typeB.info.p3" />
			</p>
		</div>
		<div class="container">
			<form id="makeBuyDecisionTypeB" class="form"
				action="makeBuyDecisionRouter" method="POST">
				<spring:message code="makeorbuy.decision.product" /> : 
				<input name="productName" type="text"><br /> <br />
				<table class="makeorbuyDecisionTable">
					<tr>
						<td width="280"><spring:message
								code="makeorbuy.decision.prototype" /> : </td>

						<td width="274"><input type="radio" id="prototype"
							name="prototype" value="In-house production"> <spring:message
								code="makeorbuy.decision.inHouseProduction" /><br></td>
						<td width="300"><input type="radio" id="outsourcing1"
							name="prototype" value="Outsourcing">
						<spring:message code="makeorbuy.decision.outsourcing" /><br></td>
					</tr>
					<tr>
						<td><spring:message
								code="makeorbuy.decision.seriesProduction" /> : </td>
						<td><input type="radio" id="seriesProd" name="seriesProd"
							value="In-house production"> <spring:message
								code="makeorbuy.decision.inHouseProduction" /><br></td>
						<td><input type="radio" id="seriesProd" name="seriesProd"
							value="Outsourcing"> <spring:message
								code="makeorbuy.decision.outsourcing" /><br></td>
					</tr>
					<tr>
						<td><spring:message
								code="makeorbuy.decision.storageDispatchPreperation" /> : </td>
						<td><input type="radio" id="storageDispatch"
							name="storageDispatch" value="In-house production"> <spring:message
								code="makeorbuy.decision.onPremiseProduction" /><br></td>
						<td><input type="radio" id="storageDispatch"
							name="storageDispatch" value="External Service
							Provider">
							<spring:message code="makeorbuy.decision.externalServiceProvider" />
							<br></td>
					</tr>

				</table>
				<br /> <br /> <br />
				<div class="navigation" align="center">
					<input type="submit" class="makebuy_button" name="typeBSave"
						value='<spring:message code="makeorbuy.save"/>' />
				</div>
				<br />

				<c:if test="${not empty makeBuyDecisionList}">
					<table border="1" class="productionCriteriaTable">
						<tbody>
							<tr>
								<th scope="col"><spring:message code="makeorbuy.decision.product" /></th>
								<th scope="col"><spring:message
								code="makeorbuy.decision.inHouseProduction" /></th>
								<th scope="col"><spring:message
								code="makeorbuy.decision.seriesProduction" /></th>
								<th scope="col"><spring:message
								code="makeorbuy.decision.storageDispatchPreperation" /></th>
								<th scope="col"><i class="	glyphicon glyphicon-trash"></i></th>
							</tr>


							<c:forEach items="${makeBuyDecisionList}" var="decision">
								<tr>
									<td id="productName" style="text-align: center;">${decision.productName}</td>
									<td id="prototype" style="text-align: center;">${decision.prototype}</td>
									<td id="seriesProd" style="text-align: center;">${decision.seriesProd}</td>
									<td id="storageDispatch" style="text-align: center;" width="250px">
										${decision.storageDispatch}</td>
									<td style="text-align: center;">
										<button type="button" name="delete-button" id="delete"
										class="btn btn-block" style="background-color: #FFF; border: none">
										<i class="	glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
							</c:forEach>


						</tbody>
					</table>
				</c:if>

				<br />
				<div class="navigation" align="center">
					<input type="submit" class="makebuy_button" name="continue"
						value='<spring:message code="makeorbuy.continue"/>' />
				</div>
			</form>
		</div>
	</c:if>
</body>
</html>