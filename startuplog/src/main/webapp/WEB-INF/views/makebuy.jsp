<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- let's add tag srping:url -->
<spring:url value="/resources/css/styles.css" var="styleCSS" />
<spring:url value="/resources/js/jquery-3.1.1.min.js" var="jQueryMin" />
<spring:url value="/resources/js/survey.js" var="surveryJS" />
<spring:url value="/resources/images/startuplog_logo_1.ico" var="favcon" />
<link href="${styleCSS}" rel="stylesheet" />
<link href="${favcon}" rel="icon" />
<script src="${jQueryMin}"></script>
<script src="${surveryJS}"></script>
<!-- end -->

<title>StartupLog</title>

<script type="text/javascript">
	function setVariant(variant) {
		$('input[name="Variant"]').val(variant); // set variant number on hidden input field with name 'Variant'

		if (variant != 0) //delete me
		{
			$.ajax({
				url : "/putVariant",
				data : {
					variant_number : variant
				},
				success : function(data) {
				},
				error : function(exception) {
				},
				type : "POST"
			});
			$('button[name="next"]').removeClass("disablediv");
		}
	}

	$(document).ready(function() {

		// language
		var lang = "${pageContext.response.locale}";
		if (lang == "de") {
			$('#de').addClass('visited');
			$('#en').removeClass('visited');

		} else {
			$("#en").addClass("visited");
			$('#de').removeClass('visited');
		}

		//right sub tree of right tree (from Q3)
		var q5_6 = getRadiobuttonsQuestion("#survey_q6");
		var q5 = getRadiobuttonsQuestion("#survey_q5", {
			"yes" : q5_6,
			"no" : new SurveyTail()
		});

		//left sub tree of right tree (from Q3)
		var q4_6 = getRadiobuttonsQuestion("#survey_q6");
		var q4_5 = getRadiobuttonsQuestion("#survey_q5", {
			"yes" : q4_6,
			"no" : new SurveyTail()
		});
		var q4 = getRadiobuttonsQuestion("#survey_q4", {
			"yes" : q4_5,
			"no" : q5
		});

		//right tree (from Q1)
		var q3 = getRadiobuttonsQuestion("#survey_q3", {
			"yes" : q4,
			"no" : q5
		});

		//left tree (from Q1)
		var q2_4 = getRadiobuttonsQuestion("#survey_q4");
		var q2_3 = getRadiobuttonsQuestion("#survey_q3", {
			"yes" : q2_4,
			"no" : new SurveyTail()
		});
		var q2 = getRadiobuttonsQuestion("#survey_q2", {
			"yes" : q2_3,
			"no" : q3
		});

		//root
		var q1 = getRadiobuttonsQuestion_Enabled("#survey_q1", {
			"yes" : q2,
			"no" : q3
		});

		$(document).on("questionChangeEvent", function(e, question, event) {
			var value = event.target.value;
			if (question == q2_3 && value == "no") {
				setVariant(2);
			} else if (question == q2_4) {
				if (value == "yes") {
					setVariant(1);
				} else if (value == "no") {
					setVariant(2);
				}
			} else if (question == q4_5 && value == "no") {
				setVariant(4);
			} else if (question == q4_6) {
				if (value == "yes") {
					setVariant(3);
				} else if (value == "no") {
					setVariant(4);
				}
			} else if (question == q5 && value == "no") {
				setVariant(6);
			} else if (question == q5_6) {
				if (value == "yes") {
					setVariant(5);
				} else if (value == "no") {
					setVariant(6);
				}
			} else {
				setVariant(0);
			}
		});
	});
</script>

</head>
<body>
	<div class="container">
		<div class="topcorner">
			<a id="en" href="?language=en">English</a> | <a id="de"
				href="?language=de">Deutsch</a>
		</div>
		<form id="makeorbuy-form" class="form" action="/function" method="GET">
			<div id="indicators">

				<h2>Series Product:</h2>

				<div class="boundary">

					<ul class="hiddenbulletpoints" id="survey_q1">


						<li>
							<div class="label head">
								<spring:message code="make_bye.q1" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="survey_q1"
							id="survey_q1yes" class="hidden" value="yes" /> <label
							class="label checkmark highlight" for="survey_q1yes"> <spring:message
									code="yes" />
						</label></li>
						<li class="choice"><input type="radio" name="survey_q1"
							id="survey_q1no" class="hidden" value="no" /> <label
							class="label checkmark highlight" for="survey_q1no"> <spring:message
									code="no" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">

					<ul class="hiddenbulletpoints hidden" id="survey_q2">

						<li>
							<div class="label head">
								<spring:message code="make_bye.q2" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="survey_q2"
							id="survey_q2yes" class="hidden" value="yes" /> <label
							class="label checkmark highlight" for="survey_q2yes"> <spring:message
									code="yes" />
						</label></li>
						<li class="choice"><input type="radio" name="survey_q2"
							id="survey_q2no" class="hidden" value="no" /> <label
							class="label checkmark highlight" for="survey_q2no"> <spring:message
									code="no" />
						</label></li>
					</ul>
				</div>
				<h2>Prototype:</h2>
				<div class="boundary">

					<ul class="hiddenbulletpoints hidden" id="survey_q3">
						<li>
							<div class="label head">
								<spring:message code="make_bye.q3" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="survey_q3"
							id="survey_q3yes" class="hidden" value="yes" /> <label
							class="label checkmark highlight" for="survey_q3yes"> <spring:message
									code="yes" />
						</label></li>
						<li class="choice"><input type="radio" name="survey_q3"
							id="survey_q3no" class="hidden" value="no" /> <label
							class="label checkmark highlight" for="survey_q3no"> <spring:message
									code="no" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="survey_q4">
						<li>
							<div class="label head">
								<spring:message code="make_bye.q4" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="survey_q4"
							id="survey_q4yes" class="hidden" value="yes" /> <label
							class="label checkmark highlight" for="survey_q4yes"> <spring:message
									code="yes" />
						</label></li>
						<li class="choice"><input type="radio" name="survey_q4"
							id="survey_q4no" class="hidden" value="no" /> <label
							class="label checkmark highlight" for="survey_q4no"> <spring:message
									code="no" />
						</label></li>
					</ul>
				</div>
				<h2>Storage / Dispatch Preparation:</h2>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="survey_q5">
						<li>
							<div class="label head">
								<spring:message code="make_bye.q5" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="survey_q5"
							id="survey_q5yes" class="hidden" value="yes" /> <label
							class="label checkmark highlight" for="survey_q5yes"> <spring:message
									code="yes" />
						</label></li>
						<li class="choice"><input type="radio" name="survey_q5"
							id="survey_q5no" class="hidden" value="no" /> <label
							class="label checkmark highlight" for="survey_q5no"> <spring:message
									code="no" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="survey_q6">
						<li>
							<div class="label head">
								<spring:message code="make_bye.q6" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="survey_q6"
							id="survey_q6yes" class="hidden" value="yes" /> <label
							class="label checkmark highlight" for="survey_q6yes"> <spring:message
									code="yes" />
						</label></li>
						<li class="choice"><input type="radio" name="survey_q6"
							id="survey_q6no" class="hidden" value="no" /> <label
							class="label checkmark highlight" for="survey_q6no"> <spring:message
									code="no" />
						</label></li>
					</ul>
				</div>
			</div>
			<input id="Variant" type="hidden" name="Variant" value="0">
			<div style="clear: both"></div>
			<div id="nav" class="table-row navigation">
				<button class="form_button" name="back">
					<spring:message code="back" />
				</button>
				<button class="form_button">
					<spring:message code="next" />
				</button>
			</div>
		</form>
	</div>
</body>
</html>