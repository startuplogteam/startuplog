<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>StartupLog</title>

<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/multiselect.css">
<link rel="stylesheet" href="resources/css/styles.css">
<link rel="stylesheet" href="resources/css/warehouse_logistics.css">
<link rel="stylesheet" href="resources/css/tabs.css">
<link rel="stylesheet" href="resources/css/tabs2.css">
<link rel="stylesheet" href="resources/css/editContent.css">
<link rel="stylesheet" href="resources/css/tooltip.css">
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script src="resources/js/jquery-3.1.1.min.js"></script>
<script src="resources/js/multiselect.min.js"></script>
<script src="resources/js/tabs.js"></script>
<script src="resources/js/survey.js"></script>


<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$('.edit').click(function(){
							  $(this).hide();
							  $('.box').addClass('editable');
							  $('.text').attr('contenteditable', 'true');  
							  $('.save').show();
							});

							$('.save').click(function(){
							  $(this).hide();
							  $('.box').removeClass('editable');
							  $('.text').removeAttr('contenteditable');
							  $('.edit').show();
							});
						
						$(".tabbable").find(".tab").hide();
					    $(".tabbable").find(".tab").first().show();
					    $(".tabbable").find(".tabs li").first().find("a").addClass("active");
					    $(".tabbable").find(".tabs").find("a").click(function(){
					        tab = $(this).attr("href");
					        $(".tabbable").find(".tab").hide();
					        $(".tabbable").find(".tabs").find("a").removeClass("active");
					        $(tab).show();
					        $(this).addClass("active");
					        return false;
					    });

						$('#search')
								.on(
										'change',
										function() {
											var material_id = this.value;

											$
													.ajax({
														url : "/previousclassificationdecision",
														type : "GET",
														data : {
															material_id : $(
																	'#search option:selected')
																	.val()
														},
														success : function(data) {
															if (data.supply_risk != undefined) {
																selectClassificationBox(data.classification);
																$(
																		"input[name=supplyrisk][value="
																				+ data.supply_risk
																				+ "]")
																		.prop(
																				'checked',
																				true);
																$(
																		"input[name=furthercriteria][value="
																				+ data.further_criteria
																				+ "]")
																		.prop(
																				'checked',
																				true);
																$(
																		"input[name=criteriarisk][value="
																				+ data.further_criteria_value
																				+ "]")
																		.prop(
																				'checked',
																				true);

																if ($(
																		'input[name="furthercriteria"]:checked')
																		.val() == "price") {
																	$("#x-axis")
																			.text(
																					"<spring:message code="material.xAxis"/>");
																} else if ($(
																		'input[name="furthercriteria"]:checked')
																		.val() == "quality") {
																	$("#x-axis")
																			.text(
																					"<spring:message code="material.Quality_Standard"/>");
																} else if ($(
																		'input[name="furthercriteria"]:checked')
																		.val() == "technical") {
																	$("#x-axis")
																			.text(
																					"<spring:message code="material.Technical_Complexity"/>");
																}

																$("#y-axis")
																		.text(
																				"<spring:message code="material.Supply_Risk"/>");
															} else {
																removeAllSelection();
															}
														}

													});

										});

						function removeAllSelection() {

							$("#bottleneck").removeClass("selected").addClass(
									"normalclassification");
							$("#key").removeClass("selected").addClass(
									"normalclassification");
							$("#leverage").removeClass("selected").addClass(
									"normalclassification");
							$("#notcritical").removeClass("selected").addClass(
									"normalclassification");

							$('input[type="radio"]').prop('checked', false);

							$("#x-axis").text("");
							$("#y-axis").text("");
						}

						function selectClassificationBox(data) {
							if (data == "upperright") {
								$("#key").removeClass("normalclassification")
										.addClass("selected");
								$("#bottleneck").removeClass("selected")
										.addClass("normalclassification");
								$("#notcritical").removeClass("selected")
										.addClass("normalclassification");
								$("#leverage").removeClass("selected")
										.addClass("normalclassification");
							} else if (data == "upperleft") {
								$("#bottleneck").removeClass(
										"normalclassification").addClass(
										"selected");
								$("#notcritical").removeClass("selected")
										.addClass("normalclassification");
								$("#key").removeClass("selected").addClass(
										"normalclassification");
								$("#leverage").removeClass("selected")
										.addClass("normalclassification");
							} else if (data == "lowerleft") {
								$("#notcritical").removeClass(
										"normalclassification").addClass(
										"selected");
								$("#bottleneck").removeClass("selected")
										.addClass("normalclassification");
								$("#key").removeClass("selected").addClass(
										"normalclassification");
								$("#leverage").removeClass("selected")
										.addClass("normalclassification");
							} else {
								$("#leverage").removeClass(
										"normalclassification").addClass(
										"selected");
								$("#bottleneck").removeClass("selected")
										.addClass("normalclassification");
								$("#notcritical").removeClass("selected")
										.addClass("normalclassification");
								$("#key").removeClass("selected").addClass(
										"normalclassification");
							}
						}

						$('input[type="radio"]')
								.click(
										function() {
											if ($('#search option:selected')
													.val() != undefined) {
												var $questions = $(".clquestion");
												if ($questions
														.find("input:radio:checked").length === $questions.length) {

													$
															.ajax({
																url : "/materialclassification",
																type : "POST",
																data : {
																	material_id : $(
																			'#search option:selected')
																			.val(),
																	supply_risk : $(
																			'input[name="supplyrisk"]:checked')
																			.val(),
																	further_criteria : $(
																			'input[name="furthercriteria"]:checked')
																			.val(),
																	further_value : $(
																			'input[name="criteriarisk"]:checked')
																			.val()
																},
																success : function(
																		data) {
																	selectClassificationBox(data);
																	if ($(
																	'input[name="furthercriteria"]:checked')
																	.val() == "price")
																		{
																		$("#x-axis").text("<spring:message code="material.Price"/>");												
																		}
																	else if ($(
																	'input[name="furthercriteria"]:checked')
																	.val() == "quality")
																	{
																		$("#x-axis").text("<spring:message code="material.Quality_Standard"/>");
																	}
																	else if ($(
																	'input[name="furthercriteria"]:checked')
																	.val() == "technical")
																		{
																		$("#x-axis").text("<spring:message code="material.Technical_Complexity"/>");
																		}
																	$("#y-axis")
																			.text(
																					"<spring:message code="material.yAxis"/>");
																}

															});

												}
											} else {
												alert("<spring:message code="select_material_first"/>");
											}

										});
					});
</script>

</head>
<body>

	
	<div class="tabbable">
    <ul class="tabs">
        <li><a href="#tab1">Beschreibung</a></li>
        <li><a href="#tab2">Anwendungs-Tool</a></li>
    </ul>
    <div class="tabcontent">
        <div id="tab1" class="tab box">
                       
  <span class="edit">Edit</span>
  <span class="save">Save</span>
  <div class="text">
    Hover this box and click on edit! - You can edit me then.
    <br>When you finished - click save and you saved it.
    </div>

            
        </div>
        <div id="tab2" class="tab">
            	<div class="info">
		<p>
			<spring:message
					code="procurement.materialClassification.info.p1" />
		</p>
		<p>
			<spring:message code="procurement.materialClassification.info.p2" />
		</p>
	</div>
	
	<div class="materialcontainer">

		<form class="materialform">

			<h4>
				<spring:message code="material.chose_material" />
			</h4>

			<select id="search" multiple style="width: 100%; height: 150px;">
				<c:forEach items="${materialdetails}" var="material">

					<option value="${material.material_number}"><spring:message
							code="${material.material_name}" /></option>
				</c:forEach>
			</select>

			<table>
				<tr>
					<td
						style="border: none; padding-left: 15px; padding-top: 00px;">
						<h3>
							<spring:message code="material.Supply_Risk" />
						</h3>
						<h4>
							<spring:message code="material.determine_the_supply_risk" />
						</h4>
						<div class="clquestion" id="supplyrisk">
							<input style="margin: 0 0px 0 0px" type="radio" name="supplyrisk"
								value="high">
							<spring:message code="material.high" />
							<input style="margin: 0 0px 0 30px" type="radio"
								name="supplyrisk" value="low">
							<spring:message code="material.low" />
						</div>
						<h3>
							<spring:message code="material.Further_Criterion" />
						</h3>
						<h4>
							<spring:message
								code="material.choose_one_of_the_following_criteria" />
						</h4>
						<div class="clquestion" id="citeria">
							<input style="margin: 0 0px 0 0px" type="radio"
								name="furthercriteria" value="quality">
							<spring:message code="material.Quality_Standard" />
							<input style="margin: 0 0px 0 5px" type="radio"
								name="furthercriteria" value="price">
							<spring:message code="material.Price" />
							<input style="margin: 0 0px 0 5px" type="radio"
								name="furthercriteria" value="technical">
							<spring:message code="material.Technical_Complexity" />
						</div>

						<h4>
							<spring:message code="material.determine_the_chosen_criterion" />
						</h4>
						<div class="clquestion" id="criteria_value">
							<input style="margin: 0 0px 0 0px" type="radio"
								name="criteriarisk" value="high">
							<spring:message code="material.high" />
							<input style="margin: 0 0px 0 30px" type="radio"
								name="criteriarisk" value="low">
							<spring:message code="material.low" />
						</div>

					</td>
					<td style="padding-left: 15px; border: none;">
						<p id="y-axis" style="writing-mode: tb-rl; font-size:15px"></p>
					</td>
					<td
						style="border: none; padding-left: 0px; padding-top: 30px">
						<h3>
							<spring:message code="material.Classification" />
						</h3>


						<table
							style="position: relative; background: #DCECF8; border: 1px solid #000; width: 100%; height: 350px;">
							<tr>
								<td style="width: 50%; border: none;">
									<div class="normalclassification" id="bottleneck">
										<label style="font-size: 10pt; margin-left: 90px;"><b><spring:message
													code="material.Bottleneck_products" /></b></label>
										<ul>
											<li><spring:message
													code="material.Bottleneck_products_1" /></li>
											<li><spring:message
													code="material.Bottleneck_products_2" /></li>
											<li><spring:message
													code="material.Bottleneck_products_3" /></li>
										</ul>
									</div>
								</td>
								<td style="border: none;">
									<div class="normalclassification" id="key">
										<label style="font-size: 10pt; margin-left: 90px;"><b><spring:message
													code="material.Key_products" /></b></label>
										<ul>
											<li><spring:message code="material.Key_products_1" /></li>
											<li><spring:message code="material.Key_products_2" /></li>
											<li><spring:message code="material.Key_products_3" /></li>
											<li><spring:message code="material.Key_products_4" /></li>
										</ul>
									</div>
								</td>
							</tr>
							<tr>
								<td style="border: none;">
									<div class="normalclassification" id="notcritical">
										<label style="font-size: 10pt; margin-left: 90px;"><b><spring:message
													code="material.No_critical_products" /></b></label>
										<ul>
											<li><spring:message
													code="material.No_critical_products_1" /></li>
											<li><spring:message
													code="material.No_critical_products_2" /></li>
											<li><spring:message
													code="material.No_critical_products_3" /></li>
										</ul>
									</div>

								</td>
								<td style="border: none;">
									<div class="normalclassification" id="leverage">
										<label style="font-size: 10pt; margin-left: 90px;"><b><spring:message
													code="material.Leverage_products" /></b></label>
										<ul>
											<li><spring:message code="material.Leverage_products_1" /></li>
											<li><spring:message code="material.Leverage_products_2" /></li>
											<li><spring:message code="material.Leverage_products_3" /></li>
											<li><spring:message code="material.Leverage_products_3" /></li>
										</ul>
									</div>
								</td>
							</tr>
						</table>

						<p id="x-axis" align="center" style="font-size: 15px;"></p>
					</td>
				</tr>
			</table>
		</form>
	</div>
	
	
        </div>
    </div>
</div>

	

</body>
</html>