<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="resources/css/styles.css">
<link rel="stylesheet" href="resources/css/tabs2.css">
<link rel="stylesheet" href="resources/css/editContent.css">
<link rel="stylesheet" href="resources/css/warehouse_logistics.css">
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script src="resources/js/jquery-3.1.1.min.js"></script>
<script src="resources/js/survey.js"></script>
<script src="resources/js/loadData.js"></script>

<title>StartupLog</title>

<script type="text/javascript">

 const supplier_number_strategy = "Supplier Number Strategy";
 const market_field_strategy = "Market Field Strategy";
 const supply_strategy = "Supply Strategy";
 
 var formID = "#procurement_strategy-form";	
 
 var getStrategySuggestions = function (form) {
     $.ajax({
         type: "POST",
         url: "/saveProcurement",
         data: {material_id: $('#search option:selected')
				.val(), market_research_result: $('input[name=market_research_result]:checked').val(), product_feature: $('input[name=product_feature]:checked').val(), 
				predictability_procurement: $('input[name=predictability_procurement]:checked').val(),
				flexibility: $('input[name=flexibility]:checked').val(),
				complexity: $('input[name=complexity]:checked').val(),
				subcription_price: $('input[name=subcription_price]:checked').val(),
				order_and_transaction_costs: $('input[name=order_and_transaction_costs]:checked').val(),
				transport_costs: $('input[name=transport_costs]:checked').val(),
				storage_and_capital_costs: $('input[name=storage_and_capital_costs]:checked').val(),
				procurement_time: $('input[name=procurement_time]:checked').val(),
				risk_by_supplier_failure: $('input[name=risk_by_supplier_failure]:checked').val(),
				transport_risk: $('input[name=transport_risk]:checked').val(),
				currency_risk: $('input[name=currency_risk]:checked').val(),
				exchange_rate_risk: $('input[name=exchange_rate_risk]:checked').val(),
				customs_problem_risk: $('input[name=customs_problem_risk]:checked').val(),
				dependence_on_supplier: $('input[name=dependence_on_supplier]:checked').val(),
				number_of_suppliers_and_contacts: $('input[name=number_of_suppliers_and_contacts]:checked').val(),
				expenditure_on_bureaucracy: $('input[name=expenditure_on_bureaucracy]:checked').val(),
				language: $('input[name=language]:checked').val(),
				currency: $('input[name=currency]:checked').val(),
				exchange_rate_utilization: $('input[name=exchange_rate_utilization]:checked').val()
				},
         success: function (data) {
        	 getPreviousProcurement($('#search option:selected').val());
         },
         error: function (exception) {
             //alert('Exeption:' + JSON.stringify(exception));
         }
     });
 }
 var printErrorMessage = function (element)
 {
     element.html("<spring:message code="procurement.strategy_suggestion.default_suggestion"/>");
     element.addClass("incomplete");
 }
 var replaceErrorMessage = function (element, text) {
     element.html(text);
     element.removeClass("incomplete");
 }

 $(document).ready(function () {
	 
		$('.edit').click(function(){
			  $(this).hide();
			  $('.box').addClass('editable');
			  $('.text').attr('contenteditable', 'true');  
			  $('.save').show();
			});

			$('.save').click(function(){
			  $(this).hide();
			  $('.box').removeClass('editable');
			  $('.text').removeAttr('contenteditable');
			  $('.edit').show();
			});

	 
	 $(".tabbable").find(".tab").hide();
	    $(".tabbable").find(".tab").first().show();
	    $(".tabbable").find(".tabs li").first().find("a").addClass("active");
	    $(".tabbable").find(".tabs").find("a").click(function(){
	        tab = $(this).attr("href");
	        $(".tabbable").find(".tab").hide();
	        $(".tabbable").find(".tabs").find("a").removeClass("active");
	        $(tab).show();
	        $(this).addClass("active");
	        return false;
	    });
	 
	 var procurement_q21 = getRadiobuttonsQuestion("#procurement_q21");
     var procurement_q20 = getRadiobuttonsQuestion("#procurement_q20");
     var procurement_q19 = getRadiobuttonsQuestion("#procurement_q19");
     var procurement_q18 = getRadiobuttonsQuestion("#procurement_q18");
     var procurement_q17 = getRadiobuttonsQuestion("#procurement_q17");
     var procurement_q16 = getRadiobuttonsQuestion("#procurement_q16");
     var procurement_q15 = getRadiobuttonsQuestion("#procurement_q15");
     var procurement_q14 = getRadiobuttonsQuestion("#procurement_q14");
     var procurement_q13 = getRadiobuttonsQuestion("#procurement_q13");
     var procurement_q12 = getRadiobuttonsQuestion("#procurement_q12");
     var procurement_q11 = getRadiobuttonsQuestion("#procurement_q11");
     var procurement_q10 = getRadiobuttonsQuestion("#procurement_q10");
     var procurement_q9 = getRadiobuttonsQuestion("#procurement_q9");
     var procurement_q8 = getRadiobuttonsQuestion("#procurement_q8");
     var procurement_q7 = getRadiobuttonsQuestion("#procurement_q7");
     var procurement_q6 = getRadiobuttonsQuestion("#procurement_q6");
     var procurement_q5 = getRadiobuttonsQuestion("#procurement_q5");
     var procurement_q4 = getRadiobuttonsQuestion("#procurement_q4");

     var q4_to_q21 = new QuestionGroup([procurement_q4, procurement_q5, procurement_q6, procurement_q7, procurement_q8, procurement_q9, procurement_q10, procurement_q11, procurement_q12,
                     procurement_q13, procurement_q14, procurement_q15, procurement_q16, procurement_q17, procurement_q18, procurement_q19, procurement_q20, procurement_q21]);

     var procurement_q3 = getRadiobuttonsQuestion("#procurement_q3", q4_to_q21);
     var procurement_q3_skip = getRadiobuttonsQuestion("#procurement_q3");

     var procurement_q2c = getRadiobuttonsQuestion("#procurement_q2",
         {
             "product_complex": procurement_q3_skip,
             "product_development": procurement_q3_skip,
             "product_customized": procurement_q3,
             "product_few_specs": procurement_q3_skip,
             "product_standardized": procurement_q3_skip
         });
     var procurement_q2b = getRadiobuttonsQuestion("#procurement_q2",
         {
             "product_complex": procurement_q3,
             "product_development": procurement_q3,
             "product_customized": procurement_q3_skip,
             "product_few_specs": procurement_q3,
             "product_standardized": procurement_q3
         });
     var procurement_q2 = getRadiobuttonsQuestion("#procurement_q2",
         {
             "product_complex": procurement_q3,
             "product_development": procurement_q3,
             "product_customized": procurement_q3,
             "product_few_specs": procurement_q3,
             "product_standardized": procurement_q3
         });
     var procurement_q1 = getRadiobuttonsQuestion_Enabled("#procurement_q1",
         {
             "one_supplier": procurement_q2b,
             "several_suppliers": procurement_q2,
             "suppliers_own_country": procurement_q2c,
             "suppliers_other_countries": procurement_q2c,
             "suppliers_own_and_other_countries": procurement_q2
         });
     $(document).on("questionChangeEvent", function (e) {
         if (procurement_q1.areAllSelectedDescendantsValid()) {
             getStrategySuggestions($(formID));
             if (procurement_q21.isValid()) {
                 $("#suggestions_button").show();
             }
             else
             {
                 $("#suggestions_button").hide();
             }
         }
         else {
             printErrorMessage($("#strategy_supplier"));
             printErrorMessage($("#strategy_market"));
             printErrorMessage($("#strategy_supply"));
             $("#suggestions_button").hide();
         }
     });
     $("#suggestions_button").hide();
     printErrorMessage($("#strategy_supplier"));
     printErrorMessage($("#strategy_market"));
     printErrorMessage($("#strategy_supply"));
     $("#suggestions_button").click(function () {
         $('html,body').animate({ scrollTop: $("#suggestions").offset().top }, 'slow');
     });
 });
    </script>
</head>
<body>

	<div class="tabbable">
    <ul class="tabs">
        <li><a href="#tab1">Beschreibung</a></li>
        <li><a href="#tab2">Anwendungs-Tool</a></li>
    </ul>
    <div class="tabcontent">
        <div id="tab1" class="tab box">
                       
  <span class="edit">Edit</span>
  <span class="save">Save</span>
  <div class="text">
    Hover this box and click on edit! - You can edit me then.
    <br>When you finished - click save and you saved it.
    </div>

            
        </div>
        <div id="tab2" class="tab">
        
        	<div class="info">
		<p>
			<spring:message code="procurement.message_1" />
		</p>
		<p>
			<spring:message code="procurement.message_2" />
		</p>
		<p>
			<spring:message code="procurement.message_3" />
		</p>
	</div>
	<div class="container">
	<form id="procurement_strategy-form" class="form">
		<div id="informations">
			<h3>				
				<spring:message code="procurement.available_materials" />
			</h3>
			<select name="material" id="search" class="form-control" size="8"
				onChange="getPreviousProcurement(this.value);">
				<c:forEach var="material" items="${materialList}">
					<option value="${material.material_number }"><spring:message
							code="${material.material_name}" /></option>
				</c:forEach>
			</select>

			<div id="suggestions" class="boundary">
				<h1>
					<spring:message code="procurement.strategy_suggestion" />
				</h1>
				<div id="suggestions_table">
					<div id="header" class="table">
						<div class="table-row">
							<div class="table-cell header">
								<spring:message code="procurement.strategy_suggestion.strategy" />
							</div>
							<div class="table-cell header">
								<spring:message
									code="procurement.strategy_suggestion.suggestion" />
							</div>
						</div>
					</div>
					<div id="content" class="table">
						<div class="table-row content">
							<div class="table-cell">
								<spring:message
									code="procurement.strategy_suggestion.supplier_number" />
							</div>
							<div id="strategy_supplier" class="table-cell"></div>
						</div>
						<div class="table-row content">
							<div class="table-cell">
								<spring:message
									code="procurement.strategy_suggestion.market_field" />
							</div>
							<div id="strategy_market" class="table-cell"></div>
						</div>
						<div class="table-row content">
							<div class="table-cell">
								<spring:message
									code="procurement.strategy_suggestion.market_supply" />
							</div>
							<div id="strategy_supply" class="table-cell"></div>
						</div>
					</div>
				</div>
			</div>
			<div id="suggestions">
			<div class="boundary">
				<ul class="hiddenbulletpoints" id="procurement_q1">
					<li>
						<div class="label head">
							<spring:message code="procurement.q1" />
						</div>
					</li>
					<li class="choice"><input type="radio"
						name="market_research_result" id="procurement_q1_1" class="hidden"
						value="one_supplier" /> <label class="label checkmark highlight"
						for="procurement_q1_1"> <spring:message
								code="procurement.q1.a1" />
					</label></li>
					<li class="choice"><input type="radio"
						name="market_research_result" id="procurement_q1_2" class="hidden"
						value="several_suppliers" /> <label
						class="label checkmark highlight" for="procurement_q1_2">
							<spring:message code="procurement.q1.a2" />
					</label></li>
					<li class="choice"><input type="radio"
						name="market_research_result" id="procurement_q1_3" class="hidden"
						value="suppliers_own_country" /> <label
						class="label checkmark highlight" for="procurement_q1_3">
							<spring:message code="procurement.q1.a3" />
					</label></li>
					<li class="choice"><input type="radio"
						name="market_research_result" id="procurement_q1_4" class="hidden"
						value="suppliers_other_countries" /> <label
						class="label checkmark highlight" for="procurement_q1_4">
							<spring:message code="procurement.q1.a4" />
					</label></li>
					<li class="choice"><input type="radio"
						name="market_research_result" id="procurement_q1_5" class="hidden"
						value="suppliers_own_and_other_countries" /> <label
						class="label checkmark highlight" for="procurement_q1_5">
							<spring:message code="procurement.q1.a5" />
					</label></li>
				</ul>
			</div>
			<div class="boundary">
				<ul class="hiddenbulletpoints hidden" id="procurement_q2">
					<li>
						<div class="label head">
							<spring:message code="procurement.q2" />
						</div>
					</li>
					<li class="choice"><input type="radio" name="product_feature"
						id="procurement_q2_1" class="hidden" value="product_complex" /> <label
						class="label checkmark highlight" for="procurement_q2_1">
							<spring:message code="procurement.q2.a1" />
					</label></li>
					<li class="choice"><input type="radio" name="product_feature"
						id="procurement_q2_2" class="hidden" value="product_development" />
						<label class="label checkmark highlight" for="procurement_q2_2">
							<spring:message code="procurement.q2.a2" />
					</label></li>
					<li class="choice"><input type="radio" name="product_feature"
						id="procurement_q2_3" class="hidden" value="product_customized" />
						<label class="label checkmark highlight" for="procurement_q2_3">
							<spring:message code="procurement.q2.a3" />
					</label></li>
					<li class="choice"><input type="radio" name="product_feature"
						id="procurement_q2_4" class="hidden" value="product_few_specs" />
						<label class="label checkmark highlight" for="procurement_q2_4">
							<spring:message code="procurement.q2.a4" />
					</label></li>
					<li class="choice"><input type="radio" name="product_feature"
						id="procurement_q2_5" class="hidden" value="product_standardized" />
						<label class="label checkmark highlight" for="procurement_q2_5">
							<spring:message code="procurement.q2.a5" />
					</label></li>
				</ul>
			</div>
			<div id="question_row_1">
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q3">
						<li>
							<div class="label head">
								<spring:message code="procurement.q3" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="predictability_procurement" id="procurement_q3_1"
							class="hidden" value="less" /> <label
							class="label checkmark highlight" for="procurement_q3_1">
								<spring:message code="procurement.q3.a1" />
						</label></li>
						<li class="choice"><input type="radio"
							name="predictability_procurement" id="procurement_q3_2"
							class="hidden" value="well" /> <label
							class="label checkmark highlight" for="procurement_q3_2">
								<spring:message code="procurement.q3.a2" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q4">
						<li>
							<div class="label head">
								<spring:message code="procurement.q4" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="flexibility"
							id="procurement_q4_1" class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q4_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio" name="flexibility"
							id="procurement_q4_2" class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q4_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio" name="flexibility"
							id="procurement_q4_3" class="hidden" value="irrelevant" checked />
							<label class="label checkmark highlight" for="procurement_q4_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q5">
						<li>
							<div class="label head">
								<spring:message code="procurement.q5" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="complexity"
							id="procurement_q5_1" class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q5_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio" name="complexity"
							id="procurement_q5_2" class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q5_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio" name="complexity"
							id="procurement_q5_3" class="hidden" value="irrelevant" /> <label
							class="label checkmark highlight" for="procurement_q5_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q6">
						<li>
							<div class="label head">
								<spring:message code="procurement.q6" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="subcription_price" id="procurement_q6_1" class="hidden"
							value="high" /> <label class="label checkmark highlight"
							for="procurement_q6_1"> <spring:message
									code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio"
							name="subcription_price" id="procurement_q6_2" class="hidden"
							value="low" /> <label class="label checkmark highlight"
							for="procurement_q6_2"> <spring:message
									code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio"
							name="subcription_price" id="procurement_q6_3" class="hidden"
							value="irrelevant" /> <label class="label checkmark highlight"
							for="procurement_q6_3"> <spring:message
									code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q7">
						<li>
							<div class="label head">
								<spring:message code="procurement.q7" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="order_and_transaction_costs" id="procurement_q7_1"
							class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q7_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio"
							name="order_and_transaction_costs" id="procurement_q7_2"
							class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q7_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio"
							name="order_and_transaction_costs" id="procurement_q7_3"
							class="hidden" value="irrelevant" /> <label
							class="label checkmark highlight" for="procurement_q7_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q8">
						<li>
							<div class="label head">
								<spring:message code="procurement.q8" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="transport_costs"
							id="procurement_q8_1" class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q8_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio" name="transport_costs"
							id="procurement_q8_2" class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q8_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio" name="transport_costs"
							id="procurement_q8_3" class="hidden" value="irrelevant" /> <label
							class="label checkmark highlight" for="procurement_q8_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q9">
						<li>
							<div class="label head">
								<spring:message code="procurement.q9" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="storage_and_capital_costs" id="procurement_q9_1"
							class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q9_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio"
							name="storage_and_capital_costs" id="procurement_q9_2"
							class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q9_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio"
							name="storage_and_capital_costs" id="procurement_q9_3"
							class="hidden" value="irrelevant" /> <label
							class="label checkmark highlight" for="procurement_q9_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q10">
						<li>
							<div class="label head">
								<spring:message code="procurement.q10" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="procurement_time" id="procurement_q10_1" class="hidden"
							value="high" /> <label class="label checkmark highlight"
							for="procurement_q10_1"> <spring:message
									code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio"
							name="procurement_time" id="procurement_q10_2" class="hidden"
							value="low" /> <label class="label checkmark highlight"
							for="procurement_q10_2"> <spring:message
									code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio"
							name="procurement_time" id="procurement_q10_3" class="hidden"
							value="irrelevant" /> <label class="label checkmark highlight"
							for="procurement_q10_3"> <spring:message
									code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q11">
						<li>
							<div class="label head">
								<spring:message code="procurement.q11" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="risk_by_supplier_failure" id="procurement_q11_1"
							class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q11_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio"
							name="risk_by_supplier_failure" id="procurement_q11_2"
							class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q11_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio"
							name="risk_by_supplier_failure" id="procurement_q11_3"
							class="hidden" value="irrelevant" /> <label
							class="label checkmark highlight" for="procurement_q11_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
			</div>
			<div id="question_row_2">
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q12">
						<li>
							<div class="label head">
								<spring:message code="procurement.q12" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="transport_risk"
							id="procurement_q12_1" class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q12_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio" name="transport_risk"
							id="procurement_q12_2" class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q12_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio" name="transport_risk"
							id="procurement_q12_3" class="hidden" value="irrelevant" /> <label
							class="label checkmark highlight" for="procurement_q12_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q13">
						<li>
							<div class="label head">
								<spring:message code="procurement.q13" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="currency_risk"
							id="procurement_q13_1" class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q13_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio" name="currency_risk"
							id="procurement_q13_2" class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q13_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio" name="currency_risk"
							id="procurement_q13_3" class="hidden" value="irrelevant" /> <label
							class="label checkmark highlight" for="procurement_q13_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q14">
						<li>
							<div class="label head">
								<spring:message code="procurement.q14" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="exchange_rate_risk" id="procurement_q14_1" class="hidden"
							value="high" /> <label class="label checkmark highlight"
							for="procurement_q14_1"> <spring:message
									code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio"
							name="exchange_rate_risk" id="procurement_q14_2" class="hidden"
							value="low" /> <label class="label checkmark highlight"
							for="procurement_q14_2"> <spring:message
									code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio"
							name="exchange_rate_risk" id="procurement_q14_3" class="hidden"
							value="irrelevant" /> <label class="label checkmark highlight"
							for="procurement_q14_3"> <spring:message
									code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q15">
						<li>
							<div class="label head">
								<spring:message code="procurement.q15" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="customs_problem_risk" id="procurement_q15_1" class="hidden"
							value="high" /> <label class="label checkmark highlight"
							for="procurement_q15_1"> <spring:message
									code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio"
							name="customs_problem_risk" id="procurement_q15_2" class="hidden"
							value="low" /> <label class="label checkmark highlight"
							for="procurement_q15_2"> <spring:message
									code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio"
							name="customs_problem_risk" id="procurement_q15_3" class="hidden"
							value="irrelevant" /> <label class="label checkmark highlight"
							for="procurement_q15_3"> <spring:message
									code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q16">
						<li>
							<div class="label head">
								<spring:message code="procurement.q16" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="dependence_on_supplier" id="procurement_q16_1"
							class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q16_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio"
							name="dependence_on_supplier" id="procurement_q16_2"
							class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q16_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio"
							name="dependence_on_supplier" id="procurement_q16_3"
							class="hidden" value="irrelevant" /> <label
							class="label checkmark highlight" for="procurement_q16_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q17">
						<li>
							<div class="label head">
								<spring:message code="procurement.q17" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="number_of_suppliers_and_contacts" id="procurement_q17_1"
							class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q17_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio"
							name="number_of_suppliers_and_contacts" id="procurement_q17_2"
							class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q17_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio"
							name="number_of_suppliers_and_contacts" id="procurement_q17_3"
							class="hidden" value="irrelevant" /> <label
							class="label checkmark highlight" for="procurement_q17_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q18">
						<li>
							<div class="label head">
								<spring:message code="procurement.q18" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="expenditure_on_bureaucracy" id="procurement_q18_1"
							class="hidden" value="high" /> <label
							class="label checkmark highlight" for="procurement_q18_1">
								<spring:message code="procurement.high" />
						</label></li>
						<li class="choice"><input type="radio"
							name="expenditure_on_bureaucracy" id="procurement_q18_2"
							class="hidden" value="low" /> <label
							class="label checkmark highlight" for="procurement_q18_2">
								<spring:message code="procurement.low" />
						</label></li>
						<li class="choice"><input type="radio"
							name="expenditure_on_bureaucracy" id="procurement_q18_3"
							class="hidden" value="irrelevant" /> <label
							class="label checkmark highlight" for="procurement_q18_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q19">
						<li>
							<div class="label head">
								<spring:message code="procurement.q19" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="language"
							id="procurement_q19_1" class="hidden" value="same" /> <label
							class="label checkmark highlight" for="procurement_q19_1">
								<spring:message code="procurement.same" />
						</label></li>
						<li class="choice"><input type="radio" name="language"
							id="procurement_q19_2" class="hidden" value="different" /> <label
							class="label checkmark highlight" for="procurement_q19_2">
								<spring:message code="procurement.different" />
						</label></li>
						<li class="choice"><input type="radio" name="language"
							id="procurement_q19_3" class="hidden" value="irrelevant" checked />
							<label class="label checkmark highlight" for="procurement_q19_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q20">
						<li>
							<div class="label head">
								<spring:message code="procurement.q20" />
							</div>
						</li>
						<li class="choice"><input type="radio" name="currency"
							id="procurement_q20_1" class="hidden" value="same" /> <label
							class="label checkmark highlight" for="procurement_q20_1">
								<spring:message code="procurement.same" />
						</label></li>
						<li class="choice"><input type="radio" name="currency"
							id="procurement_q20_2" class="hidden" value="different" /> <label
							class="label checkmark highlight" for="procurement_q20_2">
								<spring:message code="procurement.different" />
						</label></li>
						<li class="choice"><input type="radio" name="currency"
							id="procurement_q20_3" class="hidden" value="irrelevant" checked />
							<label class="label checkmark highlight" for="procurement_q20_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
				<div class="boundary">
					<ul class="hiddenbulletpoints hidden" id="procurement_q21">
						<li>
							<div class="label head">
								<spring:message code="procurement.q21" />
							</div>
						</li>
						<li class="choice"><input type="radio"
							name="exchange_rate_utilization" id="procurement_q21_1"
							class="hidden" value="possible" /> <label
							class="label checkmark highlight" for="procurement_q21_1">
								<spring:message code="procurement.possible" />
						</label></li>
						<li class="choice"><input type="radio"
							name="exchange_rate_utilization" id="procurement_q21_2"
							class="hidden" value="not_possible" /> <label
							class="label checkmark highlight" for="procurement_q21_2">
								<spring:message code="procurement.not_possible" />
						</label></li>
						<li class="choice"><input type="radio"
							name="exchange_rate_utilization" id="procurement_q21_3"
							class="hidden" value="irrelevant" checked /> <label
							class="label checkmark highlight" for="procurement_q21_3">
								<spring:message code="procurement.irrelevant" />
						</label></li>
					</ul>
				</div>
			</div>
			</div>
		</div>
		<div style="clear: both"></div>
					<div id="nav" class="navigation">
		<button id="suggestions_button" type="button"
			class="form_button navigation">suggestions</button>
		</div>
	</form>
	</div>
        
        </div>
    </div>
    </div>




</body>
</html>
