<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<!-- let's add tag srping:url -->
<spring:url value="/resources/css/styles.css" var="startupLogCSS" />
<spring:url value="/resources/js/jquery-3.1.1.min.js" var="startupLogJS" />
<spring:url value="/resources/images/startuplog_logo_1.ico" var="favcon" />
<link href="${startupLogCSS}" rel="stylesheet" />
<link href="${favcon}" rel="icon" />
<script src="${startupLogJS}"></script>
<title>StartupLog</title>
<script type="text/javascript">

        (function ($) {
            $.fn.showInfoDiv = function ()
            {
                return this.each(function ()
                {
                    $('#selected-stages .table > div[id^=' + $(this).attr("for") + ']').show();
                });
            };
        }(jQuery));

        $(document).ready(function ()
        {
        	var stagesForm = $('#selected-stages');
            $("#stages-schematic .suggestion").showInfoDiv();
            $("#stages-schematic .label").click(function ()
            {
                $("#selected-stages .table > div").hide();
                $(this).showInfoDiv();
            });
            
            $("#nav .form_button").click(function ()
            {
                 window.location = '/indicators';
            });
            
            var lang = "${pageContext.response.locale}";
    		
			if (lang == "de")
				{
					$('#de').addClass('visited');
					$('#en').removeClass('visited');
				
				}
			else
				{
					$( "#en" ).addClass( "visited" );
					$('#de').removeClass('visited');
				}

        });
    </script>
</head>
<body>

	<div class="info">
		<p>
			<b><spring:message code="stages.info.p1" /></b>
		</p>
		<p>
			<spring:message code="stages.info.p2" />
		</p>
		<p>
			<spring:message code="stages.info.p3" />
		</p>
		<p>
			<spring:message code="stages.info.p4" />
		</p>
	</div>
	<div class="container">
		<div class="topcorner">
			<a id="en" href="?language=en">en</a> | <a id="de"
				href="?language=de">de</a>
		</div>
		<div id="stages" class="form">
			<div id="stages-schematic" class="boundary">
				<div id="arrow-tail">
					<span class="arrow-cutout"></span>
				</div>
				<div id="arrow-head">
					<span class="arrow-cutout"></span>
				</div>
				<ul class="hiddenbulletpoints">
					<li><input type="radio" id="Seed_Stage" name="selection"
						class="hidden" /> <label for="Seed_Stage"
						class="label ${suggestion1}"> <strong><br /><spring:message
									code="stages.seed.name" /></strong> <span><spring:message
									code="stages.seed.summary" /></span>
					</label></li>
<%-- 					<li><input type="radio" id="Startup_Stage-Conception_info"
						name="selection" class="hidden" /> <label for="Startup_Stage-Conception_info"
						class="label ${suggestion2}"> <strong><spring:message
									code="stages.conception.name" /></strong> <span><spring:message
									code="stages.conception.summary" /></span>
					</label></li> --%>
					<li><input type="radio" id="Startup_Stage-Construction_info"
						name="selection" class="hidden" /> <label
						for="Startup_Stage-Construction_info" class="label ${suggestion2}">
							<strong><spring:message code="stages.construction.name" /></strong>
							<span><spring:message code="stages.construction.summary" /></span>
					</label></li>
					<li><input type="radio" id="Startup_Stage-Elaboration_info"
						name="selection" class="hidden" /> <label
						for="Startup_Stage-Elaboration_info" class="label ${suggestion2}">
							<strong><spring:message code="stages.elaboration.name" /></strong>
							<span><spring:message code="stages.elaboration.summary" /></span>
					</label></li>
					<li><input type="radio" id="Startup_Stage-Prototyping_info"
						name="selection" class="hidden" /> <label
						for="Startup_Stage-Prototyping_info" class="label ${suggestion2}">
							<strong><spring:message code="stages.prototyping.name" /></strong>
							<span><spring:message code="stages.prototyping.summary" /></span>
					</label></li>
					<li><input type="radio" id="Expansion_Stage" name="selection"
						class="hidden" /> <label for="Expansion_Stage"
						class="label ${suggestion3}"> <strong><br /><spring:message
									code="stages.expansion.name" /></strong> <span><spring:message
									code="stages.expansion.summary" /></span>
					</label></li>
					<li><input type="radio" id="Later_Stage" name="selection"
						class="hidden" /> <label for="Later_Stage"
						class="label ${suggestion4}"> <strong><br /><spring:message
									code="stages.later.name" /></strong> <span><spring:message
									code="stages.later.summary" /></span>
					</label></li>

				</ul>
			</div>
			<form id="selected-stages" action="/makeBuyDecisionRouter" method="POST">
				<div class="table">
					<div id="Seed_Stage_info" class="table-row">
						<div class="table-cell label">
							<strong><spring:message code="stages.seed.name" /></strong> <span><spring:message
									code="stages.seed.summary" /></span>
						</div>
						<div class="table-cell characteristics">
							<ul>
								<li><spring:message code="stages.seed.details_1" /></li>
								<li><spring:message code="stages.seed.details_2" /></li>
								<li><spring:message code="stages.seed.details_3" /></li>
								<li><spring:message code="stages.seed.details_4" /></li>
							</ul>
						</div>
						<div class="table-cell navigation">
							<input type="submit" class="form_button" name="seed"
								value="<spring:message code="choose"/>" />
						</div>
					</div>
<%-- 					<div id="Startup_Stage-Conception_info" class="table-row">
						<div class="table-cell label">
							<strong><spring:message code="stages.startup.name" />:<br />
								<spring:message code="stages.conception.name" /></strong> <span><spring:message
									code="stages.conception.summary" /></span>
						</div>
						<div class="table-cell characteristics">
							<ul>
								<li><spring:message code="stages.conception.details_1" /></li>
								<li><spring:message code="stages.conception.details_2" /></li>
							</ul>
						</div>
						<div class="table-cell navigation">
							<input type="submit" class="form_button" name="startup1"
								value="<spring:message code="choose"/>" />
						</div>
					</div> --%>
					<div id="Startup_Stage-Construction_info" class="table-row">
						<div class="table-cell label">
							<strong>
								<spring:message code="stages.construction.name" /></strong> <span><spring:message
									code="stages.construction.summary" /></span>
						</div>
						<div class="table-cell characteristics">
							<ul>
								<li><spring:message code="stages.construction.details_1" /></li>
								<li><spring:message code="stages.construction.details_2" /></li>
							</ul>
						</div>
						<div class="table-cell navigation">
							<input type="submit" class="form_button" name="startup2"
								value="<spring:message code="choose"/>" />
						</div>
					</div>
					<div id="Startup_Stage-Elaboration_info" class="table-row">
						<div class="table-cell label">
							<strong>
								<spring:message code="stages.elaboration.name" /></strong> <span><spring:message
									code="stages.elaboration.summary" /></span>
						</div>
						<div class="table-cell characteristics">
							<ul>
								<li><spring:message code="stages.elaboration.details_1" /></li>
								<li><spring:message code="stages.elaboration.details_2" /></li>
								<li><spring:message code="stages.elaboration.details_3" /></li>
							</ul>
						</div>
						<div class="table-cell navigation">
							<input type="submit" class="form_button" name="startup3"
								value="<spring:message code="choose"/>" />
						</div>
					</div>
					<div id="Startup_Stage-Prototyping_info" class="table-row">
						<div class="table-cell label">
							<strong>
								<spring:message code="stages.prototyping.name" /></strong> <span><spring:message
									code="stages.prototyping.summary" /></span>
						</div>
						<div class="table-cell characteristics">
							<ul>
								<li><spring:message code="stages.prototyping.details_1" /></li>
								<li><spring:message code="stages.prototyping.details_2" /></li>
								<li><spring:message code="stages.prototyping.details_3" /></li>
								<li><spring:message code="stages.prototyping.details_4" /></li>
							</ul>
						</div>
						<div class="table-cell navigation">
							<input type="submit" class="form_button" name="startup4"
								value="<spring:message code="choose"/>" />
						</div>
					</div>
					<div id="Expansion_Stage_info" class="table-row">
						<div class="table-cell label">
							<strong><br />
								<spring:message code="stages.expansion.name" /></strong> <span><spring:message
									code="stages.expansion.summary" /></span>
						</div>
						<div class="table-cell characteristics">
							<ul>
								<li><spring:message code="stages.expansion.details_1" /></li>
								<li><spring:message code="stages.expansion.details_2" /></li>
								<li><spring:message code="stages.expansion.details_3" /></li>
								<li><spring:message code="stages.expansion.details_4" /></li>
							</ul>
						</div>
						<div class="table-cell navigation">
							<input type="submit" class="form_button" name="expansion"
								value="<spring:message code="choose"/>" />
						</div>
					</div>
					<div id="Later_Stage_info" class="table-row">
						<div class="table-cell label">
							<strong><spring:message code="stages.later.name" /></strong>
							<span><spring:message code="stages.later.summary" /></span>
						</div>
						<div class="table-cell characteristics">
							<ul>
								<li><spring:message code="stages.later.details_1" /></li>
							</ul>
						</div>
						<div class="table-cell navigation">
							<input type="submit" class="form_button" name="later"
								value="<spring:message code="choose"/>" />
						</div>
					</div>
				</div>

			</form>
			<div style="clear: both;"></div>
			<div id="nav" class="table-row navigation">
				<button class="form_button" id="backbutton" name="back">
					<spring:message code="back" />
				</button>
			</div>

		</div>

	</div>

</body>
</html>