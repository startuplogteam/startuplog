<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>StartupLog</title>

<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/multiselect.css">
<link rel="stylesheet" href="resources/css/styles.css">
<link rel="stylesheet" href="resources/css/warehouse_logistics.css">
<link rel="stylesheet" href="resources/css/tabs.css">
<link rel="stylesheet" href="resources/css/tabs2.css">
<link rel="stylesheet" href="resources/css/editContent.css">
<link rel="stylesheet" href="resources/css/tooltip.css">
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script src="resources/js/jquery-3.1.1.min.js"></script>
<script src="resources/js/multiselect.min.js"></script>
<script src="resources/js/tabs.js"></script>
<script src="resources/js/survey.js"></script>



<script type="text/javascript">
    
    	
    	$(document).ready(function () {
    		
    		$('.edit').click(function(){
  			  $(this).hide();
  			  $('.box').addClass('editable');
  			  $('.text').attr('contenteditable', 'true');  
  			  $('.save').show();
  			});

  			$('.save').click(function(){
  			  $(this).hide();
  			  $('.box').removeClass('editable');
  			  $('.text').removeAttr('contenteditable');
  			  $('.edit').show();
  			});
    		
    		
    		$(".tabbable").find(".tab").hide();
		    $(".tabbable").find(".tab").first().show();
		    $(".tabbable").find(".tabs li").first().find("a").addClass("active");
		    $(".tabbable").find(".tabs").find("a").click(function(){
		        tab = $(this).attr("href");
		        $(".tabbable").find(".tab").hide();
		        $(".tabbable").find(".tabs").find("a").removeClass("active");
		        $(tab).show();
		        $(this).addClass("active");
		        return false;
		    });

    		  		
    		function recalculate() {
    		    
    			var totals = [];
            	var temp = [];
                var outputs = $('#resultrow').find('input[type="number"]');
                for(var i=0;i<supplierCount;i++){
                	var total = 0;
                	$('table tbody tr:not(:last):not(:last):not(:last)').each(function(j,row){
                    	var $row = $(row);
                    	var inputs = $row.find('select[name="selectValue"]');
                    	total = parseInt(total) + parseInt(inputs.eq(i).val());
                    });
                	outputs.eq(i).val(total);
                	totals[i]=total;
                	temp[i]=total;
            	}
                
                
                var ranks = $('#rankrow').find('input[type="number"]');
                temp.sort(function(a, b){return b-a});
                for(var i=0;i<supplierCount;i++){
                	ranks.eq(i).val($.inArray(totals[i],temp)+1);
                }
                
    		}
    		
    		$(document).on('change',".selectchange", function(){
    			
   				var newValue = $(this).val();
   				var $tds = $(this).parents('tr').find('td');
            	var critera = $tds.eq(1).text();
            	var columnIndex= $(this).parents('td').index();
            	var supplierName= $("#suppliertable").find('th').eq(columnIndex).html();
            	var material_number = $("#materialdrop option:selected").val();
            	
            	$.ajax({
               	 type: "GET",
               	 url: "/updateSupplierValue",
               	 contentType: 'application/json',
               	 data: {id : material_number, criteria : critera, supplier : supplierName, value : newValue},
               	 dataType: "json",
               	 success: function (data) {
            	 },
               	 error: function (exception) {
                   
               	 }
            	
				}); 
            	
            	recalculate();
    		});
    		
    		$("#materialdrop").change(function(){
 
    			var materialID = $(this).val();
    			
    			$.ajax({
               	 type: "GET",
               	 url: "/fetchsuppliers",
               	 contentType: 'application/json',
               	 data: { material_id : materialID },
               	 dataType: "json",
               	 success: function (data) {
               	 	$('#toShow').show();
               	 	$("#successmsg").hide();
               	 	if(data.length==0){
               	 		$('#toShow').hide();
               	 	}
               	 	var supplierNames = [];
               	 	$('#suppliertable thead').empty();
               	 	$('#suppliertable tbody').empty();
               	 	$('#suppliertable thead').append('<th><b><spring:message code="function.procurement.supplierSelection.select" /></b></th><th><b><spring:message code="function.procurement.supplierSelection.criteria" /></b></th>');
               	 	
               	 	for(var i=0;i<data.length;i++){
                		if(!supplierNames.includes(data[i].supplierName)){
                			supplierNames.push(data[i].supplierName);
                		}
                	}
                	supplierCount = supplierNames.length;
                	
                	for(var i=0;i<supplierNames.length;i++){
                		 $('table thead th:last-child').after('<th>'+supplierNames[i]+'</th>');
               		}
               		
               		var criterias = [];
               		for(var i=0;i<data.length;i++){
                		if(!criterias.includes(data[i].criteria)){
                			criterias.push(data[i].criteria);
                		}
                	}
                	for(var i=0;i<criterias.length;i++){
                		var appendHtml ="";
                		for(var j=0;j<supplierNames.length;j++){
                		 	for(var n=0;n<data.length;n++){
                		 		if((data[n].supplierName==supplierNames[j])&&(data[n].criteria==criterias[i])){
                		 			appendHtml = appendHtml+"<td><select name='selectValue' class='selectchange'>";
                		 			for(var k=1;k<=10;k++){
                		 				if(k==data[n].value){
                		 					appendHtml = appendHtml+"<option selected value="+k+">"+k+"</option>";
                		 				}
                		 				else
                		 					appendHtml = appendHtml+"<option value="+k+">"+k+"</option>";
                		 			}
                		 			appendHtml = appendHtml+"</select'></td>";
                		 		}
                		 	}
                		 }
                		 $('table tbody').append('<tr><td><input type="checkbox" name="record"></td><td>'+criterias[i]+'</td>'+appendHtml+'</tr>');
               		}
                	var lastrow = '<tr id="resultrow"><td><b><spring:message code="function.procurement.supplierSelection.total" /></b></td><td></td>';
                	for(var i=0;i<supplierCount;i++){
                		lastrow = lastrow + '<td><input type="number" disabled></td>';
                	}
                	lastrow = lastrow + '</tr>';
                	
                	lastrow = lastrow + '<tr id="rankrow"><td><b><spring:message code="function.procurement.supplierSelection.ranking" /></b></td><td></td>';
                	for(var i=0;i<supplierCount;i++){
                		lastrow = lastrow + '<td><input type="number" disabled></td>';
                	}
                	lastrow = lastrow + '</tr>';
                	
                	lastrow = lastrow + '<tr id="selectrow"><td><b><spring:message code="function.procurement.supplierSelection.chooseSupplier" /></b></td><td></td>';
                	for(var i=0;i<supplierCount;i++){
                		lastrow = lastrow + '<td><input type="checkbox" name="selectsupplier"></td>';
                	}
                	lastrow = lastrow + '</tr>';
                	
                	$('table tbody').append(lastrow);
                	
                	recalculate();
                	
                    
                 },
               	 error: function (exception) {
                   
               	 }
               	});
    		});
    		
    		$(".add-row").click(function(){
            	var newcriteria = $("#newcriteria").val();
            	if(newcriteria=='')
            		return;
            	var material_number = $("#materialdrop option:selected").val();
            	$.ajax({
               	 type: "GET",
               	 url: "/addcriteria",
               	 contentType: 'application/json',
               	 data: { criteria : newcriteria, material : material_number },
               	 dataType: "json",
               	 success: function () {
            	
            	 },
               	 error: function (exception) {
                   
               	 }
                });
            	
            	var appendHtml = "";
            	for(var j=0;j<supplierCount;j++){
            		appendHtml = appendHtml+"<td><select name='selectValue' class='selectchange'>";
               		for(var k=1;k<=10;k++){
                		if(k==5){
                		 	appendHtml = appendHtml+"<option selected value="+k+">"+k+"</option>";
                		}
                		else
                		 appendHtml = appendHtml+"<option value="+k+">"+k+"</option>";
                	}
                	appendHtml = appendHtml+"</select></td>";
            	}
            	var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + newcriteria + "</td>"+appendHtml+"</tr>";
           	 $("table tbody tr:last").prev().prev().prev().after(markup);
           	 
           	recalculate();
           	 
        	});
        
        
        	$("#delete-row").click(function(){
            	$("table tbody").find('input[name="record"]').each(function(){
            		if($(this).is(":checked")){
            			var criteria = $(this).closest('td').next('td').text();
            			var material_number = $("#materialdrop option:selected").val();
            			if(confirm("Are you sure to delete criteria : "+criteria)==false){
            			 $(this).prop('checked',false);
            			 return true;
            			}	 
     
            			$.ajax({
               	 			type: "GET",
               	 			url: "/deletecriteria",
               	 			contentType: 'application/json',
               				data: { criteria : criteria, material : material_number },
               	 			dataType: "json",
               	 			success: function () {

            	 			},
               	 			error: function (exception) {
                   
               				}
                		});
            			
                        $(this).parents("tr").remove(); 
                        
                        recalculate();
                	}
            	});
        	});
        	
        	$("#saveChanges").click(function(){
        		$("#selectrow").find('input[name="selectsupplier"]').each(function(){
        				if($(this).is(":checked")){
        					var columnIndex= $(this).parents('td').index();
        	            	var supplierName= $("#suppliertable").find('th').eq(columnIndex).text();
        	            	var material_number = $("#materialdrop option:selected").val();
     
            				$.ajax({
               	 				type: "GET",
               	 				url: "/savesupplier",
               	 				contentType: 'application/json',
               					data: { supplier : supplierName, material : material_number },
               	 				dataType: "json",
               	 				success: function () {

            	 				},
               	 				error: function (exception) {
                   
               					}
                		  }); 
        				}
        		});
        		$("#successmsg").show();
        	});
        	
    	});
    
    </script>
</head>
<body>

	<div class="tabbable">
    <ul class="tabs">
        <li><a href="#tab1">Beschreibung</a></li>
        <li><a href="#tab2">Anwendungs-Tool</a></li>
    </ul>
    <div class="tabcontent">
        <div id="tab1" class="tab box">
                       
  <span class="edit">Edit</span>
  <span class="save">Save</span>
  <div class="text">
    Hover this box and click on edit! - You can edit me then.
    <br>When you finished - click save and you saved it.
    </div>

            
        </div>
        <div id="tab2" class="tab">
        
        	<div class="info">
		<p>
			<spring:message code="function.procurement.supplierSelection.info.p1" />
		</p>
	</div>

	<div class="container">
		<form id="supplierSelection" class="form"
			action="/pre_supplierselection" method="post">

			<label><spring:message code="function.procurement.supplierSelection.chooseMaterial" /></label> <select id="materialdrop">
				<option value="">--select--</option>
				<c:forEach var="material" items="${materialList}">
					<option value="${material.material_number }"><%-- <spring:message code="${material.material_name}"/> --%>${material.material_name}</option>
				</c:forEach>
			</select> <br />
			<br />

			<div id="toShow" hidden="true">

				<div class="criteriaButtons">
					<input type="text" id="newcriteria" placeholder="Critera">
					<input type="button" class="add-row" value='<spring:message code="function.procurement.supplierSelection.addCriteria"/>'>
				</div>

				<br />
				<br />
				<br />
				<table id="suppliertable">
					<thead>

					</thead>
					<tbody>

					</tbody>
				</table>

				<button type="button" id="delete-row" class="criteriaButtons"><spring:message code="function.procurement.supplierSelection.deleteCriteria" /></button>
				<button type="button" id="saveChanges"><spring:message code="function.procurement.supplierSelection.saveChanges" /></button>

				<p id="successmsg" hidden="true">
					<i><spring:message code="function.procurement.supplierSelection.successMessage" /></i>
				</p>

			</div>

		</form>
	</div>
        
        </div>
        </div>
        </div>
        



</body>
</html>