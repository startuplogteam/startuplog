<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<title>StartupLog</title>
<link rel="stylesheet" href="resources/css/styles.css">
<link href="/resources/images/startuplog_logo_1.ico" rel="icon" />
<script src="resources/js/jquery-3.1.1.min.js"></script>

<script type="text/javascript">
	function getPage(url) {
		$.ajax({
			url : url,
			dataType : "html",
			success : function(data) {
				$("#workspace_content").html(data);
			},
			type : "GET"
		});
	}

	$(document).ready(
			function() {
				
				
					  $("#functions_navbar li li a ").click(function() {
					    $("#functions_navbar li li a ").removeClass("active");
					    $(this).addClass("active");
					  });

					
				

				var lang = "${pageContext.response.locale}";
				if (lang == "de") {
					$('#de').addClass('visited');
					$('#en').removeClass('visited');

				} else {
					$("#en").addClass("visited");
					$('#de').removeClass('visited');
				}

				/* Code for populating the InfluencingFactors drop downs */

				var competency = '${influencingfactors.competency}';
				var competencies = competency.split(',');
				for (var i = 0; i < (competencies.length - 1); i++) {
					$('#competency option[value=' + competencies[i] + ']')
							.attr('selected', true);
				}

				var producttype = '${influencingfactors.productType}';
				var producttypes = producttype.split(',');
				for (var i = 0; i < (producttypes.length - 1); i++) {
					$('#producttype option[value=' + producttypes[i] + ']')
							.attr('selected', true);
				}

				var distribution = '${influencingfactors.distribution}';
				var distributions = distribution.split(',');
				for (var i = 0; i < (distributions.length - 1); i++) {
					$('#distribution option[value=' + distributions[i] + ']')
							.attr('selected', true);
				}

				var productnature = '${influencingfactors.productNature}';
				var productnatures = productnature.split(',');
				for (var i = 0; i < (productnatures.length - 1); i++) {
					$('#productnature option[value=' + productnatures[i] + ']')
							.attr('selected', true);
				}

				<c:if test="${not empty influencingfactors.competency}">
				$('#competency').prop('disabled', true);
				$('#producttype').prop('disabled', true);
				$('#distribution').prop('disabled', true);
				$('#productnature').prop('disabled', true);
				$('#sales_target').prop('disabled', true);
				$('#sales_current').prop('disabled', true);
				$('#customers').prop('disabled', true);
				$('#logsitics_budget').prop('disabled', true);
				$('#savechanges').hide();
				</c:if>

				$('#savechanges').click(
						function() {

							var competency = '';
							$('#competency :selected').each(
									function(i, selected) {
										competency = competency
												+ $(selected).val() + ',';
									});

							var type = '';
							$('#producttype :selected').each(
									function(i, selected) {
										type = type + $(selected).val() + ',';
									});

							var distribution = '';
							$('#distribution :selected').each(
									function(i, selected) {
										distribution = distribution
												+ $(selected).val() + ',';
									});

							var productnature = '';
							$('#productnature :selected').each(
									function(i, selected) {
										productnature = productnature
												+ $(selected).val() + ',';
									});

							/* var competency = $('#competency').find(":selected").text();
							var type = $('#producttype').find(":selected").text();
							var distribution = $('#distribution').find(":selected").text();
							var productnature = $('#productnature').find(":selected").text(); */
							var target = $('#sales_target').val();
							var current = $('#sales_current').val();
							var customers = $('#customers').val();
							var budget = $('#logsitics_budget').val();

							$.ajax({
								type : "GET",
								url : "/saveinfluencingfactors",
								contentType : 'application/json',
								data : {
									competency : competency,
									productType : type,
									distribution : distribution,
									productNature : productnature,
									salesTarget : target,
									salesCurrent : current,
									customers : customers,
									logisticsBudget : budget
								},
								dataType : "json",
								success : function(data) {

								},
								error : function(exception) {

								}

							});

							$('#competency').prop('disabled', true);
							$('#producttype').prop('disabled', true);
							$('#distribution').prop('disabled', true);
							$('#productnature').prop('disabled', true);
							$('#sales_target').prop('disabled', true);
							$('#sales_current').prop('disabled', true);
							$('#customers').prop('disabled', true);
							$('#logsitics_budget').prop('disabled', true);
							$('#savechanges').hide();
						});

				$('#editchanges').click(function() {
					alert("Make the changes and don't forget to save!!");
					$('#competency').prop('disabled', false);
					$('#producttype').prop('disabled', false);
					$('#distribution').prop('disabled', false);
					$('#productnature').prop('disabled', false);
					$('#sales_target').prop('disabled', false);
					$('#sales_current').prop('disabled', false);
					$('#customers').prop('disabled', false);
					$('#logsitics_budget').prop('disabled', false);
					$('#savechanges').show();
				});

			});
</script>
</head>
<body>
	<div id="container">
		<div class="topcorner">
			<a id="username"><spring:message
					code="workspace.topcorner.welcome" /> <strong>${user}!</strong></a> <a
				id="en" href="?language=en">en</a> | <a id="de" href="?language=de">de</a>
			| <a href="<c:url value="/logout" />">Logout</a>
		</div>
		<h1></h1>
</div>
		<div id="functions_navbar">
			<c:if test="${fn:length(generalList) gt 0}">
				<ul>
					<li>
						<h1>
							<spring:message code="workspace.general_planning" />
						</h1>
						<ul>
							<c:forEach var="item" items="${generalList}">
								<c:set var="dataParts" value="${fn:split(item, ':')}" />
								<li><a href="javascript:void(0);"
									onclick="getPage('${dataParts[0]}')">${ dataParts[1]}</a></li>
							</c:forEach>
						</ul>
					</li>
				</ul>
			</c:if>


			<c:if test="${fn:length(procurementStrategyList) gt 0}">
				<ul>
					<li>
						<h1>
							<spring:message code="workspace.procurement_strategy" />
						</h1>
						<ul>
							<c:forEach var="item" items="${procurementStrategyList}">
								<c:set var="dataParts" value="${fn:split(item, ':')}" />
								<li><a href="javascript:void(0);"
									onclick="getPage('${dataParts[0]}')">${ dataParts[1]}</a></li>
							</c:forEach>
						</ul>
					</li>
				</ul>
			</c:if>

			<c:if test="${fn:length(wareHousePlanningList) gt 0}">
				<ul>
					<li>
						<h1>
							<spring:message code="workspace.warehouse_planning" />
						</h1>
						<ul>
							<c:forEach var="item" items="${wareHousePlanningList}">
								<c:set var="dataParts" value="${fn:split(item, ':')}" />
								<li><a href="javascript:void(0);"
									onclick="getPage('${dataParts[0]}')">${ dataParts[1]}</a></li>
							</c:forEach>
						</ul>
					</li>
				</ul>
			</c:if>
			<c:if test="${fn:length(productionLogisticsList) gt 0}">
				<ul>
					<li>
						<h1>
							<spring:message code="workspace.productionLogistics" />
						</h1>
						<ul>
							<c:forEach var="item" items="${productionLogisticsList}">
								<c:set var="dataParts" value="${fn:split(item, ':')}" />
								<li><a href="javascript:void(0);"
									onclick="getPage('${dataParts[0]}')">${ dataParts[1]}</a></li>
							</c:forEach>
						</ul>
					</li>
				</ul>
			</c:if>

			<c:if test="${fn:length(distributionLogisticsList) gt 0}">
				<ul>
					<li>
						<h1>
							<spring:message code="workspace.distributionLogistics" />
						</h1>
						<ul>
							<c:forEach var="item" items="${distributionLogisticsList}">
								<c:set var="dataParts" value="${fn:split(item, ':')}" />
								<li><a href="javascript:void(0);"
									onclick="getPage('${dataParts[0]}')">${ dataParts[1]}</a></li>
							</c:forEach>
						</ul>
					</li>
				</ul>
			</c:if>

			<c:if test="${fn:length(logisticsControllingList) gt 0}">
				<ul>
					<li>
						<h1>
							<spring:message code="workspace.logisticsControlling" />
						</h1>
						<ul>
							<c:forEach var="item" items="${logisticsControllingList}">
								<c:set var="dataParts" value="${fn:split(item, ':')}" />
								<li><a href="javascript:void(0);"
									onclick="getPage('${dataParts[0]}')">${ dataParts[1]}</a></li>
							</c:forEach>
						</ul>
					</li>
				</ul>
			</c:if>

		</div>
		<%-- 		<c:if test="${showNavBar eq true}">
 --%>
		<div id="workspace_content">
			<%-- </c:if> --%>
			<%-- 		<c:if test="${showNavBar eq false}">
			<div id="workspace_content_nonavbar">
		</c:if> --%>
			<div class="container" style="margin-left: 60px;">
				<div class="container">
					<form id="workspace_stages" action="/indicators" method="GET">
						<div id="stages" class="form" style="padding: 0px">
							<div class="container" style="margin-bottom: 10px;">
								<p
									style="margin-left: 20px; display: inline-block; margin-right: 250px; color: #086EA0; font-size: 22pt">
									<b><spring:message code="workspace.stages.info.p1" /></b>
								</p>
								<p style="display: inline-block; margin-right: 15px;">
									<button class="form_button">
										<spring:message code="workspace.stages.button.change" />
									</button>
								</p>
							</div>

							<div id="stages-schematic" class="boundary">
								<div id="arrow-tail">
									<span class="arrow-cutout"></span>
								</div>
								<div id="arrow-head">
									<span class="arrow-cutout"></span>
								</div>
								<ul class="hiddenbulletpoints">
									<li><input type="radio" id="Seed_Stage" name="selection"
										class="hidden" /> <label for="Seed_Stage"
										class="label ${suggestion1}"> <strong><br />
											<spring:message code="stages.seed.name" /></strong> <span><spring:message
													code="stages.seed.summary" /></span>
									</label></li>
									<li><input type="radio"
										id="Startup_Stage-Construction_info" name="selection"
										class="hidden" /> <label
										for="Startup_Stage-Construction_info"
										class="label ${suggestion3}"> <strong><spring:message
													code="stages.construction.name" /></strong><span><spring:message
													code="stages.construction.summary" /></span>
									</label></li>
									<li><input type="radio"
										id="Startup_Stage-Elaboration_info" name="selection"
										class="hidden" /> <label for="Startup_Stage-Elaboration_info"
										class="label ${suggestion4}"> <strong><spring:message
													code="stages.elaboration.name" /></strong> <span><spring:message
													code="stages.elaboration.summary" /></span>
									</label></li>
									<li><input type="radio"
										id="Startup_Stage-Prototyping_info" name="selection"
										class="hidden" /> <label for="Startup_Stage-Prototyping_info"
										class="label ${suggestion5}"> <strong><spring:message
													code="stages.prototyping.name" /></strong> <span><spring:message
													code="stages.prototyping.summary" /></span>
									</label></li>
									<li><input type="radio" id="Expansion_Stage"
										name="selection" class="hidden" /> <label
										for="Expansion_Stage" class="label ${suggestion6}"> <strong><br />
											<spring:message code="stages.expansion.name" /></strong> <span><spring:message
													code="stages.expansion.summary" /></span>
									</label></li>
									<li><input type="radio" id="Later_Stage" name="selection"
										class="hidden" /> <label for="Later_Stage"
										class="label ${suggestion7}"> <strong><br />
											<spring:message code="stages.later.name" /></strong> <span><spring:message
													code="stages.later.summary" /></span>
									</label></li>

								</ul>
							</div>


						</div>
					</form>
					<div id="stages" class="form" style="padding: 0px">
						<div class="container"
							style="display: block; margin-bottom: 10px;">
							<p
								style="margin-left: 20px; display: inline-block; margin-right: 100px; color: #086EA0; font-size: 22pt">
								<b><spring:message code="function.general.influencingFactors.headlineInfluencingFactors" /> </b>
							</p>
							<p style="display: inline-block; margin-right: 15px;">
								<button id="editchanges" class="form_button">
									<spring:message code="workspace.stages.button.change" />
								</button>
							</p>
						</div>
						<div style="font-weight: bold;text-align: left; margin-left: 35px;margin-bottom: -25px;">
						* <spring:message code="function.general.influencingFactors.multipleChoice" />
						</div>
						<div class="container"
							style="background: #77b9ea; display: block; text-align: left !important; margin: 3%;">
							<p
								style="height: 60px; vertical-align: middle; display: inline-block; margin-left: 50px; font-size: 12pt; margin-right: 40px">
								<spring:message
									code="function.general.influencingFactors.coreCompetency" />
							</p>
							<select id="competency" multiple
								style="vertical-align: middle; height: 60px; width: 40%">
								<option value="1"><spring:message
										code="function.general.influencingFactors.customerRelationshipManagement" /></option>
								<option value="2"><spring:message
										code="function.general.influencingFactors.productInnovation" /></option>
								<option value="3"><spring:message
										code="function.general.influencingFactors.infrastructureManagement" /></option>
							</select>

						</div>

						<div class="container"
							style="margin: 3%; background: #77b9ea; display: block; text-align: left !important;">
							<p
								style="height: 200px; vertical-align: middle; display: inline-block; margin-left: 50px; font-size: 11pt; margin-right: 20px">
								<spring:message
									code="function.general.influencingFactors.currentProductType" />
							</p>
							<select id="producttype" multiple
								style="height: 200px; width: 40%; vertical-align: middle;">
								<option value="1">Einzelfertigung</option>
								<option value="2">Sortenfertigung</option>
								<option value="3">Serienfertigung</option>
								<option value="4">Massenfertigung</option>
								<option value="5">Baustellenmontage</option>
								<option value="6">Werkstattfertigung</option>
								<option value="7">Inselfertigung</option>
								<option value="8"><spring:message
										code="function.general.influencingFactors.productionFlow" /></option>
								<option value="9">Make-to-order</option>
								<option value="10">Make-to-stock</option>
								<option value="11">Mass-customization</option>
							</select>
						</div>

						<div class="container"
							style="margin: 3%; background: #77b9ea; display: block; text-align: left !important;">
							<p
								style="height: 90px; vertical-align: middle; display: inline-block; margin-left: 50px; font-size: 12pt; margin-right: 60px">
								Vertriebskanal*</p>
							<select id="distribution" multiple
								style="height: 90px; width: 40%; vertical-align: middle;">
								<option value="1">Eigene Verkaufsabteilung</option>
								<option value="2">Internetverkauf</option>
								<option value="3">Eigene Filialen</option>
								<option value="4">Partnerfilialen</option>
								<option value="5"><spring:message
										code="function.general.influencingFactors.wholesaler" /></option>
							</select>
						</div>

						<div class="container"
							style="margin: 3%; background: #77b9ea; display: block; text-align: left !important;">
							<p
								style="width: 150px; height: 110px; vertical-align: middle; display: inline-block; margin-left: 50px; font-size: 11pt; margin-right: 20px">
								Art und Beschaffenheit des Poduktes*</p>
							<select id="productnature" multiple
								style="height: 110px; width: 40%; vertical-align: middle;">
								<option value="1"><spring:message
										code="function.general.influencingFactors.generalCargo" /></option>
								<option value="2"><spring:message
										code="function.general.influencingFactors.bulk" /></option>
								<option value="3"><spring:message
										code="function.general.influencingFactors.liquid" /></option>
								<option value="4"><spring:message
										code="function.general.influencingFactors.gasForming" /></option>
								<option value="5"><spring:message
										code="function.general.influencingFactors.coolingRequired" /></option>
								<option value="6"><spring:message
										code="function.general.influencingFactors.fragile" /></option>
							</select>
						</div>

						<div class="container"
							style="margin: 3%; background: #77b9ea; display: block; text-align: left !important;">
							<p
								style="width: 150px; display: inline-block; margin-left: 50px; font-size: 11pt; margin-right: 20px">
								Absatz im aktuellen Jahr (PLAN)</p>
							<input id="sales_target" type="number"
								value="${influencingfactors.salesTarget}"> Stueck
						</div>

						<div class="container"
							style="margin: 3%; background: #77b9ea; display: block; text-align: left !important;">
							<p
								style="width: 150px; display: inline-block; margin-left: 50px; font-size: 11pt; margin-right: 20px">
								Absatz im aktuellen Jahr (IST)</p>
							<input id="sales_current" type="number"
								value="${influencingfactors.salesCurrent}"> Stueck
						</div>

						<div class="container"
							style="margin: 3%; background: #77b9ea; display: block; text-align: left !important;">
							<p
								style="width: 150px; display: inline-block; margin-left: 50px; font-size: 11pt; margin-right: 20px">
								Aktuelle Anzahl Kunden</p>
							<input id="customers" type="number"
								value="${influencingfactors.customers}"> Kunden
						</div>

						<div class="container"
							style="margin: 3%; background: #77b9ea; display: block; text-align: left !important; margin-bottom: 20px;">
							<p
								style="width: 150px; display: inline-block; margin-left: 50px; font-size: 11pt; margin-right: 20px">
								Logistikbudget im aktuellen Jahr</p>
							<input id="logsitics_budget" type="number"
								value="${influencingfactors.logisticsBudget}"> Euro
						</div>

						<input style="width: 20%;" class="form_button" type="button"
							value='<spring:message code="workspace.stages.button.saveChanges" />' id="savechanges"> <br /> <br />

					</div>
				</div>
			</div>
		</div>
</body>
</html>