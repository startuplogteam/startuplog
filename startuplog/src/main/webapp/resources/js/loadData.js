/**
 * 
 */
var myfunction = function(){
	alert("hello");
}

function check_supplier_customer_table(material_id)
		{
			alert(material_id);
			/*
			$.ajax({
				url: "/check_supplier_customer_table",
				data: {material_id:material_id},
				dataType: "text",
				success : function (exist)
				{
					if (exist == "false")
						{
							$("#unit_q1aYes").attr('disabled', true);
							
							$("#unit_q1bYes").attr('disabled', true);
						}
				},
				error : function (exception)
				{
					
				},
				type:"POST"
			});
			*/
		}
 function getPreviousProcurement(title)
	{
	     $.ajax({
	    	 type: "POST",
	    	 url: "/getPreviousProcurement",
	    	 data:{material_id: title},
	    	 dataType: "json",
	    	 success: function (data)
	    	 {
	    		 if (data.hasOwnProperty(supplier_number_strategy)) {
	                 replaceErrorMessage($("#strategy_supplier"), data[supplier_number_strategy]);
	             }
	             else {
	                 printErrorMessage($("#strategy_supplier"));
	             }
	             if (data.hasOwnProperty(market_field_strategy)) {
	                 replaceErrorMessage($("#strategy_market"), data[market_field_strategy]);
	             }
	             else {
	                 printErrorMessage($("#strategy_market"));
	             }
	             if (data.hasOwnProperty(supply_strategy)) {
	                 replaceErrorMessage($("#strategy_supply"), data[supply_strategy]);
	             }
	             else {
	                 printErrorMessage($("#strategy_supply"));
	             }
	             
	             if (data.hasOwnProperty("market_research_result"))
	            	 {   	 
	            	  	switch (data.market_research_result)
	            	  	{
	            	  	case "one_supplier":
	            	  		
	            	  		$("input[name=market_research_result][value=one_supplier]").prop("checked", true);
	            	  		break;
	            	  	case "several_suppliers":
	            	  		
	            	  		$("input[name=market_research_result][value=several_suppliers]").prop("checked", true);
	            	  		break;
	            	  	case "suppliers_own_country":
	            	  		$("input[name=market_research_result][value=suppliers_own_country]").prop("checked", true);
	            	  		break;
	            	  	case "suppliers_other_countries":
	            	  		$("input[name=market_research_result][value=suppliers_other_countries]").prop("checked", true);
	            	  		break;
	            	  	case "suppliers_own_and_other_countries":
	            	  		$("input[name=market_research_result][value=suppliers_own_and_other_countries]").prop("checked", true);
	            	  		break;
	            	  		default:
	            	  			$("input[name=market_research_result]").prop("checked", false);
	            	  			//$("#procurement_q1").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
	            	 }
	             else
	            	 {
	            		$("input[name=market_research_result]").prop("checked", false);
	            	 	//$("#procurement_q1").addClass('hidden');
	            	 }
	             if (data.hasOwnProperty("product_feature"))
            	 {   	 
	            	 $("#procurement_q2").removeClass('hidden');
	            	 switch (data.product_feature)
	            	  	{
	            	  	case "product_complex":
	            	  		
	            	  		$("input[name=product_feature][value=product_complex]").prop("checked", true);
	            	  		break;
	            	  	case "product_development":
	            	  		
	            	  		$("input[name=product_feature][value=product_development]").prop("checked", true);
	            	  		break;
	            	  	case "product_customized":
	            	  		$("input[name=product_feature][value=product_customized]").prop("checked", true);
	            	  		break;
	            	  	case "product_few_specs":
	            	  		$("input[name=product_feature][value=product_few_specs]").prop("checked", true);
	            	  		break;
	            	  	case "product_standardized":
	            	  		$("input[name=product_feature][value=product_standardized]").prop("checked", true);
	            	  		break;
	            	  		default:
	            	  			//$("input[name=product_feature]").prop("checked", false);
	            	  			 $("#procurement_q2").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=product_feature]").prop("checked", false);
	            	 $("#procurement_q2").addClass('hidden');
            	 }
	             if (data.hasOwnProperty("predictability_procurement"))
            	 {   	 
	            	 $("#procurement_q3").removeClass('hidden');
	            	 switch (data.predictability_procurement)
	            	  	{
	            	  	case "less":
	            	  		
	            	  		$("input[name=predictability_procurement][value=less]").prop("checked", true);
	            	  		break;
	            	  	case "well":
	            	  		
	            	  		$("input[name=predictability_procurement][value=well]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=predictability_procurement]").prop("checked", false);
	            	  			 $("#procurement_q3").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=predictability_procurement]").prop("checked", false);
	            	 $("#procurement_q3").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("flexibility"))
            	 {   	 
	            	 $("#procurement_q4").removeClass('hidden');
	            	 switch (data.flexibility)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=flexibility][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=flexibility][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=flexibility][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=flexibility]").prop("checked", false);
	            	  			 $("#procurement_q4").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=flexibility]").prop("checked", false);
	            	 $("#procurement_q4").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("complexity"))
            	 {   	 
	            	 $("#procurement_q5").removeClass('hidden');
	            	 switch (data.complexity)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=complexity][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=complexity][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=complexity][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=complexity]").prop("checked", false);
	            	  			$("#procurement_q5").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=complexity]").prop("checked", false);
	            	 $("#procurement_q5").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("subcription_price"))
            	 {   	 
	            	 $("#procurement_q6").removeClass('hidden');
	            	 switch (data.subcription_price)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=subcription_price][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=subcription_price][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=subcription_price][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=subcription_price]").prop("checked", false);
	            	  			$("#procurement_q6").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=subcription_price]").prop("checked", false);
            		 $("#procurement_q6").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("order_and_transaction_costs"))
            	 {   	 
	            	 $("#procurement_q7").removeClass('hidden');
	            	 switch (data.order_and_transaction_costs)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=order_and_transaction_costs][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=order_and_transaction_costs][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=order_and_transaction_costs][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  		//	$("input[name=order_and_transaction_costs]").prop("checked", false);
	            	  			 $("#procurement_q7").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=order_and_transaction_costs]").prop("checked", false);
            		 $("#procurement_q7").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("transport_costs"))
            	 {   	 
	            	 $("#procurement_q8").removeClass('hidden');
	            	 switch (data.transport_costs)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=transport_costs][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=transport_costs][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=transport_costs][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=transport_costs]").prop("checked", false);
	            	  			 $("#procurement_q8").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=transport_costs]").prop("checked", false);
            		 $("#procurement_q8").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("storage_and_capital_costs"))
            	 {   	 
	            	 $("#procurement_q9").removeClass('hidden');
	            	 switch (data.storage_and_capital_costs)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=storage_and_capital_costs][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=storage_and_capital_costs][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=storage_and_capital_costs][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=storage_and_capital_costs]").prop("checked", false);
	            	  			 $("#procurement_q9").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=storage_and_capital_costs]").prop("checked", false);
            		 $("#procurement_q9").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("procurement_time"))
            	 {   	 
	            	 $("#procurement_q10").removeClass('hidden');
	            	 switch (data.procurement_time)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=procurement_time][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=procurement_time][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=procurement_time][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=procurement_time]").prop("checked", false);
	            	  			 $("#procurement_q10").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=procurement_time]").prop("checked", false);
            		 $("#procurement_q10").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("risk_by_supplier_failure"))
            	 {   	 
	            	 $("#procurement_q11").removeClass('hidden');
	            	 switch (data.risk_by_supplier_failure)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=risk_by_supplier_failure][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=risk_by_supplier_failure][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=risk_by_supplier_failure][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=risk_by_supplier_failure]").prop("checked", false);
	            	  			 $("#procurement_q11").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=risk_by_supplier_failure]").prop("checked", false);
	            	 $("#procurement_q11").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("transport_risk"))
            	 {   	 
	            	 $("#procurement_q12").removeClass('hidden');
	            	 switch (data.transport_risk)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=transport_risk][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=transport_risk][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=transport_risk][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=transport_risk]").prop("checked", false);
	            	  			 $("#procurement_q12").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=transport_risk]").prop("checked", false);
	            	 $("#procurement_q12").addClass('hidden');
            	 }
	             if (data.hasOwnProperty("currency_risk"))
            	 {   	 
	            	 $("#procurement_q13").removeClass('hidden');
	            	 switch (data.currency_risk)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=currency_risk][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=currency_risk][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=currency_risk][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=currency_risk]").prop("checked", false);
	            	  			 $("#procurement_q13").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=currency_risk]").prop("checked", false);
	            	 $("#procurement_q13").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("exchange_rate_risk"))
            	 {   	 
	            	 $("#procurement_q14").removeClass('hidden');
	            	 switch (data.currency_risk)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=exchange_rate_risk][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=exchange_rate_risk][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=exchange_rate_risk][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=exchange_rate_risk]").prop("checked", false);
	            	  			 $("#procurement_q14").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=exchange_rate_risk]").prop("checked", false);
	            	 $("#procurement_q14").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("customs_problem_risk"))
            	 {   	 
	            	 $("#procurement_q15").removeClass('hidden');
	            	 switch (data.customs_problem_risk)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=customs_problem_risk][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=customs_problem_risk][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=customs_problem_risk][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=customs_problem_risk]").prop("checked", false);
	            	  			 $("#procurement_q15").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=customs_problem_risk]").prop("checked", false);
	            	 $("#procurement_q15").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("dependence_on_supplier"))
            	 {   	 
	            	 $("#procurement_q16").removeClass('hidden');
	            	 switch (data.dependence_on_supplier)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=dependence_on_supplier][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=dependence_on_supplier][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=dependence_on_supplier][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=dependence_on_supplier]").prop("checked", false);
	            	  			 $("#procurement_q16").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            	//	$("input[name=dependence_on_supplier]").prop("checked", false);
            		 $("#procurement_q16").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("number_of_suppliers_and_contacts"))
            	 {   	 
	            	 $("#procurement_q17").removeClass('hidden');
	            	 switch (data.number_of_suppliers_and_contacts)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=number_of_suppliers_and_contacts][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=number_of_suppliers_and_contacts][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=number_of_suppliers_and_contacts][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  		//	$("input[name=number_of_suppliers_and_contacts]").prop("checked", false);
	            	  			 $("#procurement_q17").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=number_of_suppliers_and_contacts]").prop("checked", false);
            		 $("#procurement_q17").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("expenditure_on_bureaucracy"))
            	 {   	 
	            	 $("#procurement_q18").removeClass('hidden');
	            	 switch (data.expenditure_on_bureaucracy)
	            	  	{
	            	  	case "high":
	            	  		
	            	  		$("input[name=expenditure_on_bureaucracy][value=high]").prop("checked", true);
	            	  		break;
	            	  	case "low":
	            	  		
	            	  		$("input[name=expenditure_on_bureaucracy][value=low]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=expenditure_on_bureaucracy][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=expenditure_on_bureaucracy]").prop("checked", false);
	            	  			 $("#procurement_q18").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=expenditure_on_bureaucracy]").prop("checked", false);
	            	 $("#procurement_q18").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("language"))
            	 {   	 
	            	 $("#procurement_q19").removeClass('hidden');
	            	 switch (data.language)
	            	  	{
	            	  	case "same":
	            	  		
	            	  		$("input[name=language][value=same]").prop("checked", true);
	            	  		break;
	            	  	case "different":
	            	  		
	            	  		$("input[name=language][value=different]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=language][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=language]").prop("checked", false);
	            	  			 $("#procurement_q19").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=language]").prop("checked", false);
	            	 $("#procurement_q19").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("currency"))
            	 {   	 
	            	 $("#procurement_q20").removeClass('hidden');
	            	 switch (data.currency)
	            	  	{
	            	  	case "same":
	            	  		
	            	  		$("input[name=currency][value=same]").prop("checked", true);
	            	  		break;
	            	  	case "different":
	            	  		
	            	  		$("input[name=currency][value=different]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=currency][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=currency]").prop("checked", false);
	            	  			 $("#procurement_q20").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
            		//$("input[name=currency]").prop("checked", false);
	            	 $("#procurement_q20").addClass('hidden');
            	 }
	             
	             if (data.hasOwnProperty("exchange_rate_utilization"))
            	 {   	 
	            	 $("#procurement_q21").removeClass('hidden');
	            	 switch (data.exchange_rate_utilization)
	            	  	{
	            	  	case "possible":
	            	  		
	            	  		$("input[name=exchange_rate_utilization][value=possible]").prop("checked", true);
	            	  		break;
	            	  	case "not_possible":
	            	  		
	            	  		$("input[name=exchange_rate_utilization][value=not_possible]").prop("checked", true);
	            	  		break;
						case "irrelevant":
	            	  		
	            	  		$("input[name=exchange_rate_utilization][value=irrelevant]").prop("checked", true);
	            	  		break;
	           
	            	  		default:
	            	  			//$("input[name=exchange_rate_utilization]").prop("checked", false);
	            	  			 $("#procurement_q21").addClass('hidden');
	            	  			break;
	            	  		
	            	  	}
            	 }
	             else
            	 {
	            	 $("#procurement_q21").addClass('hidden');
            		//$("input[name=exchange_rate_utilization]").prop("checked", false);
            	 }
	    	 },
	    	 error: function (exception) {
	             //alert('Exeption:' + JSON.stringify(exception));
	         }
	    	 
	     });
	}
 var checkAreAllQuestionsValid = function(root, tabIndex)
 {
     if (root.areAllSelectedDescendantsValid())
         $("#informations .c-tabs-nav__link:eq(" + tabIndex + ")").removeClass("incomplete");
     else
         $("#informations .c-tabs-nav__link:eq("+tabIndex+")").addClass("incomplete");
 }
 function getPreviousInitial(unit_root, strategy_group, misc_group) {
		$
				.ajax({
					url : "/getPreviousInitialplanning",
					data : "{}",
					dataType : "json",
					success : function(data) {
						
						
						if (data.hasOwnProperty('material_quantity')) {
							switch (data.material_quantity) {
							case "high quantity":
								$(
										"input[name=storage_quantity][value='high quantity']")
										.prop("checked", true);
								break;
							case "medium-sized quantity":
								$(
										"input[name=storage_quantity][value='medium-sized quantity']")
										.prop("checked", true);
								break;
							case "low quantity":
								$(
										"input[name=storage_quantity][value='low quantity']")
										.prop("checked", true);
								break;
							}
							
						}
						if (data.hasOwnProperty('order_picking')) {
							switch (data.order_picking) {
							case "yes":
								$(
										"input[name=storage_picking][value='yes']")
										.prop("checked", true);
								break;
							case "no":
								$("input[name=storage_picking][value='no']")
										.prop("checked", true);
								break;

							}
							
						}
						if (data.hasOwnProperty('mech_auto_possibility')) {
							switch (data.mech_auto_possibility) {
							case "yes":
								$(
										"input[name=storage_automation][value='yes']")
										.prop("checked", true);
								break;
							case "no":
								$(
										"input[name=storage_automation][value='no']")
										.prop("checked", true);
								break;
							case "it depends on.":
								$(
										"input[name=storage_automation][value='it depends on.']")
										.prop("checked", true);
								break;
							}
							
						}
						
		              

						if (data.hasOwnProperty('storage_bin_assignment')) {
							switch (data.storage_bin_assignment) {
							case "fixed":
								$(
										"input[name=storage_bin_assignment][value='yes']")
										.prop("checked", true);

								break;
							case "free":
								$(
										"input[name=storage_bin_assignment][value='no']")
										.prop("checked", true);
								break;
							case "irrelevant":
								$(
										"input[name=storage_bin_assignment][value='irrelevant']")
										.prop("checked", true);
								break;
							}
							
						}
						if (data.hasOwnProperty('storage_allocation')) {
							switch (data.storage_allocation) {
							case "single material art":
								$(
										"input[name=storage_allocation][value='yes']")
										.prop("checked", true);

								break;
							case "mixed material arts":
								$(
										"input[name=storage_allocation][value='no']")
										.prop("checked", true);
								break;
							case "irrelevant":
								$(
										"input[name=storage_allocation][value='irrelevant']")
										.prop("checked", true);
								break;
							}
							
						}
						if (data.hasOwnProperty('storage_removal_strategy')) {
							switch (data.storage_removal_strategy) {
							case "FIFO":
								$(
										"input[name=storage_removal_strategy][value='fifo']")
										.prop("checked", true);
								break;
							case "LIFO":
								$(
										"input[name=storage_removal_strategy][value='lifo']")
										.prop("checked", true);
								break;
							case "irrelevant":
								$(
										"input[name=storage_removal_strategy][value='irrelevant']")
										.prop("checked", true);
								break;
							}
						
						}
					
						  
		               // start of storage unit

						if (data
								.hasOwnProperty('storage_in_delivery_packaging')) {
							switch(data.storage_in_delivery_packaging)
							{
							case "yes":
								$("input[name=store_in_delivery_packaging][value='yes']").prop("checked", true);
								break;
							case "no":
								$("input[name=store_in_delivery_packaging][value='no']").prop("checked", true);
								$("#unit_q2").removeClass("hidden");
								if (data.hasOwnProperty('storage_packaging_type'))
								{
									
									$("select[name=storage_unit_type]").val(data.storage_packaging_type);
									
									if (data.storage_packaging_type != "unpacked_small" || data.storage_packaging_type != "unpacked_big" || data.storage_packaging_type != "unpacked_long" )
									{
										$("#unit_q5").removeClass("hidden");
										$("#unit_q6").removeClass("hidden");
										if (data.hasOwnProperty('amount_of_items_per_storage_packaging'))
											{
											$("input[name=storage_unit_parts_max]").val(data.amount_of_items_per_storage_packaging);
											}
										if (data.hasOwnProperty('storage_packaging_length'))
											{
											$("input[name=storage_unit_length]").val(data.storage_packaging_length);
											}
										if (data.hasOwnProperty('storage_packaging_width'))
										{
										$("input[name=storage_unit_width]").val(data.storage_packaging_width);
										}
										if (data.hasOwnProperty('storage_packaging_height'))
										{
										$("input[name=storage_unit_height]").val(data.storage_packaging_height);
										}
									}
								}
								$("#unit_q3").removeClass("hidden");
								
								if (data.hasOwnProperty('storage_packaging_stacking_height'))
									{
										if (data.storage_packaging_stacking_height == 1)
											{
												$("input[name=storage_unit_stackable][value='no']").prop("checked", true);
											}
										else
											{
											$("input[name=storage_unit_stackable][value='yes']").prop("checked", true);
											
											$("#unit_q4").removeClass("hidden");
											$("input[name=storage_unit_stackable_amount]").val(data.storage_packaging_stacking_height);
											
											}
									}
								break;
							}
							
						}
						
						else if (data
								.hasOwnProperty('storage_in_dispatch_packaging')) {
							switch(data.storage_in_dispatch_packaging)
							{
							case "yes":
								$("input[name=store_in_dispatch_packaging][value='yes']").prop("checked", true);
								break;
							case "no":
								$("input[name=store_in_dispatch_packaging][value='no']").prop("checked", true);
								$("#unit_q2").removeClass("hidden");
								if (data.hasOwnProperty('storage_packaging_type'))
								{
									
									$("select[name=storage_unit_type]").val(data.storage_packaging_type);
									
									if (data.storage_packaging_type != "unpacked_small" || data.storage_packaging_type != "unpacked_big" || data.storage_packaging_type != "unpacked_long" )
									{
										$("#unit_q5").removeClass("hidden");
										$("#unit_q6").removeClass("hidden");
										if (data.hasOwnProperty('amount_of_items_per_storage_packaging'))
											{
											$("input[name=storage_unit_parts_max]").val(data.amount_of_items_per_storage_packaging);
											}
										if (data.hasOwnProperty('storage_packaging_length'))
											{
											$("input[name=storage_unit_length]").val(data.storage_packaging_length);
											}
										if (data.hasOwnProperty('storage_packaging_width'))
										{
										$("input[name=storage_unit_width]").val(data.storage_packaging_width);
										}
										if (data.hasOwnProperty('storage_packaging_height'))
										{
										$("input[name=storage_unit_height]").val(data.storage_packaging_height);
										}
									}
								}
								$("#unit_q3").removeClass("hidden");
								
								if (data.hasOwnProperty('storage_packaging_stacking_height'))
									{
										if (data.storage_packaging_stacking_height == 1)
											{
												$("input[name=storage_unit_stackable][value='no']").prop("checked", true);
											}
										else
											{
											$("input[name=storage_unit_stackable][value='yes']").prop("checked", true);
											
											$("#unit_q4").removeClass("hidden");
											$("input[name=storage_unit_stackable_amount]").val(data.storage_packaging_stacking_height);
											
											}
									}
								
								
								break;
							}
							

						}
						else
							{
							$("#unit_q2").removeClass("hidden");
							if (data.hasOwnProperty('storage_packaging_type'))
							{
								
								$("select[name=storage_unit_type]").val(data.storage_packaging_type);
								
								if (data.storage_packaging_type != "unpacked_small" || data.storage_packaging_type != "unpacked_big" || data.storage_packaging_type != "unpacked_long" )
								{
									$("#unit_q5").removeClass("hidden");
									$("#unit_q6").removeClass("hidden");
									if (data.hasOwnProperty('amount_of_items_per_storage_packaging'))
										{
										$("input[name=storage_unit_parts_max]").val(data.amount_of_items_per_storage_packaging);
										}
									if (data.hasOwnProperty('storage_packaging_length'))
										{
										$("input[name=storage_unit_length]").val(data.storage_packaging_length);
										}
									if (data.hasOwnProperty('storage_packaging_width'))
									{
									$("input[name=storage_unit_width]").val(data.storage_packaging_width);
									}
									if (data.hasOwnProperty('storage_packaging_height'))
									{
									$("input[name=storage_unit_height]").val(data.storage_packaging_height);
									}
								}
							}
							$("#unit_q3").removeClass("hidden");
							
							if (data.hasOwnProperty('storage_packaging_stacking_height'))
								{
									if (data.storage_packaging_stacking_height == 1)
										{
											$("input[name=storage_unit_stackable][value='no']").prop("checked", true);
										}
									else
										{
										$("input[name=storage_unit_stackable][value='yes']").prop("checked", true);
										
										$("#unit_q4").removeClass("hidden");
										$("input[name=storage_unit_stackable_amount]").val(data.storage_packaging_stacking_height);
										
										}
								}
							
							}
						checkAreAllQuestionsValid(unit_root, 0);
		                checkAreAllQuestionsValid(strategy_group, 1);
		                checkAreAllQuestionsValid(misc_group, 2);
						
					},

					error : function(exception) {
						//alert('Exeption:' + JSON.stringify(exception));
					},
					type : "POST"
				});
	}