//QuestionMultipleChildren prototype
function QuestionMultipleChildren(elementSelector, valueToChildDict, getSelectedValueFunc, deselectFunc, isValidFunc) {
    if (typeof elementSelector !== 'undefined' && !$(elementSelector).length) throw "Element with selector " + elementSelector + " not found";
    this.questionElement = $(elementSelector);
    this.valueToChildDict = valueToChildDict;
    this.getSelectedValueFunc = getSelectedValueFunc;
    this.deselectFunc = deselectFunc;
    this.isValidFunc = isValidFunc;
    this.enabled = false;
    this.selectedChild = null;

    this.questionElement.find('select, input').change($.proxy(this.enableChild, this));
}
QuestionMultipleChildren.prototype.enableChild = function (event) {
    if (!this.enabled) return;
    this.disableDescendants();
    var question = this.getQuestionToEnable(event);
    if (this.isValid()) {
        this.selectedChild = question;
        question.enable();
    }
    else
    {
        this.selectedChild = null;
        this.disableDescendants();
    }
    $(document).trigger("questionChangeEvent", [this, event]);
}
QuestionMultipleChildren.prototype.enable = function () {
    this.questionElement.removeClass("hidden");
    this.enabled = true;
}
QuestionMultipleChildren.prototype.getQuestionToEnable = function (event) {
    var key = this.getSelectedValueFunc(event);
    if (this.valueToChildDict.hasOwnProperty(key)) {
        return this.valueToChildDict[key];
    }
}
QuestionMultipleChildren.prototype.disable = function () {
    this.disableDescendants();
    this.deselectFunc(this.questionElement);
    this.questionElement.addClass("hidden");
    this.enabled = false;
    this.selectedChild = null;
}
QuestionMultipleChildren.prototype.disableDescendants = function () {
    for (key in this.valueToChildDict) {
        this.valueToChildDict[key].disable();
    }
}
QuestionMultipleChildren.prototype.isValid = function () {
    return this.isValidFunc(this.questionElement);
}
QuestionMultipleChildren.prototype.areAllSelectedDescendantsValid = function () {
    if (this.selectedChild == null || !this.isValid()) {
        return false;
    }
    return this.selectedChild.areAllSelectedDescendantsValid();
}
//QuestionMultipleChildren prototype

function QuestionOneChild(questionElement, child, deselectFunc, isValidFunc) { //derived from QuestionMultipleChildren prototype
    QuestionMultipleChildren.call(this, questionElement, null, null, deselectFunc, isValidFunc);
    this.child = child;
}
QuestionOneChild.prototype = Object.create(QuestionMultipleChildren.prototype);

QuestionOneChild.prototype.getQuestionToEnable = function (event) {
    return this.child;
}
QuestionOneChild.prototype.disableDescendants = function () {
    this.child.disable();
} //derived from QuestionMultipleChildren prototype

function QuestionNoChildren(questionElement, deselectFunc, isValidFunc) { //derived from QuestionMultipleChildren prototype
    QuestionMultipleChildren.call(this, questionElement, null, null, deselectFunc, isValidFunc);
}
QuestionNoChildren.prototype = Object.create(QuestionMultipleChildren.prototype);

QuestionNoChildren.prototype.enableChild = function (event) {
    if (!this.enabled) return;
    $(document).trigger("questionChangeEvent", [this, event]);
}
QuestionNoChildren.prototype.disableDescendants = function () { }
QuestionNoChildren.prototype.areAllSelectedDescendantsValid = function () {
    return this.isValid();
}//derived from QuestionMultipleChildren prototype

function QuestionGroup(questions) { //derived from QuestionMultipleChildren prototype
    QuestionMultipleChildren.call(this);
    this.questions = questions;
}
QuestionGroup.prototype = Object.create(QuestionMultipleChildren.prototype);

QuestionGroup.prototype.areAllSelectedDescendantsValid = function () {
    return !this.questions.some(function (question) { return question.areAllSelectedDescendantsValid() == false; });
}
QuestionGroup.prototype.isValid = function () {
    return !this.questions.some(function (question) { return question.isValid() == false; });
}
QuestionGroup.prototype.disableDescendants = function () {
    this.questions.forEach(function (question) {
        question.disableDescendants();
    });
}
QuestionGroup.prototype.disable = function () {
    this.questions.forEach(function (question) {
        question.disable();
    });
}
QuestionGroup.prototype.enable = function () {
    this.questions.forEach(function (question) {
        question.enable();
    });
} //derived from QuestionMultipleChildren prototype

function SurveyTail() {
    QuestionMultipleChildren.call(this);
}
SurveyTail.prototype = Object.create(QuestionMultipleChildren.prototype);
SurveyTail.prototype.areAllSelectedDescendantsValid = function () { return true; }
SurveyTail.prototype.isValid = function () { return true; }
SurveyTail.prototype.disableDescendants = function () { }
SurveyTail.prototype.disable = function () { }
SurveyTail.prototype.enable = function () { }

function checkObjectIsStringToQuestionsDictionary(obj)
{
    for (var key in obj) {
        if (!obj.hasOwnProperty(key)) continue; // skip loop if the property is from prototype
        if (!(typeof key === 'string' || key instanceof String) || !(obj[key] instanceof QuestionMultipleChildren))
            return false;
    }
    return true;
    
}

//Question factory-functions
function getRadiobuttonsQuestion(elementSelector, child) {
    var deselectFunc = function (element) { element.find("input[type=radio]").prop("checked", false); };
    var isValidFunc = function (element) { return element.find("input:radio:checked").length != 0; };

    if (typeof child === "undefined")
    {
        return new QuestionNoChildren(elementSelector, deselectFunc, isValidFunc);
    }
    if (child instanceof QuestionMultipleChildren)
    {
        return new QuestionOneChild(elementSelector, child, deselectFunc, isValidFunc);
    }
    if (checkObjectIsStringToQuestionsDictionary(child)) {
        return new QuestionMultipleChildren(elementSelector, child, function (event) { return event.target.value; }, deselectFunc, isValidFunc);
    }
    throw "child argument has to be another question, a string-to-question-dictionary or be omitted";
}
function getRadiobuttonsQuestion_Enabled(elementSelector, child) {
    var questions = getRadiobuttonsQuestion(elementSelector, child);
    questions.enabled = true;
    return questions;
}
function getSelectionQuestion(elementSelector, child) {
    var deselectFunc = function (element) { element.find("select").prop("selectedIndex", 0); }; //assumes the first option is a placeholder option which doesn't qualify as a valid option
    var isValidFunc = function (element) { return element.find("select").prop("selectedIndex") != 0; };

    if (typeof child === "undefined") {
        return new QuestionNoChildren(elementSelector, deselectFunc, isValidFunc);
    }
    if (child instanceof QuestionMultipleChildren) {
        return new QuestionOneChild(elementSelector, child, deselectFunc, isValidFunc);
    }
    if (checkObjectIsStringToQuestionsDictionary(child)) {
        return new QuestionMultipleChildren(elementSelector, child, function (event) { return $(event.target).find("option:selected").val(); }, deselectFunc, isValidFunc);
    }
    throw "child argument has to be another question, a string-to-question-dictionary or be omitted";
}
function getSelectionQuestion_Enabled(elementSelector, child) {
    var questions = getSelectionQuestion(elementSelector, child);
    questions.enabled = true;
    return questions;
}
function getNumericInputQuestion(elementSelector, child) {
    var deselectFunc = function (element) { element.find("input").val(''); };
    var isValidFunc = function (element) { return $.isNumeric(element.find("input").val()); };

    if (typeof child === "undefined") {
        return new QuestionNoChildren(elementSelector, deselectFunc, isValidFunc);
    }
    if (child instanceof QuestionMultipleChildren) {
        return new QuestionOneChild(elementSelector, child, deselectFunc, isValidFunc);
    }
    if (checkObjectIsStringToQuestionsDictionary(child)) {
        return new QuestionMultipleChildren(elementSelector, child, function (event) { return event.target.value; }, deselectFunc, isValidFunc);
    }
    throw "child argument has to be another question, a string-to-question-dictionary or be omitted";
}
function getNumericInputQuestion_Enabled(elementSelector, child) {
    var questions = getNumericInputQuestion(elementSelector, child);
    questions.enabled = true;
    return questions;
}
//Question factory-functions